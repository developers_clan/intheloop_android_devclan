package util;

/**
 * Created by macbook on 24/10/15.
 */
public class AgencyHTML {

    public static String PRE_HTML = "<html lang=\"en\">\n" +
            "<head>\n" +
            "<meta charset=\"utf-8\">\n" +
            "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n" +
            "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
            "<title>child care</title>\n" +
            "<script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js\"></script>\n" +
            "<style>\n" +
            ".cartesian_content ul li a.active {\tbackground:rgba(216, 216,216,1.0) url(http://thedevclan.co.uk/childcare/image/top.png)no-repeat scroll 96% 20px !important;\tcolor: #000; display: block; padding: 14px 0;font-size: 20px; text-decoration: none; width: 100%;}\n" +
            "* {\tpadding: 0px; margin: 0px;}\n" +
            "*, *:after, *:before { box-sizing: border-box; -moz-box-sizing: border-box;\t-ms-box-sizing: border-box;\t-o-box-sizing: border-box; -webkit-box-sizing: border-box;}\n" +
            ".cartesian_content ul li a:focus{\n" +
            "background-color:rgba(216, 216,216,1.0) !important;\n" +
            "}\n" +
            "</style>\n" +
            "</head>\n" +
            "<body style=\"margin:0px; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);\">\n" +
            "<section>\n";

    public static String POST_HTML = "</section>\n" +
            "<script>\n" +
            "$( \".cartesian_content > ul > li > a\" ).click(function() {\n" +
            "\tif($(this).hasClass(\"active\")){\n" +
            "\t\t$(\".cartesian_content ul li ul\").slideUp();\n" +
            "        $( \".cartesian_content ul li a\" ).removeClass('active');\n" +
            "      \n" +
            "     \n" +
            "\t}else{\n" +
            "\t\t$( \".cartesian_content ul li a\" ).removeClass('active');\n" +
            "\t\t$(this).addClass('active');\n" +
            "\t\t$(\".cartesian_content ul li ul\").slideUp();\n" +
            "\t\t$(this).siblings(\".cartesian_content ul li ul\").slideDown();\n" +
            "    }\n" +
            "\treturn false;\n" +
            "});\n" +
            "</script>\n" +
            "</body>\n" +
            "</html>\n";

    public static String PRE_TITLE = "  <div> \n <h2 style=\"text-align: center; padding:10px 10px;\">";

    public static String POST_TITLE = "</h2> <h3 style=\"text-align: center; padding:0 10px 0 10px;\">";

    public static String PRE_IMAGE = "  <div style=\"background:#d8d8da; padding:2px 0px 0px 0px;text-align:center; height:140px; border-top: 1px solid #a9c2e0; border-bottom: 1px solid #a9c2e0;\">\n" +
            "<div  style=\"padding-top: 40px;\"> \n <img src=";

    public static String POST_CAT = "</h3>";


    public static String POST_IMAGE = " alt=/> \n </div> \n </div>";

    public static String PRE_BODY = "<div>\n" + "<div>\n" + "<p style=\"font-size:18px; padding:15px 10px; margin:0px;\">";

    public static String POST_BODY = "</p>\n </div>\n";

    public static String PRE_PHONE = "<p style=\"font-size:18px; padding:10px; margin:75px 0px 10px 0px; text-align:center;\">\n" +
            "<label style=\"background :#9a9b9f; border-radius:5px; height:25px; width:25px;  position: relative; top: 8px; display:inline-block; \" >\n</label>\n <span style=\"padding-left:15px;\">";

    public static String POST_PHONE = "</span></p>\n </div>\n</div>";

    public static String PRE_HEADER = "<div class=\"cartesian_content\"> \n <ul style=\"padding-left:0px;\">\n" +
                    "<li style=\"list-style:none; background:#d8d8da; margin-bottom: 8px; padding-left:10px; \"> <a style=\"background:#d8d8da url('http://thedevclan.co.uk/childcare/image/drop_down.png') no-repeat scroll 96% center; color: #000; display: block;padding: 14px 45px 14px 0; font-size: 20px; text-decoration: none; width: 100%; border-bottom: 1px solid #ebebeb;\" href=\"#\">";

    public static String HEADER_LIST = "</a>\n <ul style=\"display:none; padding-left:0px;\"> \n <li style=\"list-style:none; margin-bottom: 8px; padding-left:10px; padding-bottom:20px\">" +
                                       "<p style=\"color: #000; margin:0px; display: block;padding: 14px 0; font-size: 20px;  text-decoration: none; width: 100%;\">";

//    public static String POST_LIST = "</p>\n" + "<label style=\"background :#9a9b9f; border-radius:5px; height:25px; width:25px; position: relative; top: 8px; display:inline-block; \" >\n" +
//                                     " </label>\n <span style=\"padding-left:15px;\">123-456-7890</span> <br>\n" +
//            " <i style=\"display:inline-block; margin-top:10px;\">Speech – Language Pathologists (SLP</i>\n" +
//            "<p> Evaluates and treats children who have speech, language, or communication diculties. </p>\n" +
//            "<i style=\"display:inline-block; margin-top:10px;\">Communicative Disorders Assistant (CDA)</i>\n"+
//            "<p> Works with Speech-Language Pathologists to help treat children who have speech, language, or communication diculties. </p>\n" +
//            "</li> \n </ul>\n </li>\n </ul>\n";

    public static String POST_LIST = "</p>\n" + "\n <span style=\"padding-left:15px;\">\n" +
            "</li> \n </ul>\n </li>\n </ul>\n";
}
