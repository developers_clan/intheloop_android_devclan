package util;

import android.graphics.PointF;
import android.view.View;

/**
 * Created by mac on 1/28/16.
 */
public class Functions {

    public static PointF getTopLeftCorner(View view) {
        float src[] = new float[8];
        float[] dst = new float[]{0, 0, view.getWidth(), 0, 0, view.getHeight(), view.getWidth(), view.getHeight()};
        view.getMatrix().mapPoints(src, dst);
        PointF cornerPoint = new PointF(view.getX() + src[0], view.getY() + src[1]);
        return cornerPoint;
    }

    public static PointF getTopRightCorner(View view) {
        float src[] = new float[8];
        float[] dst = new float[]{0, 0, view.getWidth(), 0, 0, view.getHeight(), view.getWidth(), view.getHeight()};
        view.getMatrix().mapPoints(src, dst);
        PointF cornerPoint = new PointF(view.getX() + src[2], view.getY() + src[3]);
        return cornerPoint;
    }

    public static PointF getBottomLeftCorner(View view) {
        float src[] = new float[8];
        float[] dst = new float[]{0, 0, view.getWidth(), 0, 0, view.getHeight(), view.getWidth(), view.getHeight()};
        view.getMatrix().mapPoints(src, dst);
        PointF cornerPoint = new PointF(view.getX() + src[4], view.getY() + src[5]);
        return cornerPoint;
    }

    public static PointF getBottomRightCorner(View view) {
        float src[] = new float[8];
        float[] dst = new float[]{0, 0, view.getWidth(), 0, 0, view.getHeight(), view.getWidth(), view.getHeight()};
        view.getMatrix().mapPoints(src, dst);
        PointF cornerPoint = new PointF(view.getX() + src[6], view.getY() + src[7]);
        return cornerPoint;
    }

//    public static int dipToPixels(Context context, float dipValue) {
//        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
//        float value = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, dipValue, metrics);
//        return (int)value;
//    }
}
