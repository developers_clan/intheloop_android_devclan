package util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;

import java.io.InputStream;

public class DrawView extends View implements Runnable {

    Bitmap mProgressBitmap;
    Bitmap mMaskProgressBitmap;
    Bitmap mResultBitmap;

    Canvas mTempCanvas;
    Canvas mMaskCanvas;

    Paint mPaint;

    Paint mWhitePaint;

    Handler mHandler = new Handler();

    float mProgress = 0;

    static final long FRAME_TIME = 50;

    public DrawView(Context context, AttributeSet attrs) {
        super(context, attrs);

        InputStream resource = getResources().openRawResource(0);
        mProgressBitmap = BitmapFactory.decodeStream(resource);

        mMaskProgressBitmap = Bitmap.createBitmap(mProgressBitmap.getWidth(), mProgressBitmap.getHeight(), Bitmap.Config.ARGB_8888);
        mMaskCanvas = new Canvas(mMaskProgressBitmap);
        mMaskCanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);

        mResultBitmap = Bitmap.createBitmap(mProgressBitmap.getWidth(), mProgressBitmap.getHeight(), Bitmap.Config.ARGB_8888);

        mTempCanvas = new Canvas(mResultBitmap);

        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        mPaint.setDither(true);

        mWhitePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mWhitePaint.setColor(Color.WHITE);
        mWhitePaint.setStrokeWidth(50);

        mHandler.postDelayed(this, FRAME_TIME);
    }

    @Override
    public void onDraw(Canvas canvas) {

        mTempCanvas.drawBitmap(mMaskProgressBitmap, 0, 0, null);
        mTempCanvas.drawBitmap(mProgressBitmap, 0, 0, mPaint);

        canvas.drawBitmap(mResultBitmap, 0, 0, null);
    }

    @Override
    public void run() {

        mProgress += 0.01f;

        mMaskCanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        mMaskCanvas.drawLine(0, 0, (float)mProgressBitmap.getWidth() * mProgress, 0, mWhitePaint);

        this.invalidate();

        mHandler.postDelayed(this, FRAME_TIME);
    }

}