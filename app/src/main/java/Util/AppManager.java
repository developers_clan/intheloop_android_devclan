package util;

import android.content.Context;
import android.graphics.Typeface;

import java.util.ArrayList;
import java.util.List;

import API.ChildCareAPI;
import Model.AddContactModel;
import Model.Agency;
import Model.ChildModel;
import Model.Contact;
import Model.HomeTextRes;
import Model.ParentData;
import Model.Team;
import retrofit.RestAdapter;
import thirdparty.VerticalParent;

/**
 * Created by macbook on 21/10/15.
 */
public class AppManager {
    private static AppManager ourInstance;

    private List<Agency> agency = new ArrayList<Agency>();
    private List<HomeTextRes> textRes = new ArrayList<HomeTextRes>();
    private List<Team> teams = new ArrayList<Team>();
    private List<Contact> contacts = new ArrayList<Contact>();
    private Typeface boldTypeface;
    private Typeface blackTypeface;
    private Typeface regularTypeface;
    private Boolean isFromServer = false;
    public List<Integer> childTeamIds = new ArrayList<Integer>();

    private ArrayList<ChildModel> childModels = new ArrayList<ChildModel>();
    private ArrayList<VerticalParent> mainDataCDP = new ArrayList<>();
    List<AddContactModel> contactList = new ArrayList<>();

    private Agency agencyModel;

    private boolean isParent;
    private boolean isView;
    private boolean isSearch;



    private int listHieght;

    ArrayList<ParentData> parentModels = new ArrayList<ParentData>();

    ArrayList<Integer> childIds = new ArrayList<>();


    public List<AddContactModel> getContactList() {
        return contactList;
    }

    public void setContactList(List<AddContactModel> contactList) {
        this.contactList = contactList;
    }

    public Boolean isFromServer() {
        return isFromServer;
    }

    public void setIsFromServer(Boolean isFromServer) {
        this.isFromServer = isFromServer;
    }

    public ArrayList<VerticalParent> getMainDataCDP() {
        return mainDataCDP;
    }

    public void setMainDataCDP(ArrayList<VerticalParent> mainDataCDP) {
        this.mainDataCDP = mainDataCDP;
    }

    public Typeface getBoldTypeface() {
        return boldTypeface;
    }

    public void setBoldTypeface(Typeface boldTypeface) {
        this.boldTypeface = boldTypeface;
    }

    public Typeface getBlackTypeface() {
        return blackTypeface;
    }

    public void setBlackTypeface(Typeface blackTypeface) {
        this.blackTypeface = blackTypeface;
    }

    public Typeface getRegularTypeface() {
        return regularTypeface;
    }

    public void setRegularTypeface(Typeface regularTypeface) {
        this.regularTypeface = regularTypeface;
    }

    public boolean isView() {
        return isView;
    }

    public void setIsView(boolean isView) {
        this.isView = isView;
    }

    public boolean isSearch() {
        return isSearch;
    }

    public void setIsSearch(boolean isSearch) {
        this.isSearch = isSearch;
    }

    public ArrayList<Integer> getChildIds() {
        return childIds;
    }

    public void setChildIds(ArrayList<Integer> childIds) {
        this.childIds = childIds;
    }


    public RestAdapter restAdapter = new RestAdapter.Builder()
            .setEndpoint(ChildCareAPI.API).build();
    public ChildCareAPI restAPI = restAdapter.create(ChildCareAPI.class);

    public ArrayList<ParentData> getParentModels() {
        return parentModels;
    }

    public void setParentModels(ArrayList<ParentData> parentModels) {
        this.parentModels = parentModels;
    }

    public boolean isParent() {
        return isParent;
    }

    public void setIsParent(boolean isParent) {
        this.isParent = isParent;
    }

    public Agency getAgencyModel() {
        return agencyModel;
    }

    public void setAgencyModel(Agency agencyModel) {
        this.agencyModel = agencyModel;
    }

    public ArrayList<ChildModel> getChildModels() {
        return childModels;
    }

    public void setChildModels(ArrayList<ChildModel> childModels) {
        this.childModels = childModels;
    }

    public int getListHieght() {
        return listHieght;
    }

    public void setListHieght(int listHieght) {
        this.listHieght = listHieght;
    }

    public List<Agency> getAgency() {
        return agency;
    }

    public void setAgency(List<Agency> agency) {
        this.agency = agency;
    }

    public List<HomeTextRes> getTextRes() {
        return textRes;
    }

    public void setTextRes(List<HomeTextRes> textRes) {
        this.textRes = textRes;
    }

    public List<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }

    public List<Team> getTeams() {
        return teams;
    }

    public void setTeams(List<Team> teams) {
        this.teams = teams;
    }

    public static AppManager getInstance() {
        if (ourInstance == null) {
            ourInstance = new AppManager();

        }
        return ourInstance;
    }

    public void setFont(Context ctx)
    {
        if (ctx != null) {
//            MyApplication.initImageLoader(ctx);
            blackTypeface = Typeface.createFromAsset(ctx.getAssets(), "font/Lato-Black.ttf");
            boldTypeface = Typeface.createFromAsset(ctx.getAssets(), "font/Lato-Bold.ttf");
            regularTypeface = Typeface.createFromAsset(ctx.getAssets(), "font/Lato-Regular.ttf");
        }
    }

//    public void setImageConfg(Context ctx)
//    {
//        if (ctx != null) {
//            MyApplication.initImageLoader(ctx);
//        }
//    }


    private AppManager() {
    }
}
