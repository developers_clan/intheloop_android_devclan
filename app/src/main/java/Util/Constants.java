package util;

import android.os.Environment;

/**
 * Created by macbook on 29/10/15.
 */
public class Constants {


    public static String AGENCY_PREF = "agency_pref";
    public static String AGREEMENT_PREF = "agreement_pref";
    public static String WWAACHILD_PREF = "child_pref";
    public static int REFRESH_DAYS = 3;
    public static String AGENCY_API = "AgencyAPI";
    public static String CHILD_POS = "childPos";
    public static String Date = "Date";
    public static String Key_AGREE = "agree";
    public static String IMG_URL = "http://intheloop.dreamhosters.com//sites//default//files//";
    public static String CDP_STATUS_PREF = "CdpStatus_pref";
    public static String CDP_STATUS = "CdpStatus";
    public static String CHILD_IDS = "child_ids";


    public static String kCatStatusHistory = "SettingStatus";
    public static String kCatSideMenu = "SideMenu";
    public static String kCatAgencyContact = "AgencyContact";
    public static String kCatAccountSettings = "AccountSettings";
    public static String kCatTeam = "Team";
    public static String kCatContactUs = "ContactUs";

    public static String AccountSettingActivity = "Account Settings Screen";
    public static String AddChildScreenActivity = "Add Child Screen";
    public static String TermConditionActivity = "Terms & Condition Screen";
    public static String Action = "Action";
    public static String SaveNewChild = "Save on AccountSettings";
    public static String UpdateNewChild = "Update on AccountSettings";
    public static String SideMenuScreen = "Side Menu Screen";

    public static String AddContactActivity = "AddContact Screen";
    public static String AddContact = "TeamAddContactPressed";
    public static String ContactsActivity = "Contact Us Screen";
    public static String kActTeamContactMapPressed = "TeamContactMapPressed";
    public static String kActTeamContactEmailPressed = "TeamContactEmailPressed";
    public static String kActTeamContactPhonePressed  = "TeamContactPhonePressed";
    public static String kActTeamContactEditPressed = "TeamContactEditPressed";
    public static String kActTeamContactExpanded = "TeamContactExpanded";
    public static String kActTeamContactCollapsed = "TeamContactCollapsed";
    public static String kActAgencyListing = "AgencyListingScreen";

    public static String CDPActivity = "Agency Detail Screen";

    public static String ChildViewActivity = "ChildView Screen";
    public static String HomeActivity = "Agency Listing Screen";
    public static String MyTeamActivity = "Select Child Screen";
    public static String TeamActivity = "Team Contacts listing Screen";
    public static String SelectChild = "Select Child";
    public static String DeleteChild = "DeleteChildPressed";
    public static String EditChild = "EditChildPressed";
    public static String TeamSearch = "Search Team Member";
    public static String ViewMyTeam = "My Team Listing Screen";
    public static String WywActivity = "While You Wait Screen";

    public static String MyServicesMenuWYW = "MenuSelectedMyServices";
    public static String WYWMenuWYW = "MenuSelectedWhileYouWait";
    public static String MyServicesMenuWWAA = "MenuSelectedMyServices";
    public static String WYWMenuWWAA = "MenuSelectedWhileYouWait";
    public static String WWAAActivity = "My Services Listing Screen";
    public static String WWAAHistoryActivity = "View History Screen";
    public static String WWAAStatusActivity = "My Services Selection Screen";
    public static String WWAAHistory = "HistoryButtonPressed";
    public static String Website = "WebsitePressed";
    public static String phone = "Phone";
    public static String CDPConnect = "CDP-ConnectPressed";
    public static String Child = "ChildSelect";
    public static String SaveStatus = "ButtonPressedSaveStatus";
    public static String SelectAgency = "Select Agency";
    public static String TeamCardRemoved = "TeamContactRemoved";
    public static String TeamCardAdded = "TeamContactAdded";
    public static String NotesClick = "NotesPressed";
    public static String CalendarClick = "CalendarPressed";
    public static String ContactClick = "ContactUsAgencySelected";

    public static String PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + "/InTheLoop/" + "thumbnail";

}
