package util;

/**
 * Created by macbook on 29/10/15.
 */
public class WyWHTML {

    public static String PRE_HTML = "<html lang=\"en\">\n" +
            "<head>\n" +
            "<meta charset=\"utf-8\">\n" +
            "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n" +
            "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
            "<title>child care</title>\n" +
            "<script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js\"></script>\n" +
            "<style>\n" +
            ".cartesian_content ul li a.active {\n" +
            "\tbackground: url('http://thedevclan.co.uk/childcare/image/top.png')no-repeat scroll 96% 20px !important;\n" +
            "\tcolor: #000;\n" +
            "\tdisplay: block;\n" +
            "\tpadding: 14px 0;\n" +
            "\tfont-size: 20px;\n" +
            "\ttext-decoration: none;\n" +
            "\twidth: 100%;\n" +
            "}\n" +
            "* {\n" +
            "\tpadding: 0px;\n" +
            "\tmargin: 0px;\n" +
            "}\n" +
            "*, *:after, *:before {\n" +
            "\tbox-sizing: border-box;\n" +
            "\t-moz-box-sizing: border-box;\n" +
            "\t-ms-box-sizing: border-box;\n" +
            "\t-o-box-sizing: border-box;\n" +
            "\t-webkit-box-sizing: border-box;\n" +
            "}\n" +
            "</style>\n" +
            "</head>\n" +
            "<body style=\"margin:0px;\">\n" +
            "<section>";

    public static String POST_HTML = "</section>\n" +
            "<script>\n" +
            "$( \".cartesian_content > ul > li > a\" ).click(function() {\n" +
            "  \n" +
            "       if($(this).hasClass(\"active\")){\n" +
            " \n" +
            "      \n" +
            "      $(\".cartesian_content ul li ul\").slideUp();\n" +
            "      $( \".cartesian_content ul li a\" ).removeClass('active');\n" +
            "      \n" +
            "     \n" +
            "     }else{\n" +
            "      \n" +
            "      $( \".cartesian_content ul li a\" ).removeClass('active');\n" +
            "       $(this).addClass('active');\n" +
            "       $(\".cartesian_content ul li ul\").slideUp();\n" +
            "       $(this).siblings(\".cartesian_content ul li ul\").slideDown();\n" +
            "    \n" +
            "    }\n" +
            " \n" +
            "     \n" +
            "     return false;\n" +
            "     \n" +
            "   });\n" +
            "   \n" +
            "   \n" +
            "</script>\n" +
            "</body>\n" +
            "</html>";

    public static String PRE_TITLE = "<div>\n<h2 style=\"text-align: center; padding:10px 10px 0 10px;\">";

    public static String POST_TITLE = "</h2> <h3 style=\"text-align: center; padding:0 10px 0 10px;\">";

    public static String POST_CAT = "</h3>\n<h2 style=\"text-align: center; padding:10px 10px 0 10px;\"> WHILE YOU WAIT </h2> </div>";


    public static String PRE_BODY = "<div style=\" padding: 0 5px; \">\n" +
            "    <div>\n" +
            "      <p style=\"font-size:18px; padding:15px 10px; margin:0px;\">";

    public static String POST_BODY = "</p>\n</div>";

    public static String PRE_RESOURCE = "<div style=\"  border: 1px solid; padding: 15px; margin:0 10px;\">" +
            "          <p>" +
            "          \t<ul style=\" padding:15px;\">";

    public static String POST_RESOURCE = "</ul>\n" +
            "            </p></div>";

    public static String PRE_PHONE = "<p style=\"font-size:18px; padding:10px; margin:75px 0px 10px 0px; text-align:center;\">\n" +
            "          <label style=\"background :#9a9b9f; border-radius:5px; height:25px; width:25px;  position: relative; top: 0px; display:inline-block; \" >\n" +
            "            &nbsp;\n" +
            "          </label>\n" +
            "          <span style=\"padding-left:15px;\">";

    public static String POST_PHONE = "</p>\n</div>\n</div>";

    public static String PRE_HEADER = "<div class=\"cartesian_content\">\n" +
            "        <ul style=\"padding-left:0px;\">\n" +
            "          <li style=\"list-style:none; background:#d8d8da; margin-bottom: 8px; padding-left:10px; \">\n" +
            "          \n" +
            "           <a style=\"background:url('http://thedevclan.co.uk/childcare/image/drop_down.png') no-repeat scroll 96% center; color: #000; display: block;padding: 14px 45px 14px 0; font-size: 20px; text-decoration: none; width: 100%; border-bottom: 1px solid #ebebeb; \" href=\"#\">";

    public static String HEADER_LIST = "</a>\n" +
            "            <ul style=\"display:none; padding-left:0px;\">\n" +
            "              <li style=\"list-style:none; margin-bottom: 8px; padding-left:10px; padding-bottom:20px; padding-right:20px; text-align:justify\">";

//    public static String POST_LIST = "</p>\n" + "<label style=\"background :#9a9b9f; border-radius:5px; height:25px; width:25px; position: relative; top: 8px; display:inline-block; \" >\n" +
//                                     " </label>\n <span style=\"padding-left:15px;\">123-456-7890</span> <br>\n" +
//            " <i style=\"display:inline-block; margin-top:10px;\">Speech – Language Pathologists (SLP</i>\n" +
//            "<p> Evaluates and treats children who have speech, language, or communication diculties. </p>\n" +
//            "<i style=\"display:inline-block; margin-top:10px;\">Communicative Disorders Assistant (CDA)</i>\n"+
//            "<p> Works with Speech-Language Pathologists to help treat children who have speech, language, or communication diculties. </p>\n" +
//            "</li> \n </ul>\n </li>\n </ul>\n";

    public static String POST_LIST = "  </li>\n" +
            "            </ul>\n" +
            "          </li>\n" +
            "          \n" +
            "          \n" +
            "        </ul>";

}
