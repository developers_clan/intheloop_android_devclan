package API;

import java.util.List;

import Model.Agency;
import Model.Contact;
import Model.HomeTextRes;
import Model.Team;
import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by devclan on 09/09/2015.
 */
public interface ChildCareAPI {
//    public String API="http://childapi.fractus.ws/api";
    public String API="http://intheloop.dreamhosters.com/api";
    @GET("/agency")
    public void getAgency(Callback<List<Agency>> response);

    @GET("/team")
    public void getTeam(Callback<List<Team>> response);

    @GET("/contact")
    public void getContact(Callback<List<Contact>> response);

    @GET("/home")
    public void getHomeText(Callback<List<HomeTextRes>> response);

}
