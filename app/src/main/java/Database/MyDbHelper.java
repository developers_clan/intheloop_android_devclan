package Database;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import Model.AddChildTeamModel;
import Model.AddContactModel;
import Model.Agency;
import Model.AgencyHome;
import Model.AgencyPhone;
import Model.CdpConnect;
import Model.ChildModel;
import Model.CtnStatusModel;
import Model.GetCdpStatausModel;
import Model.HistoryModel;
import Model.HomeMainDetails;
import Model.Image;
import Model.Resources;
import Model.TeamOffice;
import Model.TeamPhone;
import Model.Website;
import Model.Wwaa;
import Model.WwaaActualStatus;
import Model.WwaaMainDetail;
import Model.Wyw;
import Model.WywMainDetail;

public class MyDbHelper extends SQLiteOpenHelper {

    SQLiteDatabase db;
    String DB_PATH;
    Context context;
    static String DB_NAME = "ChildCareDB";

    String TBL_CHILD = "tbl_child";
    String Key_CHILD_Name = "child_name";
    String Key_CHILD_GENDER = "gender";
    String Key_CHILD_ID = "child_id";

    String TBL_CTNSTATUS = "tbl_ctnstatus";
    String Key_CTN_CHILD_ID = "child_id";
    String Key_CTN_STATUS = "status";
    String Key_CTN_DATE = "date";
    String Key_CTN_TIME = "time";

    String TBL_CDPSTATUS = "tbl_cdpstatus";
    String Key_CDP_CHILD_ID = "child_id";
    String Key_CDP_DATE = "date";
    String Key_CDP_TIME = "time";
    String Key_CDP_STATUS = "status";

    String TBL_CONTACT = "tbl_contact";
    String Key_CONTACT_ID = "id";
    String Key_NAME = "name";
    String Key_AGENCY = "agency";
    String Key_DISCIPLINE = "discipline";
    String Key_PHONE = "phone";
    String Key_EXTENSION = "extension";
    String Key_EMAIL = "email";
    String Key_IMAGE = "image";
    String Key_NODE_ID = "node_id";
    String Key_OFFICE_LIST = "office_list";
    String Key_AGENCY_LIST = "agency_list";
    String Key_PHONE_LIST = "phone_list";
    String Key_THUMB_LIST = "thumb_list";
    String Key_GENDER = "gender";

    String TBL_CHILDTEAM = "tbl_childteam";
    String Key_CHILDTEAM_ID = "id";
    String Key_CT_CONTACT_ID = "contact_id";
    String Key_CONTACT = "contact";

    String TBL_AGENCY = "tbl_agency";
    String Key_CONTACT_LOGO = "contact_logo";
    String Key_COLOR_CODE = "color_code";
    String Key_IMG_LIST = "img_list";
    String Key_AGENCY_PHONE = "agency_phone";
    String Key_WEBSITE = "website";
    String Key_CDP_CONNECT = "cdp_connect";
    String Key_RESOURCES = "resources";
    String Key_HOME_DETAILS = "home_main_details";
    String Key_AGENCY_HOME = "agency_home";
    String Key_WWAA_DETAILS = "wwaa_main_details";
    String Key_WWAA = "wwaa";
    String Key_WWAA_STATUS = "wwaa_actual_status";
    String Key_WYW_DETAILS = "wyw_main_details";
    String Key_WYW = "wyw";
    String Key_NID = "nid";

    String TBL_HISTORY = "tbl_history";
    String Key_HIS_NID = "n_id";
    String Key_HIS_CHILD_ID = "child_id";
    String Key_HIS_PARENT_ID = "parent_id";
    String Key_HIS_CHKBX = "chkbx";
    String Key_HIS_DATE = "date";
    String Key_HIS_TIME = "time";
    String Key_HIS_STATUS_ID = "status_id";

    public MyDbHelper(Context context) {
        super(context, DB_NAME, null, 1);
        // TODO Auto-generated constructor stub
        this.context = context;
        DB_PATH = "/data/data/" + context.getPackageName() + "/" + "databases/";

    }

    public void createDataBase() throws IOException {

        boolean dbExist = checkDataBase();
        if (dbExist) {

        } else {
            this.getReadableDatabase();
            this.close();
            try {
                copyDataBase();
            } catch (IOException e) {
                throw new Error("Error copying database");
            }
        }
    }

    private boolean checkDataBase() {
        File dbFile = new File(DB_PATH + DB_NAME);
        return dbFile.exists();
    }

    private void copyDataBase() throws IOException {

        InputStream myInput = context.getAssets().open(DB_NAME);
        String outFileName = DB_PATH + DB_NAME;
        OutputStream myOutput = new FileOutputStream(outFileName);
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }

        // Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();

    }

    @Override
    public void onCreate(SQLiteDatabase db) {


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        if (newVersion > oldVersion) {
            try {
                copyDataBase();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }

    public ArrayList<ChildModel> getData() {
        // TODO Auto-generated method stub
        ArrayList<ChildModel> list = new ArrayList<ChildModel>();
        try {
            String myPath = DB_PATH + DB_NAME;
//        if (!db.isOpen()) {
            db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
//        }


            Cursor cursor = db.rawQuery("select * from " + TBL_CHILD, null);
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    ChildModel childModel = new ChildModel();
                    childModel.setId(cursor.getInt(0));
                    childModel.setName(cursor.getString(1));
                    childModel.setGender(cursor.getInt(2));
                    list.add(childModel);
                }
                while (cursor.moveToNext());
            }
//            db.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public Boolean addNewChild(ChildModel childModel) {
        try {
            String myPath = DB_PATH + DB_NAME;
            db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);

            ContentValues cv = new ContentValues();
            cv.put(Key_CHILD_Name, childModel.getName());
            cv.put(Key_CHILD_GENDER, childModel.getGender());
            db.insert(TBL_CHILD, null, cv);
        } catch (Exception e) {
            e.printStackTrace();
            if (db != null) {
                db.close();
            }
            return false;
        }
        db.close();
        return true;
    }

    public Boolean updateChild(ChildModel childModel, int id) {
        try {
            String myPath = DB_PATH + DB_NAME;
            db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);

            ContentValues cv = new ContentValues();
            cv.put(Key_CHILD_Name, childModel.getName());
            cv.put(Key_CHILD_GENDER, childModel.getGender());
            String where = Key_CHILD_ID + " = ?";
            db.update(TBL_CHILD, cv, where, new String[]{String.valueOf(id)});
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
            if (db != null) {
                db.close();
            }
            return false;
        }

        return true;
    }

    public Boolean deleteChild(int childId) {
        // TODO Auto-generated method stub
        try {
            String myPath = DB_PATH + DB_NAME;
            db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);


            db.execSQL("Delete from " + TBL_CHILD + " WHERE " + Key_CHILD_ID + " = " + childId);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public Boolean addNewStatus(CtnStatusModel model) {
        try {
            String myPath = DB_PATH + DB_NAME;
            db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);

            ContentValues cv = new ContentValues();
            cv.put(Key_CTN_CHILD_ID, model.getChildId());
            cv.put(Key_CTN_STATUS, model.getStatus());
            cv.put(Key_CTN_DATE, model.getDate());
            cv.put(Key_CTN_TIME, model.getTime());
            db.insert(TBL_CTNSTATUS, null, cv);
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
            db.close();
            return false;
        }

        return true;
    }

    public ArrayList<CtnStatusModel> getCTNStatus(int childId) {
        // TODO Auto-generated method stub
        ArrayList<CtnStatusModel> status = new ArrayList<CtnStatusModel>();
        try {
            String myPath = DB_PATH + DB_NAME;
            db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);


            Cursor cursor = db.rawQuery("select " + Key_CTN_STATUS + "," + Key_CTN_DATE + "," + Key_CTN_TIME + " from " + TBL_CTNSTATUS + " where " + Key_CTN_CHILD_ID + " =" + childId, null);
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    CtnStatusModel model = new CtnStatusModel();
                    model.setStatus(cursor.getString(0));
                    model.setDate(cursor.getString(1));
                    model.setTime(cursor.getString(2));
                    status.add(model);
                }
                while (cursor.moveToNext());
            }
            db.close();

        } catch (Exception e) {
            e.printStackTrace();
            db.close();
        }
        return status;
    }

    public Boolean addNewCdpStatus(String list, String date, String time, int childId, String nodeID) {
        try {
            String myPath = DB_PATH + DB_NAME;
            db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);

            ContentValues cv = new ContentValues();
            cv.put(Key_CDP_CHILD_ID, childId);
            cv.put(Key_CDP_DATE, date);
            cv.put(Key_CDP_TIME, time);
            cv.put(Key_CDP_STATUS, list);
            cv.put(Key_NODE_ID, nodeID);
            db.insert(TBL_CDPSTATUS, null, cv);

            db.close();
        } catch (Exception e) {
            e.printStackTrace();
            db.close();
            return false;
        }

        return true;
    }

    public ArrayList<GetCdpStatausModel> getCdpStatus(int childId, String nodeId) {
        // TODO Auto-generated method stub
        ArrayList<GetCdpStatausModel> status = new ArrayList<GetCdpStatausModel>();
        try {
            String myPath = DB_PATH + DB_NAME;
            db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);

            Cursor cursor = db.rawQuery("select id, " + Key_CDP_STATUS + "," + Key_CDP_DATE + "," + Key_CDP_TIME + " from " + TBL_CDPSTATUS + " where " + Key_CDP_CHILD_ID + " =" + childId + " AND " + Key_NODE_ID + " =" + nodeId, null);
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    GetCdpStatausModel model = new GetCdpStatausModel();
                    model.setId(cursor.getInt(0));
                    model.setList(cursor.getString(1));
                    model.setDate(cursor.getString(2));
                    model.setTime(cursor.getString(3));
                    status.add(model);
                }
                while (cursor.moveToNext());
            }

            db.close();
        } catch (Exception e) {// select status,date,time from tbl_cdpstatus where child_id =1
            e.printStackTrace();
            if (db != null) {
                db.close();
            }
        }

        return status;
    }

    public Boolean addNewContact(AddContactModel model, String officeList, String phoneList, String agnecyList, String thumbList) {
        try {
            String myPath = DB_PATH + DB_NAME;
            db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);

            ContentValues cv = new ContentValues();
            cv.put(Key_NAME, model.getName());
            cv.put(Key_GENDER, model.getGender());
            cv.put(Key_AGENCY, model.getAgency());
            cv.put(Key_DISCIPLINE, model.getDiscipline());
            cv.put(Key_PHONE, model.getPhone());
            cv.put(Key_EXTENSION, model.getExtension());
            cv.put(Key_EMAIL, model.getEmail());
            cv.put(Key_NODE_ID, model.getNodeId());
            cv.put(Key_AGENCY_LIST, agnecyList);
            cv.put(Key_OFFICE_LIST, officeList);
            cv.put(Key_PHONE_LIST, phoneList);
//            cv.put(Key_THUMB_LIST, thumbList);
            cv.put(Key_GENDER, model.getGender());
            db.insert(TBL_CONTACT, null, cv);
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
            db.close();
            return false;
        }

        return true;
    }

    public Boolean updateContact(AddContactModel model, String officeList, String agnecyList, String phoneList, String thumbList, String nodeId) {
        try {
            String myPath = DB_PATH + DB_NAME;
            db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);

            ContentValues cv = new ContentValues();
            cv.put(Key_NAME, model.getName());
            cv.put(Key_AGENCY, model.getAgency());
            cv.put(Key_GENDER, model.getGender());
            cv.put(Key_DISCIPLINE, model.getDiscipline());
            cv.put(Key_PHONE, model.getPhone());
            cv.put(Key_EXTENSION, model.getExtension());
            cv.put(Key_EMAIL, model.getEmail());
            cv.put(Key_NODE_ID, model.getNodeId());
            cv.put(Key_AGENCY_LIST, agnecyList);
            cv.put(Key_OFFICE_LIST, officeList);
            cv.put(Key_PHONE_LIST, phoneList);
//            cv.put(Key_THUMB_LIST, thumbList);
            cv.put(Key_GENDER, model.getGender());

            String where = Key_NODE_ID + " = ?";
            db.update(TBL_CONTACT, cv, where, new String[]{String.valueOf(nodeId)});

            db.close();
        } catch (Exception e) {
            e.printStackTrace();
            db.close();
            return false;
        }

        return true;
    }

    public Boolean updateLocalContact(AddContactModel model, String officeList, String agnecyList, String phoneList, String thumbList, int id) {
        try {
            String myPath = DB_PATH + DB_NAME;
            db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);

            ContentValues cv = new ContentValues();
            cv.put(Key_NAME, model.getName());
            cv.put(Key_AGENCY, model.getAgency());
            cv.put(Key_GENDER, model.getGender());
            cv.put(Key_DISCIPLINE, model.getDiscipline());
            cv.put(Key_PHONE, model.getPhone());
            cv.put(Key_EXTENSION, model.getExtension());
            cv.put(Key_EMAIL, model.getEmail());
            cv.put(Key_NODE_ID, model.getNodeId());
            cv.put(Key_AGENCY_LIST, agnecyList);
            cv.put(Key_OFFICE_LIST, officeList);
            cv.put(Key_PHONE_LIST, phoneList);
//            cv.put(Key_THUMB_LIST, thumbList);
            cv.put(Key_GENDER, model.getGender());

            String where = Key_CONTACT_ID + " = ?";
            db.update(TBL_CONTACT, cv, where, new String[]{String.valueOf(id)});
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
            db.close();
            return false;
        }

        return true;
    }

    public ArrayList<AddContactModel> getContactWithLimit(String limit, String offSet) {
        // TODO Auto-generated method stub
        ArrayList<AddContactModel> list = new ArrayList<AddContactModel>();
        try {
            String myPath = DB_PATH + DB_NAME;
            db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);

            Cursor cursor = db.rawQuery("select * from " + TBL_CONTACT + " LIMIT " + limit + " OFFSET " + offSet, null); //
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    AddContactModel model = new AddContactModel();
                    model.setId(cursor.getInt(0));
                    model.setName(cursor.getString(1));
                    model.setAgency(cursor.getString(2));
                    model.setDiscipline(cursor.getString(3));
                    model.setPhone(cursor.getString(4));
                    model.setExtension(cursor.getString(5));
                    model.setEmail(cursor.getString(6));
                    model.setImage(cursor.getString(7));
                    model.setNodeId(cursor.getString(8));

                    GsonBuilder gsonb = new GsonBuilder();
                    Gson gson = gsonb.create();
////                    Type type = new TypeToken<List<TeamAgencyThumbnail>>() {
////                    }.getType();
////                    List<byte[]> agencyThumbList = gson.fromJson(cursor.getString(9), type);
////                    model.setAgencyList(agencyThumbList);

                    Type typeb = new TypeToken<List<TeamOffice>>() {
                    }.getType();
                    List<TeamOffice> officeList = gson.fromJson(cursor.getString(10), typeb);
                    model.setOfficeList(officeList);

                    Type typec = new TypeToken<List<TeamPhone>>() {
                    }.getType();
                    List<TeamPhone> phoneList = gson.fromJson(cursor.getString(11), typec);
                    model.setPhoneList(phoneList);

                    Type typed = new TypeToken<List<String>>() {
                    }.getType();
                    List<String> agencyThumbList = gson.fromJson(cursor.getString(12), typed);
                    model.setAgencyThumbnailList(agencyThumbList);

                    model.setGender(cursor.getString(13));

                    list.add(model);
                }
                while (cursor.moveToNext());
            }

            db.close();
        } catch (Exception e) {
            e.printStackTrace();
            db.close();
        }

        return list;
    }

    public ArrayList<AddContactModel> getAllContact() {
        // TODO Auto-generated method stub
        ArrayList<AddContactModel> list = new ArrayList<AddContactModel>();
        try {
            String myPath = DB_PATH + DB_NAME;
            db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);


            Cursor cursor = db.rawQuery("select * from " + TBL_CONTACT, null); // +" LIMIT 20 OFFSET 0"
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    AddContactModel model = new AddContactModel();
                    model.setId(cursor.getInt(0));
                    model.setName(cursor.getString(1));
                    model.setAgency(cursor.getString(2));
                    model.setDiscipline(cursor.getString(3));
                    model.setPhone(cursor.getString(4));
                    model.setExtension(cursor.getString(5));
                    model.setEmail(cursor.getString(6));
                    model.setImage(cursor.getString(7));
                    model.setNodeId(cursor.getString(8));

                    GsonBuilder gsonb = new GsonBuilder();
                    Gson gson = gsonb.create();
////                    Type type = new TypeToken<List<TeamAgencyThumbnail>>() {
////                    }.getType();
////                    List<byte[]> agencyThumbList = gson.fromJson(cursor.getString(9), type);
////                    model.setAgencyList(agencyThumbList);

                    Type typeb = new TypeToken<List<TeamOffice>>() {
                    }.getType();
                    List<TeamOffice> officeList = gson.fromJson(cursor.getString(10), typeb);
                    model.setOfficeList(officeList);

                    Type typec = new TypeToken<List<TeamPhone>>() {
                    }.getType();
                    List<TeamPhone> phoneList = gson.fromJson(cursor.getString(11), typec);
                    model.setPhoneList(phoneList);

                    Type typed = new TypeToken<List<String>>() {
                    }.getType();
                    List<String> agencyThumbList = gson.fromJson(cursor.getString(12), typed);
                    model.setAgencyThumbnailList(agencyThumbList);

                    model.setGender(cursor.getString(13));

                    list.add(model);
                }
                while (cursor.moveToNext());
            }

            db.close();
        } catch (Exception e) {
            e.printStackTrace();
            db.close();
        }

        return list;
    }


    public Boolean saveImage(String img, int id, String nodeId, boolean fromServer) {
        try {
            String myPath = DB_PATH + DB_NAME;
            db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);

            ContentValues cv = new ContentValues();
            cv.put(Key_IMAGE, img);
            if (fromServer) {
                String where = Key_NODE_ID + " = ?";
                db.update(TBL_CONTACT, cv, where, new String[]{String.valueOf(nodeId)});
            } else {
                String where = Key_CONTACT_ID + " = ?";
                db.update(TBL_CONTACT, cv, where, new String[]{String.valueOf(id)});
            }

            db.close();
        } catch (Exception e) {
            e.printStackTrace();
            db.close();
            return false;
        }

        return true;
    }

    public Boolean saveThumbImage(ArrayList<String> img, String nodeId) {
        try {
            String myPath = DB_PATH + DB_NAME;
            db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);

            ContentValues cv = new ContentValues();
            Gson gson = new Gson();
            String imgList = gson.toJson(img);
            cv.put(Key_THUMB_LIST, imgList);

            String where = Key_NODE_ID + " = ?";
            db.update(TBL_CONTACT, cv, where, new String[]{String.valueOf(nodeId)});

            db.close();
        } catch (Exception e) {
            e.printStackTrace();
            db.close();
            return false;
        }

        return true;
    }

    public Boolean addChildTeam(AddChildTeamModel model) {
        try {
            String myPath = DB_PATH + DB_NAME;
            db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);

            ContentValues cv = new ContentValues();
            cv.put(Key_CHILD_ID, model.getChildId());
            cv.put(Key_CT_CONTACT_ID, model.getContactId());
            cv.put(Key_CONTACT, model.getContact());
            db.insert(TBL_CHILDTEAM, null, cv);
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
            db.close();
            return false;
        }

        return true;
    }

    public ArrayList<AddChildTeamModel> getChildTeam(int childId) {
        ArrayList<AddChildTeamModel> list = new ArrayList<AddChildTeamModel>();
        try {
            String myPath = DB_PATH + DB_NAME;
            db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);

            Cursor cursor = db.rawQuery("select * from " + TBL_CHILDTEAM + " where " + Key_CHILD_ID + "=" + childId, null);
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    AddChildTeamModel model = new AddChildTeamModel();
                    model.setId(cursor.getInt(0));
                    model.setChildId(cursor.getInt(1));
                    model.setContact(cursor.getString(2));
                    model.setContactId(cursor.getInt(3));
                    list.add(model);
                }
                while (cursor.moveToNext());
            }
            db.close();
        } catch (Exception e) {
            if (db != null) {
                db.close();
            }
            e.printStackTrace();
        }

        return list;

    }

    public boolean deleteTeams() {
        try {
            String myPath = DB_PATH + DB_NAME;
            db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);


//            db.execSQL("Delete from " + TBL_CHILDTEAM + " WHERE " + Key_CHILD_ID + " = " + model.getChildId() + " AND " + Key_CHILDTEAM_ID + "=" + model.getId());
            db.execSQL("Delete from tbl_contact WHERE node_id <> ''");
            db.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            db.close();
            return false;
        }
    }

    public boolean removeChildTeam(AddChildTeamModel model) {
        try {
            String myPath = DB_PATH + DB_NAME;
            db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);


//            db.execSQL("Delete from " + TBL_CHILDTEAM + " WHERE " + Key_CHILD_ID + " = " + model.getChildId() + " AND " + Key_CHILDTEAM_ID + "=" + model.getId());
            db.execSQL("Delete from " + TBL_CHILDTEAM + " WHERE " + Key_CHILD_ID + " = " + model.getChildId() + " AND " + Key_CT_CONTACT_ID + "=" + model.getContactId());
            db.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            db.close();
            return false;
        }

    }

    public boolean CheckIsDataAlreadyInDBorNot(String fieldValue) {
        boolean flag = false;
        try {
            String myPath = DB_PATH + DB_NAME;
            db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
            String Query = "Select * from " + TBL_CONTACT + " where " + Key_NODE_ID + " = " + fieldValue;
            Cursor cursor = db.rawQuery(Query, null);
//                    db.close();
            if (cursor.getCount() > 0) {
                flag = true;
            } else {
                flag = false;
            }
            db.close();
        } catch (Exception e) {
            db.close();
        }

        return flag;
    }

    public boolean checkAgencyAlreadyInDBorNot(String fieldValue) {
        String myPath = DB_PATH + DB_NAME;
        boolean flag = false;
        try {

            db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
            String Query = "Select * from " + TBL_AGENCY + " where " + Key_NID + " = " + fieldValue;
            Cursor cursor = db.rawQuery(Query, null);
            if (cursor.getCount() > 0) {
                flag = true;
            } else {
                flag = false;
            }
            db.close();
        } catch (Exception e) {
            if (db != null) {
                db.close();
            }
        }
//
        return flag;
    }

    public Boolean addAgency(Agency agency) {
        try {
            String myPath = DB_PATH + DB_NAME;
            db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);

            Gson gson = new Gson();
            ContentValues cv = new ContentValues();
            String image = gson.toJson(agency.getImage());
            cv.put(Key_IMG_LIST, image);
            String colorCode = gson.toJson(agency.getColorCode());
            cv.put(Key_COLOR_CODE, colorCode);
            String phone = gson.toJson(agency.getAgencyPhone());
            cv.put(Key_AGENCY_PHONE, phone);
            String website = gson.toJson(agency.getWebsite());
            cv.put(Key_WEBSITE, website);
            String connect = gson.toJson(agency.getCdpConnect());
            cv.put(Key_CDP_CONNECT, connect);
            String res = gson.toJson(agency.getResources());
            cv.put(Key_RESOURCES, res);
            String home = gson.toJson(agency.getAgencyHome());
            cv.put(Key_AGENCY_HOME, home);
            String homeDetails = gson.toJson(agency.getHomeMainDetails());
            cv.put(Key_HOME_DETAILS, homeDetails);
            String wwaaDetails = gson.toJson(agency.getWwaaMainDetails());
            cv.put(Key_WWAA_DETAILS, wwaaDetails);
            String wwaa = gson.toJson(agency.getWwaa());
            cv.put(Key_WWAA, wwaa);
            String status = gson.toJson(agency.getWwaaActualStatus());
            cv.put(Key_WWAA_STATUS, status);
            String wywDetails = gson.toJson(agency.getWywMainDetails());
            cv.put(Key_WYW_DETAILS, wywDetails);
            String wyw = gson.toJson(agency.getWyw());
            cv.put(Key_WYW, wyw);
            cv.put(Key_NID, agency.getNid());
            db.insert(TBL_AGENCY, null, cv);

            db.close();

        } catch (Exception e) {
            e.printStackTrace();
            db.close();
            return false;
        }

        return true;
    }

    public Boolean deleteAgency() {
        // TODO Auto-generated method stub
        try {
            String myPath = DB_PATH + DB_NAME;
            db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);


            db.execSQL("Delete from " + TBL_AGENCY);
            db.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            if (db != null) {
                db.close();
            }
            return false;
        }
    }

    public Boolean updateAgency(Agency agency, String nodeId) {
        try {
            String myPath = DB_PATH + DB_NAME;
            db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);

            Gson gson = new Gson();
            ContentValues cv = new ContentValues();
            String image = gson.toJson(agency.getImage());
            cv.put(Key_IMG_LIST, image);
            String colorCode = gson.toJson(agency.getColorCode());
            cv.put(Key_COLOR_CODE, colorCode);
            String phone = gson.toJson(agency.getAgencyPhone());
            cv.put(Key_AGENCY_PHONE, phone);
            String website = gson.toJson(agency.getWebsite());
            cv.put(Key_WEBSITE, website);
            String connect = gson.toJson(agency.getCdpConnect());
            cv.put(Key_CDP_CONNECT, connect);
            String res = gson.toJson(agency.getResources());
            cv.put(Key_RESOURCES, res);
            String home = gson.toJson(agency.getAgencyHome());
            cv.put(Key_AGENCY_HOME, home);
            String homeDetails = gson.toJson(agency.getHomeMainDetails());
            cv.put(Key_HOME_DETAILS, homeDetails);
            String wwaaDetails = gson.toJson(agency.getWwaaMainDetails());
            cv.put(Key_WWAA_DETAILS, wwaaDetails);
            String wwaa = gson.toJson(agency.getWwaa());
            cv.put(Key_WWAA, wwaa);
            String status = gson.toJson(agency.getWwaaActualStatus());
            cv.put(Key_WWAA_STATUS, status);
            String wywDetails = gson.toJson(agency.getWywMainDetails());
            cv.put(Key_WYW_DETAILS, wywDetails);
            String wyw = gson.toJson(agency.getWyw());
            cv.put(Key_WYW, wyw);
            cv.put(Key_NID, agency.getNid());

            String where = Key_NID + " = ?";
            db.update(TBL_AGENCY, cv, where, new String[]{String.valueOf(nodeId)});
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
            if (db != null) {
                db.close();
            }
            return false;
        }

        return true;
    }

    public ArrayList<Agency> getAgenciesData() {
        // TODO Auto-generated method stub
        ArrayList<Agency> list = new ArrayList<Agency>();
        try {
            String myPath = DB_PATH + DB_NAME;
            db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);


            Cursor cursor = db.rawQuery("select * from " + TBL_AGENCY, null);
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    GsonBuilder gsonb = new GsonBuilder();
                    Gson gson = gsonb.create();

                    Agency model = new Agency();
                    model.setAgencyImage(cursor.getString(0));
                    model.setAgencyLogo(cursor.getBlob(1));

                    Type type1 = new TypeToken<List<String>>() {
                    }.getType();
                    List<String> codeList = gson.fromJson(cursor.getString(2), type1);
                    model.setColorCode(codeList);

                    Type type2 = new TypeToken<List<AgencyPhone>>() {
                    }.getType();
                    List<AgencyPhone> phoneList = gson.fromJson(cursor.getString(3), type2);
                    model.setAgencyPhone(phoneList);

                    Type type3 = new TypeToken<List<Website>>() {
                    }.getType();
                    List<Website> websiteList = gson.fromJson(cursor.getString(4), type3);
                    model.setWebsite(websiteList);

                    Type type4 = new TypeToken<List<CdpConnect>>() {
                    }.getType();
                    List<CdpConnect> connectList = gson.fromJson(cursor.getString(5), type4);
                    model.setCdpConnect(connectList);

                    Type type5 = new TypeToken<List<Resources>>() {
                    }.getType();
                    List<Resources> resList = gson.fromJson(cursor.getString(6), type5);
                    model.setResources(resList);

                    Type type6 = new TypeToken<List<HomeMainDetails>>() {
                    }.getType();
                    List<HomeMainDetails> homeList = gson.fromJson(cursor.getString(7), type6);
                    model.setHomeMainDetails(homeList);

                    Type type7 = new TypeToken<List<AgencyHome>>() {
                    }.getType();
                    List<AgencyHome> agencyHomeList = gson.fromJson(cursor.getString(8), type7);
                    model.setAgencyHome(agencyHomeList);

                    Type type8 = new TypeToken<List<WwaaMainDetail>>() {
                    }.getType();
                    List<WwaaMainDetail> wwaaDetailList = gson.fromJson(cursor.getString(9), type8);
                    model.setWwaaMainDetails(wwaaDetailList);

                    Type type9 = new TypeToken<List<Wwaa>>() {
                    }.getType();
                    List<Wwaa> wwaaList = gson.fromJson(cursor.getString(10), type9);
                    model.setWwaa(wwaaList);

                    Type type10 = new TypeToken<List<WwaaActualStatus>>() {
                    }.getType();
                    List<WwaaActualStatus> statusList = gson.fromJson(cursor.getString(11), type10);
                    model.setWwaaActualStatus(statusList);

                    Type type11 = new TypeToken<List<WywMainDetail>>() {
                    }.getType();
                    List<WywMainDetail> wywDetailList = gson.fromJson(cursor.getString(12), type11);
                    model.setWywMainDetails(wywDetailList);

                    Type type12 = new TypeToken<List<Wyw>>() {
                    }.getType();
                    List<Wyw> wywList = gson.fromJson(cursor.getString(13), type12);
                    model.setWyw(wywList);

                    model.setNid(cursor.getString(14));

                    Type type13 = new TypeToken<List<Image>>() {
                    }.getType();
                    List<Image> imgList = gson.fromJson(cursor.getString(15), type13);
                    model.setImage(imgList);

                    list.add(model);
                }
                while (cursor.moveToNext());
            }
            db.close();

        } catch (Exception e) {
            e.printStackTrace();
            if (db != null) {
                db.close();
            }
        }
        return list;
    }

    public Boolean saveAgencyImage(String imgPath, String nId) {
        try {
            String myPath = DB_PATH + DB_NAME;
            db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);

            ContentValues cv = new ContentValues();
            cv.put(Key_IMAGE, imgPath);
            String where = Key_NID + " = ?";
            db.update(TBL_AGENCY, cv, where, new String[]{nId});
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
            if (db != null) {
                db.close();
            }
            return false;
        }

        return true;
    }

    public Boolean saveAgencyLogo(byte[] img, String nId) {
        try {
            String myPath = DB_PATH + DB_NAME;
            db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);

            ContentValues cv = new ContentValues();
            cv.put(Key_CONTACT_LOGO, img);
            String where = Key_NID + " = ?";
            db.update(TBL_AGENCY, cv, where, new String[]{nId});
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
            if (db != null) {
                db.close();
            }
            return false;
        }

        return true;
    }

    public Boolean addCdpHistory(String nID, int childId, HistoryModel model) {
        try {
            String myPath = DB_PATH + DB_NAME;
            db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);

            ContentValues cv = new ContentValues();
            cv.put(Key_HIS_CHILD_ID, childId);
            cv.put(Key_HIS_DATE, model.getDate());
            cv.put(Key_HIS_TIME, model.getTime());
            cv.put(Key_HIS_CHKBX, model.getChkbx());
            cv.put(Key_HIS_NID, nID);
            cv.put(Key_HIS_PARENT_ID, model.getParentId());
            cv.put(Key_HIS_STATUS_ID, model.getStatusId());
            db.insert(TBL_HISTORY, null, cv);

            db.close();
        } catch (Exception e) {
            e.printStackTrace();
            if (db != null) {
                db.close();
            }
            return false;
        }

        return true;
    }

    public ArrayList<HistoryModel> getCdpHistory(int childId, String nodeId,int parentId) {
        // TODO Auto-generated method stub
        ArrayList<HistoryModel> status = new ArrayList<HistoryModel>();
        try {
            String myPath = DB_PATH + DB_NAME;
            db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);

            Cursor cursor = db.rawQuery("select " + Key_HIS_CHKBX + "," + Key_HIS_DATE + "," + Key_HIS_TIME + "," + Key_HIS_STATUS_ID + " from " + TBL_HISTORY + " where " + Key_HIS_CHILD_ID + " =" + childId + " AND " + Key_HIS_NID + " =" + nodeId+ " AND " + Key_HIS_PARENT_ID + " =" + parentId, null);
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    HistoryModel model = new HistoryModel();
                    model.setChkbx(cursor.getString(0));
                    model.setDate(cursor.getString(1));
                    model.setTime(cursor.getString(2));
                    model.setStatusId(cursor.getInt(3));
                    status.add(model);
                }
                while (cursor.moveToNext());
            }

            db.close();
        } catch (Exception e) {// select status,date,time from tbl_cdpstatus where child_id =1
            e.printStackTrace();
            if (db != null) {
                db.close();
            }
        }

        return status;
    }

    public Boolean deleteHistory(String nID, int childId, HistoryModel model) {
        // TODO Auto-generated method stub
        try {
            String myPath = DB_PATH + DB_NAME;
            db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);


            db.execSQL("Delete from " + TBL_HISTORY + " WHERE " + Key_HIS_CHILD_ID + " = " + childId +" AND " +
                     Key_HIS_NID +" = "+nID+" AND " + Key_HIS_PARENT_ID +" = " +model.getParentId()+
                     " AND "+Key_HIS_CHKBX +" = '"+model.getChkbx()+"'");

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}


