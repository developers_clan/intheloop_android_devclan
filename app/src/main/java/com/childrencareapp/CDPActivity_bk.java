package com.childrencareapp;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.kogitune.activity_transition.ActivityTransition;
import com.special.ResideMenu.ResideMenu;
import com.special.ResideMenu.ResideMenuItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Model.ExpandableListItem;
import thirdparty.VerticalChild;
import thirdparty.VerticalParent;


public class CDPActivity_bk extends Activity implements View.OnClickListener, ExpandableRecyclerAdapter.ExpandCollapseListener {

    ArrayList<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    Button openDrawer,btnBack;
    RelativeLayout rlList,rlHeader;
    LinearLayout layoutImg;
    ImageView img;
    Animation out;
    String key;


    private RecyclerView mRecyclerView;

    ResideMenu resideMenu;


    private ResideMenuItem itemWhile;
    private ResideMenuItem itemWhere;

    private final int CELL_DEFAULT_HEIGHT = 200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_cdp_bk);

        openDrawer = (Button)findViewById(R.id.btn_drawer);
        layoutImg = (LinearLayout)findViewById(R.id.linear);
        img = (ImageView)findViewById(R.id.img_cdp);
        rlHeader = (RelativeLayout) findViewById(R.id.rlHeader);
        btnBack = (Button)findViewById(R.id.btn_back);

        rlList = (RelativeLayout) findViewById(R.id.rlList);

        //////////Animation/////////////
        ActivityTransition.with(getIntent()).to(layoutImg).start(savedInstanceState);
        Animation zoom = AnimationUtils.loadAnimation(CDPActivity_bk.this, R.anim.zoom);
        rlList.startAnimation(zoom);
        Animation anim = AnimationUtils.loadAnimation(CDPActivity_bk.this, R.anim.slide_in_right);
        rlHeader.startAnimation(anim);
        ///////////////////end//////////

        setUpMenu();

        Bundle bundle = getIntent().getExtras();
        if(bundle != null)
        {
            key = bundle.getString("FLAG");
        }

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CDPActivity_bk.this.finish();
            }
        });

        ArrayList<ExpandableListItem> list = new ArrayList<ExpandableListItem>();
        prepareListData(list);


        ArrayList<ExpandableListItem> expListItems = new ArrayList<ExpandableListItem>();
        prepareListData(expListItems);

        String[] array = {"Hello", "World", "Android", "is", "Awesome", "World", "Android", "is", "Awesome", "World", "Android", "is", "Awesome", "World", "Android", "is", "Awesome"};
//        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, R.layout.view_row, R.id.header_text, array);
//        CustomArrayAdapter adapter = new CustomArrayAdapter(this, R.layout.view_row, expListItems);
//        expListview.setAdapter(adapter);
//        expListview.setDivider(null);

//

    }

    /////////////Slide animation///////////
    private void setUpMenu() {

        // attach to current activity;
        resideMenu = new ResideMenu(this);
        resideMenu.setUse3D(true);
        resideMenu.setBackground(R.drawable.menu_background);
        resideMenu.attachToActivity(this);
        resideMenu.setShadowVisible(false);
        resideMenu.setMenuListener(menuListener);
        //valid scale factor is between 0.0f and 1.0f. leftmenu'width is 150dip.
        resideMenu.setScaleValue(0.6f);

        // create menu items;
        itemWhere   = new ResideMenuItem(this, R.drawable.walking,     "Where We Are AT");
        itemWhile  = new ResideMenuItem(this, R.drawable.walking,  "While You Wait");
//        itemCalendar = new ResideMenuItem(this, R.drawable.icon_calendar, "Calendar");
//        itemSettings = new ResideMenuItem(this, R.drawable.icon_settings, "Settings");

        itemWhere.setOnClickListener(this);
        itemWhile.setOnClickListener(this);
//        itemCalendar.setOnClickListener(this);
//        itemSettings.setOnClickListener(this);

//        resideMenu.addMenuItem(itemWhere, ResideMenu.DIRECTION_LEFT);
//        resideMenu.addMenuItem(itemWhile, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(itemWhere, ResideMenu.DIRECTION_RIGHT);
        resideMenu.addMenuItem(itemWhile, ResideMenu.DIRECTION_RIGHT);

        // You can disable a direction by setting ->
         resideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_LEFT);

//        findViewById(R.id.title_bar_left_menu).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                resideMenu.openMenu(ResideMenu.DIRECTION_LEFT);
//            }
//        });
        openDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resideMenu.openMenu(ResideMenu.DIRECTION_RIGHT);
            }
        });
    }

    private ResideMenu.OnMenuListener menuListener = new ResideMenu.OnMenuListener() {
        @Override
        public void openMenu() {
//            Toast.makeText(getApplicationContext(), "Menu is opened!", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void closeMenu() {
//            Toast.makeText(getApplicationContext(), "Menu is closed!", Toast.LENGTH_SHORT).show();
        }
    };


    public int GetPixelFromDips(float pixels) {
        // Get the screen's density scale
        final float scale = getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f);
    }

    private void prepareListData(ArrayList<ExpandableListItem> listItems) {

        for (int i = 0;i<6;i++) {
            ExpandableListItem listItem = new ExpandableListItem();

            // Adding child data
            listItem.setDataHeader("York Region Preschool Speech and Language Program");


//            listDataHeader.add("Tri-Regional Infant Hearing Program");
//            listDataHeader.add("Tri-Regional Blind-Low Vision Program");
//            listDataHeader.add("York Region Preschool Speech and Language Program");
//            listDataHeader.add("Tri-Regional Infant Hearing Program");
//            listDataHeader.add("Tri-Regional Blind-Low Vision Program");

            // Adding child data
            List<String> top250 = new ArrayList<String>();
            top250.add("The Shawshank Redemption");
            top250.add("The Godfather");
            top250.add("The Godfather: Part II");
            top250.add("Pulp Fiction");
            top250.add("The Good, the Bad and the Ugly");
            top250.add("The Dark Knight");
            top250.add("12 Angry Men");

//            listItem.setListDataChild(top250);
            listItems.add(listItem);
        }
//            List<String> nowShowing = new ArrayList<String>();
//            nowShowing.add("The Conjuring");
//            nowShowing.add("Despicable Me 2");
//            nowShowing.add("Turbo");
//            nowShowing.add("Grown Ups 2");
//            nowShowing.add("Red 2");
//            nowShowing.add("The Wolverine");
//
//            List<String> comingSoon = new ArrayList<String>();
//            comingSoon.add("2 Guns");
//            comingSoon.add("The Smurfs 2");
//            comingSoon.add("The Spectacular Now");
//            comingSoon.add("The Canyons");
//            comingSoon.add("Europa Report");
//
//            listDataChild.put(listDataHeader.get(0), top250); // Header, Child data
//            listDataChild.put(listDataHeader.get(1), nowShowing);
//            listDataChild.put(listDataHeader.get(2), comingSoon);
//            listDataChild.put(listDataHeader.get(3), top250); // Header, Child data
//            listDataChild.put(listDataHeader.get(4), nowShowing);
//            listDataChild.put(listDataHeader.get(5), comingSoon);
//
//        }

    }


    @Override
    public void onBackPressed() {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cd, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

    }

    private List<VerticalParent> setUpTestData(int numItems) {
        List<VerticalParent> verticalParentList = new ArrayList<>();

        for (int i = 0; i < numItems; i++) {
            List<VerticalChild> childItemList = new ArrayList<>();

            VerticalChild verticalChild = new VerticalChild();
            verticalChild.setChildText("The Shawshank Redemption");
            childItemList.add(verticalChild);

            // Evens get 2 children, odds get 1
            if (i % 2 == 0) {
                VerticalChild verticalChild2 = new VerticalChild();
                verticalChild2.setChildText("The Good, the Bad and the Ugly");
                childItemList.add(verticalChild2);
            }

            VerticalParent verticalParent = new VerticalParent();
            verticalParent.setChildItemList(childItemList);
            verticalParent.setParentNumber(i);
            verticalParent.setParentText("York Region Preschool ");
            if (i == 0) {
                verticalParent.setInitiallyExpanded(true);
            }
            verticalParentList.add(verticalParent);
        }

        return verticalParentList;
    }

    @Override
    public void onListItemExpanded(int position) {

    }

    @Override
    public void onListItemCollapsed(int position) {

    }
}
