package com.childrencareapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import Database.MyDbHelper;
import Model.Agency;
import util.AppManager;
import Model.CtnStatusModel;
import util.Constants;

public class WwaaCTNActivity extends Activity {

//    int index;
    MyDbHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_wwaa_ctn);

        TextView heading = (TextView) findViewById(R.id.heading);
        TextView status = (TextView) findViewById(R.id.status);
        ImageView img_agency = (ImageView)findViewById(R.id.img_agency);
        Button btnHistory = (Button)findViewById(R.id.btn_history);
        TextView contact = (TextView) findViewById(R.id.contact);
        TextView tvChildName = (TextView) findViewById(R.id.tv_child_name);
        TextView tvStatus = (TextView) findViewById(R.id.tv_status);
        TextView tvPhone = (TextView) findViewById(R.id.tv_phone);
        TextView tvWebsite = (TextView) findViewById(R.id.tv_website);
        TextView tvConnect = (TextView) findViewById(R.id.tv_connect);

        heading.setTypeface(AppManager.getInstance().getBlackTypeface());
        status.setTypeface(AppManager.getInstance().getBlackTypeface());
        btnHistory.setTypeface(AppManager.getInstance().getBoldTypeface());
        contact.setTypeface(AppManager.getInstance().getBlackTypeface());
        tvPhone.setTypeface(AppManager.getInstance().getRegularTypeface());
        tvWebsite.setTypeface(AppManager.getInstance().getRegularTypeface());
        tvConnect.setTypeface(AppManager.getInstance().getRegularTypeface());
        tvChildName.setTypeface(AppManager.getInstance().getRegularTypeface());
        tvStatus.setTypeface(AppManager.getInstance().getBoldTypeface());

        dbHelper = new MyDbHelper(this);

//        Bundle bundle = getIntent().getExtras();
//        if(bundle != null)
//        {
//            index = bundle.getInt("INDEX");
//        }

        Agency agency = AppManager.getInstance().getAgencyModel();
//        heading.setText(agency.getCategory());
        tvPhone.setText(agency.getAgencyPhone().get(0).getNumber());

        ArrayList<CtnStatusModel> list = null;
        SharedPreferences pref = getSharedPreferences(Constants.WWAACHILD_PREF,Activity.MODE_PRIVATE);
        if(pref.contains(Constants.CHILD_POS))
        {
            int pos = pref.getInt(Constants.CHILD_POS,-1);
            tvChildName.setText(AppManager.getInstance().getChildModels().get(pos).getName());

            list = dbHelper.getCTNStatus(AppManager.getInstance().getChildModels().get(pos).getId());
        }

        if (list !=null && list.size()>0)
        {
            tvStatus.setText(list.get(list.size()-1).getStatus());
        }

    }

    public void onClick(View v)
    {
        if (v.getId()==R.id.btn_back)
        {
            finish();
        }
        else if (v.getId()==R.id.btn_status)
        {
            Intent intent = new Intent(WwaaCTNActivity.this, WwaaCtnStatusActivity.class);
            startActivity(intent);
            finish();
        }
        else if (v.getId()==R.id.btn_history)
        {
            Intent intent = new Intent(WwaaCTNActivity.this, WwaaCTNHistoryActivity.class);
            startActivity(intent);
        }

    }

}
