package com.childrencareapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.analytics.Tracker;

import util.AppManager;
import util.MyApp;

public class AccountActivity extends Activity {

    Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_account_setup);

        AppManager.getInstance().setFont(this);

        // Obtain the shared Tracker instance.
        MyApp application = (MyApp) getApplication();
        mTracker = application.getDefaultTracker();

        TextView heading = (TextView)findViewById(R.id.heading);
        TextView tvSection = (TextView)findViewById(R.id.tv_section);
        TextView text = (TextView)findViewById(R.id.text);
        Button btnSetup = (Button)findViewById(R.id.btn_setup);

        heading.setTypeface(AppManager.getInstance().getBlackTypeface());
        tvSection.setTypeface(AppManager.getInstance().getBlackTypeface());
        text.setTypeface(AppManager.getInstance().getRegularTypeface());
        btnSetup.setTypeface(AppManager.getInstance().getBoldTypeface());

    }

    @Override
    protected void onResume() {
        super.onResume();

//        mTracker.setScreenName("AccountActivity");
//        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

    }

    public void onClick(View v)
    {
        if(v.getId() == R.id.btn_setup)
        {
            Intent intent = new Intent(this, AccountSettingActivity.class);
            startActivity(intent);
//            TabGroupActivity parentActivity = (TabGroupActivity)getParent();
//            parentActivity.startChildActivity("AccountSettingActivity", intent);
        }
    }

}
