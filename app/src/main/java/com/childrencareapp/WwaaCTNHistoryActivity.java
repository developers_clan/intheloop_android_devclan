package com.childrencareapp;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import Database.MyDbHelper;
import Model.Agency;
import util.AppManager;
import Model.CtnStatusModel;
import util.Constants;
import adapter.WWAACTNHistoryAdapter;

public class WwaaCTNHistoryActivity extends Activity {

//    int index;
    MyDbHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_wwaa_ctnhistory);

        TextView heading = (TextView) findViewById(R.id.heading);
        ImageView imgAgency = (ImageView) findViewById(R.id.img_agency);
        TextView tvChildName = (TextView) findViewById(R.id.tv_child_name);
        TextView tvBody = (TextView) findViewById(R.id.tv_body);
        ListView listView = (ListView) findViewById(R.id.listView);
        TextView history = (TextView) findViewById(R.id.history);
        TextView contact = (TextView) findViewById(R.id.contact);
        TextView tvPhone = (TextView) findViewById(R.id.tv_phone);
        TextView tvConnect = (TextView) findViewById(R.id.tv_connect);
        TextView tvWebsite = (TextView) findViewById(R.id.tv_website);

        heading.setTypeface(AppManager.getInstance().getBlackTypeface());
        contact.setTypeface(AppManager.getInstance().getBlackTypeface());
        history.setTypeface(AppManager.getInstance().getBlackTypeface());
        tvPhone.setTypeface(AppManager.getInstance().getRegularTypeface());
        tvWebsite.setTypeface(AppManager.getInstance().getRegularTypeface());
        tvConnect.setTypeface(AppManager.getInstance().getRegularTypeface());
        tvChildName.setTypeface(AppManager.getInstance().getRegularTypeface());

        dbHelper = new MyDbHelper(this);


//        Bundle bundle = getIntent().getExtras();
//        if(bundle != null)
//        {
//            index = bundle.getInt("INDEX");
//        }

        Agency agency = AppManager.getInstance().getAgencyModel();
        heading.setText(agency.getHomeMainDetails().get(0).getHomeMainTitle());
        tvBody.setText(agency.getHomeMainDetails().get(0).getHomeMainBody());
        tvPhone.setText(agency.getAgencyPhone().get(0).getNumber());


        ArrayList<CtnStatusModel> list = null;
        SharedPreferences pref = getSharedPreferences(Constants.WWAACHILD_PREF,Activity.MODE_PRIVATE);
        if(pref.contains(Constants.CHILD_POS))
        {
            int pos = pref.getInt(Constants.CHILD_POS,-1);
            tvChildName.setText(AppManager.getInstance().getChildModels().get(pos).getName());
            list = dbHelper.getCTNStatus(AppManager.getInstance().getChildModels().get(pos).getId());
        }

        WWAACTNHistoryAdapter adapter = new WWAACTNHistoryAdapter(this,R.layout.custom_ctn_history,list);
        listView.setAdapter(adapter);

    }

    public void onClick(View v)
    {
        if(v.getId() == R.id.btn_back)
        {
            finish();
        }
    }

}
