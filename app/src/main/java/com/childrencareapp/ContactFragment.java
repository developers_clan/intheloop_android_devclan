package com.childrencareapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;


public class ContactFragment extends Fragment {

    public  static RelativeLayout rl;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_contact, null, false);

        ListView listView = (ListView) root.findViewById(R.id.listView);
        String[] arr = {"Children’s Treatment Network Logo","York Region’s Logo","Child Development Programs’ Logo"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),R.layout.custom_contactlist,R.id.img,arr);
        listView.setAdapter(adapter);

        rl = (RelativeLayout) root.findViewById(R.id.rl);

        return root;
    }
}
