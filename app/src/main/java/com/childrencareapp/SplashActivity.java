package com.childrencareapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import Database.MyDbHelper;
import Model.AddContactModel;
import Model.Agency;
import Model.ChildModel;
import Model.Contact;
import Model.HomeTextRes;
import Model.Team;
import io.fabric.sdk.android.Fabric;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import util.AppManager;
import util.Constants;

public class SplashActivity extends Activity {

    int SPLASH_TIME_OUT = 1000;
    MyDbHelper dbHelper;
    boolean flag, flag2, flag3,flag4;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context c;
    Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash);

        c = this;
        pref = this.getPreferences(Context.MODE_PRIVATE);
        editor = pref.edit();

        AppManager.getInstance().setFont(this);
        dbHelper = new MyDbHelper(this);
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }

        flag = false;
        flag2 = false;
        flag3 = false;
        flag4 = false;

        TextView welcome = (TextView) findViewById(R.id.welcome);
        TextView moment = (TextView) findViewById(R.id.moment);

        welcome.setTypeface(AppManager.getInstance().getBlackTypeface());
        moment.setTypeface(AppManager.getInstance().getRegularTypeface());

        homeAPI();

        homeTextAPI();

        teamAPI();

        //if(isOnline() || pref.contains("key_name"))
        ContactAPI();


    }

    public boolean isOnline() {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }

        return false;
    }

    private void homeTextAPI() {
        Handler mainHandler = new Handler(SplashActivity.this.getMainLooper());
        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {

                AppManager.getInstance().restAPI.getHomeText(new Callback<List<HomeTextRes>>() {
                    @Override
                    public void success(List<HomeTextRes> textRes, Response response) {

                        editor.putString("key_name", textRes.get(0).getHome());
                        editor.apply();
                        Log.i("TAG1", String.valueOf(textRes.size()));
                        flag2 = true;
                        AppManager.getInstance().setTextRes(textRes);

                    }

                    @Override
                    public void failure(RetrofitError error) {
                        if(pref.contains("key_name"))
                        {
                            flag2 = true;
                            String hs = pref.getString("key_name", "Connection to server failed to get home texts");
                            HomeTextRes hss = new HomeTextRes();
                            hss.setHome(hs);
                            List<HomeTextRes> fs = new ArrayList<HomeTextRes>();
                            fs.add(hss);
                            AppManager.getInstance().setTextRes(fs);
                            Log.i("TAG2", fs.get(0).getHome());
                        }
                        else
                        {
                            flag2 = false;
                        }

                    }
                });

            }
        };
        mainHandler.post(myRunnable);
    }

    private void homeAPI() {
        Handler mainHandler = new Handler(SplashActivity.this.getMainLooper());
        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {

                AppManager.getInstance().restAPI.getAgency(new Callback<List<Agency>>() {
                    @Override
                    public void success(List<Agency> agency, Response response) {

                        AppManager.getInstance().setAgency(agency);
                        AppManager.getInstance().setIsFromServer(true);

                        flag = true;
//                        GoNext();
                    }

                    @Override
                    public void failure(RetrofitError error) {

                        List<Agency> agency = dbHelper.getAgenciesData();
                        if (agency.size() > 0) {
                            AppManager.getInstance().setAgency(agency);
                            AppManager.getInstance().setIsFromServer(false);

                            flag = true;
//                            GoNext();

                        } else {
//                            Toast.makeText(getApplicationContext(), error.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                            flag = false;
//                            finish();
                        }
                    }
                });

            }
        };
        mainHandler.post(myRunnable);
    }

    private void teamAPI() {
        Handler mainHandler = new Handler(SplashActivity.this.getMainLooper());
        final Runnable myRunnable = new Runnable() {
            @Override
            public void run() {

                AppManager.getInstance().restAPI.getTeam(new Callback<List<Team>>() {
                    @Override
                    public void success(List<Team> teams, Response response) {

                        AppManager.getInstance().setTeams(teams);

                        Log.e("TEAM_API --- ", "API Called");

                        AsyncTask.execute(new Runnable() {
                            @Override
                            public void run() {

                                addDatatoArray();

                            }
                        });

                        flag3 = true;
                        GoNext();

                        Log.e("TEAM_API --- ", "Data Saved");

                    }

                    @Override
                    public void failure(RetrofitError error) {
//                        Toast.makeText(getApplicationContext(), error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
//                        finish();
                        if(pref.contains("key_name") || pref.contains("contacts"))
                        {
                            flag3 = true;
                        }
                        else {
                            flag3 = false;
                        }

                        GoNext();
                    }
                });

            }
        };
        mainHandler.post(myRunnable);
    }


    private void ContactAPI() {
        Handler mainHandler = new Handler(SplashActivity.this.getMainLooper());
        gson = new Gson();

        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {

                AppManager.getInstance().restAPI.getContact(new Callback<List<Contact>>() {
                    @Override
                    public void success(List<Contact> contacts, Response response) {

                        String inputString = gson.toJson(contacts);
                        editor.putString("contacts", inputString);
                        editor.apply();
                        Log.i("TAG3", String.valueOf(contacts.size()));
                        flag4 = true;
                        AppManager.getInstance().setContacts(contacts);


                    }

                    @Override
                    public void failure(RetrofitError error) {
                        if(pref.contains("contacts")) {
                            flag4 = true;
                            Type type = new TypeToken<ArrayList<Contact>>() {
                            }.getType();
                            String outputarray = pref.getString("contacts", "Connection to server failed to get contacts");
                            ArrayList<Contact> finalOutputString = gson.fromJson(outputarray, type);
                            Log.i("TAG4", outputarray);
                            AppManager.getInstance().setContacts(finalOutputString);
                        }
                        else {
                            flag4 = false;
                        }

                    }
                });

            }
        };
        mainHandler.post(myRunnable);
    }

    private void GoNext() {
        if (flag || flag2 || flag3 || flag4) {
            SharedPreferences pref = SplashActivity.this.getSharedPreferences(Constants.AGREEMENT_PREF, Activity.MODE_PRIVATE);
            if (pref.contains(Constants.Key_AGREE) && pref.getBoolean(Constants.Key_AGREE, false)) {

                MyDbHelper dbHelper = new MyDbHelper(SplashActivity.this);
                final ArrayList<ChildModel> list = dbHelper.getData();
                AppManager.getInstance().setChildModels(list);

                Intent intent = new Intent(SplashActivity.this, TabActivity.class);
                startActivity(intent);
            } else {
                Intent welcome = new Intent(SplashActivity.this, TermConditionActivity.class);
                startActivity(welcome);
            }

            finish();
        }
        else {
            //finish();
            try {
                AlertDialog alertDialog = new AlertDialog.Builder(c).create();

                alertDialog.setTitle("Info");
                alertDialog.setMessage("Internet not available, check your internet connectivity and try again!");
                alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();

                    }
                });

                alertDialog.show();
            }
            catch(Exception e)
            {
                Log.d("TAG Dailog", "Show Dialog: "+e.getMessage());
            }


        }
    }

    private void addDatatoArray() {
        dbHelper.deleteTeams();

        for (int i = 0; i < AppManager.getInstance().getTeams().size(); i++) {

            Team team = AppManager.getInstance().getTeams().get(i);

            if (!dbHelper.CheckIsDataAlreadyInDBorNot(team.getNid())) {
                AddContactModel contactModel = new AddContactModel();
                contactModel.setNodeId(team.getNid());
                if (team.getName().size() > 0) {
                    contactModel.setName(team.getName().get(0).getValue());
                }
                if (team.getDiscipline().size() > 0) {
                    contactModel.setDiscipline(team.getDiscipline().get(0).getName());
                }
                if (team.getTeamPhone().size() > 0) {
                    for (int a = 0; a < team.getTeamPhone().size(); a++) {
                        if (team.getTeamPhone().get(a).getPrimary()) {
                            contactModel.setPhone(team.getTeamPhone().get(a).getNumber());
                            contactModel.setExtension(team.getTeamPhone().get(a).getExtension());
                            break;
                        }
                    }
                }
                if (team.getEmail().size() > 0) {
                    contactModel.setEmail(team.getEmail().get(0).getEmail());
                }
                if (team.getGender().size() > 0) {
                    contactModel.setGender(team.getGender().get(0).getValue());
                }


                if (team.getHeadshot().size() > 0 && team.getHeadshot().get(0).getUri() != null) {
                    String[] arr = team.getHeadshot().get(0).getUri().split("/");
                    String name = arr[arr.length - 1];
                    new ImageDownloadAndSave().execute(team.getHeadshot().get(0).getUri(), team.getNid(), name);
                }

                for (int j = 0; j < team.getAgencyThumbnail().size(); j++) {
                    if (team.getAgencyThumbnail().get(j).getUri() != null) {
                        String[] arr = team.getAgencyThumbnail().get(j).getUri().split("/");
                        String name = arr[arr.length - 1];
                        new ThumbnailDownloadAndSave().execute(team.getAgencyThumbnail().get(j).getUri(), team.getNid(), "" + team.getAgencyThumbnail().size(), name);
                    }
                }
                Gson gson = new Gson();
                String officeList = gson.toJson(team.getOffices());
                String agencyList = gson.toJson(team.getAgencyThumbnail());
                String phoneList = gson.toJson(team.getTeamPhone());

                if (dbHelper.addNewContact(contactModel, officeList, phoneList, agencyList, "")) {

                } else {
//                    Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
                }

            } else {
                AddContactModel contactModel = new AddContactModel();
                contactModel.setNodeId(team.getNid());
                if (team.getName().size() > 0) {
                    contactModel.setName(team.getName().get(0).getValue());
                }
                if (team.getDiscipline().size() > 0) {
                    contactModel.setDiscipline(team.getDiscipline().get(0).getName());
                }
                if (team.getTeamPhone().size() > 0) {
                    for (int a = 0; a < team.getTeamPhone().size(); a++) {
                        if (team.getTeamPhone().get(a).getPrimary()) {
                            contactModel.setPhone(team.getTeamPhone().get(a).getNumber());
                            contactModel.setExtension(team.getTeamPhone().get(a).getExtension());
                            break;
                        }
                    }
                }
                if (team.getEmail().size() > 0) {
                    contactModel.setEmail(team.getEmail().get(0).getEmail());
                }
                if (team.getGender().size() > 0) {
                    contactModel.setGender(team.getGender().get(0).getValue());
                }
                if (team.getHeadshot().size() > 0 && team.getHeadshot().get(0).getUri() != null) {
                    String[] arr = team.getHeadshot().get(0).getUri().split("/");
                    String name = arr[arr.length - 1];
                    new ImageDownloadAndSave().execute(team.getHeadshot().get(0).getUri(), team.getNid(), name);
                }
                for (int j = 0; j < team.getAgencyThumbnail().size(); j++) {
                    if (team.getAgencyThumbnail().get(j).getUri() != null) {
                        String[] arr = team.getAgencyThumbnail().get(j).getUri().split("/");
                        String name = arr[arr.length - 1];
                        new ThumbnailDownloadAndSave().execute(team.getAgencyThumbnail().get(j).getUri(), team.getNid(), "" + team.getAgencyThumbnail().size(), name);
                    }
                }
                Gson gson = new Gson();
                String officeList = gson.toJson(team.getOffices());
                String agencyList = gson.toJson(team.getAgencyThumbnail());
                String phoneList = gson.toJson(team.getTeamPhone());

                if (dbHelper.updateContact(contactModel, officeList, agencyList, phoneList, "", team.getNid())) {

                } else {
//                    Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
                }
            }
        }

//        AppManager.getInstance().setContactList(dbHelper.getAllContact());

//        if (flag) {
//            contactList = dbHelper.getContactWithLimit(next, "0");
//            contactListSearch = dbHelper.getAllContact();
//            Collections.sort(contactList, teamomparator);
//
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    updateUI(contactList);
//
//                    AppManager.getInstance().setIsSearch(false);
//                    AppManager.getInstance().setIsView(false);
//                    TeamCustomAdapter adapter = new TeamCustomAdapter(contactList, getParent());
//                    listView.setAdapter(adapter);
//                }
//            });
//        }
    }


    private class ImageDownloadAndSave extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... arg0) {
            try {
                URL url = new URL(arg0[0]);
                String nodeID = arg0[1];
                String imgName = arg0[2];
                URLConnection ucon = url.openConnection();
                InputStream inputStream = null;
                HttpURLConnection httpConn = (HttpURLConnection) ucon;
                httpConn.setRequestMethod("GET");
                httpConn.connect();

                if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    inputStream = httpConn.getInputStream();
                }

                byte[] byteImage = convertToByte(inputStream);

                Bitmap bmp = BitmapFactory.decodeByteArray(byteImage, 0,
                        byteImage.length);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.PNG, 80, stream);
                bmp.recycle();
                byte[] byteArray = stream.toByteArray();

                String path = saveImageToExternalStorage(byteArray, imgName);
                if (!path.equalsIgnoreCase("")) {
//                    dbHelper.saveImage(byteArray,-1,nodeID,true);
                    dbHelper.saveImage(path, -1, nodeID, true);
                }

                stream.close();
                stream = null;


//                contactList = dbHelper.getAllContact();
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        setAdapter();
//                    }
//                });


            } catch (IOException io) {
                io.printStackTrace();
            }

            return null;
        }


    }

    ArrayList<String> thumbList = new ArrayList<String>();

    private class ThumbnailDownloadAndSave extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... arg0) {
            try {
                URL url = new URL(arg0[0]);
                String nodeID = arg0[1];
                String count = arg0[2];
                String imgName = arg0[3];
                URLConnection ucon = url.openConnection();
                InputStream inputStream = null;
                HttpURLConnection httpConn = (HttpURLConnection) ucon;
                httpConn.setRequestMethod("GET");
                httpConn.connect();

                if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    inputStream = httpConn.getInputStream();
                }

                byte[] byteImage = convertToByte(inputStream);

                Bitmap bmp = BitmapFactory.decodeByteArray(byteImage, 0,
                        byteImage.length);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.PNG, 80, stream);
                bmp.recycle();
                byte[] byteArray = stream.toByteArray();

                stream.close();
                stream = null;

                String path = saveImageToExternalStorage(byteArray, imgName);

                thumbList.add(path);

                if (count.equals("" + thumbList.size())) {
                    dbHelper.saveThumbImage(thumbList, nodeID);
                    thumbList.clear();
//                    contactList = dbHelper.getAllContact();
                }

//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        setAdapter();
//                    }
//                });

            } catch (IOException io) {
                io.printStackTrace();
            }

            return null;
        }


    }

    private byte[] convertToByte(InputStream input) throws IOException {
        byte[] buffer = new byte[8192];
        int bytesRead;
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }

        byte[] arr = output.toByteArray();

        output.close();
        output = null;

        return arr;
    }

    public String saveImageToExternalStorage(byte[] image, String name) {
        String fullPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/InTheLoop/" + "thumbnail";

        try {
            File dir = new File(fullPath);
            if (!dir.exists()) {
                dir.mkdirs();
            }

            OutputStream fOut = null;
            File file = new File(fullPath, name);

            if (!file.exists()) {
                file.createNewFile();
            }
            fOut = new FileOutputStream(file);

// 100 means no compression, the lower you go, the stronger the compression
//            image.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.write(image);
            fOut.flush();
            fOut.close();

//            MediaStore.Images.Media.insertImage(context.getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());

            return file.getAbsolutePath();

        } catch (Exception e) {
            Log.e("saveToExternalStorage()", e.getMessage());
            return "";
        }
    }

}
