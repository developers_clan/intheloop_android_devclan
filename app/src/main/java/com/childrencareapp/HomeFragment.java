package com.childrencareapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import Model.Agency;
import util.AppManager;
import util.Constants;
import adapter.AgencyListAdapter;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class HomeFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match

    RelativeLayout rlCDP, rlCTN, rlYorkRegion;
    ImageView imgCTN, imgCDP, imgYR;
    public static RelativeLayout rl;
    ProgressDialog pg;
    ListView listView;

    boolean flag;

    Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_home, null, false);

        rl = (RelativeLayout) root.findViewById(R.id.rl);
        listView = (ListView) root.findViewById(R.id.listView);
//        ArrayList<AgencyModel> list = new ArrayList<AgencyModel>();
//        AgencyListAdapter adapter = new AgencyListAdapter(getActivity(), R.layout.custom_list_home, list);
//        listView.setAdapter(adapter);

        flag = false;

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(getActivity(), CDPActivity.class);
                intent.putExtra("INDEX", position);
                startActivity(intent);
//                ActivityTransitionLauncher.with(getActivity()).from(view.findViewById(R.id.img)).launch(intent);
            }
        });

        pg = new ProgressDialog(getActivity(), ProgressDialog.THEME_HOLO_DARK);
        pg.setCancelable(false);
        pg.setTitle("Please Wait");
        pg.setMessage("Refreshing...");

        Date date = null;
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date currentDate = null;
        try {
            currentDate = sdf.parse(sdf.format(new Date(c.getTimeInMillis())));
            date = sdf.parse(sdf.format(new Date(c.getTimeInMillis())));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SharedPreferences pref = null;
//        if (flag) {
        pref = getActivity().getSharedPreferences(Constants.AGENCY_PREF, Activity.MODE_PRIVATE);
        if (pref.contains(Constants.Date)) {
            try {
                date = sdf.parse(sdf.format(new Date(pref.getLong(Constants.Date, 0))));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }


//        }
        if (currentDate.compareTo(date) == 0 || currentDate.compareTo(date) > 0) {

            Handler mainHandler = new Handler(getActivity().getMainLooper());
            Runnable myRunnable = new Runnable() {
                @Override
                public void run() {
                    pg.show();
                    AppManager.getInstance().restAPI.getAgency(new Callback<List<Agency>>() {
                        @Override
                        public void success(List<Agency> agency, Response response) {
                            pg.cancel();
                            AppManager.getInstance().setAgency(agency);

                            flag = true;

                            SharedPreferences agency_pr = getActivity().getSharedPreferences(Constants.AGENCY_PREF, Activity.MODE_PRIVATE);
                            SharedPreferences.Editor editor = agency_pr.edit();

                            Calendar calendar = Calendar.getInstance();
                            calendar.add(Calendar.DATE, Constants.REFRESH_DAYS);
                            Date date = new Date(calendar.getTimeInMillis());

                            Gson gson = new Gson();
                            String value = gson.toJson(agency);
                            editor.putString(Constants.AGENCY_API, value);
                            editor.putLong(Constants.Date, date.getTime());
                            editor.commit();

                            List<Agency> list = AppManager.getInstance().getAgency();
                            AgencyListAdapter adapter = new AgencyListAdapter(getActivity(), R.layout.custom_list_home, list);
                            listView.setAdapter(adapter);

                        }

                        @Override
                        public void failure(RetrofitError error) {
                            pg.cancel();
                            Toast.makeText(getActivity(), error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });

                }
            };
            mainHandler.post(myRunnable);


        } else {
            GsonBuilder gsonb = new GsonBuilder();
            Gson gson = gsonb.create();
            Type type = new TypeToken<List<Agency>>() {
            }.getType();
            if (pref != null) {
                List<Agency> list = gson.fromJson(pref.getString(Constants.AGENCY_API, ""), type);
                AppManager.getInstance().setAgency(list);
            }

            List<Agency> list = AppManager.getInstance().getAgency();
            AgencyListAdapter adapter = new AgencyListAdapter(getActivity(), R.layout.custom_list_home, list);
            listView.setAdapter(adapter);

        }


        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
