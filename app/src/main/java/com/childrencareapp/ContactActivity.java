package com.childrencareapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.picasso.Picasso;

import java.util.List;

import Model.Agency;
import Model.Contact;
import util.AppManager;
import util.Constants;
import util.MyApp;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ContactActivity extends Activity {

    ProgressDialog pg;
    List<Contact> contacts;
    public static RelativeLayout rl;

    Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_contact);

        AppManager.getInstance().setFont(this);

        // Obtain the shared Tracker instance.
        MyApp application = (MyApp) getApplication();
        mTracker = application.getDefaultTracker();

        rl = (RelativeLayout) findViewById(R.id.rl);
        final ListView listView = (ListView) findViewById(R.id.listView);
        TextView heading = (TextView) findViewById(R.id.heading);
        TextView text = (TextView) findViewById(R.id.text);

//get the sunrise animation
        Animation sunRise = AnimationUtils.loadAnimation(ContactActivity.this, R.anim.sun_rise_five);
//                            ContactFragment.rl.setVisibility(View.GONE);

        rl.startAnimation(sunRise);


        heading.setTypeface(AppManager.getInstance().getBlackTypeface());
        text.setTypeface(AppManager.getInstance().getRegularTypeface());

        pg = new ProgressDialog(getParent(), ProgressDialog.THEME_HOLO_DARK);
        pg.setCancelable(false);
        pg.setTitle("Please Wait");
        pg.setMessage("Loading...");


        if (AppManager.getInstance().getContacts().size() > 0) {
            this.contacts = AppManager.getInstance().getContacts();

            ArrAdapter adapter = new ArrAdapter(getParent(), R.layout.custom_contactlist, contacts);
            listView.setAdapter(adapter);

        } else {
            Handler mainHandler = new Handler(ContactActivity.this.getMainLooper());
            Runnable myRunnable = new Runnable() {
                @Override
                public void run() {

                    pg.show();
                    AppManager.getInstance().restAPI.getContact(new Callback<List<Contact>>() {
                        @Override
                        public void success(List<Contact> contacts, Response response) {
                            pg.cancel();

                            ContactActivity.this.contacts = contacts;

                            AppManager.getInstance().setContacts(contacts);

                            ArrAdapter adapter = new ArrAdapter(getParent(), R.layout.custom_contactlist, contacts);
                            listView.setAdapter(adapter);

                        }

                        @Override
                        public void failure(RetrofitError error) {
                            pg.cancel();
                        }
                    });

                }
            };
            mainHandler.post(myRunnable);
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                Contact contact = contacts.get(position);

                String title = "";
                List<Agency> agencies = AppManager.getInstance().getAgency();
                if (agencies != null){
                    for (int i=0;i<agencies.size();i++){
                        if (contact.getAgency().equalsIgnoreCase(agencies.get(i).getNid())){
                            title = agencies.get(i).getHomeMainDetails().get(0).getHomeMainTitle();
                            break;
                        }
                    }
                }


                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory(Constants.kCatContactUs)
                        .setAction(Constants.ContactClick+"-"+title)
                        .build());


                if (contact.getPhone().getNumber() != null && !contact.getPhone().getNumber().equalsIgnoreCase("")) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("tel:" + contact.getPhone().getNumber())));
                }

            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();

        mTracker.setScreenName(Constants.ContactsActivity);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

    }

    class ArrAdapter extends ArrayAdapter<Contact> {

        Context context;
        List<Contact> list;

        public ArrAdapter(Context context, int resource, List<Contact> objects) {
            super(context, resource, objects);
            this.context = context;
            this.list = objects;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                view = LayoutInflater.from(context).inflate(R.layout.custom_contactlist, parent, false);
            }

            TextView tvPhone = (TextView) view.findViewById(R.id.tv_phone);
            tvPhone.setTypeface(AppManager.getInstance().getBlackTypeface());
            ImageView img = (ImageView) view.findViewById(R.id.img);

            if (list.get(position).getPhone() != null) {
                tvPhone.setText(list.get(position).getPhone().getNumber());
            } else {
                tvPhone.setText("");
            }

            if (list.get(position).getColorCode() != null) {
                tvPhone.setBackgroundColor(Color.parseColor(list.get(position).getColorCode()));
            }

            if (list.get(position).getPhone().getNumberAlias() != null && !list.get(position).getPhone().getNumberAlias().equalsIgnoreCase("")) {
                tvPhone.setText(list.get(position).getPhone().getNumberAlias());
            }
            else if (list.get(position).getPhone().getNumber() != null && !list.get(position).getPhone().getNumber().equalsIgnoreCase("")) {
                tvPhone.setText(list.get(position).getPhone().getNumber());
            }

            if (AppManager.getInstance().isFromServer()) {
                if (list.get(position).getContactLogo().size() > 0) {
                    Picasso.with(context).load(list.get(position).getContactLogo().get(0).getUri()).into(img);
                }
            }
//            else{
//                if (list.get(position).getAgencyLogo() != null) {
//                    img.setImageBitmap(BitmapFactory.decodeByteArray(list.get(position).getAgencyLogo(), 0,
//                            list.get(position).getAgencyLogo().length));
//                }
//            }

            return view;
        }
    }


}
