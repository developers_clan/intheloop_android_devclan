package com.childrencareapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;


public class AccountSetupFragment extends Fragment {

    public static RelativeLayout rl;
    Button btnSetup;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_account_setup, null, false);

        rl = (RelativeLayout)root.findViewById(R.id.rl);
        btnSetup = (Button)root.findViewById(R.id.btn_setup);

        btnSetup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),AccountSettingActivity.class);
                startActivity(intent);
            }
        });

        return root;
    }
}
