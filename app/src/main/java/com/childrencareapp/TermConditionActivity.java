package com.childrencareapp;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;

import Database.MyDbHelper;
import Model.ChildModel;
import util.AppManager;
import util.Constants;
import util.MyApp;

public class TermConditionActivity extends Activity {

    Button btnCancel, btnContinue,btnTerm;
    CheckBox chkTerm;
    MyDbHelper dbHelper;
    TextView textTerm;
    boolean show = true;
    Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_term_condition);

        AppManager.getInstance().setFont(this);

        // Obtain the shared Tracker instance.
        MyApp application = (MyApp) getApplication();
        mTracker = application.getDefaultTracker();

        dbHelper = new MyDbHelper(this);
//        try {
//            dbHelper.createDataBase();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        btnContinue = (Button) findViewById(R.id.btn_continue);
        btnCancel = (Button) findViewById(R.id.btn_cancel);
        btnTerm = (Button) findViewById(R.id.btnTerm);
        chkTerm = (CheckBox) findViewById(R.id.chk_term);
        TextView term = (TextView)findViewById(R.id.term);
        textTerm = (TextView)findViewById(R.id.textTerm);
        TextView termText = (TextView)findViewById(R.id.term_text);
        TextView termText2 = (TextView)findViewById(R.id.term_text_2);

        //////underline text////
        SpannableString content = new SpannableString("TERMS AND CONDITIONS");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        btnTerm.setText(content);
        ////// end underline text////

        term.setTypeface(AppManager.getInstance().getBlackTypeface());
        termText.setTypeface(AppManager.getInstance().getRegularTypeface());
        termText2.setTypeface(AppManager.getInstance().getRegularTypeface());
        chkTerm.setTypeface(AppManager.getInstance().getBoldTypeface());
        btnCancel.setTypeface(AppManager.getInstance().getBoldTypeface());
        btnContinue.setTypeface(AppManager.getInstance().getBoldTypeface());
        btnTerm.setTypeface(AppManager.getInstance().getBoldTypeface());


        chkTerm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    btnContinue.setEnabled(true);
                    btnContinue.setBackgroundResource(R.drawable.btn_bg);
                } else {
                    btnContinue.setEnabled(false);
                    btnContinue.setBackgroundResource(R.drawable.btn_disable_bg);
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        mTracker.setScreenName(Constants.TermConditionActivity);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

    }

    public void onClick(View v) {
        if (v.getId() == R.id.btn_continue) {
//            Intent intent = new Intent(TermConditionActivity.this, TabActivity.class);
//            startActivity(intent);
//            finish();

            SharedPreferences agency_pr = TermConditionActivity.this.getSharedPreferences(Constants.AGREEMENT_PREF, Activity.MODE_PRIVATE);
            SharedPreferences.Editor editor = agency_pr.edit();
            editor.putBoolean(Constants.Key_AGREE,true);
            editor.commit();

            ArrayList<ChildModel> list = dbHelper.getData();
            if (list.size()>0)
            {
                AppManager.getInstance().setIsParent(false);
                Intent intent = new Intent(TermConditionActivity.this, ChildViewActivity.class);
                startActivity(intent);
                finish();
            }
            else {
                Intent intent = new Intent(TermConditionActivity.this, AccountSettingActivity.class);
                startActivity(intent);
                finish();
            }

        } else if (v.getId() == R.id.btn_cancel) {
            finish();
        }
         else if (v.getId() == R.id.btnTerm) {
            if (show) {
                textTerm.setVisibility(View.VISIBLE);
                show = false;
            }
            else {
                textTerm.setVisibility(View.GONE);
                show = true;
            }
        }
    }

}
