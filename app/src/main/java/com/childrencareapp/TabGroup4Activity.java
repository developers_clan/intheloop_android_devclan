package com.childrencareapp;

import android.content.Intent;
import android.os.Bundle;

public class TabGroup4Activity extends TabGroupActivity{
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startChildActivity("ToolsActivity", new Intent(this,ToolsActivity.class));
    }
}
