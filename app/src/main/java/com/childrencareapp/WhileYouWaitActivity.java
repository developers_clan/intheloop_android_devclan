package com.childrencareapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.special.ResideMenu.ResideMenu;
import com.special.ResideMenu.ResideMenuItem;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import Database.MyDbHelper;
import Model.Agency;
import Model.Contact;
import Model.ParentData;
import Model.Wyw;
import util.AppManager;
import util.Constants;
import util.MyApp;

public class WhileYouWaitActivity extends Activity {

    WebView webView;
//    int index;

    String summary;
    ProgressDialog pg;

    boolean loadingFinished = true;
    boolean redirect = false;

    ResideMenu resideMenu;
    TextView heading;

    ImageButton openDrawer;
    RelativeLayout rlHeader;
    Agency agency;

    private ResideMenuItem itemWhile;
    private ResideMenuItem itemWhere;
    private ResideMenuItem itemSocial;
    private ResideMenuItem itemFB=null;
    private ResideMenuItem itemTW=null;
    private ResideMenuItem itemPin=null;
    private ResideMenuItem itemYT=null;
    String fbLink = "",twLink ="",pinLink = "",ytLink ="";

    Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_while_you_wait);

        AppManager.getInstance().setFont(this);

        // Obtain the shared Tracker instance.
        MyApp application = (MyApp) getApplication();
        mTracker = application.getDefaultTracker();

        webView = (WebView) findViewById(R.id.webView);
        heading = (TextView) findViewById(R.id.heading);
        rlHeader = (RelativeLayout) findViewById(R.id.rlHeader);
        openDrawer = (ImageButton) findViewById(R.id.btn_drawer);

        heading.setTypeface(AppManager.getInstance().getBlackTypeface());


        agency = AppManager.getInstance().getAgencyModel();

        setUpMenu();

        rlHeader.setVisibility(View.VISIBLE);
        rlHeader.setBackgroundColor(getResources().getColor(R.color.red_text));

        if (agency.getWywMainDetails().size() > 0) {
            heading.setText(agency.getWywMainDetails().get(0).getWywMainTitle());
        }


        pg = new ProgressDialog(getParent(), ProgressDialog.THEME_HOLO_DARK);
        pg.setCancelable(false);
        pg.setTitle("Please Wait");
        pg.setMessage("Loading...");


        WebSettings webSettings = webView.getSettings();
//        webSettings.setRenderPriority(WebSettings.RenderPriority.HIGH);
//        webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDefaultTextEncodingName("utf-8");

        webView.setWebViewClient(new HelloWebViewClient());
        if (Build.VERSION.SDK_INT >= 19) {
            webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }

        byte[] buffer = null;
        try {
            InputStream is = getAssets().open("AgencyHTML/agency.html");
            int size = is.available();

            buffer = new byte[size];
            is.read(buffer);
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String str = new String(buffer);

//        str = str.replace("agencyBannerPlaceholder", "");
        if (AppManager.getInstance().isFromServer()) {
            String[] arr = agency.getImage().get(0).getUri().split("/");
            String name = arr[arr.length - 1];
            str = str.replace("agencyBannerPlaceholder", Constants.PATH+"/"+name);//file:///android_asset/AgencyHTML/background-ctn-banner.png
        } else {
            if (agency.getAgencyImage() != null && !agency.getAgencyImage().equalsIgnoreCase("")) {
                str = str.replace("agencyBannerPlaceholder", agency.getAgencyImage());//file:///android_asset/AgencyHTML/background-ctn-banner.png
            }
        }
        if (agency.getColorCode() != null && agency.getColorCode().size() > 0) {
            str = str.replace("borderColorPlaceholder", agency.getColorCode().get(0));
        }


        if (agency.getWywMainDetails() != null && agency.getWywMainDetails().size() > 0) {
            str = str.replace("agencyBodyPlaceholder", agency.getWywMainDetails().get(0).getWywMainBody());
        }
        byte[] bufferSection = null;
        try {
            InputStream is = getAssets().open("AgencyHTML/sectionListItem.html");
            int size = is.available();

            bufferSection = new byte[size];
            is.read(bufferSection);
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String sections = "";
        for (int i = 0; i < agency.getWyw().size(); i++) {
            Wyw section = agency.getWyw().get(i);
            String strSection = new String(bufferSection);
            strSection = strSection.replace("sectionTitlePlaceholder", section.getWYWTitle().getMarkup());
            strSection = strSection.replace("sectionBodyPlaceholder", section.getWYWBody().getMarkup());
            sections += strSection;
        }

        str = str.replace("sectionItemsPlaceholder", sections);
        if (agency.getResources() != null && agency.getResources().size()>0) {
            str = str.replace("resourcesDiv", "<div class=\"resources\"><div class=\"resources_inner\">" + agency.getResources().get(0).getValue() + "</div></div>");
        } else{
            str = str.replace("resourcesDiv","");
        }
        if (agency.getAgencyPhone() !=null && agency.getAgencyPhone().size() > 0) {
            str = str.replace("agencyPhoneNumber", agency.getAgencyPhone().get(0).getNumber());
            if (agency.getAgencyPhone().get(0).getNumberAlias() != null && !agency.getAgencyPhone().get(0).getNumberAlias().equalsIgnoreCase("")){
                str = str.replace("agencyDisplayPhoneNumber", agency.getAgencyPhone().get(0).getNumberAlias());
            }
            else {
                str = str.replace("agencyDisplayPhoneNumber", agency.getAgencyPhone().get(0).getNumber());
            }
        } else {
            str = str.replace("agencyPhoneNumber", "");
            str = str.replace("contact-number", "");
        }
        if (agency.getWebsite() !=null && agency.getWebsite().size() > 0) {
            str = str.replace("WebsiteURLPlaceholder", agency.getWebsite().get(0).getUrl());
            str = str.replace("WebsitePlaceholder", agency.getWebsite().get(0).getTitle());
        } else {
            str = str.replace("WebsitePlaceholder", "");
            str = str.replace("website-background", "");
        }
        if (agency.getAgencyEmail() != null && agency.getAgencyEmail().size() > 0) {
            str = str.replace("CDP_ConnectPlaceHolder", agency.getAgencyEmail().get(0).getTitle());
            str = str.replace("agencyConnectUrlPlaceholder", "mailto:" + agency.getAgencyEmail().get(0).getUrl());
            str = str.replace("imgPlaceHolder", "cdp");
        } else if (agency.getCdpConnect() != null && agency.getCdpConnect().size() > 0) {
            str = str.replace("CDP_ConnectPlaceHolder", agency.getCdpConnect().get(0).getTitle());
            str = str.replace("agencyConnectUrlPlaceholder", agency.getCdpConnect().get(0).getUrl());
            str = str.replace("imgPlaceHolder", "events");
        } else {
            str = str.replace("CDP_ConnectPlaceHolder", "");
            str = str.replace("imgPlaceHolder-background", "");
        }

        webView.loadDataWithBaseURL("file:///android_asset/AgencyHTML/stylesheet.css", str, "text/html", null, null);
//        webView.loadData(summary, "text/html; charset=UTF-8", null);

    }

    @Override
    protected void onResume() {
        super.onResume();

        mTracker.setScreenName(Constants.WywActivity+"-"+ agency.getHomeMainDetails().get(0).getHomeMainTitle());
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

    }

    private void setUpMenu() {

        Contact contct = null;

        // attach to current activity;
        resideMenu = new ResideMenu(this);
        resideMenu.setUse3D(false);
        resideMenu.setBackground(R.drawable.menu_bar_background);
        resideMenu.attachToActivity(this);
        resideMenu.setShadowVisible(false);
        resideMenu.setMenuListener(menuListener);
        //valid scale factor is between 0.0f and 1.0f. leftmenu'width is 150dip.
        resideMenu.setScaleValue(0.6f);

        for (int i = 0; i < AppManager.getInstance().getContacts().size(); i++) {
            if (agency.getNid().equals(AppManager.getInstance().getContacts().get(i).getAgency())) {
                contct = AppManager.getInstance().getContacts().get(i);
                break;
            }
        }

        // create menu items;
        if (agency.getWwaaMainDetails().size()>0) {
            itemWhere = new ResideMenuItem(this, R.drawable.where_we_are_at, agency.getWwaaMainDetails().get(0).getWwaaMainTitle());
        }
        if (agency.getWywMainDetails().size() > 0) {
            itemWhile = new ResideMenuItem(this, R.drawable.while_you_wait, agency.getWywMainDetails().get(0).getWywMainTitle());
        }
//        itemSocial  = new ResideMenuItem(this, R.drawable.fb,R.drawable.twitter);
        if (contct != null) {
            try {

                for(int i=0;i<contct.getSocialLinks().size();i++) {
                    if (contct.getSocialLinks().get(i).getTitle().equalsIgnoreCase("Facebook")) {
                        fbLink = contct.getSocialLinks().get(i).getUrl();
                        itemFB = new ResideMenuItem(this, R.drawable.fb, "Facebook");
                    }
                }
                for(int i=0;i<contct.getSocialLinks().size();i++) {
                    if (contct.getSocialLinks().get(i).getTitle().equalsIgnoreCase("Twitter")){
                        twLink = contct.getSocialLinks().get(i).getUrl();
                        itemTW = new ResideMenuItem(this, R.drawable.twitter, "Twitter");
                    }

                }
                for(int i=0;i<contct.getSocialLinks().size();i++) {
                    if (contct.getSocialLinks().get(i).getTitle().equalsIgnoreCase("Pinterest")) {
                        pinLink = contct.getSocialLinks().get(i).getUrl();
                        itemPin = new ResideMenuItem(this, R.drawable.menu_bar_pin, "Pinterest");
                    }
                }
                for(int i=0;i<contct.getSocialLinks().size();i++) {
                    if (contct.getSocialLinks().get(i).getTitle().equalsIgnoreCase("YouTube")){
                        ytLink = contct.getSocialLinks().get(i).getUrl();
                        itemYT = new ResideMenuItem(this, R.drawable.menu_bar_yotube, "YouTube");
                    }

                }
//                itemSocial = new ResideMenuItem(this, R.drawable.fb, R.drawable.twitter,R.drawable.menu_bar_pin,R.drawable.menu_bar_yotube, twLink, fbLink,pinLink,ytLink);
            } catch (Exception e) {
                Log.e("WYW MENU", e.getLocalizedMessage());
            }
        } else {
//            itemSocial = new ResideMenuItem(this, R.drawable.fb, R.drawable.twitter);
        }

        itemWhere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory(Constants.kCatSideMenu)
                        .setAction(Constants.MyServicesMenuWYW+"-"+agency.getHomeMainDetails().get(0).getHomeMainTitle())
                        .build());

                if (AppManager.getInstance().getChildModels().size() > 0) {
                    MyDbHelper dbHelper = new MyDbHelper(WhileYouWaitActivity.this);
                    SharedPreferences pref = getSharedPreferences(Constants.WWAACHILD_PREF, Activity.MODE_PRIVATE);
                    int pos = -1;
                    if (pref.contains(Constants.CHILD_POS)) {
                        pos = pref.getInt(Constants.CHILD_POS, -1);
                    }

                    if (dbHelper.getCdpStatus(AppManager.getInstance().getChildModels().get(pos).getId(), agency.getNid()).size() > 0) {
//                        Intent intent = new Intent(WhileYouWaitActivity.this, WwaaCdpActivity.class);
//                        startActivity(intent);
                        resideMenu.closeMenu();
                        finish();
                        Intent intent = new Intent(getParent(), WwaaCdpActivity.class);
                        TabGroupActivity parentActivity = (TabGroupActivity) getParent();
                        parentActivity.startChildActivity("WwaaCdpActivity", intent);
                    } else {
                        AppManager.getInstance().setParentModels(new ArrayList<ParentData>());
//                        Intent intent = new Intent(WhileYouWaitActivity.this, WwaaCdpStatusActivity.class);
//                        startActivity(intent);
                        Intent intent = new Intent(getParent(), WwaaCdpStatusActivity.class);
                        TabGroupActivity parentActivity = (TabGroupActivity) getParent();
                        parentActivity.startChildActivity("WwaaCdpStatusActivity", intent);
                    }
//                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Add Child First!", Toast.LENGTH_SHORT).show();
                }
                resideMenu.closeMenu();

            }
        });
        itemWhile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory(Constants.kCatSideMenu)
                        .setAction(Constants.WYWMenuWYW+"-"+agency.getHomeMainDetails().get(0).getHomeMainTitle())
                        .build());

                resideMenu.closeMenu();

            }
        });

        if (itemFB != null) {
            itemFB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!fbLink.equalsIgnoreCase("")) {

                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory(Constants.kCatSideMenu)
                                .setAction(agency.getHomeMainDetails().get(0).getHomeMainTitle() + " - Facebook")
                                .build());

                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(fbLink)));
                    }
                }
            });
        }
        if (itemTW != null) {
            itemTW.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!twLink.equalsIgnoreCase("")) {

                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory(Constants.kCatSideMenu)
                                .setAction(agency.getHomeMainDetails().get(0).getHomeMainTitle() + " - Twitter")
                                .build());
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(twLink)));
                    }
                }
            });
        }
        if (itemPin != null) {
            itemPin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!pinLink.equalsIgnoreCase("")) {
                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory(Constants.kCatSideMenu)
                                .setAction(agency.getHomeMainDetails().get(0).getHomeMainTitle() + " - Pinterest")
                                .build());
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(pinLink)));
                    }
                }
            });
        }
        if (itemYT != null) {
            itemYT.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!ytLink.equalsIgnoreCase("")) {
                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory(Constants.kCatSideMenu)
                                .setAction(agency.getHomeMainDetails().get(0).getHomeMainTitle() + " -  YouTube")
                                .build());
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(ytLink)));
                    }
                }
            });
        }

//        itemCalendar.setOnClickListener(this);
//        itemSettings.setOnClickListener(this);

//        resideMenu.addMenuItem(itemWhere, ResideMenu.DIRECTION_LEFT);
//        resideMenu.addMenuItem(itemWhile, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(itemWhere, ResideMenu.DIRECTION_RIGHT);
        resideMenu.addMenuItem(itemWhile, ResideMenu.DIRECTION_RIGHT);
//        resideMenu.addMenuItem(itemSocial, ResideMenu.DIRECTION_RIGHT);

        if (itemFB != null){
            resideMenu.addMenuItem(itemFB, ResideMenu.DIRECTION_RIGHT);
        }
        if (itemTW != null){
            resideMenu.addMenuItem(itemTW, ResideMenu.DIRECTION_RIGHT);
        }
        if (itemPin != null){
            resideMenu.addMenuItem(itemPin, ResideMenu.DIRECTION_RIGHT);
        }
        if (itemYT != null){
            resideMenu.addMenuItem(itemYT, ResideMenu.DIRECTION_RIGHT);
        }

        // You can disable a direction by setting ->
        resideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_LEFT);

//        findViewById(R.id.title_bar_left_menu).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                resideMenu.openMenu(ResideMenu.DIRECTION_LEFT);
//            }
//        });
        openDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resideMenu.openMenu(ResideMenu.DIRECTION_RIGHT);
                mTracker.setScreenName(Constants.SideMenuScreen+"-"+ agency.getHomeMainDetails().get(0).getHomeMainTitle());
                mTracker.send(new HitBuilders.ScreenViewBuilder().build());
            }
        });
    }

    private ResideMenu.OnMenuListener menuListener = new ResideMenu.OnMenuListener() {
        @Override
        public void openMenu() {
            rlHeader.setVisibility(View.GONE);
//            Toast.makeText(getApplicationContext(), "Menu is opened!", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void closeMenu() {
            rlHeader.setVisibility(View.VISIBLE);
//            Toast.makeText(getApplicationContext(), "Menu is closed!", Toast.LENGTH_SHORT).show();
        }
    };

    public void onClick(View v) {
        if (v.getId() == R.id.btn_back) {
            try {
                this.finish();
            }catch (Exception e){
                Log.e("WYW",e.getLocalizedMessage());
            }
//            startActivity(new Intent(WhileYouWaitActivity.this, HomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        }
    }

    private class HelloWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
//            webView.loadData(summary, "text/html; charset=UTF-8", null);

            if (url != null && url.startsWith("http://")) {
                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory(Constants.kCatAgencyContact)
                        .setAction(agency.getHomeMainDetails().get(0).getHomeMainTitle() + " -Contact Block Footer- "+agency.getWebsite().get(0).getTitle())
                        .build());
                view.getContext().startActivity(
                        new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                return true;
            } else if (url != null && url.startsWith("tel:")) {
                mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory(Constants.kCatAgencyContact)
                    .setAction(agency.getHomeMainDetails().get(0).getHomeMainTitle() + " -Contact Block Footer- "+Constants.phone)
                    .build());
                view.getContext().startActivity(
                        new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                return true;
            } else if (url != null && url.startsWith("mailto:")) {
                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory(Constants.kCatAgencyContact)
                        .setAction(agency.getHomeMainDetails().get(0).getHomeMainTitle() + " -Contact Block Footer- "+agency.getAgencyEmail().get(0).getTitle())
                        .build());
                view.getContext().startActivity(
                        new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                return true;
            } else {
                return true;
            }

//            if (!loadingFinished) {
//                redirect = true;
//            }
//
//            loadingFinished = false;
//            view.loadUrl(url);
//            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            if (!redirect) {
                loadingFinished = true;
            }

            if (loadingFinished && !redirect) {
                pg.cancel();
            } else {
                redirect = false;
            }
            //what you want to do when the page finished loading, eg. give some message, show progress bar, etc
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            loadingFinished = false;
            pg.show();

            //what you want to do when the page starts loading, eg. give some message
        }

    }


}
