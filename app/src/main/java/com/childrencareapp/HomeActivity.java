package com.childrencareapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Transformation;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import Database.MyDbHelper;
import Model.Agency;
import Model.ChildModel;
import util.AppManager;
import util.Constants;
import util.MyApp;
import adapter.AgencyListAdapter;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class HomeActivity extends Activity {

    public static RelativeLayout rl;
    ProgressDialog pg;
    ListView listView;

    boolean flag;
    MyDbHelper dbHelper;
    Activity context;
    ScrollView scrollView;

    int DURATION = 100;

    Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_home);

        context = this;
        AppManager.getInstance().setFont(context);
        dbHelper = new MyDbHelper(this);

        // Obtain the shared Tracker instance.
        MyApp application = (MyApp) getApplication();
        mTracker = application.getDefaultTracker();

        rl = (RelativeLayout) findViewById(R.id.rl);
        listView = (ListView) findViewById(R.id.listView);
        scrollView = (ScrollView) findViewById(R.id.scrollView);
        TextView text = (TextView) findViewById(R.id.text);
        text.setTypeface(AppManager.getInstance().getRegularTypeface());
//        ArrayList<AgencyModel> list = new ArrayList<AgencyModel>();
//        AgencyListAdapter adapter = new AgencyListAdapter(getActivity(), R.layout.custom_list_home, list);
//        listView.setAdapter(adapter);

        if (AppManager.getInstance().getTextRes() != null && AppManager.getInstance().getTextRes().size()>0){
            text.setText(AppManager.getInstance().getTextRes().get(0).getHome());
        }

        Animation sunRise = AnimationUtils.loadAnimation(HomeActivity.this, R.anim.sun_rise_one);

        if (rl != null) {
            rl.startAnimation(sunRise);
        }

        flag = false;

        scrollView.fullScroll(ScrollView.FOCUS_UP);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, final int position, long id) {


                String title="";
                if (AppManager.getInstance().getAgency().get(position).getHomeMainDetails().size() > 0) {
                    title = AppManager.getInstance().getAgency().get(position).getHomeMainDetails().get(0).getHomeMainTitle();
                }
//                else {
//                    title = Constants.SelectAgency;
//                }

                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory(Constants.kActAgencyListing)
                        .setAction("Agency Listing - Agency selected: "+title)
                        .build());

//                Intent intent = new Intent(getParent(), CDPActivity.class);
//                AppManager.getInstance().setAgencyModel(AppManager.getInstance().getAgency().get(position)); //intent.putExtra("INDEX", position);
//                TabGroupActivity parentActivity = (TabGroupActivity) getParent();
//                parentActivity.startChildActivity("CDPActivity", intent);

                final RelativeLayout relAnim = (RelativeLayout) view.findViewById(R.id.relAnim);
                final RelativeLayout relAnimBack = (RelativeLayout) view.findViewById(R.id.relAnimBack);
                final RelativeLayout relSecond = (RelativeLayout) view.findViewById(R.id.relSecond);
                final RelativeLayout relSecondBack = (RelativeLayout) view.findViewById(R.id.relSecondBack);
                final LinearLayout linear = (LinearLayout) view.findViewById(R.id.linear);

                relAnim.setBackgroundColor(Color.parseColor(AppManager.getInstance().getAgency().get(position).getColorCode().get(0)));



                relAnim.clearAnimation();
                relSecond.clearAnimation();

                relAnim.getLayoutParams().width = 10;
                relAnim.getLayoutParams().height = 10;
                relSecond.getLayoutParams().width = 0;
                relSecond.getLayoutParams().height = 0;

                final Animation width = new Animation() {
                    @Override
                    protected void applyTransformation(float interpolatedTime, Transformation t) {
                        relAnim.getLayoutParams().width = interpolatedTime == 1
                                ? (int) (view.getWidth() * interpolatedTime) //ViewGroup.LayoutParams.WRAP_CONTENT
                                : (int) (view.getWidth() * interpolatedTime);
                        relAnim.requestLayout();
                    }

                    @Override
                    public boolean willChangeBounds() {
                        return true;
                    }
                };

                width.setDuration(DURATION);
                relAnim.startAnimation(width);

                Animation.AnimationListener widthLis = new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) relAnimBack.getLayoutParams();
                        params.setMargins(0, 0, 5, 0);
                        relAnimBack.setLayoutParams(params);
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {

                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) relAnimBack.getLayoutParams();
                        params.setMargins(0, 0, 0, 0);
                        relAnimBack.setLayoutParams(params);


//                        relAnimBack.setBackgroundColor(Color.parseColor(AppManager.getInstance().getAgency().get(position).getColorCode().get(0)));

                        Animation hieght = new Animation() {
                            @Override
                            protected void applyTransformation(float interpolatedTime, Transformation t) {
                                relAnim.getLayoutParams().height = interpolatedTime == 1
                                        ? (int) (relAnimBack.getHeight() * interpolatedTime) //ViewGroup.LayoutParams.WRAP_CONTENT
                                        : (int) (relAnimBack.getHeight() * interpolatedTime);
                                relAnim.requestLayout();
                            }

                            @Override
                            public boolean willChangeBounds() {
                                return true;
                            }

                        };

                        hieght.setDuration(DURATION);
                        relAnim.startAnimation(hieght);

                        Animation.AnimationListener hieghtLis = new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) relAnimBack.getLayoutParams();
                                params.setMargins(0, 0, 0, 0);
                                relAnimBack.setLayoutParams(params);


                                relSecond.setBackgroundColor(Color.parseColor(AppManager.getInstance().getAgency().get(position).getColorCode().get(0)));

                                relSecond.getLayoutParams().width = 10;
                                relSecond.getLayoutParams().height = 10;

                                final Animation widthSecond = new Animation() {
                                    @Override
                                    protected void applyTransformation(float interpolatedTime, Transformation t) {
                                        relSecond.getLayoutParams().width = interpolatedTime == 1
                                                ? (int) (view.getWidth() * interpolatedTime) //ViewGroup.LayoutParams.WRAP_CONTENT
                                                : (int) (view.getWidth() * interpolatedTime);
                                        relSecond.requestLayout();
                                    }

                                    @Override
                                    public boolean willChangeBounds() {
                                        return true;
                                    }
                                };

                                widthSecond.setDuration(DURATION);
                                relSecond.startAnimation(widthSecond);

                                Animation.AnimationListener widthSecondLis = new Animation.AnimationListener() {
                                    @Override
                                    public void onAnimationStart(Animation animation) {

                                    }

                                    @Override
                                    public void onAnimationEnd(Animation animation) {

                                        relAnim.setBackgroundColor(getResources().getColor(R.color.bg_white));
                                        relSecond.setBackgroundColor(getResources().getColor(R.color.bg_white));
                                        relAnim.setBackgroundColor(getResources().getColor(R.color.bg_white));

                                        relAnim.clearAnimation();
                                        relSecond.clearAnimation();

                                        Intent intent = new Intent(getParent(), CDPActivity.class);
                                        AppManager.getInstance().setAgencyModel(AppManager.getInstance().getAgency().get(position)); //intent.putExtra("INDEX", position);
                                        TabGroupActivity parentActivity = (TabGroupActivity) getParent();
                                        parentActivity.startChildActivity("CDPActivity", intent);

                                    }

                                    @Override
                                    public void onAnimationRepeat(Animation animation) {

                                    }
                                };

                                widthSecond.setAnimationListener(widthSecondLis);

                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        };

                        hieght.setAnimationListener(hieghtLis);

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                };

                width.setAnimationListener(widthLis);

//                ActivityTransitionLauncher.with(getActivity()).from(view.findViewById(R.id.img)).launch(intent);
            }
        });

        pg = new ProgressDialog(getParent(), ProgressDialog.THEME_HOLO_DARK);
        pg.setCancelable(false);
        pg.setTitle("Please Wait");
        pg.setMessage("Loading...");


        if (AppManager.getInstance().isFromServer() != null && AppManager.getInstance().isFromServer() && AppManager.getInstance().getAgency()!=null && AppManager.getInstance().getAgency().size() > 0) {

            List<Agency> list = AppManager.getInstance().getAgency();

            dbHelper.deleteAgency();

            for (int i = 0; i < list.size(); i++) {
                if (!dbHelper.checkAgencyAlreadyInDBorNot(list.get(i).getNid())) {
                    if (dbHelper.addAgency(list.get(i))) {
                        if (list.get(i).getImage().size() > 0 && list.get(i).getImage().get(0).getUri() != null) {
                            String[] arr = list.get(i).getImage().get(0).getUri().split("/");
                            String name = arr[arr.length - 1];
                            new ImageDownloadAndSave().execute(list.get(i).getImage().get(0).getUri(), list.get(i).getNid(), name);
                        }
                        if (list.get(i).getContactLogo().size() > 0 && list.get(i).getContactLogo().get(0).getUri() != null) {
                            String[] arr = list.get(i).getContactLogo().get(0).getUri().split("/");
                            String name = arr[arr.length - 1];
                            new LogoDownloadAndSave().execute(list.get(i).getContactLogo().get(0).getUri(), list.get(i).getNid(), name);
                        }
                    }
                } else {
                    if (dbHelper.updateAgency(list.get(i), list.get(i).getNid())) {
                        if (list.get(i).getImage().size() > 0 && list.get(i).getImage().get(0).getUri() != null) {
                            String[] arr = list.get(i).getImage().get(0).getUri().split("/");
                            String name = arr[arr.length - 1];
                            new ImageDownloadAndSave().execute(list.get(i).getImage().get(0).getUri(), list.get(i).getNid(), name);
                        }
                        if (list.get(i).getContactLogo().size() > 0 && list.get(i).getContactLogo().get(0).getUri() != null) {
                            String[] arr = list.get(i).getContactLogo().get(0).getUri().split("/");
                            String name = arr[arr.length - 1];
                            new LogoDownloadAndSave().execute(list.get(i).getContactLogo().get(0).getUri(), list.get(i).getNid(), name);
                        }
                    }
                }
            }

            updateUI(list.size());

            AgencyListAdapter adapter = new AgencyListAdapter(HomeActivity.this, R.layout.custom_list_home, list);
            listView.setAdapter(adapter);

        } else if (!AppManager.getInstance().isFromServer() && AppManager.getInstance().getAgency().size() > 0) {

            AppManager.getInstance().setIsFromServer(false);
            List<Agency> list = AppManager.getInstance().getAgency();

            updateUI(list.size());
            AgencyListAdapter adapter = new AgencyListAdapter(HomeActivity.this, R.layout.custom_list_home, list);
            listView.setAdapter(adapter);

        }

//        List<Agency> agency = dbHelper.getAgenciesData();
//        AppManager.getInstance().setAgency(agency);
////        AppManager.getInstance().setIsFromServer(false);
//
//        updateUI(agency.size());

        final ArrayList<ChildModel> childList = dbHelper.getData();
        AppManager.getInstance().setChildModels(childList);

    }

    private void agencyAPI() {

        Handler mainHandler = new Handler(HomeActivity.this.getMainLooper());
        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {

                pg.show();
                AppManager.getInstance().restAPI.getAgency(new Callback<List<Agency>>() {
                    @Override
                    public void success(List<Agency> agency, Response response) {

                        pg.cancel();

                        AppManager.getInstance().setAgency(agency);
                        AppManager.getInstance().setIsFromServer(true);

                        flag = true;

                        List<Agency> list = AppManager.getInstance().getAgency();

                        dbHelper.deleteAgency();

                        for (int i = 0; i < list.size(); i++) {
                            if (!dbHelper.checkAgencyAlreadyInDBorNot(list.get(i).getNid())) {
                                if (dbHelper.addAgency(list.get(i))) {
                                    if (list.get(i).getImage().size() > 0 && list.get(i).getImage().get(0).getUri() != null) {
                                        String[] arr = list.get(i).getImage().get(0).getUri().split("/");
                                        String name = arr[arr.length - 1];
                                        new ImageDownloadAndSave().execute(list.get(i).getImage().get(0).getUri(), list.get(i).getNid(), name);
                                    }
                                    if (list.get(i).getContactLogo().size() > 0 && list.get(i).getContactLogo().get(0).getUri() != null) {
                                        String[] arr = list.get(i).getContactLogo().get(0).getUri().split("/");
                                        String name = arr[arr.length - 1];
                                        new LogoDownloadAndSave().execute(list.get(i).getContactLogo().get(0).getUri(), list.get(i).getNid(), name);
                                    }
                                }
                            } else {
                                if (dbHelper.updateAgency(list.get(i), list.get(i).getNid())) {
                                    if (list.get(i).getImage().size() > 0 && list.get(i).getImage().get(0).getUri() != null) {
                                        String[] arr = list.get(i).getImage().get(0).getUri().split("/");
                                        String name = arr[arr.length - 1];
                                        new ImageDownloadAndSave().execute(list.get(i).getImage().get(0).getUri(), list.get(i).getNid(), name);
                                    }
                                    if (list.get(i).getContactLogo().size() > 0 && list.get(i).getContactLogo().get(0).getUri() != null) {
                                        String[] arr = list.get(i).getContactLogo().get(0).getUri().split("/");
                                        String name = arr[arr.length - 1];
                                        new LogoDownloadAndSave().execute(list.get(i).getContactLogo().get(0).getUri(), list.get(i).getNid(), name);
                                    }
                                }
                            }
                        }


                        updateUI(list.size());

                        AgencyListAdapter adapter = new AgencyListAdapter(HomeActivity.this, R.layout.custom_list_home, list);
                        listView.setAdapter(adapter);

                    }

                    @Override
                    public void failure(RetrofitError error) {
                        pg.cancel();

                        List<Agency> agency = dbHelper.getAgenciesData();
                        AppManager.getInstance().setAgency(agency);


                        AppManager.getInstance().setIsFromServer(false);
                        List<Agency> list = AppManager.getInstance().getAgency();
                        updateUI(list.size());
                        AgencyListAdapter adapter = new AgencyListAdapter(HomeActivity.this, R.layout.custom_list_home, list);
                        listView.setAdapter(adapter);

                    }
                });

            }
        };
        mainHandler.post(myRunnable);

    }

    private void updateUI(int count) {
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.custom_list_home, null, false);

        int h = ((LinearLayout) view.findViewById(R.id.linear)).getMinimumHeight();
        h = h + (int)dipToPixels(30);
        int hieght = h * count;

        ViewGroup.LayoutParams prams = listView.getLayoutParams();
        prams.height = hieght;
        listView.setLayoutParams(prams);

        scrollView.fullScroll(ScrollView.FOCUS_UP);

    }

    public float dipToPixels(float dipValue) {
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        float value = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue, metrics);
        return value;
    }

    private class ImageDownloadAndSave extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... arg0) {
            try {
                URL url = new URL(arg0[0]);
                String nodeID = arg0[1];
                String imgName = arg0[2];
                URLConnection ucon = url.openConnection();
                InputStream inputStream = null;
                HttpURLConnection httpConn = (HttpURLConnection) ucon;
                httpConn.setRequestMethod("GET");
                httpConn.connect();

                if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    inputStream = httpConn.getInputStream();
                }

                if (inputStream != null) {
                    byte[] byteImage = convertToByte(inputStream);


                    Bitmap bitmap = BitmapFactory.decodeByteArray(byteImage, 0, byteImage.length);
                    String path = saveImageToExternalStorage(byteImage, imgName);
                    if (!path.equalsIgnoreCase("")) {
                        dbHelper.saveAgencyImage(path, nodeID);
                    }

                }

            } catch (Exception io) {
                io.printStackTrace();
            }

            return null;
        }
    }

    @Override
    protected void onResume() {

        mTracker.setScreenName(Constants.HomeActivity);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        List<Agency> list = AppManager.getInstance().getAgency();

//        updateUI(list.size());
        AgencyListAdapter adapter = new AgencyListAdapter(HomeActivity.this, R.layout.custom_list_home, list);
        listView.setAdapter(adapter);

        super.onResume();
    }

    private class LogoDownloadAndSave extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... arg0) {
            try {
                URL url = new URL(arg0[0]);
                String nodeID = arg0[1];
                String imgName = arg0[2];
                URLConnection ucon = url.openConnection();
                InputStream inputStream = null;
                HttpURLConnection httpConn = (HttpURLConnection) ucon;
                httpConn.setRequestMethod("GET");
                httpConn.connect();

                if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    inputStream = httpConn.getInputStream();
                }

                if (inputStream != null) {
                    byte[] byteImage = convertToByte(inputStream);

                    dbHelper.saveAgencyLogo(byteImage, nodeID);

                }

            } catch (Exception io) {
                io.printStackTrace();
            }

            return null;
        }
    }

    private byte[] convertToByte(InputStream input) throws IOException {
        byte[] buffer = new byte[8192];
        int bytesRead;
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }

        return output.toByteArray();
    }

    public String saveImageToExternalStorage(byte[] image, String name) {
        String fullPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/InTheLoop/" + "thumbnail";

        try {
            File dir = new File(fullPath);
            if (!dir.exists()) {
                dir.mkdirs();
            }

            OutputStream fOut = null;
            File file = new File(fullPath, name);

            if (!file.exists()) {
                file.createNewFile();
            }
            fOut = new FileOutputStream(file);

// 100 means no compression, the lower you go, the stronger the compression
//            image.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.write(image);
            fOut.flush();
            fOut.close();

//            MediaStore.Images.Media.insertImage(context.getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());

            return file.getAbsolutePath();

        } catch (Exception e) {
            Log.e("saveToExternalStorage()", e.getMessage());
            return "";
        }
    }

}
