package com.childrencareapp;

import android.app.ActionBar;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

import Database.MyDbHelper;


public class MainActivity extends FragmentActivity {

    private RelativeLayout open_drawer;
    public TabHost mTabHost;
    MyDbHelper dbHelper;

    private int prevInd = 0;


    int t;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_ACTION_BAR);
        setContentView(R.layout.activity_main);

        mTabHost = (TabHost) findViewById(android.R.id.tabhost);
        dbHelper = new MyDbHelper(this);

        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.tab1, new HomeFragment(), "Home");
        fragmentTransaction.add(R.id.tab2, new AccountSetupFragment(), "AccountSetup");
        fragmentTransaction.add(R.id.tab3, new MyTeamFragment(), "MyTeam");
        fragmentTransaction.add(R.id.tab4, new ContactFragment(), "Contact");
        fragmentTransaction.add(R.id.tab5, new ContactFragment(), "Contact");

//        android.support.v4.app.Fragment fragment = fragmentManager.findFragmentById(R.id.fragment);
//        fragmentTransaction.hide(fragment);


        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragmentTransaction.commit();
        mTabHost.setup();
        mTabHost.getTabWidget().setStripEnabled(false);
        TabSpec specs = mTabHost.newTabSpec("tab1");
        specs.setContent(R.id.tab1);
        specs.setIndicator("");
        mTabHost.addTab(specs);

        specs = mTabHost.newTabSpec("tab2");
        specs.setContent(R.id.tab2);
        specs.setIndicator("");
        mTabHost.addTab(specs);

        specs = mTabHost.newTabSpec("tab3");
        specs.setContent(R.id.tab3);
        specs.setIndicator("");
        mTabHost.addTab(specs);


        specs = mTabHost.newTabSpec("tab4");
        specs.setContent(R.id.tab4);
        specs.setIndicator("");
        mTabHost.addTab(specs);

        specs = mTabHost.newTabSpec("tab5");
        specs.setContent(R.id.tab5);
        specs.setIndicator("");
        mTabHost.addTab(specs);

        mTabHost.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.tab_selector_home);
        mTabHost.getTabWidget().getChildAt(1).setBackgroundResource(R.drawable.tab_selector_setting);
        mTabHost.getTabWidget().getChildAt(2).setBackgroundResource(R.drawable.tab_selector_team);
        mTabHost.getTabWidget().getChildAt(3).setBackgroundResource(R.drawable.tab_selector_tools);
        mTabHost.getTabWidget().getChildAt(4).setBackgroundResource(R.drawable.tab_selector_contact);
        mTabHost.getTabWidget().setDividerDrawable(null);

        for (t = 0; t < 5; t++) {
            mTabHost.getTabWidget().getChildAt(t).setOnTouchListener(new View.OnTouchListener() {
                int ind = t;

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    // TODO Auto-generated method stub
                    if (event.getAction() == MotionEvent.ACTION_UP) {

                        if (ind == 0) {

                            prevInd = 0;
                            //get the sunrise animation
                            Animation sunRise = AnimationUtils.loadAnimation(MainActivity.this, R.anim.sun_rise_one);
//                            HomeFragment.rl.setVisibility(View.GONE);
                            HomeFragment.rl.startAnimation(sunRise);

                            android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            android.support.v4.app.Fragment fragment = fragmentManager.findFragmentByTag("Home");

                            if (!fragment.isVisible()) {
                                android.support.v4.app.Fragment currentFragment = fragmentManager.findFragmentById(R.id.tab1);
                                fragmentTransaction.hide(currentFragment);
                                fragmentTransaction.show(fragment);
                                fragmentTransaction.commit();
                            }
                        }
                        if (ind == 1) {

                            if (dbHelper.getData().size() > 0) {

                                Intent intent = new Intent(MainActivity.this,ChildViewActivity.class);
                                startActivity(intent);

                            } else {
                                //get the sunrise animation
                                Animation sunRise = AnimationUtils.loadAnimation(MainActivity.this, R.anim.sun_rise_two);
//                            AccountSetupFragment.rl.setVisibility(View.GONE);
                                AccountSetupFragment.rl.startAnimation(sunRise);

                                android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                android.support.v4.app.Fragment fragment = fragmentManager.findFragmentByTag("AccountSetup");//Calender
                                if (!fragment.isVisible()) {
                                    android.support.v4.app.Fragment currentFragment = fragmentManager.findFragmentById(R.id.tab2);
                                    fragmentTransaction.hide(currentFragment);
                                    fragmentTransaction.show(fragment);
                                    fragmentTransaction.commit();
                                }
                            }
                        }
                        if (ind == 2) {

                            //get the sunrise animation
                            Animation sunRise = AnimationUtils.loadAnimation(MainActivity.this, R.anim.sun_rise);
//                            MyTeamFragment.rl.setVisibility(View.GONE);
                            MyTeamFragment.rl.startAnimation(sunRise);

                            android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            android.support.v4.app.Fragment fragment = fragmentManager.findFragmentByTag("MyTeam");//Notes
                            if (!fragment.isVisible()) {
                                android.support.v4.app.Fragment currentFragment = fragmentManager.findFragmentById(R.id.tab3);
                                fragmentTransaction.hide(currentFragment);
                                fragmentTransaction.show(fragment);
                                fragmentTransaction.commit();
                            }
                        }
                        if (ind == 3) {

                            //get the sunrise animation
//                            Animation sunRise = AnimationUtils.loadAnimation(MainActivity.this, R.anim.slide_in_right);
//                            MyTeamFragment.rl.startAnimation(sunRise);

//                            android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
//                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                            android.support.v4.app.Fragment fragment = fragmentManager.findFragmentById(R.id.fragment);
//                            fragmentTransaction.setCustomAnimations(R.anim.slide_up, R.anim.slide_up);
//                            fragmentTransaction.show(fragment);
//                            fragmentTransaction.commit();


//                            android.support.v4.app.com.childrencareapp.Fragment fragment = fragmentManager.findFragmentByTag("Tool");//MyTeam
//
//                            if (!fragment.isVisible()) {
//                                android.support.v4.app.com.childrencareapp.Fragment currentFragment = fragmentManager.findFragmentById(R.id.tab4);
//                                fragmentTransaction.hide(currentFragment);
//                                fragmentTransaction.show(fragment);
//                                fragmentTransaction.commit();
//                            }
                        }
                        if (ind == 4) {

                            //get the sunrise animation
                            Animation sunRise = AnimationUtils.loadAnimation(MainActivity.this, R.anim.sun_rise_five);
//                            ContactFragment.rl.setVisibility(View.GONE);
                            ContactFragment.rl.startAnimation(sunRise);

                            android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            android.support.v4.app.Fragment fragment = fragmentManager.findFragmentByTag("Contact");//
                            if (!fragment.isVisible()) {
                                android.support.v4.app.Fragment currentFragment = fragmentManager.findFragmentById(R.id.tab5);
                                fragmentTransaction.hide(currentFragment);
                                fragmentTransaction.show(fragment);
                                fragmentTransaction.commit();
                            }
                        }
                    }
                    return false;
                }
            });
        }


        //Customize ActionBar
        final ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
                R.layout.customize_actionbar,
                null);
        ActionBar actionBar = getActionBar();
        actionBar.setCustomView(actionBarLayout);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayUseLogoEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowHomeEnabled(false);

    }

    @Override
    protected void onResume() {

        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
