package com.childrencareapp;

import android.content.Intent;
import android.os.Bundle;

import Database.MyDbHelper;
import util.AppManager;

public class TabGroup2Activity extends TabGroupActivity {

    MyDbHelper dbHelper;

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dbHelper = new MyDbHelper(getParent());

        if(dbHelper.getData().size()>0) {
            AppManager.getInstance().setIsParent(true);
            startChildActivity("ChildViewActivity", new Intent(this, ChildViewActivity.class));
        }
        else
        {
            AppManager.getInstance().setIsParent(true);
            startChildActivity("AccountSettingActivity", new Intent(this, AccountSettingActivity.class));
        }
    }
}
