package com.childrencareapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.special.ResideMenu.ResideMenu;
import com.special.ResideMenu.ResideMenuItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import Database.MyDbHelper;
import Model.Agency;
import Model.Contact;
import Model.CtnStatusModel;
import Model.HistoryModel;
import Model.ParentData;
import util.AppManager;
import util.Constants;
import util.MyApp;
import adapter.WWAANewCdpHistoryAdapter;

public class WwaaCdpHistoryActivity extends Activity {

    MyDbHelper dbHelper;
    int childId;
    Spinner spn;
    ArrayList<ParentData> datas;
    Agency agency;
    RelativeLayout rlHeader;

    ImageView imgPhone,imgWeb,imgConnect;

    ResideMenu resideMenu;
    private ResideMenuItem itemWhile;
    private ResideMenuItem itemWhere;
    private ResideMenuItem itemSocial;
    private ResideMenuItem itemFB=null;
    private ResideMenuItem itemTW=null;
    private ResideMenuItem itemPin=null;
    private ResideMenuItem itemYT=null;
    String fbLink = "",twLink ="",pinLink = "",ytLink ="";
    ImageButton openDrawer;

    Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_wwaa_cdp_history);

        AppManager.getInstance().setFont(this);

        TextView heading = (TextView) findViewById(R.id.heading);
        ImageView imgAgency = (ImageView) findViewById(R.id.img_agency);
        TextView tvChildName = (TextView) findViewById(R.id.tv_child_name);
        final ListView listView = (ListView) findViewById(R.id.listView);
        TextView history = (TextView) findViewById(R.id.history);
        TextView contact = (TextView) findViewById(R.id.contact);
        TextView tvPhone = (TextView) findViewById(R.id.tv_phone);
        TextView tvConnect = (TextView) findViewById(R.id.tv_connect);
        TextView tvWebsite = (TextView) findViewById(R.id.tv_website);
        spn = (Spinner) findViewById(R.id.spn_program);
        spn.setDropDownVerticalOffset(1);
        RelativeLayout relBar = (RelativeLayout)findViewById(R.id.rel_bar);
        RelativeLayout relBar2 = (RelativeLayout)findViewById(R.id.rel_bar2);
        final ScrollView scrollView = (ScrollView) findViewById(R.id.scrollView);
        scrollView.fullScroll(ScrollView.FOCUS_UP);
        ImageView imgChild = (ImageView)findViewById(R.id.imgChild);
        openDrawer = (ImageButton) findViewById(R.id.btn_drawer);
        rlHeader = (RelativeLayout) findViewById(R.id.rlHeader);
        imgPhone = (ImageView)findViewById(R.id.imgPhone);
        imgWeb = (ImageView)findViewById(R.id.imgWeb);
        imgConnect = (ImageView)findViewById(R.id.imgConnect);

        heading.setTypeface(AppManager.getInstance().getBlackTypeface());
        contact.setTypeface(AppManager.getInstance().getBlackTypeface());
        history.setTypeface(AppManager.getInstance().getBlackTypeface());
        tvPhone.setTypeface(AppManager.getInstance().getRegularTypeface());
        tvWebsite.setTypeface(AppManager.getInstance().getRegularTypeface());
        tvConnect.setTypeface(AppManager.getInstance().getRegularTypeface());
        tvChildName.setTypeface(AppManager.getInstance().getRegularTypeface());

        dbHelper = new MyDbHelper(this);


        // Obtain the shared Tracker instance.
        MyApp application = (MyApp) getApplication();
        mTracker = application.getDefaultTracker();


//        Bundle bundle = getIntent().getExtras();
//        if(bundle != null)
//        {
//            index = bundle.getInt("INDEX");
//        }

        ArrayList<String> spnList = new ArrayList<>();
        for (int i = 0; i < AppManager.getInstance().getAgencyModel().getWwaa().size(); i++) {
            spnList.add(AppManager.getInstance().getAgencyModel().getWwaa().get(i).getWWAATitle().getMarkup());
        }

        spnList.add("Select a Program");

//        ArrayAdapter<String> spnAdapter = new ArrayAdapter<String>(this, R.layout.custom_spnr_his, R.id.textView39, spnList);
        // set whatever dropdown resource you want
//        spnAdapter.setDropDownViewResource(R.layout.custom_spnr_his);
        ArrAdapter adapter = new ArrAdapter(this,R.layout.custom_spnr_his,spnList);
        spn.setAdapter(adapter);
        spn.setSelection(adapter.getCount());

        agency = AppManager.getInstance().getAgencyModel();

        setUpMenu();

        rlHeader.setVisibility(View.VISIBLE);
        rlHeader.setBackgroundColor(getResources().getColor(R.color.red_text));

        if (agency.getWwaaMainDetails().size()>0) {
            heading.setText(agency.getWwaaMainDetails().get(0).getWwaaMainTitle());
        }
        if (agency.getAgencyPhone().size()>0  && (agency.getAgencyPhone().get(0).getNumberAlias() != null && !agency.getAgencyPhone().get(0).getNumberAlias().equalsIgnoreCase(""))) {
            tvPhone.setText(agency.getAgencyPhone().get(0).getNumberAlias());
            imgPhone.setVisibility(View.VISIBLE);
        }
        else if (agency.getAgencyPhone().size()>0  && (agency.getAgencyPhone().get(0).getNumber() != null && !agency.getAgencyPhone().get(0).getNumber().equalsIgnoreCase(""))) {
            tvPhone.setText(agency.getAgencyPhone().get(0).getNumber());
            imgPhone.setVisibility(View.VISIBLE);
        }
        else {
            imgPhone.setVisibility(View.INVISIBLE);
        }
        if (agency.getWebsite().size()>0 && !agency.getWebsite().get(0).getTitle().equalsIgnoreCase("")) {
            tvWebsite.setText(agency.getWebsite().get(0).getTitle());
            imgWeb.setVisibility(View.VISIBLE);
        }
        else {
            imgWeb.setVisibility(View.INVISIBLE);
        }
        if (agency.getAgencyEmail().size()>0 && !agency.getAgencyEmail().get(0).getTitle().equalsIgnoreCase("")) {
            tvConnect.setText(agency.getAgencyEmail().get(0).getTitle());
            imgConnect.setBackgroundResource(R.drawable.email);
            imgConnect.setVisibility(View.VISIBLE);
        }
        else if (agency.getCdpConnect().size()>0 && !agency.getCdpConnect().get(0).getTitle().equalsIgnoreCase("")) {
            tvConnect.setText(agency.getCdpConnect().get(0).getTitle());
            imgConnect.setBackgroundResource(R.drawable.events);
            imgConnect.setVisibility(View.VISIBLE);
        }else {
            imgConnect.setVisibility(View.INVISIBLE);
        }
//        if (agency.getContactLogo().size()>0) {
//            Picasso.with(this).load(Constants.IMG_URL + agency.getContactLogo().get(0).getFilename()).into(imgAgency);
//        }
        if (AppManager.getInstance().isFromServer()) {
            if (agency.getContactLogo().size()>0) {
                Picasso.with(this).load(agency.getContactLogo().get(0).getUri()).into(imgAgency);
            }
        }
        else{
            if (agency.getAgencyLogo() != null) {
                imgAgency.setImageBitmap(BitmapFactory.decodeByteArray(agency.getAgencyLogo(), 0,
                        agency.getAgencyLogo().length));
            }
        }
        if (agency.getColorCode().size()>0) {
            relBar.setBackgroundColor(Color.parseColor(agency.getColorCode().get(0)));
            relBar2.setBackgroundColor(Color.parseColor(agency.getColorCode().get(0)));
        }


        final ArrayList<CtnStatusModel> list = null;
        SharedPreferences pref = getSharedPreferences(Constants.WWAACHILD_PREF, Activity.MODE_PRIVATE);
        if (pref.contains(Constants.CHILD_POS)) {
            int pos = pref.getInt(Constants.CHILD_POS, -1);
            tvChildName.setText(AppManager.getInstance().getChildModels().get(pos).getName());
            childId = AppManager.getInstance().getChildModels().get(pos).getId();
            tvChildName.setText(AppManager.getInstance().getChildModels().get(pos).getName());
            if (AppManager.getInstance().getChildModels().get(pos).getGender() == 0){
                imgChild.setBackgroundResource(R.drawable.boy_in_circle);
            }
            else if (AppManager.getInstance().getChildModels().get(pos).getGender() == 1){
                imgChild.setBackgroundResource(R.drawable.where_are_banner_girl_user_placeholder);
            }
        }

//        final ArrayList<GetCdpStatausModel> historyList = dbHelper.getCdpStatus(childId,agency.getNid());


//        final ArrayList<ParentData> hisList = new ArrayList<>();

        spn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                hisList.clear();
//                for (int i = 0; i < historyList.size(); i++) {
//                    GsonBuilder gsonb = new GsonBuilder();
//                    Gson gson = gsonb.create();
//                    Type type = new TypeToken<ArrayList<ParentData>>() {
//                    }.getType();
//                    datas = gson.fromJson(historyList.get(i).getList(), type);
//
//
//                    for (int j = 0; j < datas.size(); j++) {
//
//                        if (position == datas.get(j).getParentId()) {
//                            ParentData data = datas.get(j);
//                            hisList.add(data);
//                        }
//                    }
//                }

                ArrayList<HistoryModel> hisList = dbHelper.getCdpHistory(childId,agency.getNid(),position); ///NEW CODE

                int a = hisList.size();


                LayoutInflater inflater = (LayoutInflater) WwaaCdpHistoryActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view2 = inflater.inflate(R.layout.custom_ctn_history, null, false);
                int h = ((LinearLayout) view2.findViewById(R.id.linear)).getMinimumHeight();

                ViewGroup.LayoutParams prams = listView.getLayoutParams();
                prams.height = (h * hisList.size())+GetPixelFromDips(5);
                listView.setLayoutParams(prams);

                WWAANewCdpHistoryAdapter adapter = new WWAANewCdpHistoryAdapter(WwaaCdpHistoryActivity.this, R.layout.custom_ctn_history, hisList);
                listView.setAdapter(adapter);

                scrollView.fullScroll(ScrollView.FOCUS_UP);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });




    }

    @Override
    protected void onResume() {
        super.onResume();

        mTracker.setScreenName(Constants.WWAAHistoryActivity + "-" + agency.getHomeMainDetails().get(0).getHomeMainTitle());
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

    }

    public void onClick(View v) {
        if(v.getId()==R.id.btn_back)
        {
            try {
                finish();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        else if(v.getId() == R.id.tv_website){

            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory(Constants.kCatAgencyContact)
                    .setAction(agency.getHomeMainDetails().get(0).getHomeMainTitle() + " -Contact Block Footer- "+agency.getWebsite().get(0).getTitle())
                    .build());

            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(agency.getWebsite().get(0).getUrl())));
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        else if(v.getId() == R.id.tv_phone){

            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory(Constants.kCatAgencyContact)
                    .setAction(agency.getHomeMainDetails().get(0).getHomeMainTitle() + " -Contact Block Footer- "+Constants.phone)
                    .build());

            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("tel:" + agency.getAgencyPhone().get(0).getNumber())));
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        else if(v.getId() == R.id.tv_connect){


            try {
                if (agency.getAgencyEmail().size() > 0) {
                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory(Constants.kCatAgencyContact)
                            .setAction(agency.getHomeMainDetails().get(0).getHomeMainTitle() + " -Contact Block Footer- "+agency.getAgencyEmail().get(0).getTitle())
                            .build());
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + agency.getAgencyEmail().get(0).getUrl())));
                } else if (agency.getCdpConnect().size() > 0) {
                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory(Constants.kCatAgencyContact)
                            .setAction(agency.getHomeMainDetails().get(0).getHomeMainTitle() + " -Contact Block Footer- "+agency.getCdpConnect().get(0).getTitle())
                            .build());
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(agency.getCdpConnect().get(0).getUrl())));
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        else if(v.getId()==R.id.tv_child_name)
        {

//            mTracker.send(new HitBuilders.EventBuilder()
//                    .setCategory(Constants.kCatStatusHistory)
//                    .setAction(Constants.SelectChild)
//                    .build());

            try {
                finish();
                Intent intent = new Intent(getParent(), MyTeamActivity.class);
                intent.putExtra("WWAA", true);
                TabGroupActivity parentActivity = (TabGroupActivity) getParent();
                parentActivity.startChildActivity("MyTeamActivity", intent);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public int GetPixelFromDips(float pixels) {
        // Get the screen's density scale
        final float scale = getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f);
    }

    class ArrAdapter extends ArrayAdapter<String> {
        Context ctx;
        List<String> stringList;

        public ArrAdapter(Context context, int resource, List<String> objects) {
            super(context, resource, objects);

            this.ctx = context;
            this.stringList = objects;

        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (view == null)
            {
                view = inflater.inflate(R.layout.custom_spnr_his,parent,false);
            }

            TextView tvText = (TextView) view.findViewById(R.id.text);

            tvText.setTypeface(AppManager.getInstance().getRegularTypeface());

            tvText.setText(stringList.get(position));
            tvText.setWidth(spn.getWidth());
            view.setBackgroundResource(R.drawable.spn_his_border);
//            view.getLayoutParams().width = 200;
            return  view;
        }

        @Override
        public int getCount() {
            //return stringList.size();
            int count = super.getCount();
            return count > 0 ? count - 1 : count;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (view == null)
            {
                view = inflater.inflate(R.layout.custom_spnr_his,parent,false);
            }

            TextView tvText = (TextView) view.findViewById(R.id.text);
            tvText.setTypeface(AppManager.getInstance().getRegularTypeface());

            tvText.setText(stringList.get(position));
//            view.getLayoutParams().width = 200;
            return  view;


        }
    }

    private void setUpMenu() {

        Contact contct = null;

        // attach to current activity;
        resideMenu = new ResideMenu(this);
        resideMenu.setUse3D(false);
        resideMenu.setBackground(R.drawable.menu_bar_background);
        resideMenu.attachToActivity(this);
        resideMenu.setShadowVisible(false);
        resideMenu.setMenuListener(menuListener);
        //valid scale factor is between 0.0f and 1.0f. leftmenu'width is 150dip.
        resideMenu.setScaleValue(0.6f);

        for (int i = 0; i < AppManager.getInstance().getContacts().size(); i++) {
            if (agency.getNid().equals(AppManager.getInstance().getContacts().get(i).getAgency())) {
                contct = AppManager.getInstance().getContacts().get(i);
                break;
            }
        }

        // create menu items;
        if (agency.getWwaaMainDetails().size()>0) {
            itemWhere = new ResideMenuItem(this, R.drawable.where_we_are_at, agency.getWwaaMainDetails().get(0).getWwaaMainTitle());
        }
        if (agency.getWywMainDetails().size() > 0) {
            itemWhile = new ResideMenuItem(this, R.drawable.while_you_wait, agency.getWywMainDetails().get(0).getWywMainTitle());
        }
//        itemSocial  = new ResideMenuItem(this, R.drawable.fb,R.drawable.twitter);
        if (contct != null) {
            try {

                for(int i=0;i<contct.getSocialLinks().size();i++) {
                    if (contct.getSocialLinks().get(i).getTitle().equalsIgnoreCase("Facebook")) {
                        fbLink = contct.getSocialLinks().get(i).getUrl();
                        itemFB = new ResideMenuItem(this, R.drawable.fb, "Facebook");
                    }
                }
                for(int i=0;i<contct.getSocialLinks().size();i++) {
                    if (contct.getSocialLinks().get(i).getTitle().equalsIgnoreCase("Twitter")){
                        twLink = contct.getSocialLinks().get(i).getUrl();
                        itemTW = new ResideMenuItem(this, R.drawable.twitter, "Twitter");
                    }

                }
                for(int i=0;i<contct.getSocialLinks().size();i++) {
                    if (contct.getSocialLinks().get(i).getTitle().equalsIgnoreCase("Pinterest")) {
                        pinLink = contct.getSocialLinks().get(i).getUrl();
                        itemPin = new ResideMenuItem(this, R.drawable.menu_bar_pin, "Pinterest");
                    }
                }
                for(int i=0;i<contct.getSocialLinks().size();i++) {
                    if (contct.getSocialLinks().get(i).getTitle().equalsIgnoreCase("YouTube")){
                        ytLink = contct.getSocialLinks().get(i).getUrl();
                        itemYT = new ResideMenuItem(this, R.drawable.menu_bar_yotube, "YouTube");
                    }

                }
//                itemSocial = new ResideMenuItem(this, R.drawable.fb, R.drawable.twitter,R.drawable.menu_bar_pin,R.drawable.menu_bar_yotube, twLink, fbLink,pinLink,ytLink);
            } catch (Exception e) {
                Log.e("CDP MENU", e.getLocalizedMessage());
            }
        } else {
//            itemSocial = new ResideMenuItem(this, R.drawable.fb, R.drawable.twitter);
        }

        itemWhere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory(Constants.kCatSideMenu)
                        .setAction(Constants.MyServicesMenuWWAA+"-"+agency.getHomeMainDetails().get(0).getHomeMainTitle())
                        .build());

                if (AppManager.getInstance().getChildModels().size() > 0) {
                    MyDbHelper dbHelper = new MyDbHelper(WwaaCdpHistoryActivity.this);
                    SharedPreferences pref = getSharedPreferences(Constants.WWAACHILD_PREF, Activity.MODE_PRIVATE);
                    int pos = -1;
                    if (pref.contains(Constants.CHILD_POS)) {
                        pos = pref.getInt(Constants.CHILD_POS, -1);
                    }



                    if (dbHelper.getCdpStatus(AppManager.getInstance().getChildModels().get(pos).getId(), agency.getNid()).size() > 0) {
                        finish();
                        Intent intent = new Intent(getParent(), WwaaCdpActivity.class);
                        TabGroupActivity parentActivity = (TabGroupActivity) getParent();
                        parentActivity.startChildActivity("WwaaCdpActivity", intent);
//                    Intent intent = new Intent(CDPActivity.this, WhileYouWaitActivity.class);
//                    startActivity(intent);
                        resideMenu.closeMenu();
                    } else {
                        AppManager.getInstance().setParentModels(new ArrayList<ParentData>());
                        try {
                            finish();
                            Intent intent = new Intent(getParent(), WwaaCdpStatusActivity.class);
                            TabGroupActivity parentActivity = (TabGroupActivity) getParent();
                            parentActivity.startChildActivity("WwaaCdpStatusActivity", intent);
//                    Intent intent = new Intent(CDPActivity.this, WhileYouWaitActivity.class);
//                    startActivity(intent);
                            resideMenu.closeMenu();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
//                    }
                } else {
//                    Toast.makeText(getApplicationContext(), "Add Child First!", Toast.LENGTH_SHORT).show();
                    if (TabActivity.tabHost != null){
                        TabActivity.tabHost.setCurrentTab(1);
                    }
                }
                resideMenu.closeMenu();
            }
        });
        itemWhile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory(Constants.kCatSideMenu)
                        .setAction(Constants.WYWMenuWWAA+"-"+agency.getHomeMainDetails().get(0).getHomeMainTitle())
                        .build());

                if (AppManager.getInstance().getChildModels().size() > 0) {

                    try {
                        finish();
                        Intent intent = new Intent(getParent(), WhileYouWaitActivity.class);
                        TabGroupActivity parentActivity = (TabGroupActivity) getParent();
                        parentActivity.startChildActivity("WhileYouWaitActivity", intent);
//                    Intent intent = new Intent(CDPActivity.this, WhileYouWaitActivity.class);
//                    startActivity(intent);
                        resideMenu.closeMenu();
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                } else {
//                    Toast.makeText(getApplicationContext(), "Add Child First!", Toast.LENGTH_SHORT).show();
                    if (TabActivity.tabHost != null){
                        TabActivity.tabHost.setCurrentTab(1);
                    }
                    resideMenu.closeMenu();
                }

            }
        });
        if (itemFB != null) {
            itemFB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!fbLink.equalsIgnoreCase("")) {

                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory(Constants.kCatSideMenu)
                                .setAction(agency.getHomeMainDetails().get(0).getHomeMainTitle() + " - Facebook")
                                .build());

                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(fbLink)));
                    }
                }
            });
        }
        if (itemTW != null) {
            itemTW.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!twLink.equalsIgnoreCase("")) {

                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory(Constants.kCatSideMenu)
                                .setAction(agency.getHomeMainDetails().get(0).getHomeMainTitle() + " - Twitter")
                                .build());
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(twLink)));
                    }
                }
            });
        }
        if (itemPin != null) {
            itemPin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!pinLink.equalsIgnoreCase("")) {
                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory(Constants.kCatSideMenu)
                                .setAction(agency.getHomeMainDetails().get(0).getHomeMainTitle() + " - Pinterest")
                                .build());
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(pinLink)));
                    }
                }
            });
        }
        if (itemYT != null) {
            itemYT.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!ytLink.equalsIgnoreCase("")) {
                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory(Constants.kCatSideMenu)
                                .setAction(agency.getHomeMainDetails().get(0).getHomeMainTitle() + " -  YouTube")
                                .build());
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(ytLink)));
                    }
                }
            });
        }
//        itemCalendar.setOnClickListener(this);
//        itemSettings.setOnClickListener(this);

//        resideMenu.addMenuItem(itemWhere, ResideMenu.DIRECTION_LEFT);
//        resideMenu.addMenuItem(itemWhile, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(itemWhere, ResideMenu.DIRECTION_RIGHT);
        resideMenu.addMenuItem(itemWhile, ResideMenu.DIRECTION_RIGHT);
//        resideMenu.addMenuItem(itemSocial, ResideMenu.DIRECTION_RIGHT);
        if (itemFB != null){
            resideMenu.addMenuItem(itemFB, ResideMenu.DIRECTION_RIGHT);
        }
        if (itemTW != null){
            resideMenu.addMenuItem(itemTW, ResideMenu.DIRECTION_RIGHT);
        }
        if (itemPin != null){
            resideMenu.addMenuItem(itemPin, ResideMenu.DIRECTION_RIGHT);
        }
        if (itemYT != null){
            resideMenu.addMenuItem(itemYT, ResideMenu.DIRECTION_RIGHT);
        }
        // You can disable a direction by setting ->
        resideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_LEFT);

//        findViewById(R.id.title_bar_left_menu).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                resideMenu.openMenu(ResideMenu.DIRECTION_LEFT);
//            }
//        });
        openDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resideMenu.openMenu(ResideMenu.DIRECTION_RIGHT);
                mTracker.setScreenName(Constants.SideMenuScreen+"-"+ agency.getHomeMainDetails().get(0).getHomeMainTitle());
                mTracker.send(new HitBuilders.ScreenViewBuilder().build());
            }
        });
    }

    private ResideMenu.OnMenuListener menuListener = new ResideMenu.OnMenuListener() {
        @Override
        public void openMenu() {
            rlHeader.setVisibility(View.GONE);
//            Toast.makeText(getApplicationContext(), "Menu is opened!", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void closeMenu() {
            rlHeader.setVisibility(View.VISIBLE);
//            Toast.makeText(getApplicationContext(), "Menu is closed!", Toast.LENGTH_SHORT).show();
        }
    };



}