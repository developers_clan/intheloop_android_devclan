package com.childrencareapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import Database.MyDbHelper;
import Model.ChildModel;
import util.AppManager;
import util.Constants;
import util.MyApp;
import adapter.GenderAdapter;

public class AccountSettingActivity extends Activity {

    EditText txtName;
    Spinner spnGender;
    Button btnSave, btnUpdate;
    TextView tvSkip;
    Activity context;
    int ID;
    ChildModel model;

    Tracker mTracker;

    MyDbHelper dbHelper;
    public static RelativeLayout rl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_account_setting);
        context = this;
        AppManager.getInstance().setFont(context);

        // Obtain the shared Tracker instance.
        MyApp application = (MyApp) getApplication();
        mTracker = application.getDefaultTracker();

        rl = (RelativeLayout) findViewById(R.id.rl);
        txtName = (EditText) findViewById(R.id.et_name);
        spnGender = (Spinner) findViewById(R.id.spn_gender);
        btnSave = (Button) findViewById(R.id.btn_save);
        btnUpdate = (Button) findViewById(R.id.btn_update);
        btnUpdate.setVisibility(View.INVISIBLE);
        btnSave.setVisibility(View.VISIBLE);
        tvSkip = (TextView) findViewById(R.id.tv_skip);
        TextView heading = (TextView) findViewById(R.id.heading);
        TextView text = (TextView) findViewById(R.id.text);

        heading.setTypeface(AppManager.getInstance().getBlackTypeface());
        text.setTypeface(AppManager.getInstance().getRegularTypeface());
        txtName.setTypeface(AppManager.getInstance().getRegularTypeface());
        tvSkip.setTypeface(AppManager.getInstance().getBoldTypeface());
        btnSave.setTypeface(AppManager.getInstance().getBoldTypeface());
        btnUpdate.setTypeface(AppManager.getInstance().getBoldTypeface());
        spnGender.setDropDownVerticalOffset(1);

//        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.spn_items, R.layout.custom_spnr);
//        ArrayList veg = new ArrayList(Arrays.asList(getResources().getStringArray(R.array.spn_items)));
        // set whatever dropdown resource you want
//        adapter.setDropDownViewResource(R.layout.custom_spnr);



        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            btnUpdate.setVisibility(View.VISIBLE);
            btnSave.setVisibility(View.INVISIBLE);
            int pos = bundle.getInt("Pos");
            model = AppManager.getInstance().getChildModels().get(pos);
            ID = model.getId();
            txtName.setText(model.getName());
            spnGender.setSelection(model.getGender());
        }

        if (AppManager.getInstance().isParent()) {
//            tvSkip.setVisibility(View.GONE);
            tvSkip.setVisibility(View.VISIBLE);
            //get the sunrise animation
            Animation sunRise = AnimationUtils.loadAnimation(AccountSettingActivity.this, R.anim.sun_rise_two);
//                            AccountSetupFragment.rl.setVisibility(View.GONE);
            if (rl != null) {
                rl.startAnimation(sunRise);
            }

            //////underline text////
            SpannableString content = new SpannableString("Cancel");
            content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
            tvSkip.setText(content);
            ////// end underline text////

//            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
//
//            params.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
//            btnUpdate.setLayoutParams(params);
//            btnSave.setLayoutParams(params);

        } else {
            tvSkip.setVisibility(View.VISIBLE);

            //////underline text////
            SpannableString content = new SpannableString("Skip, I will do it later");
            content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
            tvSkip.setText(content);
            ////// end underline text////

            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.CENTER_HORIZONTAL, 0);
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
            btnUpdate.setLayoutParams(params);
            btnSave.setLayoutParams(params);
        }

        ///////Animation/////
//        Animation anim = AnimationUtils.loadAnimation(AccountSettingActivity.this, R.anim.slide_in_right);
//        findViewById(R.id.rl).startAnimation(anim);



        dbHelper = new MyDbHelper(context);
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }



    }

    @Override
    protected void onResume() {

        List<String> strList = new ArrayList<String>();

        strList.add("Boy");
        strList.add("Girl");
        strList.add("Set Gender (Boy or Girl)");

        GenderAdapter adapter = new GenderAdapter(this, R.layout.custom_spnr, strList);
        spnGender.setAdapter(adapter);

        if (model==null || model.getGender()<0){
            spnGender.setSelection(2);
        }else{
            spnGender.setSelection(model.getGender());
        }


        mTracker.setScreenName(Constants.AddChildScreenActivity);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        super.onResume();
    }

    public void onClick(View v) {
        if (v.getId() == R.id.btn_save) {

            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory(Constants.kCatAccountSettings)
                    .setAction(Constants.SaveNewChild)
                    .build());

            if (txtName.getText().toString().equals("") || txtName.getText().toString().length()>11) {
                txtName.setBackgroundResource(R.drawable.txt_error);

//                Toast.makeText(getApplicationContext(),"Enter Name",Toast.LENGTH_SHORT).show();
                return;
            }
            if ((spnGender.getSelectedItemPosition() == 2)) {
                txtName.setBackgroundResource(R.drawable.textfield_bg);
                spnGender.setBackgroundResource(R.drawable.txt_error);
//                Toast.makeText(getApplicationContext(),"Select Gender",Toast.LENGTH_SHORT).show();
                return;
            }

            txtName.setBackgroundResource(R.drawable.textfield_bg);
            spnGender.setBackgroundResource(R.drawable.spn_bg);

            ChildModel childModel = new ChildModel();
            childModel.setName(txtName.getText().toString());
            childModel.setGender(spnGender.getSelectedItemPosition());
            if (dbHelper.addNewChild(childModel)) {
                if (!AppManager.getInstance().isParent()) {
                    AppManager.getInstance().setIsParent(false);
                    Intent intent = new Intent(AccountSettingActivity.this, ChildViewActivity.class);
                    startActivity(intent);
                    finish();
                } else {
//                        Toast.makeText(context,"Saved",Toast.LENGTH_SHORT).show();
                    txtName.setText("");
                    Intent intent = new Intent(getParent(), ChildViewActivity.class);
                    TabGroupActivity parentActivity = (TabGroupActivity) getParent();
                    parentActivity.startChildActivity("ChildViewActivity", intent);
//                        finish();
                }
            }


        } else if (v.getId() == R.id.btn_update) {

            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory(Constants.kCatAccountSettings)
                    .setAction(Constants.UpdateNewChild)
                    .build());

            if (txtName.getText().toString().equals("")) {
                txtName.setBackgroundResource(R.drawable.txt_error);
//                Toast.makeText(getApplicationContext(),"Enter Name",Toast.LENGTH_SHORT).show();
                return;
            }
            if ((spnGender.getSelectedItemPosition() == 2)) {
                txtName.setBackgroundResource(R.drawable.textfield_bg);
                spnGender.setBackgroundResource(R.drawable.txt_error);
//                Toast.makeText(getApplicationContext(),"Select Gender",Toast.LENGTH_SHORT).show();
                return;
            }

            txtName.setBackgroundResource(R.drawable.textfield_bg);
            spnGender.setBackgroundResource(R.drawable.spn_bg);

            ChildModel childModel = new ChildModel();
            childModel.setName(txtName.getText().toString());
            childModel.setGender(spnGender.getSelectedItemPosition());
            if (dbHelper.updateChild(childModel, ID)) {
                AppManager.getInstance().setIsParent(false);
                Intent intent = new Intent(AccountSettingActivity.this, ChildViewActivity.class);
                startActivity(intent);
                finish();
            }


        } else if (v.getId() == R.id.tv_skip) {
            if (AppManager.getInstance().isParent()) {
                finish();
            }
            else {
                Intent intent = new Intent(AccountSettingActivity.this, TabActivity.class);
                startActivity(intent);
                finish();
            }
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        boolean handleReturn = super.dispatchTouchEvent(ev);

        View view = getCurrentFocus();

        int x = (int) ev.getX();
        int y = (int) ev.getY();

        if (view instanceof EditText) {
            View innerView = getCurrentFocus();

            if (ev.getAction() == MotionEvent.ACTION_UP &&
                    !getLocationOnScreen(txtName).contains(x, y)) {

                try {
                    InputMethodManager input = (InputMethodManager)
                            getSystemService(Context.INPUT_METHOD_SERVICE);
                    input.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
                } catch (Exception e) {
                    Log.e("NULL", e.getLocalizedMessage());
                }
            }
        }

        return handleReturn;
    }

    private Rect getLocationOnScreen(EditText mEditText) {
        Rect mRect = new Rect();
        int[] location = new int[2];

        mEditText.getLocationOnScreen(location);

        mRect.left = location[0];
        mRect.top = location[1];
        mRect.right = location[0] + mEditText.getWidth();
        mRect.bottom = location[1] + mEditText.getHeight();

        return mRect;
    }

}
