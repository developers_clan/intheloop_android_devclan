package com.childrencareapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.kogitune.activity_transition.ExitActivityTransition;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import Database.MyDbHelper;
import Model.AddContactModel;
import Model.Team;
import adapter.TeamCustomAdapter;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import thirdparty.HorizontialListView;
import util.AppManager;
import util.Constants;
import util.MyApp;

public class TeamActivity extends Activity {


    public static RelativeLayout rl;
    ImageButton btnBack;
    TextView tvAddContact, tvChildName;
    public static TextView tvCount;
    ExitActivityTransition exitTransition;
    EditText txtSearch;
    int hieght;
    View childView;
    MyDbHelper dbHelper;
    public static Button btnViewTeam;
    View view;
    ProgressDialog pg = null;
    List<AddContactModel> contactList;
    List<AddContactModel> contactListSearch;
    boolean isSearch;
    ArrayList<AddContactModel> listSearch = new ArrayList<AddContactModel>();

    public static RelativeLayout relAnim;
    public static TextView relAnimName, relAnimdiscipline, relAnimPhoneExt, relAnimEmail, relAnimAgency;
    public static ImageView relAnimImg, imgEmail, imgLoc, imgPhone;
    public static HorizontialListView listViewAgency;

    boolean loadingMore = false;

    int childId = -1;

    private ExpandableListView listView;

    String next = "20";
    String offsett = "0";
    Button btnLoadMore;
    Context context;
    Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(R.layout.activity_team);

        AppManager.getInstance().setFont(this);
        context = this;

        // Obtain the shared Tracker instance.
        MyApp application = (MyApp) getApplication();
        mTracker = application.getDefaultTracker();


        dbHelper = new MyDbHelper(this);

        rl = (RelativeLayout) findViewById(R.id.rl);
        btnBack = (ImageButton) findViewById(R.id.btn_back);
        tvAddContact = (TextView) findViewById(R.id.tv_addcontact);
        TextView heading = (TextView) findViewById(R.id.heading);
        TextView text = (TextView) findViewById(R.id.tv_body);
        tvChildName = (TextView) findViewById(R.id.tv_childname);
        tvCount = (TextView) findViewById(R.id.tvCount);
        txtSearch = (EditText) findViewById(R.id.et_search);
        btnViewTeam = (Button) findViewById(R.id.btn_viewmyteam);
        ImageView imgChild = (ImageView) findViewById(R.id.imgChild);
        ImageView imgCross = (ImageView) findViewById(R.id.imgCross);
        final ImageView imgSearch = (ImageView) findViewById(R.id.imgSearch);

        relAnim = (RelativeLayout) findViewById(R.id.relAnim);
        relAnim.setBackgroundColor(getResources().getColor(R.color.select_child_color));
        relAnimName = (TextView) findViewById(R.id.tv_name);
        relAnimdiscipline = (TextView) findViewById(R.id.tv_discipline);
//        relAnimPhoneExt = (TextView) findViewById(R.id.tv_phoneExt);
//        relAnimEmail = (TextView) findViewById(R.id.imgPhone);
        TextView more = (TextView) findViewById(R.id.tv_more);
        relAnimAgency = (TextView) findViewById(R.id.tv_agency);
        relAnimImg = (ImageView) findViewById(R.id.img);
        final Button btnAddRem = (Button) findViewById(R.id.btn_addremove);
        btnAddRem.setBackgroundResource(R.drawable.my_team_icon_plus_green);
        listViewAgency = (HorizontialListView) findViewById(R.id.listview);
        listViewAgency.setBackgroundColor(getResources().getColor(R.color.select_child_color));
        imgEmail = (ImageView) findViewById(R.id.imgEmail);
        imgLoc = (ImageView) findViewById(R.id.imgLoc);
        imgPhone = (ImageView) findViewById(R.id.imgPhone);

        heading.setTypeface(AppManager.getInstance().getBlackTypeface());
        tvAddContact.setTypeface(AppManager.getInstance().getRegularTypeface());
        text.setTypeface(AppManager.getInstance().getRegularTypeface());
        txtSearch.setTypeface(AppManager.getInstance().getRegularTypeface());
        tvChildName.setTypeface(AppManager.getInstance().getRegularTypeface());
        btnViewTeam.setTypeface(AppManager.getInstance().getBoldTypeface());

        SharedPreferences pref = getSharedPreferences(Constants.WWAACHILD_PREF, Activity.MODE_PRIVATE);
        if (pref.contains(Constants.CHILD_POS)) {
            int pos = pref.getInt(Constants.CHILD_POS, -1);
            childId = AppManager.getInstance().getChildModels().get(pos).getId();
            tvChildName.setText(AppManager.getInstance().getChildModels().get(pos).getName());
            if (AppManager.getInstance().getChildModels().get(pos).getGender() == 0) {
                imgChild.setBackgroundResource(R.drawable.my_team_user_boy_place_holder);
            } else if (AppManager.getInstance().getChildModels().get(pos).getGender() == 1) {
                imgChild.setBackgroundResource(R.drawable.my_team_user_girl_place_holder);
            }
        }


//        //////underline text////
//        final SpannableString content = new SpannableString("Add Contact");
//        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
//        tvAddContact.setText(content);
//        ////// end underline text////

        ////Animation///
//        exitTransition = ActivityTransition
//                .with(getIntent())
//                .to(findViewById(R.id.linearLayout))
//                .interpolator(new BounceInterpolator())
//                .start(savedInstanceState);


        if (getParent() != null) {
            pg = new ProgressDialog(getParent(), ProgressDialog.THEME_HOLO_DARK);
            pg.setCancelable(true);
            pg.setTitle("Please Wait");
            pg.setMessage("Loading...");
        }
//        Animation anim = AnimationUtils.loadAnimation(TeamActivity.this, R.anim.fade_in);
//        findViewById(R.id.relative).startAnimation(anim);
        ////end Animation///

        listView = (ExpandableListView) findViewById(R.id.expandableListView);


//
//
//        AppManager.getInstance().setIsSearch(false);
//        AppManager.getInstance().setIsView(false);
//        TeamCustomAdapter adapter = new TeamCustomAdapter(contactList, this);
//        listView.setAdapter(adapter);

        isSearch = false;

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int width = metrics.widthPixels;
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
            listView.setIndicatorBounds(width - GetPixelFromDips(50), width - GetPixelFromDips(10));
            listView.setIndicatorBounds(width - GetPixelFromDips(50), width - GetPixelFromDips(10));
        } else {
            listView.setIndicatorBoundsRelative(width - GetPixelFromDips(50), width - GetPixelFromDips(10));
            listView.setIndicatorBoundsRelative(width - GetPixelFromDips(50), width - GetPixelFromDips(10));
        }

        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.team_list_parent_vertical, null, false);
        childView = inflater.inflate(R.layout.list_item_child_vertical, null, false);
//        childList = inflater.inflate(R.layout.childlist, null, false);
        int h = ((RelativeLayout) view.findViewById(R.id.rel)).getMinimumHeight() + ((RelativeLayout) view.findViewById(R.id.relbar)).getMinimumHeight();
        h = h + 20;


        if (AppManager.getInstance().getTeams().size() > 0) {

//            AsyncTask.execute(new Runnable() {
//                @Override
//                public void run() {
//                    contactList = dbHelper.getContactWithLimit(next, "0");
//                    contactListSearch = dbHelper.getAllContact();
//                    if (contactList.size() == 0) {
//                        addDatatoArray(true);
//                    } else {
//                        addDatatoArray(false);
//                    }
//                }
//            });


//            setAdapter();
        } else {
//            teamAPI();
        }

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {

                contactListSearch = dbHelper.getAllContact();

            }
        });


//        contactListSearch = AppManager.getInstance().getContactList();

        // LoadMore button
        btnLoadMore = new Button(this);
        btnLoadMore.setText("Load More");
        btnLoadMore.setTextSize(15);
        btnLoadMore.setTypeface(AppManager.getInstance().getBoldTypeface());
        btnLoadMore.setBackgroundColor(getResources().getColor(R.color.select_child_color));
        btnLoadMore.setTextColor(getResources().getColor(R.color.team_name));

        // Adding Load More button to lisview at bottom
        listView.addFooterView(btnLoadMore);


        btnLoadMore.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // Starting a new async task
                loadMore();
            }
        });


        listView.setOnGroupClickListener(new OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                if (isSearch) {
                    Collections.sort(listSearch, teamomparator);
                    if (listSearch.get(groupPosition).getOfficeList() != null && listSearch.get(groupPosition).getOfficeList().size() > 0) {
                        return false;
                    } else {
                        return true; //!parent.isGroupExpanded(groupPosition);
                    }
                } else {
                    Collections.sort(contactList, teamomparator);
                    if (contactList.get(groupPosition).getOfficeList() != null && contactList.get(groupPosition).getOfficeList().size() > 0) {
                        return false;
                    } else {
                        return true; //!parent.isGroupExpanded(groupPosition);
                    }
                }
            }
        });

        listView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                if (parent.isGroupExpanded(groupPosition)) {
                    parent.collapseGroup(groupPosition);
                }
                return true;
            }
        });

//        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
//            @Override
//            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
//
//                final AlertDialog.Builder builder = new AlertDialog.Builder(getParent());
//                if (isSearch) {
//                    Collections.sort(listSearch, teamomparator);
//                    if (listSearch.get(position).getOfficeList() != null && listSearch.get(position).getOfficeList().size() > 0) {
//                        return false;
//                    } else {
//                        if (listSearch.get(position).getNodeId() == null || listSearch.get(position).getNodeId().equalsIgnoreCase("")) {
//
//                            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    AddContactModel contact = listSearch.get(position);
//                                    Intent intent = new Intent(TeamActivity.this,AddContactActivity.class);
//                                    Gson gson = new Gson();
//                                    String value = gson.toJson(contact);
//                                    intent.putExtra("DATA",value);
//                                    startActivity(intent);
//                                }
//                            });
//                            builder.setNegativeButton("NO", null);
//                            builder.setMessage("Do you want to Edit ?");
//                            builder.create().show();
//
//                        }
//                        return true;
//                    }
//                } else {
//                    Collections.sort(contactList, teamomparator);
//                    if (contactList.get(position).getOfficeList() != null && contactList.get(position).getOfficeList().size() > 0) {
//                        return false;
//                    } else {
//                        if (contactList.get(position).getNodeId() == null || contactList.get(position).getNodeId().equalsIgnoreCase("")) {
//
//                            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    AddContactModel contact = contactList.get(position);
//                                    Intent intent = new Intent(TeamActivity.this, AddContactActivity.class);
//                                    Gson gson = new Gson();
//                                    String value = gson.toJson(contact);
//                                    intent.putExtra("DATA", value);
//                                    startActivity(intent);
//                                }
//                            });
//                            builder.setNegativeButton("NO", null);
//                            builder.setMessage("Do you want to Edit ?");
//                            builder.create().show();
//
//                        }
//                        return true;
//                    }
//                }
//            }
//        });

        listView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {

                Collections.sort(contactList, teamomparator);
                if (contactList.get(groupPosition).getOfficeList() != null && contactList.get(groupPosition).getOfficeList().size() > 0) {


                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory(Constants.kCatTeam)
                            .setAction(Constants.kActTeamContactExpanded)
                            .build());

                    int h = childView.getMinimumHeight() + 40;
                    int s = contactList.get(groupPosition).getOfficeList().size();

                    int phoneSize = GetPixelFromDips(30);
                    if (contactList.get(groupPosition).getPhoneList() != null) {
                        phoneSize = contactList.get(groupPosition).getPhoneList().size() * GetPixelFromDips(30);
                    }

                    int childListH = (h * s) + GetPixelFromDips(60) + phoneSize;

                    //AppManager.getInstance().setListHieght(childListH);

                    hieght += childListH;

                    ViewGroup.LayoutParams prams = listView.getLayoutParams();
                    prams.height = hieght; //AppManager.getInstance().getListHieght();
                    listView.setLayoutParams(prams);

                }
            }
        });


        listView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {
                Collections.sort(contactList, teamomparator);
                if (contactList.get(groupPosition).getOfficeList() != null && contactList.get(groupPosition).getOfficeList().size() > 0) {

                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory(Constants.kCatTeam)
                            .setAction(Constants.kActTeamContactCollapsed)
                            .build());

                    int h = childView.getMinimumHeight() + 40;
                    int s = contactList.get(groupPosition).getOfficeList().size();

                    int phoneSize = 0;
                    if (contactList.get(groupPosition).getPhoneList() != null) {
                        phoneSize = contactList.get(groupPosition).getPhoneList().size() * GetPixelFromDips(30);
                    }

                    int childListH = (h * s) + GetPixelFromDips(60) + phoneSize;
                    hieght -= childListH;

                    ViewGroup.LayoutParams prams = listView.getLayoutParams();
                    prams.height = hieght; //AppManager.getInstance().getListHieght();
                    listView.setLayoutParams(prams);
                }
            }
        });

//        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
//
//            @Override
//            public void onScrollStateChanged(AbsListView view, int scrollState) {
//            }
//
//            @Override
//            public void onScroll(AbsListView view, int firstVisibleItem,
//                                 int visibleItemCount, int totalItemCount) {
//
//                int lastInScreen = firstVisibleItem + visibleItemCount;
//                if ((lastInScreen >= totalItemCount) && !(loadingMore)) {
//
//                    loadingMore = true;
//                    loadMore();
//                    Toast.makeText(getApplicationContext(),"END",Toast.LENGTH_SHORT).show();
//
//                }
//            }
//        });

        txtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!txtSearch.getText().toString().equalsIgnoreCase("")) {
//                    btnLoadMore.setVisibility(View.INVISIBLE);
//
//                    listSearch.clear();
//
//                    if (contactListSearch != null) {
//                        for (int i = 0; i < contactListSearch.size(); i++) {
//                            if (contactListSearch.get(i).getName().toLowerCase().contains(s.toString().toLowerCase())) {
//                                listSearch.add(contactListSearch.get(i));
//                            }
//                        }
//
//                        updateUI(listSearch);
//
//                        Collections.sort(listSearch, teamomparator);
//
//                        isSearch = true;
//
//                        AppManager.getInstance().setIsView(false);
//                        AppManager.getInstance().setIsSearch(true);
//                        TeamCustomAdapter adapter = new TeamCustomAdapter(listSearch, getParent());
//                        listView.setAdapter(adapter);
//                    }
//
                } else {
                    btnLoadMore.setVisibility(View.VISIBLE);

                    updateUI(contactList);

                    Collections.sort(contactList, teamomparator);

                    isSearch = false;

                    AppManager.getInstance().setIsView(false);
                    AppManager.getInstance().setIsSearch(false);
                    TeamCustomAdapter adapter = new TeamCustomAdapter(contactList, getParent(),mTracker);
                    listView.setAdapter(adapter);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        imgCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                btnLoadMore.setVisibility(View.VISIBLE);
                txtSearch.setText("");
                isSearch = false;
            }
        });

        imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory(Constants.kCatTeam)
                        .setAction(Constants.TeamSearch)
                        .build());

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                if (!txtSearch.getText().toString().equalsIgnoreCase("")) {
                    btnLoadMore.setVisibility(View.INVISIBLE);

                    listSearch.clear();

                    if (contactListSearch != null) {
                        for (int i = 0; i < contactListSearch.size(); i++) {
                            if (contactListSearch.get(i).getName().toLowerCase().contains(txtSearch.getText().toString().toLowerCase())) {
                                listSearch.add(contactListSearch.get(i));
                            }
                        }

                        updateUI(listSearch);

                        Collections.sort(listSearch, teamomparator);

                        isSearch = true;

                        AppManager.getInstance().setIsView(false);
                        AppManager.getInstance().setIsSearch(true);
                        TeamCustomAdapter adapter = new TeamCustomAdapter(listSearch, getParent(),mTracker);
                        listView.setAdapter(adapter);
                    }

                } else {
                    btnLoadMore.setVisibility(View.VISIBLE);

                    updateUI(contactList);

                    Collections.sort(contactList, teamomparator);

                    isSearch = false;

                    AppManager.getInstance().setIsView(false);
                    AppManager.getInstance().setIsSearch(false);
                    TeamCustomAdapter adapter = new TeamCustomAdapter(contactList, getParent(),mTracker);
                    listView.setAdapter(adapter);
                }

            }
        });

        txtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
//                    performSearch();
                    imgSearch.callOnClick();
                    return true;
                }
                return false;
            }
        });

    }

    private void updateUI(List<AddContactModel> contactList) {
        hieght = 0;
        int h = ((RelativeLayout) view.findViewById(R.id.rel)).getMinimumHeight() + ((RelativeLayout) view.findViewById(R.id.relbar)).getMinimumHeight();
        h = h + (int) GetPixelFromDips(10);
        hieght = h * contactList.size();

        hieght = hieght + GetPixelFromDips(50);

        ViewGroup.LayoutParams prams = listView.getLayoutParams();
        prams.height = hieght;
        listView.setLayoutParams(prams);
        listView.requestLayout();
    }

    public void onClick(View v) {
        if (v.getId() == R.id.btn_back) {
            try {
                finish();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (v.getId() == R.id.tv_addcontact) {

            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory(Constants.kCatTeam)
                    .setAction(Constants.AddContact)
                    .build());

            startActivity(new Intent(TeamActivity.this, AddContactActivity.class));
        } else if (v.getId() == R.id.btn_viewmyteam) {
//            startActivity(new Intent(TeamActivity.this, ViewMyTeamActivity.class));

//            mTracker.send(new HitBuilders.EventBuilder()
//                    .setCategory(Constants.kCatTeam)
//                    .setAction(Constants.ViewMyTeam)
//                    .build());

            txtSearch.setText("");
            isSearch = false;

            Intent intent = new Intent(getParent(), ViewMyTeamActivity.class);
            TabGroupActivity parentActivity = (TabGroupActivity) getParent();
            parentActivity.startChildActivity("ViewMyTeamActivity", intent);

        } else if (v.getId() == R.id.tv_childname) {
            try {
                finish();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
//        teamAPI();


        mTracker.setScreenName(Constants.TeamActivity);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        SharedPreferences pref = getSharedPreferences(Constants.WWAACHILD_PREF, Activity.MODE_PRIVATE);
        if (pref.contains(Constants.CHILD_POS)) {
            int pos = pref.getInt(Constants.CHILD_POS, -1);
            childId = AppManager.getInstance().getChildModels().get(pos).getId();
            tvChildName.setText(AppManager.getInstance().getChildModels().get(pos).getName());
        }

        tvCount = (TextView) findViewById(R.id.tvCount);
        tvCount.setText("" + dbHelper.getChildTeam(childId).size());

        if (dbHelper.getChildTeam(childId).size() > 0) {
            btnViewTeam.setBackgroundResource(R.drawable.big_btn_bg);
            btnViewTeam.setEnabled(true);
        } else {
            btnViewTeam.setBackgroundResource(R.drawable.big_disable_btn_bg);
            btnViewTeam.setEnabled(false);
        }

        contactList = dbHelper.getContactWithLimit(next, "0");
        offsett = next;
        Collections.sort(contactList, teamomparator);


//        addDatatoArray(); // server data

        updateUI(contactList);


        btnLoadMore.setVisibility(View.VISIBLE);

        txtSearch.setText("");
        isSearch = false;

        AppManager.getInstance().setIsSearch(false);
        AppManager.getInstance().setIsView(false);
        TeamCustomAdapter adapter = new TeamCustomAdapter(contactList, getParent(),mTracker);
        listView.setAdapter(adapter);

//        super.onResume();
    }

    public float convertDpToPixel(float dp) {
//        Resources resources = this.getResources();
//        DisplayMetrics metrics = resources.getDisplayMetrics();
//        float px = (dp * metrics.densityDpi) / 320f;
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    private void loadMore() {

        ArrayList<AddContactModel> list = dbHelper.getContactWithLimit(next, offsett);
        if (list.size() == 0) {
            btnLoadMore.setVisibility(View.INVISIBLE);
        } else {
            btnLoadMore.setVisibility(View.VISIBLE);
        }
        hieght = 0;
        contactList.addAll(list);
        offsett = String.valueOf(Integer.valueOf(offsett) + 20);

        updateUI(contactList);

        Collections.sort(contactList, teamomparator);

        int currentPosition = listView.getFirstVisiblePosition();

        TeamCustomAdapter adapter = new TeamCustomAdapter(contactList, getParent(),mTracker);
        listView.setAdapter(adapter);

        // Setting new scroll position
        listView.setSelectionFromTop(currentPosition + 1, 0);


    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        boolean handleReturn = super.dispatchTouchEvent(ev);

//        View view = getCurrentFocus();
//
//        int x = (int) ev.getX();
//        int y = (int) ev.getY();
//
//        if (view instanceof EditText) {
//            View innerView = getCurrentFocus();
//
//            if (ev.getAction() == MotionEvent.ACTION_UP && !(getLocationOnScreen(txtSearch).contains(x, y))) {
//
//                try {
//                    InputMethodManager input = (InputMethodManager)
//                            getSystemService(Context.INPUT_METHOD_SERVICE);
//                    input.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
//
//                } catch (Exception e) {
//                    Log.e("NULL", e.getLocalizedMessage());
//                }
//            }
//        }

        return handleReturn;
    }

    private Rect getLocationOnScreen(EditText mEditText) {
        Rect mRect = new Rect();
        int[] location = new int[2];

        mEditText.getLocationOnScreen(location);

        mRect.left = location[0];
        mRect.top = location[1];
        mRect.right = location[0] + mEditText.getWidth();
        mRect.bottom = location[1] + mEditText.getHeight();

        return mRect;
    }


    public int GetPixelFromDips(float pixels) {
        // Get the screen's density scale
        final float scale = getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f);
    }

    private void teamAPI() {
        Handler mainHandler = new Handler(TeamActivity.this.getMainLooper());
        final Runnable myRunnable = new Runnable() {
            @Override
            public void run() {

                if(pg!=null) {
                    pg.show();
                }
                AppManager.getInstance().restAPI.getTeam(new Callback<List<Team>>() {
                    @Override
                    public void success(List<Team> teams, Response response) {
                        if(pg!=null) {
                            pg.cancel();
                        }
                        AppManager.getInstance().setTeams(teams);

                        contactList = dbHelper.getContactWithLimit(next, "0");

                        if (contactList.size() == 0) {
                            addDatatoArray(true);
                        } else {
                            addDatatoArray(false);
                        }

                    }

                    @Override
                    public void failure(RetrofitError error) {
                        if(pg!=null) {
                            pg.cancel();
                        }
                        Toast.makeText(getApplicationContext(), error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        };
        mainHandler.post(myRunnable);
    }


    private void addDatatoArray(boolean flag) {
        dbHelper.deleteTeams();

        for (int i = 0; i < AppManager.getInstance().getTeams().size(); i++) {

            Team team = AppManager.getInstance().getTeams().get(i);

            if (!dbHelper.CheckIsDataAlreadyInDBorNot(team.getNid())) {
                AddContactModel contactModel = new AddContactModel();
                contactModel.setNodeId(team.getNid());
                if (team.getName().size() > 0) {
                    contactModel.setName(team.getName().get(0).getValue());
                }
                if (team.getDiscipline().size() > 0) {
                    contactModel.setDiscipline(team.getDiscipline().get(0).getName());
                }
                if (team.getTeamPhone().size() > 0) {
                    for (int a = 0; a < team.getTeamPhone().size(); a++) {
                        if (team.getTeamPhone().get(a).getPrimary()) {
                            contactModel.setPhone(team.getTeamPhone().get(a).getNumber());
                            contactModel.setExtension(team.getTeamPhone().get(a).getExtension());
                            break;
                        }
                    }
                }
                if (team.getEmail().size() > 0) {
                    contactModel.setEmail(team.getEmail().get(0).getEmail());
                }
                if (team.getGender().size() > 0) {
                    contactModel.setGender(team.getGender().get(0).getValue());
                }


                if (team.getHeadshot().size() > 0 && team.getHeadshot().get(0).getUri() != null) {
                    String[] arr = team.getHeadshot().get(0).getUri().split("/");
                    String name = arr[arr.length - 1];
                    new ImageDownloadAndSave().execute(team.getHeadshot().get(0).getUri(), team.getNid(), name);
                }

                for (int j = 0; j < team.getAgencyThumbnail().size(); j++) {
                    if (team.getAgencyThumbnail().get(j).getUri() != null) {
                        String[] arr = team.getAgencyThumbnail().get(j).getUri().split("/");
                        String name = arr[arr.length - 1];
                        new ThumbnailDownloadAndSave().execute(team.getAgencyThumbnail().get(j).getUri(), team.getNid(), "" + team.getAgencyThumbnail().size(), name);
                    }
                }
                Gson gson = new Gson();
                String officeList = gson.toJson(team.getOffices());
                String agencyList = gson.toJson(team.getAgencyThumbnail());
                String phoneList = gson.toJson(team.getTeamPhone());

                if (dbHelper.addNewContact(contactModel, officeList, phoneList, agencyList, "")) {

                } else {
                    Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
                }

            } else {
                AddContactModel contactModel = new AddContactModel();
                contactModel.setNodeId(team.getNid());
                if (team.getName().size() > 0) {
                    contactModel.setName(team.getName().get(0).getValue());
                }
                if (team.getDiscipline().size() > 0) {
                    contactModel.setDiscipline(team.getDiscipline().get(0).getName());
                }
                if (team.getTeamPhone().size() > 0) {
                    for (int a = 0; a < team.getTeamPhone().size(); a++) {
                        if (team.getTeamPhone().get(a).getPrimary()) {
                            contactModel.setPhone(team.getTeamPhone().get(a).getNumber());
                            contactModel.setExtension(team.getTeamPhone().get(a).getExtension());
                            break;
                        }
                    }
                }
                if (team.getEmail().size() > 0) {
                    contactModel.setEmail(team.getEmail().get(0).getEmail());
                }
                if (team.getGender().size() > 0) {
                    contactModel.setGender(team.getGender().get(0).getValue());
                }
                if (team.getHeadshot().size() > 0 && team.getHeadshot().get(0).getUri() != null) {
                    String[] arr = team.getHeadshot().get(0).getUri().split("/");
                    String name = arr[arr.length - 1];
                    new ImageDownloadAndSave().execute(team.getHeadshot().get(0).getUri(), team.getNid(), name);
                }
                for (int j = 0; j < team.getAgencyThumbnail().size(); j++) {
                    if (team.getAgencyThumbnail().get(j).getUri() != null) {
                        String[] arr = team.getAgencyThumbnail().get(j).getUri().split("/");
                        String name = arr[arr.length - 1];
                        new ThumbnailDownloadAndSave().execute(team.getAgencyThumbnail().get(j).getUri(), team.getNid(), "" + team.getAgencyThumbnail().size(), name);
                    }
                }
                Gson gson = new Gson();
                String officeList = gson.toJson(team.getOffices());
                String agencyList = gson.toJson(team.getAgencyThumbnail());
                String phoneList = gson.toJson(team.getTeamPhone());

                if (dbHelper.updateContact(contactModel, officeList, agencyList, phoneList, "", team.getNid())) {

                } else {
//                    Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
                }
            }
        }
        if (flag) {
            contactList = dbHelper.getContactWithLimit(next, "0");
//            contactListSearch = dbHelper.getAllContact();
            Collections.sort(contactList, teamomparator);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateUI(contactList);

                    AppManager.getInstance().setIsSearch(false);
                    AppManager.getInstance().setIsView(false);
                    TeamCustomAdapter adapter = new TeamCustomAdapter(contactList, getParent(),mTracker);
                    listView.setAdapter(adapter);
                }
            });
        }
    }

    private class ImageDownloadAndSave extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... arg0) {
            try {
                URL url = new URL(arg0[0]);
                String nodeID = arg0[1];
                String imgName = arg0[2];
                URLConnection ucon = url.openConnection();
                InputStream inputStream = null;
                HttpURLConnection httpConn = (HttpURLConnection) ucon;
                httpConn.setRequestMethod("GET");
                httpConn.connect();

                if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    inputStream = httpConn.getInputStream();
                }

                byte[] byteImage = convertToByte(inputStream);

                Bitmap bmp = BitmapFactory.decodeByteArray(byteImage, 0,
                        byteImage.length);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.PNG, 80, stream);
                bmp.recycle();
                byte[] byteArray = stream.toByteArray();

                String path = saveImageToExternalStorage(byteArray, imgName);
                if (!path.equalsIgnoreCase("")) {
//                    dbHelper.saveImage(byteArray,-1,nodeID,true);
                    dbHelper.saveImage(path, -1, nodeID, true);
                }

                stream.close();
                stream = null;


//                contactList = dbHelper.getAllContact();
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        setAdapter();
//                    }
//                });


            } catch (IOException io) {
                io.printStackTrace();
            }

            return null;
        }


    }

    ArrayList<String> thumbList = new ArrayList<String>();

    private class ThumbnailDownloadAndSave extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... arg0) {
            try {
                URL url = new URL(arg0[0]);
                String nodeID = arg0[1];
                String count = arg0[2];
                String imgName = arg0[3];
                URLConnection ucon = url.openConnection();
                InputStream inputStream = null;
                HttpURLConnection httpConn = (HttpURLConnection) ucon;
                httpConn.setRequestMethod("GET");
                httpConn.connect();

                if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    inputStream = httpConn.getInputStream();
                }

                byte[] byteImage = convertToByte(inputStream);

                Bitmap bmp = BitmapFactory.decodeByteArray(byteImage, 0,
                        byteImage.length);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.PNG, 80, stream);
                bmp.recycle();
                byte[] byteArray = stream.toByteArray();

                stream.close();
                stream = null;

                String path = saveImageToExternalStorage(byteArray, imgName);

                thumbList.add(path);

                if (count.equals("" + thumbList.size())) {
                    dbHelper.saveThumbImage(thumbList, nodeID);
                    thumbList.clear();
//                    contactList = dbHelper.getAllContact();
                }

//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        setAdapter();
//                    }
//                });

            } catch (IOException io) {
                io.printStackTrace();
            }

            return null;
        }


    }

    private byte[] convertToByte(InputStream input) throws IOException {
        byte[] buffer = new byte[8192];
        int bytesRead;
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }

        byte[] arr = output.toByteArray();

        output.close();
        output = null;

        return arr;
    }

    public String saveImageToExternalStorage(byte[] image, String name) {
        String fullPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/InTheLoop/" + "thumbnail";

        try {
            File dir = new File(fullPath);
            if (!dir.exists()) {
                dir.mkdirs();
            }

            OutputStream fOut = null;
            File file = new File(fullPath, name);

            if (!file.exists()) {
                file.createNewFile();
            }
            fOut = new FileOutputStream(file);

// 100 means no compression, the lower you go, the stronger the compression
//            image.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.write(image);
            fOut.flush();
            fOut.close();

//            MediaStore.Images.Media.insertImage(context.getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());

            return file.getAbsolutePath();

        } catch (Exception e) {
            Log.e("saveToExternalStorage()", e.getMessage());
            return "";
        }
    }


    //////////// SORTING /////////
    private Comparator<AddContactModel> teamomparator = new Comparator<AddContactModel>() {

        public int compare(AddContactModel con1, AddContactModel con2) {

            String contact1 = con1.getName();
            String contact2 = con2.getName();

            //ascending order
            return contact1.compareToIgnoreCase(contact2);

            //descending order
            //return fruitName2.compareTo(fruitName1);
        }

    };


}
