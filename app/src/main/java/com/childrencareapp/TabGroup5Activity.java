package com.childrencareapp;

import android.content.Intent;
import android.os.Bundle;

public class TabGroup5Activity extends TabGroupActivity{
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startChildActivity("ContactActivity", new Intent(this,ContactActivity.class));
    }
}
