package com.childrencareapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;

import Database.MyDbHelper;
import Model.Agency;
import Model.ChildModel;
import Model.ParentData;
import util.AppManager;
import util.Constants;
import util.MyApp;

public class MyTeamActivity extends Activity {

    Button btnCreateTeam;
    SharedPreferences child_pr;
    Boolean WWAA;
    MyDbHelper dbHelper;
    Agency agency;
    public static RelativeLayout rl;
    ListView listView;
    RelativeLayout relAnim;
    ImageView relAnimImg;
    TextView relAnimText;
    ImageView relAnimImgTeam;
    Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_my_team);

        AppManager.getInstance().setFont(this);

        // Obtain the shared Tracker instance.
        MyApp application = (MyApp) getApplication();
        mTracker = application.getDefaultTracker();

        btnCreateTeam = (Button) findViewById(R.id.btn_create_team);
        TextView heading = (TextView)findViewById(R.id.heading);
        TextView text = (TextView)findViewById(R.id.text);
        ImageView banner = (ImageView)findViewById(R.id.banner);
        rl = (RelativeLayout) findViewById(R.id.rl);
        ScrollView scrollView = (ScrollView) findViewById(R.id.scrollView);
        scrollView.smoothScrollTo(0,0);

        relAnim = (RelativeLayout) findViewById(R.id.relAnim);
        relAnimImg = (ImageView) findViewById(R.id.relAnimImg);
        relAnimImgTeam = (ImageView) findViewById(R.id.relAnimImgTeam);
        relAnimText = (TextView) findViewById(R.id.relAnimText);

        btnCreateTeam.setBackgroundResource(R.drawable.big_disable_btn_bg);
        btnCreateTeam.setEnabled(false);

        WWAA = false;
        Bundle bundle = getIntent().getExtras();
        if (bundle!=null){
            WWAA = bundle.getBoolean("WWAA");
        }

        agency = AppManager.getInstance().getAgencyModel();

        if (WWAA){
            if (agency.getWwaaMainDetails().size()>0) {
                heading.setText(agency.getWwaaMainDetails().get(0).getWwaaMainTitle());
            }
            btnCreateTeam.setText("CONTINUE");
            text.setText("To continue, you will need to select a child's name.");
            banner.setVisibility(View.GONE);
        }
        else {
            heading.setText("CREATE MY TEAM");
            btnCreateTeam.setText("CREATE MY TEAM");
            text.setText("In order to Create My Team, you will need to select a child’s name.");
            banner.setVisibility(View.VISIBLE);

            //get the sunrise animation
            Animation sunRise = AnimationUtils.loadAnimation(MyTeamActivity.this, R.anim.sun_rise);
//                            MyTeamFragment.rl.setVisibility(View.GONE);
            if(rl != null) {
                rl.startAnimation(sunRise);
            }

        }

        btnCreateTeam.setTypeface(AppManager.getInstance().getBoldTypeface());
        heading.setTypeface(AppManager.getInstance().getBlackTypeface());
        text.setTypeface(AppManager.getInstance().getRegularTypeface());

        listView = (ListView) findViewById(R.id.listView);

        child_pr = getSharedPreferences(Constants.WWAACHILD_PREF, Activity.MODE_PRIVATE);

        dbHelper = new MyDbHelper(this);
        final ArrayList<ChildModel> list = dbHelper.getData();
        AppManager.getInstance().setChildModels(list);

        updateUI(list.size());

        ArrAdapter adapter = new ArrAdapter(this,R.layout.custom_childview,list);
        listView.setAdapter(adapter);

//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
////                View vie = listView.getAdapter().getView(position,null,parent);
//
//                final Animation animAnticipate = AnimationUtils.loadAnimation(MyTeamActivity.this, R.anim.anticipate);
//                view.startAnimation(animAnticipate);
//                view.bringToFront();
//            }
//        });


        btnCreateTeam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                goNextActivity();

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        mTracker.setScreenName(Constants.MyTeamActivity);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

    }

    private void goNextActivity(){
        if (WWAA) {

            SharedPreferences pref = getSharedPreferences(Constants.WWAACHILD_PREF, Activity.MODE_PRIVATE);
            int pos = -1;
            if (pref.contains(Constants.CHILD_POS)) {
                pos = pref.getInt(Constants.CHILD_POS, -1);
            }

            if (dbHelper.getCdpStatus(AppManager.getInstance().getChildModels().get(pos).getId(), agency.getNid()).size() > 0) {
                finish();
                Intent intent = new Intent(getParent(), WwaaCdpActivity.class);
                TabGroupActivity parentActivity = (TabGroupActivity) getParent();
                parentActivity.startChildActivity("WwaaCdpActivity", intent);
//                    Intent intent = new Intent(CDPActivity.this, WhileYouWaitActivity.class);
//                    startActivity(intent);
            } else {
                AppManager.getInstance().setParentModels(new ArrayList<ParentData>());
                finish();
                Intent intent = new Intent(getParent(), WwaaCdpStatusActivity.class);
                TabGroupActivity parentActivity = (TabGroupActivity) getParent();
                parentActivity.startChildActivity("WwaaCdpStatusActivity", intent);
//                    Intent intent = new Intent(CDPActivity.this, WhileYouWaitActivity.class);
//                    startActivity(intent);
            }
        } else {
            if (AppManager.getInstance().getChildModels().size() > 0) {
                Intent intent = new Intent(getParent(), TeamActivity.class);
                TabGroupActivity parentActivity = (TabGroupActivity) getParent();
                parentActivity.startChildActivity("TeamActivity", intent);
            } else {
//                    Toast.makeText(getApplicationContext(), "Select Child", Toast.LENGTH_SHORT).show();
                if (TabActivity.tabHost != null) {
                    TabActivity.tabHost.setCurrentTab(1);
                }
            }
        }
    }

    private void updateUI(int count) {
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.custom_childview, null, false);

        int h = ((RelativeLayout) view.findViewById(R.id.relative)).getMinimumHeight();
        h = h + 50;
        int hieght = h * count;

        ViewGroup.LayoutParams prams = listView.getLayoutParams();
        prams.height = hieght;
        listView.setLayoutParams(prams);

//        scrollView.fullScroll(ScrollView.FOCUS_UP);

    }

    class ArrAdapter extends ArrayAdapter<ChildModel> {

        Context context;
        ArrayList<ChildModel> list;
        View view;
        boolean first = false;

        public ArrAdapter(Context context, int resource, ArrayList<ChildModel> objects) {
            super(context, resource, objects);
            this.context = context;
            this.list = objects;

            first = true;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public View getView(final int position, final View convertView, final ViewGroup parent) {
            view = convertView;
            if (view == null)
            {
                view = LayoutInflater.from(context).inflate(R.layout.custom_childview, parent, false);
            }

            TextView tvName = (TextView) view.findViewById(R.id.img);
            tvName.setTypeface(AppManager.getInstance().getRegularTypeface());
            Button btnDel = (Button) view.findViewById(R.id.btn_del);
            Button btnEdit = (Button) view.findViewById(R.id.btn_update);
            ImageView img = (ImageView) view.findViewById(R.id.img_child);
            final ImageView imgTeam = (ImageView) view.findViewById(R.id.img_team);
            final RelativeLayout rel = (RelativeLayout) view.findViewById(R.id.relative);

            btnDel.setVisibility(View.GONE);
            btnEdit.setVisibility(View.GONE);
            imgTeam.setVisibility(View.VISIBLE);

            int p = -1;
            if (child_pr.contains(Constants.CHILD_POS)) {
                p = child_pr.getInt(Constants.CHILD_POS, -1);

            }

            if (p == position) {
                rel.setBackgroundResource(R.drawable.selected_et_border);
                imgTeam.setBackgroundResource(R.drawable.team_plceholder_selected);


            } else {
                rel.setBackgroundResource(R.drawable.et_border);
                imgTeam.setBackgroundResource(R.drawable.team_plceholder_unselected);

            }

            tvName.setText(list.get(position).getName());
            if (list.get(position).getGender()==0)
            {
                img.setBackgroundResource(R.drawable.boy);
            }
            else if (list.get(position).getGender()==1) {
                img.setBackgroundResource(R.drawable.girl);
            }

            rel.setOnClickListener(new View.OnClickListener() {
                int pos = position;
                View vi = view;
                @Override
                public void onClick(View v) {

                    if(WWAA){
                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory(Constants.kCatTeam)
                                .setAction(Constants.SelectChild+" "+agency.getHomeMainDetails().get(0).getHomeMainTitle())
                                .build());
                    }
//                    else {
//                        mTracker.send(new HitBuilders.EventBuilder()
//                                .setCategory(Constants.kCatTeam)
//                                .setAction(Constants.SelectChild+" TEAM")
//                                .build());
//                    }


                    rel.setBackgroundColor(Color.GRAY);

                    int fromLoc[] = new int[2];
                    imgTeam.getLocationOnScreen(fromLoc);
                    float startX = fromLoc[0];
                    float startY = fromLoc[1];

                    int toLoc[] = new int[2];
                    btnCreateTeam.getLocationOnScreen(toLoc);
                    float destX = toLoc[0];
                    float destY = toLoc[1];


                    SharedPreferences.Editor editor = child_pr.edit();
                    editor.clear();
                    editor.putInt(Constants.CHILD_POS, pos);
                    editor.commit();

//                    View vie = listView.getAdapter().getView(pos,null,null);


//                    View animView = LayoutInflater.from(context).inflate(R.layout.custom_childview_anim, null, false);
//
//                    TextView tvName = (TextView) animView.findViewById(R.id.img);
//                    tvName.setTypeface(AppManager.getInstance().getRegularTypeface());
//                    Button btnDel = (Button) animView.findViewById(R.id.btn_del);
//                    Button btnEdit = (Button) animView.findViewById(R.id.btn_update);
//                    ImageView img = (ImageView) animView.findViewById(R.id.img_child);
//                    final ImageView imgTeam = (ImageView) animView.findViewById(R.id.img_team);
//                    final RelativeLayout rel = (RelativeLayout) animView.findViewById(R.id.relative);

//                    tvName.setText("check anim");

                    relAnim.setVisibility(View.VISIBLE);

//                    final RelativeLayout rel = (RelativeLayout) v.findViewById(R.id.relative);

//                    ViewGroup.LayoutParams params = rel.getLayoutParams();
//
                    int[] loc = new int[2];
                    v.getLocationOnScreen(loc);



//                    relAnim.setLayoutParams(params);


//                    int left = getRelativeLeft(v);
//                    int right = getRelativeTop(v);
//                    relAnim.setLeft(getRelativeLeft(v));
//                    relAnim.setTop(getRelativeTop(v));
//                    relAnim.setY(v.getY());
//                    relAnim.setX(v.getX());


                    relAnimImgTeam.setBackgroundResource(R.drawable.team_plceholder_selected);
                    relAnim.setBackgroundResource(R.drawable.selected_et_border);
                    relAnimText.setText(list.get(position).getName());

                    if (list.get(position).getGender()==0)
                    {
                        relAnimImg.setBackgroundResource(R.drawable.boy);
                    }
                    else if (list.get(position).getGender()==1) {
                        relAnimImg.setBackgroundResource(R.drawable.girl);
                    }

                    btnCreateTeam.setBackgroundResource(R.drawable.big_btn_bg);
                    btnCreateTeam.setEnabled(true);

                    DisplayMetrics displaymetrics = new DisplayMetrics();
                    getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
                    int height = displaymetrics.heightPixels;
                    int width = displaymetrics.widthPixels;

                    Animation animAnticipate = null;
//                    if (loc[1]>(height/4)){
//                        animAnticipate = AnimationUtils.loadAnimation(context, R.anim.accelrate_btm);
//                    }
//                    else
                    if (loc[1]<(height/4)){
                        animAnticipate = AnimationUtils.loadAnimation(context, R.anim.accelrate_top);
                    }
                    else if (loc[1]<(height/3)){
                        animAnticipate = AnimationUtils.loadAnimation(context, R.anim.accelrate);
                    }
                    else if (loc[1]<(height/2)){
                        animAnticipate = AnimationUtils.loadAnimation(context, R.anim.accelrate_btm);
                    }
                    else if (loc[1]>(height/2) && loc[1]<((height/2)+(height/4)/2)){
                        animAnticipate = AnimationUtils.loadAnimation(context, R.anim.accelrate_scnd_most_btm);
                    }
                    else {
                        animAnticipate = AnimationUtils.loadAnimation(context, R.anim.accelrate_most_btm);
                    }

                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(v.getWidth(),v.getHeight());
                    params.leftMargin = getRelativeLeft(v); //(int)Functions.getTopLeftCorner(v).x; //getRelativeLeft(v);
                    params.topMargin = loc[1]; //(int)Functions.getTopLeftCorner(v).y; //getRelativeTop(v) - (v.getHeight()/2);
//                    params.addRule(RelativeLayout.ALIGN_TOP,v.getId());

                    relAnim.setLayoutParams(params);

//                    relAnim.setTranslationX(v.getX());
//                    relAnim.setTranslationY(v.getY());
//
                    relAnim.startAnimation(animAnticipate);
                    relAnim.bringToFront();

                    AccelerateDecelerateInterpolator acc = new AccelerateDecelerateInterpolator();


                    Animation.AnimationListener anim = new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            relAnim.setVisibility(View.INVISIBLE);

                            btnCreateTeam.setBackgroundResource(R.drawable.big_disable_btn_bg);
                            btnCreateTeam.setEnabled(false);

                            goNextActivity();
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    };


                    animAnticipate.setAnimationListener(anim);


//

                    notifyDataSetChanged();
                }
            });

            return view;
        }
    }

    private int getRelativeLeft(View myView) {
        if (myView.getParent() == myView.getRootView())
            return myView.getLeft();
        else
            return myView.getLeft() + getRelativeLeft((View) myView.getParent());
    }

    private int getRelativeTop(View myView) {
        if (myView.getParent() == myView.getRootView())
            return myView.getTop();
        else
            return myView.getTop() + getRelativeTop((View) myView.getParent());
    }

    public class Animations {
        public Animation fromAtoB(float fromX, float fromY, float toX, float toY, Animation.AnimationListener l, int speed){


            Animation fromAtoB = new TranslateAnimation(
                    Animation.ABSOLUTE, //from xType
                    fromX,
                    Animation.ABSOLUTE, //to xType
                    toX,
                    Animation.ABSOLUTE, //from yType
                    fromY,
                    Animation.ABSOLUTE, //to yType
                    toY
            );

            fromAtoB.setDuration(speed);
            fromAtoB.setInterpolator(new AnticipateOvershootInterpolator(1.0f));


            if(l != null)
                fromAtoB.setAnimationListener(l);
            return fromAtoB;
        }
    }

    Animation.AnimationListener animL = new Animation.AnimationListener() {

        @Override
        public void onAnimationStart(Animation animation) {
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }

        @Override
        public void onAnimationEnd(Animation animation) {
            //this is just a method call you can create to delete the animated view or hide it until you need it again.
//            clearAnimation();
        }
    };

}
