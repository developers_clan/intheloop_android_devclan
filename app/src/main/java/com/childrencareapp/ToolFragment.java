package com.childrencareapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.childrencareapp.R;


public class ToolFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_tool, null, false);

        Button btn = (Button) root.findViewById(R.id.btn_cancel);

//        btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                android.support.v4.app.FragmentManager fragmentManager = getFragmentManager();
//                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                android.support.v4.app.Fragment fragment = fragmentManager.findFragmentById(R.id.fragment);
//                fragmentTransaction.setCustomAnimations(R.anim.slide_bottom,R.anim.slide_bottom);
//                fragmentTransaction.hide(fragment);
//                fragmentTransaction.commit();
//            }
//        });

        return root;
    }
}
