package com.childrencareapp;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import Database.MyDbHelper;
import Model.AddChildTeamModel;
import Model.AddContactModel;
import util.AppManager;
import util.Constants;
import util.MyApp;
import adapter.GenderAdapter;

public class AddContactActivity extends Activity {

    EditText txtName, txtAgency, txtDiscipline, txtPhone, txtExt, txtEmail;
    MyDbHelper dbHelper;
    Spinner spn;
    boolean isUpdate;
    int contactID;

    Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_add_contact);

        AppManager.getInstance().setFont(this);

        // Obtain the shared Tracker instance.
        MyApp application = (MyApp) getApplication();
        mTracker = application.getDefaultTracker();

        dbHelper = new MyDbHelper(this);

        txtName = (EditText) findViewById(R.id.et_name);
        txtAgency = (EditText) findViewById(R.id.et_agency);
        txtDiscipline = (EditText) findViewById(R.id.et_discipline);
        txtPhone = (EditText) findViewById(R.id.et_phone);
        txtExt = (EditText) findViewById(R.id.et_ext);
        txtEmail = (EditText) findViewById(R.id.et_email);
        TextView heading = (TextView) findViewById(R.id.heading);
        TextView text = (TextView) findViewById(R.id.text);
        TextView fields = (TextView) findViewById(R.id.fields);
        TextView tvCancel = (TextView) findViewById(R.id.tv_cancel);
        Button btnAdd = (Button) findViewById(R.id.btn_add);
        spn = (Spinner) findViewById(R.id.spinner);
        ScrollView scrollView = (ScrollView) findViewById(R.id.scrollView);
        scrollView.fullScroll(View.FOCUS_UP);

        List<String> strList = new ArrayList<String>();
        strList.add("Male");
        strList.add("Female");
        strList.add("Set Gender(Male or Female)*");

        GenderAdapter adapter = new GenderAdapter(this, R.layout.custom_spnr, strList);
        spn.setAdapter(adapter);
        spn.setSelection(adapter.getCount());
        spn.setDropDownVerticalOffset(1);

        heading.setTypeface(AppManager.getInstance().getBlackTypeface());
        text.setTypeface(AppManager.getInstance().getRegularTypeface());
        fields.setTypeface(AppManager.getInstance().getRegularTypeface());
        btnAdd.setTypeface(AppManager.getInstance().getBoldTypeface());
        tvCancel.setTypeface(AppManager.getInstance().getBoldTypeface());
        txtName.setTypeface(AppManager.getInstance().getRegularTypeface());
        txtAgency.setTypeface(AppManager.getInstance().getRegularTypeface());
        txtDiscipline.setTypeface(AppManager.getInstance().getRegularTypeface());
        txtPhone.setTypeface(AppManager.getInstance().getRegularTypeface());
        txtExt.setTypeface(AppManager.getInstance().getRegularTypeface());
        txtEmail.setTypeface(AppManager.getInstance().getRegularTypeface());

        //////underline text////
        SpannableString content = new SpannableString("Cancel");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        tvCancel.setText(content);
        ////// end underline text////

        Bundle bundle = getIntent().getExtras();
        if (bundle!=null){
            btnAdd.setText("UPDATE CONTACT");
            heading.setText("UPDATE CONTACT");
            String value = bundle.getString("DATA");

            GsonBuilder gsonb = new GsonBuilder();
            Gson gson = gsonb.create();
            Type typeb = new TypeToken<AddContactModel>() {
            }.getType();
            AddContactModel contact = gson.fromJson(value, typeb);
            isUpdate = true;

            contactID = contact.getId();
            txtName.setText(contact.getName());
            txtAgency.setText(contact.getAgency());
            txtDiscipline.setText(contact.getDiscipline());
            txtPhone.setText(contact.getPhone());
            txtExt.setText(contact.getExtension());
            txtEmail.setText(contact.getEmail());
            if (contact.getGender().equalsIgnoreCase("Male")){
                spn.setSelection(0);
            }
            else if (contact.getGender().equalsIgnoreCase("Female")){
                spn.setSelection(1);
            }
        }
        else {
            btnAdd.setText("ADD CONTACT");
            heading.setText("ADD CONTACT");
            isUpdate = false;
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        mTracker.setScreenName(Constants.AddContactActivity);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

    }

    public void onClick(View v) {
        if (v.getId() == R.id.btn_back || v.getId() == R.id.tv_cancel) {
            finish();
        }
        else if (v.getId() == R.id.btn_add) {

            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory(Constants.kCatTeam)
                    .setAction(Constants.AddContact)
                    .build());

            if (txtName.getText().toString().equals("")) {
                txtName.setBackgroundResource(R.drawable.txt_error);
//                Toast.makeText(getApplicationContext(),"Enter Name",Toast.LENGTH_SHORT).show();
                return;
            }
            if ((spn.getSelectedItemPosition() == 2)) {
                txtName.setBackgroundResource(R.drawable.textfield_bg);
                spn.setBackgroundResource(R.drawable.txt_error);
//                Toast.makeText(getApplicationContext(),"Select Gender",Toast.LENGTH_SHORT).show();
                return;
            }
//            else if (txtDiscipline.getText().toString().equals(""))
//            {
//                Toast.makeText(getApplicationContext(),"Enter Discipline",Toast.LENGTH_SHORT).show();
//                return;
//            }
//            else if (txtAgency.getText().toString().equals(""))
//            {
//                Toast.makeText(getApplicationContext(),"Enter Agency",Toast.LENGTH_SHORT).show();
//                return;
//            }
//            else if (txtPhone.getText().toString().equals(""))
//            {
//                Toast.makeText(getApplicationContext(),"Enter Phone",Toast.LENGTH_SHORT).show();
//                return;
//            }
            else if (txtPhone.getText().toString().equals(""))
            {
                txtName.setBackgroundResource(R.drawable.textfield_bg);
                spn.setBackgroundResource(R.drawable.spn_bg);
                txtPhone.setBackgroundResource(R.drawable.txt_error);
//                Toast.makeText(getApplicationContext(),"Enter Email",Toast.LENGTH_SHORT).show();
                return;
            }

            txtName.setBackgroundResource(R.drawable.textfield_bg);
            spn.setBackgroundResource(R.drawable.spn_bg);
            txtPhone.setBackgroundResource(R.drawable.textfield_bg);

            AddContactModel model = new AddContactModel();
            model.setNodeId("");
            model.setAgency(txtAgency.getText().toString());
            model.setName(txtName.getText().toString());
            model.setDiscipline(txtDiscipline.getText().toString());
            model.setPhone(txtPhone.getText().toString());
            model.setExtension(txtExt.getText().toString());
            model.setEmail(txtEmail.getText().toString());
            model.setGender(spn.getSelectedItem().toString());

            if (isUpdate){
                if (dbHelper.updateLocalContact(model,"","","","",contactID)){
                    finish();
                }
                else {
                    Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
                }
            }
            else {
                if (dbHelper.addNewContact(model, "", "", "", "")) {
//                    Toast.makeText(getApplicationContext(),"Saved",Toast.LENGTH_SHORT).show();
//                    Intent intent = new Intent(AddContactActivity.this,ChildViewActivity.class);
//                    intent.putExtra("ADDCONTACT",true);
//                    Gson gson = new Gson();
//                    String value = gson.toJson(model);
//                    intent.putExtra("CONTACT",value);
//                    startActivity(intent);

                    SharedPreferences pref = getSharedPreferences(Constants.WWAACHILD_PREF, Activity.MODE_PRIVATE);
                    if (pref.contains(Constants.CHILD_POS)) {
                        int pos = pref.getInt(Constants.CHILD_POS, -1);
                        int childId = AppManager.getInstance().getChildModels().get(pos).getId();

                        AddChildTeamModel childModel = new AddChildTeamModel();
                        childModel.setChildId(childId);
                        childModel.setContactId(model.getId());
                        Gson gson = new Gson();
                        String value = gson.toJson(model);
                        childModel.setContact(value);

                        if (dbHelper.addChildTeam(childModel)) {
                            finish();
                        }
                    }

                } else {
                    Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
                }
            }
        }

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        boolean handleReturn = super.dispatchTouchEvent(ev);

        View view = getCurrentFocus();

        int x = (int) ev.getX();
        int y = (int) ev.getY();

        if (view instanceof EditText) {
            View innerView = getCurrentFocus();

            if (ev.getAction() == MotionEvent.ACTION_UP && !(getLocationOnScreen(txtName).contains(x, y) || getLocationOnScreen(txtAgency).contains(x, y)
                    || getLocationOnScreen(txtDiscipline).contains(x, y) || getLocationOnScreen(txtEmail).contains(x, y)
                    || getLocationOnScreen(txtExt).contains(x, y) || getLocationOnScreen(txtPhone).contains(x, y))) {

                try {
                    InputMethodManager input = (InputMethodManager)
                            getSystemService(Context.INPUT_METHOD_SERVICE);
                    input.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
                } catch (Exception e) {
                    Log.e("NULL", e.getLocalizedMessage());
                }
            }
        }

        return handleReturn;
    }

    private Rect getLocationOnScreen(EditText mEditText) {
        Rect mRect = new Rect();
        int[] location = new int[2];

        mEditText.getLocationOnScreen(location);

        mRect.left = location[0];
        mRect.top = location[1];
        mRect.right = location[0] + mEditText.getWidth();
        mRect.bottom = location[1] + mEditText.getHeight();

        return mRect;
    }
}
