package com.childrencareapp;

import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import util.AppManager;

public class ToolsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_tool);


        TextView heading = (TextView) findViewById(R.id.heading);
        Button btnCAl = (Button) findViewById(R.id.btnCalender);
        Button btnNOtes = (Button) findViewById(R.id.btnNotes);

        btnNOtes.setTypeface(AppManager.getInstance().getBoldTypeface());
        btnCAl.setTypeface(AppManager.getInstance().getBoldTypeface());
        heading.setTypeface(AppManager.getInstance().getBlackTypeface());

    }

    public void onClick(View v)
    {
        if (v.getId() == R.id.btnCalender){
//            Intent i = new Intent();
//            i.setClassName("com.android.calendar","com.android.calendar.AgendaActivity");
//            startActivity(i);
            ComponentName cn;
            Intent i = new Intent();
            cn = new ComponentName("com.android.calendar", "com.android.calendar.LaunchActivity");
            i.setComponent(cn);
            startActivity(i);
        }
        else if (v.getId() == R.id.btnNotes){
//            try {
//               Intent intent = new Intent(Intent.ACTION_EDIT);
////                Uri uri = Uri.parse("");
////               intent.setDataAndType(uri, "text/plain");
//               startActivity(intent);
//
//                } catch (ActivityNotFoundException e) {
//                Toast toast = Toast.makeText(this, "No editor on this device", Toast.LENGTH_SHORT);
//                toast.show();
//             }
        }
    }

}
