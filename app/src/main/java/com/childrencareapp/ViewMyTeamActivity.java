package com.childrencareapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Activity;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import Database.MyDbHelper;
import Model.AddChildTeamModel;
import Model.AddContactModel;
import util.AppManager;
import util.Constants;
import util.MyApp;
import adapter.TeamCustomAdapter;

public class ViewMyTeamActivity extends Activity {

    int childId;
    private static ExpandableListView listView;
    MyDbHelper dbHelper;
    ArrayList<AddContactModel> contactList;
//    public static ArrayList<Integer> childTeamIds;

    public static TextView tvCount;
    private static int hieght;
    View childView;
    private static View view;
    Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_view_my_team);

        AppManager.getInstance().setFont(this);
        dbHelper = new MyDbHelper(this);

        // Obtain the shared Tracker instance.
        MyApp application = (MyApp) getApplication();
        mTracker = application.getDefaultTracker();

        TextView tvChildName = (TextView) findViewById(R.id.tv_childname);
        TextView heading = (TextView) findViewById(R.id.heading);
        TextView text = (TextView) findViewById(R.id.tv_body);
        Button btnAddMore = (Button) findViewById(R.id.btn_addmore);
        ImageView imgChild = (ImageView)findViewById(R.id.imgChild);
        tvCount = (TextView) findViewById(R.id.tvCount);

        heading.setTypeface(AppManager.getInstance().getBlackTypeface());
        text.setTypeface(AppManager.getInstance().getRegularTypeface());
        tvChildName.setTypeface(AppManager.getInstance().getRegularTypeface());
        btnAddMore.setTypeface(AppManager.getInstance().getBoldTypeface());

        Bundle bundle = getIntent().getExtras();
        if (bundle != null)
        {
            int pos = bundle.getInt("CHILD_ID");
            childId = AppManager.getInstance().getChildModels().get(pos).getId();
            tvChildName.setText(AppManager.getInstance().getChildModels().get(pos).getName());
            if (AppManager.getInstance().getChildModels().get(pos).getGender() == 0){
                imgChild.setBackgroundResource(R.drawable.my_team_user_boy_place_holder);
            }
            else if (AppManager.getInstance().getChildModels().get(pos).getGender() == 1){
                imgChild.setBackgroundResource(R.drawable.my_team_user_girl_place_holder);
            }
        }
        else
        {
            SharedPreferences pref = getSharedPreferences(Constants.WWAACHILD_PREF, Activity.MODE_PRIVATE);
            if (pref.contains(Constants.CHILD_POS)) {
                int pos = pref.getInt(Constants.CHILD_POS, -1);
                childId = AppManager.getInstance().getChildModels().get(pos).getId();
                tvChildName.setText(AppManager.getInstance().getChildModels().get(pos).getName());
                tvChildName.setText(AppManager.getInstance().getChildModels().get(pos).getName());
                if (AppManager.getInstance().getChildModels().get(pos).getGender() == 0){
                    imgChild.setBackgroundResource(R.drawable.my_team_user_boy_place_holder);
                }
                else if (AppManager.getInstance().getChildModels().get(pos).getGender() == 1){
                    imgChild.setBackgroundResource(R.drawable.my_team_user_girl_place_holder);
                }
            }
        }

        listView = (ExpandableListView) findViewById(R.id.expandableListView);

        contactList = new ArrayList<>();
//        childTeamIds = new ArrayList<>();
        ArrayList<AddChildTeamModel> childTeam = dbHelper.getChildTeam(childId);
        AppManager.getInstance().childTeamIds.clear();
        for (int i = 0;i<childTeam.size();i++)
        {
            GsonBuilder gsonb = new GsonBuilder();
            Gson gson = gsonb.create();
            Type type = new TypeToken<AddContactModel>() {
            }.getType();
            AddContactModel model = gson.fromJson(childTeam.get(i).getContact(), type);
            AppManager.getInstance().childTeamIds.add(childTeam.get(i).getId());
            contactList.add(model);
        }

        AppManager.getInstance().setIsView(true);
        TeamCustomAdapter adapter = new TeamCustomAdapter(contactList,getParent(),mTracker);
        listView.setAdapter(adapter);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int width = metrics.widthPixels;
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
            listView.setIndicatorBounds(width - GetPixelFromDips(50), width - GetPixelFromDips(10));
            listView.setIndicatorBounds(width - GetPixelFromDips(50), width - GetPixelFromDips(10));
        } else {
            listView.setIndicatorBoundsRelative(width - GetPixelFromDips(50), width - GetPixelFromDips(10));
            listView.setIndicatorBoundsRelative(width - GetPixelFromDips(50), width - GetPixelFromDips(10));
        }

        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.team_list_parent_vertical, null, false);
        childView = inflater.inflate(R.layout.list_item_child_vertical, null, false);
//        childList = inflater.inflate(R.layout.childlist, null, false);

        updateUI(contactList.size());

        tvCount.setText("" + dbHelper.getChildTeam(childId).size());

        listView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                if (contactList.get(groupPosition).getOfficeList().size() > 0) {
                    return false; //return parent.isGroupExpanded(groupPosition);
                } else {
                    return true; //!parent.isGroupExpanded(groupPosition);
                }
            }
        });

        listView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {

                if (contactList.get(groupPosition).getOfficeList() != null && contactList.get(groupPosition).getOfficeList().size() > 0) {
                    int h = childView.getMinimumHeight() + 40;
                    int s = contactList.get(groupPosition).getOfficeList().size();
                    int phoneSize = 0;
                    if (contactList.get(groupPosition).getPhoneList() != null) {
                        phoneSize = contactList.get(groupPosition).getPhoneList().size() * GetPixelFromDips(30);
                    }
                    int childListH = (h * s) + GetPixelFromDips(60) + phoneSize;

                    //AppManager.getInstance().setListHieght(childListH);

                    hieght += childListH;

                    ViewGroup.LayoutParams prams = listView.getLayoutParams();
                    prams.height = hieght; //AppManager.getInstance().getListHieght();
                    listView.setLayoutParams(prams);
                }

            }
        });


        listView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {

                if (contactList.get(groupPosition).getOfficeList() != null && contactList.get(groupPosition).getOfficeList().size() > 0) {
                    int h = childView.getMinimumHeight() + 40;
                    int s = contactList.get(groupPosition).getOfficeList().size();

                    int phoneSize = 0;
                    if (contactList.get(groupPosition).getPhoneList() != null) {
                        phoneSize = contactList.get(groupPosition).getPhoneList().size() * GetPixelFromDips(30);
                    }

                    int childListH = (h * s) + GetPixelFromDips(60) + phoneSize;
                    hieght -= childListH;

                    ViewGroup.LayoutParams prams = listView.getLayoutParams();
                    prams.height = hieght; //AppManager.getInstance().getListHieght();
                    listView.setLayoutParams(prams);
                }
            }
        });

        listView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                if (parent.isGroupExpanded(groupPosition)) {
                    parent.collapseGroup(groupPosition);
                }
                return true;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        mTracker.setScreenName(Constants.ViewMyTeam);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

    }

    public void onClick(View v) {
        if(v.getId() == R.id.btn_back)
        {
            try {
                finish();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        else if (v.getId() == R.id.tv_addcontact) {

            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory(Constants.kCatTeam)
                    .setAction(Constants.AddContact)
                    .build());

            startActivity(new Intent(ViewMyTeamActivity.this, AddContactActivity.class));
        }
        else if (v.getId() == R.id.tv_childname) {
            try {
                finish();
                Intent intent = new Intent(getParent(), MyTeamActivity.class);
                TabGroupActivity parentActivity = (TabGroupActivity) getParent();
                parentActivity.startChildActivity("MyTeamActivity", intent);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        else if (v.getId() == R.id.btn_addmore) {

            try {
                finish();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public int GetPixelFromDips(float pixels) {
        // Get the screen's density scale
        final float scale = getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f);
    }

    public static void updateUI(int count){
        int h = ((RelativeLayout) view.findViewById(R.id.rel)).getMinimumHeight();
        h = h + 20;
        hieght = h * count;

        ViewGroup.LayoutParams prams = listView.getLayoutParams();
        prams.height = hieght;
        listView.setLayoutParams(prams);
    }
}
