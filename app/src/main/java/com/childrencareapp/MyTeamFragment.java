package com.childrencareapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.kogitune.activity_transition.ActivityTransitionLauncher;


public class MyTeamFragment extends Fragment {

    public  static RelativeLayout rl;
    Button btnCreateTeam;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_my_team, null, false);

        rl = (RelativeLayout) root.findViewById(R.id.rl);
        btnCreateTeam = (Button)root.findViewById(R.id.btn_create_team);

        btnCreateTeam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(),TeamActivity.class);
//                startActivity(intent);
                ActivityTransitionLauncher
                        .with(getActivity())

                        .from(v)
                        .launch(intent);
            }
        });

        return root;
    }

}
