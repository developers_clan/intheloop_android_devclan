package com.childrencareapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import Database.MyDbHelper;
import util.AppManager;

public class TabGroup3Activity extends TabGroupActivity{
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        MyDbHelper dbHelper = new MyDbHelper(getParent());


        if(dbHelper.getData().size()>0) {
            AppManager.getInstance().setIsParent(true);
            startChildActivity("MyTeamActivity", new Intent(this, MyTeamActivity.class));
        }
        else
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(getParent());
            builder.setPositiveButton("Go To Account Settings", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

//                            Toast.makeText(getApplicationContext(), "You have not setup your account. To proceed, please add your child/children.", Toast.LENGTH_SHORT).show();
                    if (TabActivity.tabHost != null) {
                        TabActivity.tabHost.setCurrentTab(1);
                    }
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    AppManager.getInstance().setIsParent(true);
                    startChildActivity("MyTeamActivity", new Intent(TabGroup3Activity.this, MyTeamActivity.class));
                }
            });
            builder.setMessage("You have not setup your account. To proceed, please add your child/children.");
            builder.create().show();
//            if (TabActivity.tabHost != null) {
//                TabActivity.tabHost.setCurrentTab(1);
//            }
        }
    }
}
