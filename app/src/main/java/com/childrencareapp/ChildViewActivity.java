package com.childrencareapp;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;

import Database.MyDbHelper;
import Model.ChildModel;
import adapter.ChildListAdapter;
import util.AppManager;
import util.Constants;
import util.MyApp;

public class ChildViewActivity extends Activity {

    ListView listView;
    Button btnAdd;
    Boolean flag = false;
    String contact;
    TextView tvCon;
    public static Activity ref;
    public static RelativeLayout rl;
    Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_child_view);

        // Obtain the shared Tracker instance.
        MyApp application = (MyApp) getApplication();
        mTracker = application.getDefaultTracker();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null)
        {
            flag = bundle.getBoolean("ADDCONTACT");
            contact = bundle.getString("CONTACT");
        }

        ref = getParent();

        rl = (RelativeLayout) findViewById(R.id.rl);
        listView = (ListView) findViewById(R.id.listView);
        btnAdd = (Button) findViewById(R.id.btn_add);
        ImageButton btnBack = (ImageButton) findViewById(R.id.btn_back);
        TextView tvBody = (TextView) findViewById(R.id.tv_body);
        TextView tvTitle = (TextView) findViewById(R.id.tv_title);
        tvCon = (TextView) findViewById(R.id.tv_con);

        //////underline text////
        SpannableString content = new SpannableString("Continue");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        tvCon.setText(content);
        ////// end underline text////

        tvTitle.setTypeface(AppManager.getInstance().getBlackTypeface());
        tvBody.setTypeface(AppManager.getInstance().getRegularTypeface());
        tvCon.setTypeface(AppManager.getInstance().getBoldTypeface());
        btnAdd.setTypeface(AppManager.getInstance().getBoldTypeface());

        if (flag)
        {
            tvTitle.setText("Add Contact To:");
            tvBody.setVisibility(View.GONE);
            btnAdd.setVisibility(View.GONE);
            btnBack.setVisibility(View.VISIBLE);
            tvCon.setVisibility(View.GONE);


        }
        else
        {
            tvTitle.setText("ACCOUNT SETTINGS");
            tvBody.setVisibility(View.VISIBLE);
            btnAdd.setVisibility(View.VISIBLE);
            btnBack.setVisibility(View.GONE);
            tvCon.setVisibility(View.VISIBLE);


        }

        if (AppManager.getInstance().isParent()){
            //get the sunrise animation
            Animation sunRise = AnimationUtils.loadAnimation(ChildViewActivity.this, R.anim.sun_rise_two);
//                            AccountSetupFragment.rl.setVisibility(View.GONE);
            if (rl != null) {
                rl.startAnimation(sunRise);
            }
        }


        ///////Animation/////
//        Animation anim = AnimationUtils.loadAnimation(ChildViewActivity.this, R.anim.slide_in_right);
//        findViewById(R.id.rl).startAnimation(anim);

//        MyDbHelper dbHelper = new MyDbHelper(this);
//        final ArrayList<ChildModel> list = dbHelper.getData();
//        if (list.size()==1) {
//            SharedPreferences child_pr = getSharedPreferences(Constants.WWAACHILD_PREF, Activity.MODE_PRIVATE);
//            SharedPreferences.Editor editor = child_pr.edit();
//            editor.clear();
//            editor.putInt(Constants.CHILD_POS, 0);
//            editor.commit();
//        }
//        AppManager.getInstance().setChildModels(list);
//
////        String[] arr = {"JOE SMITH","SAMANTHA SMITH"};
//        if (AppManager.getInstance().isParent()) {
//            tvCon.setVisibility(View.GONE);
//            ChildListAdapter adapter = new ChildListAdapter(getParent(), R.layout.custom_childview, list,flag,contact);
//            listView.setAdapter(adapter);
//
//            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
//
//            params.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
//            btnAdd.setLayoutParams(params);
//
//        }
//        else
//        {
//
//            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
//            params.addRule(RelativeLayout.CENTER_HORIZONTAL, 0);
//            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
//            btnAdd.setLayoutParams(params);
//
//            tvCon.setVisibility(View.VISIBLE);
//            ChildListAdapter adapter = new ChildListAdapter(this,R.layout.custom_childview,list,flag,contact);
//            listView.setAdapter(adapter);
//        }

//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//                for(int i=0;i<list.size();i++) {
//                    if(i==position) {
//                        view.findViewById(R.id.relative).setBackgroundColor(Color.GRAY);
//                        SharedPreferences child_pr = getSharedPreferences(Constants.WWAACHILD_PREF, Activity.MODE_PRIVATE);
//                        SharedPreferences.Editor editor = child_pr.edit();
//                        editor.clear();
//                        editor.putInt(Constants.CHILD_POS, position);
//                        editor.commit();
//                    }
//                    else
//                    {
////                        view.findViewById(R.id.relative).setBackgroundColor(Color.parseColor("#CBCCCB"));
//                    }
//
//                }
//            }
//        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (AppManager.getInstance().isParent()) {
//                    finish();
                    Intent intent = new Intent(getParent(), AccountSettingActivity.class);
                    TabGroupActivity parentActivity = (TabGroupActivity) getParent();
                    parentActivity.startChildActivity("AccountSettingActivity", intent);

                }
                else {
                    Intent intent = new Intent(ChildViewActivity.this, AccountSettingActivity.class);
                    startActivity(intent);
                    finish();
                }


            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tvCon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChildViewActivity.this, TabActivity.class);
                startActivity(intent);
                finish();
            }
        });


    }

    public static void goBack(){
        Intent intent = new Intent(ref, AccountSettingActivity.class);
        TabGroupActivity parentActivity = (TabGroupActivity) ref;
        parentActivity.startChildActivity("AccountSettingActivity", intent);
    }

    @Override
    protected void onResume() {

        mTracker.setScreenName(Constants.AccountSettingActivity);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        MyDbHelper dbHelper = new MyDbHelper(this);
        final ArrayList<ChildModel> list = dbHelper.getData();
        if (list.size()==1) {
            SharedPreferences child_pr = getSharedPreferences(Constants.WWAACHILD_PREF, Activity.MODE_PRIVATE);
            SharedPreferences.Editor editor = child_pr.edit();
            editor.clear();
            editor.putInt(Constants.CHILD_POS, 0);
            editor.commit();
        }
        AppManager.getInstance().setChildModels(list);

//        String[] arr = {"JOE SMITH","SAMANTHA SMITH"};
        if (AppManager.getInstance().isParent()) {
            tvCon.setVisibility(View.GONE);
            ChildListAdapter adapter = new ChildListAdapter(getParent(), R.layout.custom_childview, list,flag,contact,mTracker);
            listView.setAdapter(adapter);

            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

            params.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
            btnAdd.setLayoutParams(params);

        }
        else
        {

            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.CENTER_HORIZONTAL, 0);
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
            btnAdd.setLayoutParams(params);

            tvCon.setVisibility(View.VISIBLE);
            ChildListAdapter adapter = new ChildListAdapter(this,R.layout.custom_childview,list,flag,contact,mTracker);
            listView.setAdapter(adapter);
        }

        super.onResume();

    }
}
