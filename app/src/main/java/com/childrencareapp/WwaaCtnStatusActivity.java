package com.childrencareapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import Database.MyDbHelper;
import Model.Agency;
import util.AppManager;
import Model.CtnStatusModel;
import Model.Wwaa;
import util.Constants;
import adapter.WWAACTNAdapter;

public class WwaaCtnStatusActivity extends Activity {

//    int index;
    String status = null;
    int pos;
    MyDbHelper dbHelper;

    List<Wwaa> list;
    WWAACTNAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_wwaa_ctn_status);

        AppManager.getInstance().setFont(this);

        TextView heading = (TextView) findViewById(R.id.heading);
        ImageView img_agency = (ImageView)findViewById(R.id.img_agency);
        TextView tvChildName = (TextView) findViewById(R.id.tv_child_name);
        TextView tvBody = (TextView) findViewById(R.id.tv_body);
        ListView listView = (ListView) findViewById(R.id.listView);
        Button btnSave = (Button)findViewById(R.id.btn_save);
        TextView contact = (TextView) findViewById(R.id.contact);
        TextView tvPhone = (TextView) findViewById(R.id.tv_phone);
        TextView tvWebsite = (TextView) findViewById(R.id.tv_website);
        TextView tvConnect = (TextView) findViewById(R.id.tv_connect);


        heading.setTypeface(AppManager.getInstance().getBlackTypeface());
        btnSave.setTypeface(AppManager.getInstance().getBoldTypeface());
        contact.setTypeface(AppManager.getInstance().getBlackTypeface());
        tvPhone.setTypeface(AppManager.getInstance().getRegularTypeface());
        tvWebsite.setTypeface(AppManager.getInstance().getRegularTypeface());
        tvConnect.setTypeface(AppManager.getInstance().getRegularTypeface());
        tvChildName.setTypeface(AppManager.getInstance().getRegularTypeface());

        dbHelper = new MyDbHelper(this);

//        Bundle bundle = getIntent().getExtras();
//        if(bundle != null)
//        {
//            index = bundle.getInt("INDEX");
//        }

        Agency agency = AppManager.getInstance().getAgencyModel();
//        heading.setText(agency.getCategory());
//        tvBody.setText(agency.getBody());
        tvPhone.setText(agency.getAgencyPhone().get(0).getNumber());

        SharedPreferences pref = getSharedPreferences(Constants.WWAACHILD_PREF,Activity.MODE_PRIVATE);
        if(pref.contains(Constants.CHILD_POS))
        {
            pos = pref.getInt(Constants.CHILD_POS,-1);
            tvChildName.setText(AppManager.getInstance().getChildModels().get(pos).getName());
        }

        list = agency.getWwaa();
        adapter = new WWAACTNAdapter(this,R.layout.custom_wwaa_ctn_status_list,list);
        listView.setAdapter(adapter);



    }

    public void onClick(View v)
    {
        if (v.getId()==R.id.btn_back)
        {
            Intent intent = new Intent(WwaaCtnStatusActivity.this, WwaaCTNActivity.class);
            startActivity(intent);
            finish();
        }
        else if (v.getId()==R.id.btn_save)
        {
            if(adapter.p > -1)
            {
//                status = list.get(adapter.p).getWWAAStatus().getMarkup();
            }

            if (status!=null) {
                CtnStatusModel model = new CtnStatusModel();
                model.setChildId(AppManager.getInstance().getChildModels().get(pos).getId());
                model.setStatus(status);

                Date date = new Date();
                String strTimeFormat = "hh:mm a";
                String strDateFormat = "MMM-dd-yyyy";
                DateFormat timeFormat = new SimpleDateFormat(strTimeFormat, Locale.US);
                DateFormat dateFormat = new SimpleDateFormat(strDateFormat, Locale.US);
                String formattedTime= timeFormat.format(date);
                String formattedDate= dateFormat.format(date);

                model.setDate(formattedDate);
                model.setTime(formattedTime);

                if (dbHelper.addNewStatus(model))
                {
                    Toast.makeText(getApplicationContext(),"Saved",Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(WwaaCtnStatusActivity.this, WwaaCTNActivity.class);
                    startActivity(intent);
                    finish();
                }

            }
            else
            {
                Toast.makeText(getApplicationContext(),"Please Select Status",Toast.LENGTH_SHORT).show();
            }

        }
    }

}
