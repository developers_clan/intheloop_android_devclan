package com.childrencareapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.special.ResideMenu.ResideMenu;
import com.special.ResideMenu.ResideMenuItem;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Database.MyDbHelper;
import Model.Agency;
import Model.AgencyHome;
import Model.Contact;
import Model.ParentData;
import util.AppManager;
import util.Constants;
import util.MyApp;
import thirdparty.VerticalChild;
import thirdparty.VerticalParent;


public class CDPActivity extends Activity implements ExpandableRecyclerAdapter.ExpandCollapseListener {

    ArrayList<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    ImageButton openDrawer;
    ImageButton btnBack;
    RelativeLayout rlList, rlHeader;
    LinearLayout layoutImg;
    ImageView img;
    Animation out;
    String key;
    ProgressDialog pg;
    String summary;

    Agency agency;

    Activity con;


    private RecyclerView mRecyclerView;

    ResideMenu resideMenu;
    WebView webView;

    Tracker mTracker;
//    int index;

    private ResideMenuItem itemWhile;
    private ResideMenuItem itemWhere;
    private ResideMenuItem itemSocial;

    private ResideMenuItem itemFB=null;
    private ResideMenuItem itemTW=null;
    private ResideMenuItem itemPin=null;
    private ResideMenuItem itemYT=null;
    String fbLink = "",twLink ="",pinLink = "",ytLink ="";

    boolean loadingFinished = true;
    boolean redirect = false;

    private final int CELL_DEFAULT_HEIGHT = 200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_cdp);

        AppManager.getInstance().setFont(this);

        // Obtain the shared Tracker instance.
        MyApp application = (MyApp) getApplication();
        mTracker = application.getDefaultTracker();


        con = this;
        openDrawer = (ImageButton) findViewById(R.id.btn_drawer);
        btnBack = (ImageButton) findViewById(R.id.btn_back);
        webView = (WebView) findViewById(R.id.webView);
        TextView heading = (TextView) findViewById(R.id.heading);
        rlHeader = (RelativeLayout) findViewById(R.id.rlHeader);


        heading.setTypeface(AppManager.getInstance().getBlackTypeface());


        agency = AppManager.getInstance().getAgencyModel();

        if (agency.getHomeMainDetails() != null && agency.getHomeMainDetails().size() > 0) {
            heading.setText(agency.getHomeMainDetails().get(0).getHomeMainTitle());
        }

        pg = new ProgressDialog(getParent(), ProgressDialog.THEME_HOLO_DARK);
        pg.setCancelable(false);
        pg.setTitle("Please Wait");
        pg.setMessage("Loading...");


        WebSettings webSettings = webView.getSettings();
//        webSettings.setRenderPriority(WebSettings.RenderPriority.HIGH);
//        webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        webSettings.setJavaScriptEnabled(true);

        webView.setWebViewClient(new HelloWebViewClient());
        if (Build.VERSION.SDK_INT >= 19) {
            webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }


        byte[] buffer = null;
        try {
            InputStream is = getAssets().open("AgencyHTML/agency.html");
            int size = is.available();

            buffer = new byte[size];
            is.read(buffer);
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String str = new String(buffer);
        if (AppManager.getInstance().isFromServer()) {/// /agency.getImage().get(0).getUri()

            String[] arr = agency.getImage().get(0).getUri().split("/");
            String name = arr[arr.length - 1];
            str = str.replace("agencyBannerPlaceholder", Constants.PATH+"/"+name);//file:///android_asset/AgencyHTML/background-ctn-banner.png
        } else {
            if (agency.getAgencyImage() != null && !agency.getAgencyImage().equalsIgnoreCase("")) {
                str = str.replace("agencyBannerPlaceholder", agency.getAgencyImage());//file:///android_asset/AgencyHTML/background-ctn-banner.png
            }
        }
        if (agency.getColorCode().size() > 0) {
            str = str.replace("borderColorPlaceholder", agency.getColorCode().get(0));
        }

        if (agency.getHomeMainDetails().size() > 0) {
            str = str.replace("agencyBodyPlaceholder", agency.getHomeMainDetails().get(0).getHomeMainBody());
        }
        byte[] bufferSection = null;
        try {
            InputStream is = getAssets().open("AgencyHTML/sectionListItem.html");
            int size = is.available();

            bufferSection = new byte[size];
            is.read(bufferSection);
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String sections = "";
        for (int i = 0; i < agency.getAgencyHome().size(); i++) {
            AgencyHome section = agency.getAgencyHome().get(i);
            String strSection = new String(bufferSection);
            strSection = strSection.replace("sectionTitlePlaceholder", section.getHomeTitle().getMarkup());
            strSection = strSection.replace("sectionBodyPlaceholder", section.getHomeBody().getMarkup());
            sections += strSection;
        }

        str = str.replace("sectionItemsPlaceholder", sections);
        str = str.replace("resourcesDiv", "");
        if (agency.getAgencyPhone() !=null && agency.getAgencyPhone().size() > 0) {
            str = str.replace("agencyPhoneNumber", agency.getAgencyPhone().get(0).getNumber());
            if (agency.getAgencyPhone().get(0).getNumberAlias() != null && !agency.getAgencyPhone().get(0).getNumberAlias().equalsIgnoreCase("")){
                str = str.replace("agencyDisplayPhoneNumber", agency.getAgencyPhone().get(0).getNumberAlias());
            }
            else {
                str = str.replace("agencyDisplayPhoneNumber", agency.getAgencyPhone().get(0).getNumber());
            }
        } else {
            str = str.replace("agencyPhoneNumber", "");
            str = str.replace("contact-number", "");
        }
        if (agency.getWebsite() !=null && agency.getWebsite().size() > 0) {
            str = str.replace("WebsiteURLPlaceholder", agency.getWebsite().get(0).getUrl());
            str = str.replace("WebsitePlaceholder", agency.getWebsite().get(0).getTitle());
        } else {
            str = str.replace("WebsitePlaceholder", "");
            str = str.replace("website-background", "");
        }
        if (agency.getAgencyEmail() != null && agency.getAgencyEmail().size() > 0) {
            str = str.replace("CDP_ConnectPlaceHolder", agency.getAgencyEmail().get(0).getTitle());
            str = str.replace("agencyConnectUrlPlaceholder", "mailto:" + agency.getAgencyEmail().get(0).getUrl());
            str = str.replace("imgPlaceHolder", "cdp");
        } else if (agency.getCdpConnect() != null && agency.getCdpConnect().size() > 0) {
            str = str.replace("CDP_ConnectPlaceHolder", agency.getCdpConnect().get(0).getTitle());
            str = str.replace("agencyConnectUrlPlaceholder", agency.getCdpConnect().get(0).getUrl());
            str = str.replace("imgPlaceHolder", "events");
        } else {
            str = str.replace("CDP_ConnectPlaceHolder", "");
            str = str.replace("imgPlaceHolder-background", "");
        }

        webView.loadDataWithBaseURL("file:///android_asset/AgencyHTML/stylesheet.css", str, "text/html", "utf-8", "");


        setUpMenu();

        rlHeader.setVisibility(View.VISIBLE);
        rlHeader.setBackgroundColor(getResources().getColor(R.color.red_text));

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CDPActivity.this.finish();
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();

        mTracker.setScreenName(Constants.CDPActivity+"-"+ agency.getHomeMainDetails().get(0).getHomeMainTitle());
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

    }

    private void setUpMenu() {

        Contact contct = null;

        // attach to current activity;
        resideMenu = new ResideMenu(this);
        resideMenu.setUse3D(false);
        resideMenu.setBackground(R.drawable.menu_bar_background);
        resideMenu.attachToActivity(this);
        resideMenu.setShadowVisible(false);
        resideMenu.setMenuListener(menuListener);
        //valid scale factor is between 0.0f and 1.0f. leftmenu'width is 150dip.
        resideMenu.setScaleValue(0.6f);

        for (int i = 0; i < AppManager.getInstance().getContacts().size(); i++) {
            if (agency.getNid().equals(AppManager.getInstance().getContacts().get(i).getAgency())) {
                contct = AppManager.getInstance().getContacts().get(i);
                break;
            }
        }

        // create menu items;
        if (agency.getWwaaMainDetails().size()>0) {
            itemWhere = new ResideMenuItem(this, R.drawable.where_we_are_at, agency.getWwaaMainDetails().get(0).getWwaaMainTitle());
        }
        if (agency.getWywMainDetails().size() > 0) {
            itemWhile = new ResideMenuItem(this, R.drawable.while_you_wait, agency.getWywMainDetails().get(0).getWywMainTitle());
        }



//        itemSocial  = new ResideMenuItem(this, R.drawable.fb,R.drawable.twitter);
        if (contct != null) {
            try {

                for(int i=0;i<contct.getSocialLinks().size();i++) {
                    if (contct.getSocialLinks().get(i).getTitle().equalsIgnoreCase("Facebook")) {
                        fbLink = contct.getSocialLinks().get(i).getUrl();
                        itemFB = new ResideMenuItem(this, R.drawable.fb, "Facebook");
                    }
                }
                for(int i=0;i<contct.getSocialLinks().size();i++) {
                    if (contct.getSocialLinks().get(i).getTitle().equalsIgnoreCase("Twitter")){
                        twLink = contct.getSocialLinks().get(i).getUrl();
                        itemTW = new ResideMenuItem(this, R.drawable.twitter, "Twitter");
                    }

                }
                for(int i=0;i<contct.getSocialLinks().size();i++) {
                    if (contct.getSocialLinks().get(i).getTitle().equalsIgnoreCase("Pinterest")) {
                        pinLink = contct.getSocialLinks().get(i).getUrl();
                        itemPin = new ResideMenuItem(this, R.drawable.menu_bar_pin, "Pinterest");
                    }
                }
                for(int i=0;i<contct.getSocialLinks().size();i++) {
                    if (contct.getSocialLinks().get(i).getTitle().equalsIgnoreCase("YouTube")){
                        ytLink = contct.getSocialLinks().get(i).getUrl();
                        itemYT = new ResideMenuItem(this, R.drawable.menu_bar_yotube, "YouTube");
                    }

                }
//                itemSocial = new ResideMenuItem(this, R.drawable.fb, R.drawable.twitter,R.drawable.menu_bar_pin,R.drawable.menu_bar_yotube, twLink, fbLink,pinLink,ytLink);
            } catch (Exception e) {
                Log.e("CDP MENU", e.getLocalizedMessage());
            }
        } else {
//            itemSocial = new ResideMenuItem(this, R.drawable.fb, R.drawable.twitter);
        }

        itemWhere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory(Constants.kCatSideMenu)
                        .setAction(Constants.MyServicesMenuWYW+"-"+agency.getHomeMainDetails().get(0).getHomeMainTitle())
                        .build());

                if (AppManager.getInstance().getChildModels().size() > 0) {
                    MyDbHelper dbHelper = new MyDbHelper(CDPActivity.this);
                    SharedPreferences pref = getSharedPreferences(Constants.WWAACHILD_PREF, Activity.MODE_PRIVATE);
                    int pos = -1;
                    if (pref.contains(Constants.CHILD_POS)) {
                        pos = pref.getInt(Constants.CHILD_POS, -1);
                    }

                    AppManager.getInstance().setMainDataCDP(setUpTestData());

                    if (AppManager.getInstance().getChildModels().size() > 1) {

                        Intent intent = new Intent(getParent(), MyTeamActivity.class);
                        intent.putExtra("WWAA",true);
                        TabGroupActivity parentActivity = (TabGroupActivity) getParent();
                        parentActivity.startChildActivity("MyTeamActivity", intent);
                    }
                    else {
                        if (dbHelper.getCdpStatus(AppManager.getInstance().getChildModels().get(pos).getId(), agency.getNid()).size() > 0) {
//                        finish();
                            Intent intent = new Intent(getParent(), WwaaCdpActivity.class);
                            TabGroupActivity parentActivity = (TabGroupActivity) getParent();
                            parentActivity.startChildActivity("WwaaCdpActivity", intent);
//                    Intent intent = new Intent(CDPActivity.this, WhileYouWaitActivity.class);
//                    startActivity(intent);
                            resideMenu.closeMenu();
                        } else {
                            AppManager.getInstance().setParentModels(new ArrayList<ParentData>());
//                        finish();
                            Intent intent = new Intent(getParent(), WwaaCdpStatusActivity.class);
                            TabGroupActivity parentActivity = (TabGroupActivity) getParent();
                            parentActivity.startChildActivity("WwaaCdpStatusActivity", intent);
//                    Intent intent = new Intent(CDPActivity.this, WhileYouWaitActivity.class);
//                    startActivity(intent);
                            resideMenu.closeMenu();
                        }
                    }
//                    }
                } else {

                    AlertDialog.Builder builder = new AlertDialog.Builder(getParent());
                    builder.setPositiveButton("Go To Account Settings", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

//                            Toast.makeText(getApplicationContext(), "You have not setup your account. To proceed, please add your child/children.", Toast.LENGTH_SHORT).show();
                            if (TabActivity.tabHost != null) {
                                TabActivity.tabHost.setCurrentTab(1);
                            }
                        }
                    });
                    builder.setNegativeButton("Cancel", null);
                    builder.setMessage("You have not setup your account. To proceed, please add your child/children.");
                    builder.create().show();


                }
                resideMenu.closeMenu();
            }
        });
        itemWhile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory(Constants.kCatSideMenu)
                        .setAction(Constants.WYWMenuWYW + "-" + agency.getHomeMainDetails().get(0).getHomeMainTitle())
                        .build());

                if (AppManager.getInstance().getChildModels().size() > 0) {

//                    finish();
                    Intent intent = new Intent(getParent(), WhileYouWaitActivity.class);
                    TabGroupActivity parentActivity = (TabGroupActivity) getParent();
                    parentActivity.startChildActivity("WhileYouWaitActivity", intent);
//                    Intent intent = new Intent(CDPActivity.this, WhileYouWaitActivity.class);
//                    startActivity(intent);
                    resideMenu.closeMenu();

                } else {
//                    Toast.makeText(getApplicationContext(), "Add Child First!", Toast.LENGTH_SHORT).show();
                    if (TabActivity.tabHost != null){
                        TabActivity.tabHost.setCurrentTab(1);
                    }
                    resideMenu.closeMenu();
                }

            }
        });
        if (itemFB != null) {
            itemFB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!fbLink.equalsIgnoreCase("")) {

                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory(Constants.kCatSideMenu)
                                .setAction(agency.getHomeMainDetails().get(0).getHomeMainTitle() + " - Facebook")
                                .build());

                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(fbLink)));
                    }
                }
            });
        }
        if (itemTW != null) {
            itemTW.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!twLink.equalsIgnoreCase("")) {

                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory(Constants.kCatSideMenu)
                                .setAction(agency.getHomeMainDetails().get(0).getHomeMainTitle() + " - Twitter")
                                .build());
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(twLink)));
                    }
                }
            });
        }
        if (itemPin != null) {
            itemPin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!pinLink.equalsIgnoreCase("")) {
                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory(Constants.kCatSideMenu)
                                .setAction(agency.getHomeMainDetails().get(0).getHomeMainTitle() + " - Pinterest")
                                .build());
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(pinLink)));
                    }
                }
            });
        }
        if (itemYT != null) {
            itemYT.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!ytLink.equalsIgnoreCase("")) {
                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory(Constants.kCatSideMenu)
                                .setAction(agency.getHomeMainDetails().get(0).getHomeMainTitle() + " -  YouTube")
                                .build());
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(ytLink)));
                    }
                }
            });
        }
//        itemCalendar.setOnClickListener(this);
//        itemSettings.setOnClickListener(this);

//        resideMenu.addMenuItem(itemWhere, ResideMenu.DIRECTION_LEFT);
//        resideMenu.addMenuItem(itemWhile, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(itemWhere, ResideMenu.DIRECTION_RIGHT);
        resideMenu.addMenuItem(itemWhile, ResideMenu.DIRECTION_RIGHT);
//        resideMenu.addMenuItem(itemSocial, ResideMenu.DIRECTION_RIGHT);
        if (itemFB != null){
            resideMenu.addMenuItem(itemFB, ResideMenu.DIRECTION_RIGHT);
        }
        if (itemTW != null){
            resideMenu.addMenuItem(itemTW, ResideMenu.DIRECTION_RIGHT);
        }
        if (itemPin != null){
            resideMenu.addMenuItem(itemPin, ResideMenu.DIRECTION_RIGHT);
        }
        if (itemYT != null){
            resideMenu.addMenuItem(itemYT, ResideMenu.DIRECTION_RIGHT);
        }

        // You can disable a direction by setting ->
        resideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_LEFT);

//        findViewById(R.id.title_bar_left_menu).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                resideMenu.openMenu(ResideMenu.DIRECTION_LEFT);
//            }
//        });
        openDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resideMenu.openMenu(ResideMenu.DIRECTION_RIGHT);
                mTracker.setScreenName(Constants.SideMenuScreen+"-"+ agency.getHomeMainDetails().get(0).getHomeMainTitle());
                mTracker.send(new HitBuilders.ScreenViewBuilder().build());
            }
        });
    }

    private ResideMenu.OnMenuListener menuListener = new ResideMenu.OnMenuListener() {
        @Override
        public void openMenu() {
            rlHeader.setVisibility(View.GONE);
//            Toast.makeText(getApplicationContext(), "Menu is opened!", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void closeMenu() {
            rlHeader.setVisibility(View.VISIBLE);
//            Toast.makeText(getApplicationContext(), "Menu is closed!", Toast.LENGTH_SHORT).show();
        }
    };


    @Override
    public void onBackPressed() {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cd, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onListItemExpanded(int position) {

    }

    @Override
    public void onListItemCollapsed(int position) {

    }


    private class HelloWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
//            webView.loadData(summary, "text/html; charset=UTF-8", null);

            if (url != null && url.startsWith("http://")) {
                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory(Constants.kCatAgencyContact)
                        .setAction(agency.getHomeMainDetails().get(0).getHomeMainTitle() + " -Contact Block Footer- "+agency.getWebsite().get(0).getTitle())
                        .build());
                view.getContext().startActivity(
                        new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                return true;
            } else if (url != null && url.startsWith("tel:")) {
                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory(Constants.kCatAgencyContact)
                        .setAction(agency.getHomeMainDetails().get(0).getHomeMainTitle() + " -Contact Block Footer- " + Constants.phone)
                        .build());
                view.getContext().startActivity(
                        new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                return true;
            } else if (url != null && url.startsWith("mailto:")) {
                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory(Constants.kCatAgencyContact)
                        .setAction(agency.getHomeMainDetails().get(0).getHomeMainTitle() + " -Contact Block Footer- "+agency.getAgencyEmail().get(0).getTitle())
                        .build());
                view.getContext().startActivity(
                        new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                return true;
            } else {
                return true;
            }
//            if (!loadingFinished) {
//                redirect = true;
//            }
//
//            loadingFinished = false;
//            view.loadUrl(url);
//            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            if (!redirect) {
                loadingFinished = true;
            }

            if (loadingFinished && !redirect) {
                pg.cancel();
            } else {
                redirect = false;
            }
            //what you want to do when the page finished loading, eg. give some message, show progress bar, etc
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            loadingFinished = false;
            pg.show();
            //what you want to do when the page starts loading, eg. give some message
        }

    }

    private ArrayList<VerticalParent> setUpTestData() {
        ArrayList<VerticalParent> verticalParentList = new ArrayList<>();
        ArrayList<VerticalChild> childItemList = new ArrayList<>();

        VerticalChild verticalChild = new VerticalChild();
        verticalChild.setChildText("Assessment");
        childItemList.add(verticalChild);
        VerticalChild verticalChild2 = new VerticalChild();
        verticalChild2.setChildText("Early Language Facilitation parent program");
        childItemList.add(verticalChild2);
        VerticalChild verticalChild3 = new VerticalChild();
        verticalChild3.setChildText("More Than Words parent program");
        childItemList.add(verticalChild3);
        VerticalChild verticalChild4 = new VerticalChild();
        verticalChild4.setChildText("Family Focused Stuttering Therapy parent program");
        childItemList.add(verticalChild4);

        VerticalParent verticalParent = new VerticalParent();
        verticalParent.setChildItemList(childItemList);
        verticalParent.setParentNumber(0);
        verticalParent.setParentText("York Region Preschool Speech and Language Program");
        verticalParent.setChildItemList(childItemList);
        verticalParentList.add(verticalParent);
        VerticalParent verticalParent2 = new VerticalParent();
        verticalParent2.setParentNumber(1);
        verticalParent2.setParentText("Tri-Regional Infant Hearing Program");
        verticalParent2.setChildItemList(childItemList);
        verticalParentList.add(verticalParent2);
        VerticalParent verticalParent3 = new VerticalParent();
        verticalParent3.setParentNumber(2);
        verticalParent3.setParentText("Tri-Regional Blind-Low Vision Program");
        verticalParent3.setChildItemList(childItemList);
        verticalParentList.add(verticalParent3);


//        for (int i = 0; i < 3; i++) {
//            List<VerticalChild> childItemList = new ArrayList<>();
//
//            VerticalChild verticalChild = new VerticalChild();
//            verticalChild.setChildText("123 Main Street, Suite 108 \n Toronto, Ontario M4C 4X3");
//            childItemList.add(verticalChild);
//            childItemList.add(verticalChild);
//
//            // Evens get 2 children, odds get 1
////            if (i % 2 == 0) {
////                VerticalChild verticalChild2 = new VerticalChild();
////                verticalChild2.setChildText("123 Main Street, Suite 108 \n Toronto, Ontario M4C 4X3");
////                childItemList.add(verticalChild2);
////            }
//
//            VerticalParent verticalParent = new VerticalParent();
//            verticalParent.setChildItemList(childItemList);
//            verticalParent.setParentNumber(i);
//            verticalParent.setParentText("York Region Preschool");
//            if (i == 0) {
//                verticalParent.setInitiallyExpanded(false);
//            }
//            verticalParentList.add(verticalParent);
//        }

        return verticalParentList;
    }

}
