package com.childrencareapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.special.ResideMenu.ResideMenu;
import com.special.ResideMenu.ResideMenuItem;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import Database.MyDbHelper;
import Model.Agency;
import Model.CdpStatusModel;
import Model.Contact;
import Model.HistoryModel;
import Model.ParentData;
import Model.Wwaa;
import util.AppManager;
import util.Constants;
import util.MyApp;
import adapter.CustomAdapter;

public class WwaaCdpStatusActivity extends Activity {


    View childView;
    View childList;
    ExpandableListView listView;

    CheckBox checkBox;
    Spinner spn;

    MyDbHelper dbHelper;
    int pos;
//    ArrayList<VerticalParent> dataList;
    List<Wwaa> dataList;
    int hieght;

    ResideMenu resideMenu;
    private ResideMenuItem itemWhile;
    private ResideMenuItem itemWhere;
    private ResideMenuItem itemSocial;

    private ResideMenuItem itemFB=null;
    private ResideMenuItem itemTW=null;
    private ResideMenuItem itemPin=null;
    private ResideMenuItem itemYT=null;
    String fbLink = "",twLink ="",pinLink = "",ytLink ="";

    ImageView imgPhone,imgWeb,imgConnect;
    ImageButton openDrawer;
    RelativeLayout rlHeader;
    int scrollPos;

    Agency agency;

    Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_wwaa_cdp_status);

        AppManager.getInstance().setFont(this);
        dbHelper = new MyDbHelper(this);

        AppManager.getInstance().setListHieght(0);

        // Obtain the shared Tracker instance.
        MyApp application = (MyApp) getApplication();
        mTracker = application.getDefaultTracker();


        TextView heading = (TextView) findViewById(R.id.heading);
        ImageView img_agency = (ImageView)findViewById(R.id.img_agency);
        listView = (ExpandableListView) findViewById(R.id.expandableListView);
        TextView tvChildName = (TextView) findViewById(R.id.tv_child_name);
        TextView tvBody = (TextView) findViewById(R.id.tv_body);
        Button btnSave = (Button)findViewById(R.id.btn_save);
        TextView contact = (TextView) findViewById(R.id.contact);
        TextView tvPhone = (TextView) findViewById(R.id.tv_phone);
        TextView tvWebsite = (TextView) findViewById(R.id.tv_website);
        TextView tvConnect = (TextView) findViewById(R.id.tv_connect);
        RelativeLayout relBar = (RelativeLayout)findViewById(R.id.rel_bar);
        RelativeLayout relBar2 = (RelativeLayout)findViewById(R.id.rel_bar2);
        ImageView imgChild = (ImageView)findViewById(R.id.imgChild);
        openDrawer = (ImageButton) findViewById(R.id.btn_drawer);
        rlHeader = (RelativeLayout) findViewById(R.id.rlHeader);
        imgPhone = (ImageView)findViewById(R.id.imgPhone);
        imgWeb = (ImageView)findViewById(R.id.imgWeb);
        imgConnect = (ImageView)findViewById(R.id.imgConnect);
        final ScrollView scrollView = (ScrollView) findViewById(R.id.scrollView);

        heading.setTypeface(AppManager.getInstance().getBlackTypeface());
        btnSave.setTypeface(AppManager.getInstance().getBoldTypeface());
        contact.setTypeface(AppManager.getInstance().getBlackTypeface());
        tvPhone.setTypeface(AppManager.getInstance().getRegularTypeface());
        tvWebsite.setTypeface(AppManager.getInstance().getRegularTypeface());
        tvConnect.setTypeface(AppManager.getInstance().getRegularTypeface());
        tvChildName.setTypeface(AppManager.getInstance().getRegularTypeface());

        agency = AppManager.getInstance().getAgencyModel();

        setUpMenu();

        rlHeader.setVisibility(View.VISIBLE);
        rlHeader.setBackgroundColor(getResources().getColor(R.color.red_text));

        if (agency.getWwaaMainDetails().size()>0) {
            heading.setText(agency.getWwaaMainDetails().get(0).getWwaaMainTitle());
            tvBody.setText(Html.fromHtml(agency.getWwaaMainDetails().get(0).getWwaaMainBody()));
        }
        if (agency.getAgencyPhone().size()>0  && (agency.getAgencyPhone().get(0).getNumberAlias() != null && !agency.getAgencyPhone().get(0).getNumberAlias().equalsIgnoreCase(""))) {
            tvPhone.setText(agency.getAgencyPhone().get(0).getNumberAlias());
            imgPhone.setVisibility(View.VISIBLE);
        }
        else if (agency.getAgencyPhone().size()>0  && (agency.getAgencyPhone().get(0).getNumber() != null && !agency.getAgencyPhone().get(0).getNumber().equalsIgnoreCase(""))) {
            tvPhone.setText(agency.getAgencyPhone().get(0).getNumber());
            imgPhone.setVisibility(View.VISIBLE);
        }
        else {
            imgPhone.setVisibility(View.INVISIBLE);
        }
        if (agency.getWebsite().size()>0 && !agency.getWebsite().get(0).getTitle().equalsIgnoreCase("")) {
            tvWebsite.setText(agency.getWebsite().get(0).getTitle());
            imgWeb.setVisibility(View.VISIBLE);
        }
        else {
            imgWeb.setVisibility(View.INVISIBLE);
        }
        if (agency.getAgencyEmail().size()>0 && !agency.getAgencyEmail().get(0).getTitle().equalsIgnoreCase("")) {
            tvConnect.setText(agency.getAgencyEmail().get(0).getTitle());
            imgConnect.setBackgroundResource(R.drawable.email);
            imgConnect.setVisibility(View.VISIBLE);
        }
        else if (agency.getCdpConnect().size()>0 && !agency.getCdpConnect().get(0).getTitle().equalsIgnoreCase("")) {
            tvConnect.setText(agency.getCdpConnect().get(0).getTitle());
            imgConnect.setBackgroundResource(R.drawable.events);
            imgConnect.setVisibility(View.VISIBLE);
        }else {
            imgConnect.setVisibility(View.INVISIBLE);
        }
//        if (agency.getContactLogo().size()>0) {
//            Picasso.with(this).load(Constants.IMG_URL + agency.getContactLogo().get(0).getFilename()).into(img_agency);
//        }
        if (AppManager.getInstance().isFromServer()) {
            if (agency.getContactLogo().size()>0) {
                Picasso.with(this).load(agency.getContactLogo().get(0).getUri()).into(img_agency);
            }
        }
        else{
            if (agency.getAgencyLogo() != null) {
                img_agency.setImageBitmap(BitmapFactory.decodeByteArray(agency.getAgencyLogo(), 0,
                        agency.getAgencyLogo().length));
            }
        }
        if (agency.getColorCode().size()>0) {
            relBar.setBackgroundColor(Color.parseColor(agency.getColorCode().get(0)));
            relBar2.setBackgroundColor(Color.parseColor(agency.getColorCode().get(0)));
        }

        SharedPreferences pref = getSharedPreferences(Constants.WWAACHILD_PREF,Activity.MODE_PRIVATE);
        if(pref.contains(Constants.CHILD_POS))
        {
            pos = pref.getInt(Constants.CHILD_POS,-1);
            tvChildName.setText(AppManager.getInstance().getChildModels().get(pos).getName());
            if (AppManager.getInstance().getChildModels().get(pos).getGender() == 0){
                imgChild.setBackgroundResource(R.drawable.boy_in_circle);
            }
            else if (AppManager.getInstance().getChildModels().get(pos).getGender() == 1){
                imgChild.setBackgroundResource(R.drawable.where_are_banner_girl_user_placeholder);
            }
        }

        final ArrayList<CdpStatusModel> list = new ArrayList<CdpStatusModel>();
//        dataList = AppManager.getInstance().getMainDataCDP();
        dataList = agency.getWwaa();
        CustomAdapter adapter = new CustomAdapter(dataList, this, list);
        listView.setAdapter(adapter);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int width = metrics.widthPixels;
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
            listView.setIndicatorBounds(width - GetPixelFromDips(70), width - GetPixelFromDips(30));
            listView.setIndicatorBounds(width - GetPixelFromDips(70), width - GetPixelFromDips(30));
        } else {
            listView.setIndicatorBoundsRelative(width - GetPixelFromDips(70), width - GetPixelFromDips(30));
            listView.setIndicatorBoundsRelative(width - GetPixelFromDips(70), width - GetPixelFromDips(30));
        }


        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view = inflater.inflate(R.layout.cdp_list_parent_vertical, null, false);
        childView = inflater.inflate(R.layout.cdp_list_child_vertical, null, false);
        childList = inflater.inflate(R.layout.childlist, null, false);
        int h = ((RelativeLayout) view.findViewById(R.id.relativeLayout5)).getMinimumHeight();
        h = h + 10;
        hieght = h * dataList.size();

        ViewGroup.LayoutParams prams = listView.getLayoutParams();
        prams.height = hieght+20; //AppManager.getInstance().getListHieght();
        listView.setLayoutParams(prams);

        final ArrayList<ParentData> datas = AppManager.getInstance().getParentModels();

//        Bundle bundle = getIntent().getExtras();
//        if (bundle!=null) {
//            try {
//                int ind = bundle.getInt("INDEX");
//                listView.expandGroup(ind);
//                int hight = ((LinearLayout) childView.findViewById(R.id.linear)).getMinimumHeight();
//                int s = dataList.get(ind).getWWAAStatus().size();
//
//                int childListH = hight * s;
//                hieght += childListH;
//                ViewGroup.LayoutParams pram = listView.getLayoutParams();
//                prams.height = hieght + 20; //AppManager.getInstance().getListHieght();
//                listView.setLayoutParams(pram);
//            }catch (Exception e){
//                e.printStackTrace();
//            }
//        }
//        else {
//            try {
//                listView.expandGroup(0);
//                int hight = ((LinearLayout) childView.findViewById(R.id.linear)).getMinimumHeight();
//                int s = dataList.get(0).getWWAAStatus().size();
//
//                int childListH = hight * s;
//                hieght += childListH;
//                ViewGroup.LayoutParams pram = listView.getLayoutParams();
//                prams.height = hieght + 20; //AppManager.getInstance().getListHieght();
//                listView.setLayoutParams(pram);
//            }catch (Exception e){
//                e.printStackTrace();
//            }
//        }



        listView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

//                int[] xy = new int[2];
//                v.getLocationOnScreen(xy);
//                scrollPos = xy[1];

                return false;
            }
        });

        listView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            int previousGroup = -1;
            @Override
            public void onGroupExpand(int groupPosition) {


                int h = ((LinearLayout) childView.findViewById(R.id.linear)).getMinimumHeight();
                int s = dataList.get(groupPosition).getWWAAStatus().size();

                int childListH = h * s;

                AppManager.getInstance().setListHieght(childListH);

                if(groupPosition != previousGroup) {
                    if (listView.isGroupExpanded(previousGroup)) {
                        listView.collapseGroup(previousGroup);
                    }
//                    hieght -= childListH;
                }
                else {
//                    hieght -= childListH;
                }
                previousGroup = groupPosition;

                hieght += childListH;

                int childId = AppManager.getInstance().getChildModels().get(pos).getId();

//                scrollView.fullScroll(View.FOCUS_UP);


                Date date = new Date();
                String strTimeFormat = "hh:mm a";
                String strDateFormat = "MMM-dd-yyyy";
                DateFormat timeFormat = new SimpleDateFormat(strTimeFormat, Locale.US);
                DateFormat dateFormat = new SimpleDateFormat(strDateFormat, Locale.US);
                String formattedTime= timeFormat.format(date);
                String formattedDate= dateFormat.format(date);


                if (AppManager.getInstance().getParentModels().size() > 0) {
                    if (!isPosExist(groupPosition)) {
                        ParentData parentData = new ParentData();
                        parentData.setParentId(groupPosition);
                        parentData.setDate(formattedDate);
                        parentData.setTime(formattedTime);
                        parentData.setChildId(childId);
                        datas.add(parentData);
                        AppManager.getInstance().setParentModels(datas);
                    }
                }
                else {
                    ParentData parentData = new ParentData();
                    parentData.setParentId(groupPosition);
                    parentData.setDate(formattedDate);
                    parentData.setTime(formattedTime);
                    parentData.setChildId(childId);
                    datas.add(parentData);
                    AppManager.getInstance().setParentModels(datas);
                }
//                ViewGroup.LayoutParams prams2 = childList.findViewById(R.id.listView).getLayoutParams();
//                prams2.height = childListH; //AppManager.getInstance().getListHieght();
//                childList.findViewById(R.id.listView).setLayoutParams(prams2);

                ViewGroup.LayoutParams prams = listView.getLayoutParams();
                prams.height = hieght+GetPixelFromDips(20); //AppManager.getInstance().getListHieght();
                listView.setLayoutParams(prams);




                scrollView.smoothScrollTo(0, 0);

            }
        });


        listView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {
                if (groupPosition > -1) {
                    int childListH = ((LinearLayout) childView.findViewById(R.id.linear)).getMinimumHeight() * dataList.get(groupPosition).getWWAAStatus().size();

                    hieght -= childListH;

                    ViewGroup.LayoutParams prams = listView.getLayoutParams();
                    prams.height = hieght+GetPixelFromDips(20);; //AppManager.getInstance().getListHieght();
                    listView.setLayoutParams(prams);

                    scrollView.smoothScrollTo(0, 0);
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        mTracker.setScreenName(Constants.WWAAStatusActivity + "-" + agency.getHomeMainDetails().get(0).getHomeMainTitle());
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

    }

    public void onClick(View v)
    {
        if(v.getId() == R.id.btn_back)
        {
            if (dbHelper.getCdpStatus(AppManager.getInstance().getChildModels().get(pos).getId(), agency.getNid()).size() > 0) {
                try {
                    finish();
                    Intent intent = new Intent(getParent(), WwaaCdpActivity.class);
                    TabGroupActivity parentActivity = (TabGroupActivity) getParent();
                    parentActivity.startChildActivity("WwaaCdpActivity", intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else {
                try {
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        else if(v.getId()==R.id.tv_child_name)
        {
//            mTracker.send(new HitBuilders.EventBuilder()
//                    .setCategory(Constants.kCatStatusHistory)
//                    .setAction(Constants.SelectChild)
//                    .build());

            try {
                finish();
                Intent intent = new Intent(getParent(), MyTeamActivity.class);
                intent.putExtra("WWAA", true);
                TabGroupActivity parentActivity = (TabGroupActivity) getParent();
                parentActivity.startChildActivity("MyTeamActivity", intent);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        else if(v.getId()==R.id.btn_save)
        {

            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory(Constants.kCatStatusHistory)
                    .setAction(Constants.SaveStatus+ "-" + agency.getHomeMainDetails().get(0).getHomeMainTitle())
                    .build());

            ArrayList<ParentData> parentDatas = AppManager.getInstance().getParentModels();
            int a = parentDatas.size();

            int childId = AppManager.getInstance().getChildModels().get(pos).getId();

//            ArrayList<GetCdpStatausModel> historyList = dbHelper.getCdpStatus(childId, agency.getNid());
//            for (int i = 0; i < historyList.size(); i++) {
//                GsonBuilder gsonb = new GsonBuilder();
//                Gson gson = gsonb.create();
//                Type type = new TypeToken<ArrayList<ParentData>>() {
//                }.getType();
//                ArrayList<ParentData> datas = gson.fromJson(historyList.get(i).getList(), type);
//
//
//                for (int j = 0; j < datas.size(); j++) {
//                    if (isAlreadyExist(parentDatas,datas.get(j).getParentId(),datas.get(j).getChildData().getChkbx())){
//                        if (dbHelper.removeStatus(historyList.get(i).getId())){
//
//                        }
//                    }
//
////                    if (position == datas.get(j).getParentId()) {
////                        ParentData data = datas.get(j);
////
////                    }
//
//                }
//            }

            Date date = new Date();
            String strTimeFormat = "hh:mm a";
            String strDateFormat = "MMM-dd-yyyy";
            DateFormat timeFormat = new SimpleDateFormat(strTimeFormat, Locale.US);
            DateFormat dateFormat = new SimpleDateFormat(strDateFormat, Locale.US);
            String formattedTime= timeFormat.format(date);
            String formattedDate= dateFormat.format(date);

            int index=0;
            for (int i=0;i<a;){
                if (index <a) {
                    if (parentDatas.get(i).getChildData() == null) {
                        parentDatas.remove(i);
                    }
                    else {
                        i++;
                    }
                    index++;
                }
                else{
                    break;
                }

            }

            Gson gson = new Gson();
            String value = gson.toJson(parentDatas);

            if (parentDatas.size()>0) {
                if (dbHelper.addNewCdpStatus(value, formattedDate, formattedTime, childId, agency.getNid())) {
                    try {

                        //   ADD HISTORY //////



                        for (int i=0;i<parentDatas.size();i++){
                            HistoryModel model = new HistoryModel();
                            model.setParentId(parentDatas.get(i).getParentId());
                            model.setChkbx(parentDatas.get(i).getChildData().getChkbx());
                            model.setDate(parentDatas.get(i).getChildData().getDate());
                            model.setTime(parentDatas.get(i).getChildData().getTime());
                            model.setStatusId(parentDatas.get(i).getChildData().getStatusId());

                            if(dbHelper.deleteHistory(agency.getNid(),childId,model)){

                            }


                            if(dbHelper.addCdpHistory(agency.getNid(),childId,model)){

                            }

                        }


                        finish();
                        Intent intent = new Intent(getParent(), WwaaCdpActivity.class);
                        TabGroupActivity parentActivity = (TabGroupActivity) getParent();
                        parentActivity.startChildActivity("WwaaCdpActivity", intent);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
            else {
                for (int i=0;i<a;i++){
                    if (listView.isGroupExpanded(i)) {
                        listView.collapseGroup(i);
                    }

                }
            }
        }
        else if(v.getId() == R.id.tv_website){

            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory(Constants.kCatAgencyContact)
                    .setAction(agency.getHomeMainDetails().get(0).getHomeMainTitle() + " -Contact Block Footer- "+agency.getWebsite().get(0).getTitle())
                    .build());

            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(agency.getWebsite().get(0).getUrl())));
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        else if(v.getId() == R.id.tv_phone){

            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory(Constants.kCatAgencyContact)
                    .setAction(agency.getHomeMainDetails().get(0).getHomeMainTitle() + " -Contact Block Footer- "+Constants.phone)
                    .build());

            try{
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("tel:"+agency.getAgencyPhone().get(0).getNumber())));
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        else if(v.getId() == R.id.tv_connect){


            try {
                if (agency.getAgencyEmail().size() > 0) {
                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory(Constants.kCatAgencyContact)
                            .setAction(agency.getHomeMainDetails().get(0).getHomeMainTitle() + " -Contact Block Footer- "+agency.getAgencyEmail().get(0).getTitle())
                            .build());
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + agency.getAgencyEmail().get(0).getUrl())));
                } else if (agency.getCdpConnect().size() > 0) {
                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory(Constants.kCatAgencyContact)
                            .setAction(agency.getHomeMainDetails().get(0).getHomeMainTitle() + " -Contact Block Footer- "+agency.getCdpConnect().get(0).getTitle())
                            .build());
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(agency.getCdpConnect().get(0).getUrl())));
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private boolean isAlreadyExist(ArrayList<ParentData> data,int id,String chkbx) {
        boolean r = false;
        for (int i = 0; i < data.size(); i++) {
            if (id == data.get(i).getParentId()) {
                if (chkbx.equalsIgnoreCase(data.get(i).getChildData().getChkbx())){
                    r = true;
                    break;
                }

            } else {
                r = false;
            }
        }
        return r;
    }

    private boolean isPosExist(int id) {
        boolean r = false;
        for (int i = 0; i < AppManager.getInstance().getParentModels().size(); i++) {
            if (id == AppManager.getInstance().getParentModels().get(i).getParentId()) {
                r = true;
                break;
            } else {
                r = false;
            }
        }
        return r;
    }


    public int GetPixelFromDips(float pixels) {
        // Get the screen's density scale
        final float scale = getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f);
    }

    private void setUpMenu() {

        Contact contct = null;

        // attach to current activity;
        resideMenu = new ResideMenu(this);
        resideMenu.setUse3D(false);
        resideMenu.setBackground(R.drawable.menu_bar_background);
        resideMenu.attachToActivity(this);
        resideMenu.setShadowVisible(false);
        resideMenu.setMenuListener(menuListener);
        //valid scale factor is between 0.0f and 1.0f. leftmenu'width is 150dip.
        resideMenu.setScaleValue(0.6f);

        for (int i = 0; i < AppManager.getInstance().getContacts().size(); i++) {
            if (agency.getNid().equals(AppManager.getInstance().getContacts().get(i).getAgency())) {
                contct = AppManager.getInstance().getContacts().get(i);
                break;
            }
        }

        // create menu items;
        if (agency.getWwaaMainDetails().size()>0) {
            itemWhere = new ResideMenuItem(this, R.drawable.where_we_are_at, agency.getWwaaMainDetails().get(0).getWwaaMainTitle());
        }
        if (agency.getWywMainDetails().size() > 0) {
            itemWhile = new ResideMenuItem(this, R.drawable.while_you_wait, agency.getWywMainDetails().get(0).getWywMainTitle());
        }
//        itemSocial  = new ResideMenuItem(this, R.drawable.fb,R.drawable.twitter);
        if (contct != null) {
            try {

                for(int i=0;i<contct.getSocialLinks().size();i++) {
                    if (contct.getSocialLinks().get(i).getTitle().equalsIgnoreCase("Facebook")) {
                        fbLink = contct.getSocialLinks().get(i).getUrl();
                        itemFB = new ResideMenuItem(this, R.drawable.fb, "Facebook");
                    }
                }
                for(int i=0;i<contct.getSocialLinks().size();i++) {
                    if (contct.getSocialLinks().get(i).getTitle().equalsIgnoreCase("Twitter")){
                        twLink = contct.getSocialLinks().get(i).getUrl();
                        itemTW = new ResideMenuItem(this, R.drawable.twitter, "Twitter");
                    }

                }
                for(int i=0;i<contct.getSocialLinks().size();i++) {
                    if (contct.getSocialLinks().get(i).getTitle().equalsIgnoreCase("Pinterest")) {
                        pinLink = contct.getSocialLinks().get(i).getUrl();
                        itemPin = new ResideMenuItem(this, R.drawable.menu_bar_pin, "Pinterest");
                    }
                }
                for(int i=0;i<contct.getSocialLinks().size();i++) {
                    if (contct.getSocialLinks().get(i).getTitle().equalsIgnoreCase("YouTube")){
                        ytLink = contct.getSocialLinks().get(i).getUrl();
                        itemYT = new ResideMenuItem(this, R.drawable.menu_bar_yotube, "YouTube");
                    }

                }
//                itemSocial = new ResideMenuItem(this, R.drawable.fb, R.drawable.twitter,R.drawable.menu_bar_pin,R.drawable.menu_bar_yotube, twLink, fbLink,pinLink,ytLink);
            } catch (Exception e) {
                Log.e("CDP MENU", e.getLocalizedMessage());
            }
        } else {
//            itemSocial = new ResideMenuItem(this, R.drawable.fb, R.drawable.twitter);
        }

        itemWhere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory(Constants.kCatSideMenu)
                        .setAction(Constants.MyServicesMenuWWAA+"-"+agency.getHomeMainDetails().get(0).getHomeMainTitle())
                        .build());

                if (AppManager.getInstance().getChildModels().size() > 0) {
                    MyDbHelper dbHelper = new MyDbHelper(WwaaCdpStatusActivity.this);
                    SharedPreferences pref = getSharedPreferences(Constants.WWAACHILD_PREF, Activity.MODE_PRIVATE);
                    int pos = -1;
                    if (pref.contains(Constants.CHILD_POS)) {
                        pos = pref.getInt(Constants.CHILD_POS, -1);
                    }



                    if (dbHelper.getCdpStatus(AppManager.getInstance().getChildModels().get(pos).getId(), agency.getNid()).size() > 0) {
                        try {
                            finish();
                            Intent intent = new Intent(getParent(), WwaaCdpActivity.class);
                            TabGroupActivity parentActivity = (TabGroupActivity) getParent();
                            parentActivity.startChildActivity("WwaaCdpActivity", intent);
//                    Intent intent = new Intent(CDPActivity.this, WhileYouWaitActivity.class);
//                    startActivity(intent);
                            resideMenu.closeMenu();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    } else {
                        AppManager.getInstance().setParentModels(new ArrayList<ParentData>());
                        try {
                            finish();
                            Intent intent = new Intent(getParent(), WwaaCdpStatusActivity.class);
                            TabGroupActivity parentActivity = (TabGroupActivity) getParent();
                            parentActivity.startChildActivity("WwaaCdpStatusActivity", intent);
//                    Intent intent = new Intent(CDPActivity.this, WhileYouWaitActivity.class);
//                    startActivity(intent);
                            resideMenu.closeMenu();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
//                    }
                } else {
//                    Toast.makeText(getApplicationContext(), "Add Child First!", Toast.LENGTH_SHORT).show();
                    if (TabActivity.tabHost != null){
                        TabActivity.tabHost.setCurrentTab(1);
                    }
                }
                resideMenu.closeMenu();
            }
        });
        itemWhile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory(Constants.kCatSideMenu)
                        .setAction(Constants.WYWMenuWWAA+"-"+agency.getHomeMainDetails().get(0).getHomeMainTitle())
                        .build());

                if (AppManager.getInstance().getChildModels().size() > 0) {

                    try {
                        finish();
                        Intent intent = new Intent(getParent(), WhileYouWaitActivity.class);
                        TabGroupActivity parentActivity = (TabGroupActivity) getParent();
                        parentActivity.startChildActivity("WhileYouWaitActivity", intent);
//                    Intent intent = new Intent(CDPActivity.this, WhileYouWaitActivity.class);
//                    startActivity(intent);
                        resideMenu.closeMenu();
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                } else {
//                    Toast.makeText(getApplicationContext(), "Add Child First!", Toast.LENGTH_SHORT).show();
                    if (TabActivity.tabHost != null){
                        TabActivity.tabHost.setCurrentTab(1);
                    }
                    resideMenu.closeMenu();
                }

            }
        });
        if (itemFB != null) {
            itemFB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!fbLink.equalsIgnoreCase("")) {

                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory(Constants.kCatSideMenu)
                                .setAction(agency.getHomeMainDetails().get(0).getHomeMainTitle() + " - Facebook")
                                .build());

                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(fbLink)));
                    }
                }
            });
        }
        if (itemTW != null) {
            itemTW.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!twLink.equalsIgnoreCase("")) {

                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory(Constants.kCatSideMenu)
                                .setAction(agency.getHomeMainDetails().get(0).getHomeMainTitle() + " - Twitter")
                                .build());
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(twLink)));
                    }
                }
            });
        }
        if (itemPin != null) {
            itemPin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!pinLink.equalsIgnoreCase("")) {
                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory(Constants.kCatSideMenu)
                                .setAction(agency.getHomeMainDetails().get(0).getHomeMainTitle() + " - Pinterest")
                                .build());
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(pinLink)));
                    }
                }
            });
        }
        if (itemYT != null) {
            itemYT.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!ytLink.equalsIgnoreCase("")) {
                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory(Constants.kCatSideMenu)
                                .setAction(agency.getHomeMainDetails().get(0).getHomeMainTitle() + " -  YouTube")
                                .build());
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(ytLink)));
                    }
                }
            });
        }
//        itemCalendar.setOnClickListener(this);
//        itemSettings.setOnClickListener(this);

//        resideMenu.addMenuItem(itemWhere, ResideMenu.DIRECTION_LEFT);
//        resideMenu.addMenuItem(itemWhile, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(itemWhere, ResideMenu.DIRECTION_RIGHT);
        resideMenu.addMenuItem(itemWhile, ResideMenu.DIRECTION_RIGHT);
//        resideMenu.addMenuItem(itemSocial, ResideMenu.DIRECTION_RIGHT);
        if (itemFB != null){
            resideMenu.addMenuItem(itemFB, ResideMenu.DIRECTION_RIGHT);
        }
        if (itemTW != null){
            resideMenu.addMenuItem(itemTW, ResideMenu.DIRECTION_RIGHT);
        }
        if (itemPin != null){
            resideMenu.addMenuItem(itemPin, ResideMenu.DIRECTION_RIGHT);
        }
        if (itemYT != null){
            resideMenu.addMenuItem(itemYT, ResideMenu.DIRECTION_RIGHT);
        }
        // You can disable a direction by setting ->
        resideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_LEFT);

//        findViewById(R.id.title_bar_left_menu).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                resideMenu.openMenu(ResideMenu.DIRECTION_LEFT);
//            }
//        });
        openDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resideMenu.openMenu(ResideMenu.DIRECTION_RIGHT);
                mTracker.setScreenName(Constants.SideMenuScreen+"-"+ agency.getHomeMainDetails().get(0).getHomeMainTitle());
                mTracker.send(new HitBuilders.ScreenViewBuilder().build());
            }
        });
    }

    private ResideMenu.OnMenuListener menuListener = new ResideMenu.OnMenuListener() {
        @Override
        public void openMenu() {
            rlHeader.setVisibility(View.GONE);
//            Toast.makeText(getApplicationContext(), "Menu is opened!", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void closeMenu() {
            rlHeader.setVisibility(View.VISIBLE);
//            Toast.makeText(getApplicationContext(), "Menu is closed!", Toast.LENGTH_SHORT).show();
        }
    };


}
