package com.childrencareapp;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.ViewGroup.LayoutParams;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TabHost;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import util.Constants;
import util.MyApp;

public class TabActivity extends android.app.TabActivity {

    public static TabHost tabHost;
    boolean isShow = false;
    Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE); //FEATURE_ACTION_BAR
        setContentView(R.layout.activity_main);

        tabHost = (TabHost) findViewById(android.R.id.tabhost);
//        tabHost.setBackgroundColor(Color.WHITE);
        tabHost.getTabWidget().setBackgroundResource(R.drawable.tab_bg);

        // Obtain the shared Tracker instance.
        MyApp application = (MyApp) getApplication();
        mTracker = application.getDefaultTracker();


        tabHost.addTab(tabHost.newTabSpec("tab1")
                .setIndicator("", getResources().getDrawable(R.drawable.tab_selector_home))
                .setContent(new Intent(this, TabGroup1Activity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)));


        tabHost.addTab(tabHost.newTabSpec("tab2")//prepareTabView(this, "Account", getResources().getDrawable(R.drawable.tab_selector_setting))
                .setIndicator("", getResources().getDrawable(R.drawable.tab_selector_setting)) //Account
                .setContent(new Intent(this, TabGroup2Activity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)));

        tabHost.addTab(tabHost.newTabSpec("tab3")
                .setIndicator("", getResources().getDrawable(R.drawable.tab_selector_team)) //team
                .setContent(new Intent(this, TabGroup3Activity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)));

        tabHost.addTab(tabHost.newTabSpec("tab4")
                .setIndicator("", getResources().getDrawable(R.drawable.tab_selector_tools)) //Tools
                .setContent(new Intent(this, TabGroup4Activity.class))); //new EmptyTabFactory()

        tabHost.addTab(tabHost.newTabSpec("tab5")
                .setIndicator("", getResources().getDrawable(R.drawable.tab_selector_contact)) //Contact
                .setContent(new Intent(this, TabGroup5Activity.class)));


//        tabHost.setCurrentTab(6);
        tabHost.getTabWidget().getChildAt(0).setTag("");
        tabHost.getTabWidget().getChildAt(1).setTag("");
        tabHost.getTabWidget().getChildAt(2).setTag("");
        tabHost.getTabWidget().getChildAt(3).setTag("tab4");
        tabHost.getTabWidget().getChildAt(4).setTag("");

        tabHost.getTabWidget().setStripEnabled(false);

        Display display = getWindowManager().getDefaultDisplay();
        final int width = display.getWidth();

        for (int i = 0; i < 5; i++) {
            tabHost.getTabWidget().getChildAt(i).setBackgroundColor(Color.WHITE);
            tabHost.getTabWidget().getChildAt(i).setLayoutParams(new
                    LinearLayout.LayoutParams((width / 5), GetPixalFromDP(this,35)));

        }

//        tabHost.getTabWidget().getChildAt(3).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                PopupMenu popup = new PopupMenu(getBaseContext(), v);
//                popup.getMenuInflater().inflate(R.menu.popup, popup.getMenu());
//
//                // Force icons to show
//                Object menuHelper;
//                Class[] argTypes;
//                try {
//                    Field fMenuHelper = PopupMenu.class.getDeclaredField("mPopup");
//                    fMenuHelper.setAccessible(true);
//                    menuHelper = fMenuHelper.get(popup);
//                    argTypes = new Class[]{boolean.class};
//                    menuHelper.getClass().getDeclaredMethod("setForceShowIcon", argTypes).invoke(menuHelper, true);
//                } catch (Exception e) {
//                    Log.w("TAB_Activity", "error forcing menu icons to show", e);
//                    popup.show();
//                    return;
//                }
//
//                popup.setOnMenuItemClickListener(new OnMenuItemClickListener() {
//
//                    @Override
//                    public boolean onMenuItemClick(MenuItem item) {
//                        return true;
//                    }
//                });
//
//                popup.show();
//            }
//        });

        tabHost.getTabWidget().getChildAt(3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
                View popupView = layoutInflater.inflate(R.layout.custom_popup, null);
                final PopupWindow popupWindow = new PopupWindow(
                        popupView,
                        LayoutParams.WRAP_CONTENT,
                        LayoutParams.WRAP_CONTENT);

                popupWindow.setOutsideTouchable(true);
                popupWindow.setFocusable(true);

                final String tag = tabHost.getTabWidget().getChildAt(3).getTag().toString();

                if (tag.equalsIgnoreCase("tab4")) {
                    ImageButton btnCalendar = (ImageButton) popupView.findViewById(R.id.btnCalender);
                    ImageButton btnNotes = (ImageButton) popupView.findViewById(R.id.btnNotes);
                    ImageView btnRemove = (ImageView) popupView.findViewById(R.id.btnRemove);
                    btnCalendar.setOnClickListener(new Button.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub

                            mTracker.send(new HitBuilders.EventBuilder()
                                    .setCategory(Constants.Action)
                                    .setAction(Constants.CalendarClick)
                                    .build());

                            try {
                                ComponentName cn;
                                Intent i = new Intent();
                                cn = new ComponentName("com.android.calendar", "com.android.calendar.LaunchActivity");
                                i.setComponent(cn);
                                startActivity(i);

                                tabHost.getTabWidget().setCurrentTab(tabHost.getCurrentTab());
                                popupWindow.dismiss();
                            } catch (Exception e) {
                                Log.e("TAB ACTIVITy", e.getLocalizedMessage());
                                startNewCalanderActivity(TabActivity.this, "com.google.android.calendar");
                            }
                        }
                    });

                    btnNotes.setOnClickListener(new Button.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub

                            mTracker.send(new HitBuilders.EventBuilder()
                                    .setCategory(Constants.Action)
                                    .setAction(Constants.NotesClick)
                                    .build());

                            try {
                                startNewNotesActivity(TabActivity.this, "com.google.android.keep");
                            }catch (Exception e){
                                e.printStackTrace();
                            }
//                            try {
////                                Intent intent = new Intent("com.evernote.action.EDIT_NOTE");
////                //              Uri uri = Uri.parse("");
////                //              intent.setDataAndType(uri, "text/plain");
////                                startActivity(intent);
//
//                                ComponentName cn;
//                                Intent i = new Intent();
//                                cn = new ComponentName("com.evernote.action.EDIT_NOTE", "com.evernote.action.EDIT_NOTE.LaunchActivity");
//                                i.setComponent(cn);
//                                startActivity(i);
//
//                            } catch (ActivityNotFoundException e) {
//                                Toast toast = Toast.makeText(TabActivity.this, "No editor on this device", Toast.LENGTH_SHORT);
//                                toast.show();
//                            }
////                            LaunchComponent ("com.notepad", "notepad");
                            tabHost.getTabWidget().setCurrentTab(tabHost.getCurrentTab());
                            popupWindow.dismiss();
                        }
                    });

                    btnRemove.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            tabHost.getTabWidget().setCurrentTab(tabHost.getCurrentTab());
                            popupWindow.dismiss();
                        }
                    });

//                    popupWindow.showAsDropDown(v);
//                    popupWindow.showAsDropDown(v, 0, 0);
                    tabHost.getTabWidget().focusCurrentTab(3);
                    popupWindow.showAtLocation(v, Gravity.BOTTOM, width / 5, 0);
                } else {
                    tabHost.getTabWidget().setCurrentTab(tabHost.getCurrentTab());
                    popupWindow.dismiss();
                }
            }

        });

        int t;
        for (t = 0; t < 5; t++) {
            final int finalT = t;
            tabHost.getTabWidget().getChildAt(t).setOnTouchListener(new View.OnTouchListener() {
                int ind = finalT;

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    // TODO Auto-generated method stub
                    if (event.getAction() == MotionEvent.ACTION_UP) {

                        if (ind == 0) {
                            //get the sunrise animation
                            Animation sunRise = AnimationUtils.loadAnimation(TabActivity.this, R.anim.sun_rise_one);
//                            HomeFragment.rl.setVisibility(View.GONE);
                            if (HomeActivity.rl != null) {
                                HomeActivity.rl.startAnimation(sunRise);
                            }
                            tabHost.setCurrentTab(3);
                            tabHost.setCurrentTab(0);


                        }
                        if (ind == 1) {

                            //get the sunrise animation
                            Animation sunRise = AnimationUtils.loadAnimation(TabActivity.this, R.anim.sun_rise_two);
//                            AccountSetupFragment.rl.setVisibility(View.GONE);
                            if (AccountSettingActivity.rl != null) {
                                AccountSettingActivity.rl.startAnimation(sunRise);
                            }
                            if (ChildViewActivity.rl != null){
                                ChildViewActivity.rl.startAnimation(sunRise);
                            }
                            tabHost.setCurrentTab(3);
                            tabHost.setCurrentTab(1);
                        }
                        if (ind == 2) {

                            //get the sunrise animation
                            Animation sunRise = AnimationUtils.loadAnimation(TabActivity.this, R.anim.sun_rise);
//                            MyTeamFragment.rl.setVisibility(View.GONE);
                            if(MyTeamActivity.rl != null) {
                                MyTeamActivity.rl.startAnimation(sunRise);
                            }
                            tabHost.setCurrentTab(3);
                            tabHost.setCurrentTab(2);

                        }
                        if (ind == 4) {
                            //get the sunrise animation
                            Animation sunRise = AnimationUtils.loadAnimation(TabActivity.this, R.anim.sun_rise_five);
//                            ContactFragment.rl.setVisibility(View.GONE);
                            if (ContactActivity.rl != null) {
                                ContactActivity.rl.startAnimation(sunRise);
                            }
                            tabHost.setCurrentTab(3);
                            tabHost.setCurrentTab(4);
                        }
                    }
                    return false;
                }
            });
        }
    }

    public void startNewNotesActivity(Context context, String packageName) {
        Intent intent = context.getPackageManager().getLaunchIntentForPackage(packageName);
        if (intent == null) {
            // Bring user to the market or let them choose an app?
            intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("market://details?id=" + packageName));
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public void startNewCalanderActivity(Context context, String packageName) {
        Intent intent = context.getPackageManager().getLaunchIntentForPackage(packageName);
        if (intent == null) {
            // Bring user to the market or let them choose an app?
            intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("market://details?id=" + packageName));
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public int GetPixalFromDP(Context context,float dp){
        Resources r = context.getResources();
        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
        return px;
    }
}
