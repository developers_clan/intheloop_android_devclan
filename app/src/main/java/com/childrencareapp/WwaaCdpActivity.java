package com.childrencareapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.special.ResideMenu.ResideMenu;
import com.special.ResideMenu.ResideMenuItem;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Type;
import java.util.ArrayList;

import Database.MyDbHelper;
import Model.Agency;
import Model.Contact;
import Model.GetCdpStatausModel;
import Model.ParentData;
import Model.Wwaa;
import util.AppManager;
import util.Constants;
import util.MyApp;

public class WwaaCdpActivity extends Activity {

    MyDbHelper dbHelper;

    int childId = -1;
    ArrayList<Integer> matchId;
    TextView tvWebsite,tvPhone,tvConnect;
    Agency agency;

    ResideMenu resideMenu;
    ImageView imgPhone,imgWeb,imgConnect;

    ImageButton openDrawer;
    RelativeLayout rlHeader;

    private ResideMenuItem itemWhile;
    private ResideMenuItem itemWhere;
    private ResideMenuItem itemSocial;

    private ResideMenuItem itemFB=null;
    private ResideMenuItem itemTW=null;
    private ResideMenuItem itemPin=null;
    private ResideMenuItem itemYT=null;
    String fbLink = "",twLink ="",pinLink = "",ytLink ="";

    Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_wwaa_cdp);

        AppManager.getInstance().setFont(this);

        // Obtain the shared Tracker instance.
        MyApp application = (MyApp) getApplication();
        mTracker = application.getDefaultTracker();

        TextView heading = (TextView) findViewById(R.id.heading);
        TextView status = (TextView) findViewById(R.id.status);
        ImageView img_agency = (ImageView)findViewById(R.id.img_agency);
        Button btnHistory = (Button)findViewById(R.id.btn_history);
        TextView contact = (TextView) findViewById(R.id.contact);
        TextView tvChildName = (TextView) findViewById(R.id.tv_childname);
         tvPhone = (TextView) findViewById(R.id.tv_phone);
         tvWebsite = (TextView) findViewById(R.id.tv_website);
         tvConnect = (TextView) findViewById(R.id.tv_connect);
        imgPhone = (ImageView)findViewById(R.id.imgPhone);
        imgWeb = (ImageView)findViewById(R.id.imgWeb);
        imgConnect = (ImageView)findViewById(R.id.imgConnect);
        ListView listView = (ListView) findViewById(R.id.listView);
        RelativeLayout relBar = (RelativeLayout)findViewById(R.id.rel_bar);
        RelativeLayout relBar2 = (RelativeLayout)findViewById(R.id.rel_bar2);
//        RelativeLayout relImg = (RelativeLayout)findViewById(R.id.relImg);
        ImageView imgChild = (ImageView)findViewById(R.id.imgChild);

        rlHeader = (RelativeLayout) findViewById(R.id.rlHeader);
        openDrawer = (ImageButton)findViewById(R.id.btn_drawer);

        heading.setTypeface(AppManager.getInstance().getBlackTypeface());
        status.setTypeface(AppManager.getInstance().getBlackTypeface());
        btnHistory.setTypeface(AppManager.getInstance().getBoldTypeface());
        contact.setTypeface(AppManager.getInstance().getBlackTypeface());
        tvPhone.setTypeface(AppManager.getInstance().getRegularTypeface());
        tvWebsite.setTypeface(AppManager.getInstance().getRegularTypeface());
        tvConnect.setTypeface(AppManager.getInstance().getRegularTypeface());
        tvChildName.setTypeface(AppManager.getInstance().getRegularTypeface());




        dbHelper = new MyDbHelper(this);

         agency = AppManager.getInstance().getAgencyModel();

        setUpMenu();

        rlHeader.setVisibility(View.VISIBLE);
        rlHeader.setBackgroundColor(getResources().getColor(R.color.red_text));

        if (agency.getWwaaMainDetails().size()>0) {
            heading.setText(agency.getWwaaMainDetails().get(0).getWwaaMainTitle());
        }

        if (agency.getAgencyPhone().size()>0  && (agency.getAgencyPhone().get(0).getNumberAlias() != null && !agency.getAgencyPhone().get(0).getNumberAlias().equalsIgnoreCase(""))) {
            tvPhone.setText(agency.getAgencyPhone().get(0).getNumberAlias());
            imgPhone.setVisibility(View.VISIBLE);
        }
        else if (agency.getAgencyPhone().size()>0  && (agency.getAgencyPhone().get(0).getNumber() != null && !agency.getAgencyPhone().get(0).getNumber().equalsIgnoreCase(""))) {
            tvPhone.setText(agency.getAgencyPhone().get(0).getNumber());
            imgPhone.setVisibility(View.VISIBLE);
        }
        else {
            imgPhone.setVisibility(View.INVISIBLE);
        }
        if (agency.getWebsite().size()>0 && !agency.getWebsite().get(0).getTitle().equalsIgnoreCase("")) {
            tvWebsite.setText(agency.getWebsite().get(0).getTitle());
            imgWeb.setVisibility(View.VISIBLE);
        }
        else {
            imgWeb.setVisibility(View.INVISIBLE);
        }
        if (agency.getAgencyEmail().size()>0 && !agency.getAgencyEmail().get(0).getTitle().equalsIgnoreCase("")) {
            tvConnect.setText(agency.getAgencyEmail().get(0).getTitle());
            imgConnect.setBackgroundResource(R.drawable.email);
            imgConnect.setVisibility(View.VISIBLE);
        }
        else if (agency.getCdpConnect().size()>0 && !agency.getCdpConnect().get(0).getTitle().equalsIgnoreCase("")) {
            tvConnect.setText(agency.getCdpConnect().get(0).getTitle());
            imgConnect.setBackgroundResource(R.drawable.events);
            imgConnect.setVisibility(View.VISIBLE);
        }else {
            imgConnect.setVisibility(View.INVISIBLE);
        }

//        if (agency.getContactLogo().size()>0) {
//            Picasso.with(this).load(Constants.IMG_URL + agency.getContactLogo().get(0).getFilename()).into(img_agency);
//        }
        if (AppManager.getInstance().isFromServer()) {
            if (agency.getContactLogo().size()>0) {
                Picasso.with(this).load(agency.getContactLogo().get(0).getUri()).into(img_agency);
            }
        }
        else{
            if (agency.getAgencyLogo() != null) {
                img_agency.setImageBitmap(BitmapFactory.decodeByteArray(agency.getAgencyLogo(), 0,
                        agency.getAgencyLogo().length));
            }
        }
        if (agency.getColorCode().size()>0) {
            relBar.setBackgroundColor(Color.parseColor(agency.getColorCode().get(0)));
            relBar2.setBackgroundColor(Color.parseColor(agency.getColorCode().get(0)));
        }

        SharedPreferences pref = getSharedPreferences(Constants.WWAACHILD_PREF, Activity.MODE_PRIVATE);
        if (pref.contains(Constants.CHILD_POS)) {
            int pos = pref.getInt(Constants.CHILD_POS, -1);
            childId = AppManager.getInstance().getChildModels().get(pos).getId();
            tvChildName.setText(AppManager.getInstance().getChildModels().get(pos).getName());
            if (AppManager.getInstance().getChildModels().get(pos).getGender() == 0){
                imgChild.setBackgroundResource(R.drawable.boy_in_circle);
            }
            else if (AppManager.getInstance().getChildModels().get(pos).getGender() == 1){
                imgChild.setBackgroundResource(R.drawable.where_are_banner_girl_user_placeholder);
            }
        }


        ArrayList<GetCdpStatausModel> list = dbHelper.getCdpStatus(childId,agency.getNid());
        ArrayList<ParentData> datas = new ArrayList<>();
        if (list.size()>0) {
            GetCdpStatausModel model = list.get(list.size() - 1);
            GsonBuilder gsonb = new GsonBuilder();
            Gson gson = gsonb.create();
            Type type = new TypeToken<ArrayList<ParentData>>() {
            }.getType();
            datas = gson.fromJson(model.getList(), type);
        }
        AppManager.getInstance().setParentModels(datas);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.custom_cdp_list, null, false);
        int h = ((LinearLayout) view.findViewById(R.id.linear)).getMinimumHeight();

        ViewGroup.LayoutParams prams = listView.getLayoutParams();
        prams.height = (h+GetPixelFromDips(11)) * datas.size(); //AppManager.getInstance().getListHieght();
        listView.setLayoutParams(prams);

        CustomAdapter adapter = new CustomAdapter(this, R.layout.custom_cdp_list, datas);
        listView.setAdapter(adapter);


//        SharedPreferences status_pref = getSharedPreferences(Constants.CDP_STATUS_PREF,Activity.MODE_PRIVATE);
//        if(status_pref.contains(Constants.CDP_STATUS))
//        {
//            GsonBuilder gsonb = new GsonBuilder();
//            Gson gson = gsonb.create();
//            Type type = new TypeToken<ArrayList<ArrayList<ParentData>>>() {
//            }.getType();
//
//                ArrayList<ArrayList<ParentData>> listArrayList = gson.fromJson(status_pref.getString(Constants.CDP_STATUS, ""), type);
//                AppManager.getInstance().setListArrayList(listArrayList);
//        }


//        if (childId>-1)
//        {
//            for (int i=0;i<AppManager.getInstance().getChildIds().size();i++)
//            {
//                if (childId == AppManager.getInstance().getChildIds().get(i))
//                {
//                    matchId.add(AppManager.getInstance().getChildIds().get(i));
//                }
//
////                ArrayList<ParentData> dataWRTchildID = getChildIdList(parentDatas);
//
//            }
//        }
//
//        ArrayList<ParentData> showDataList = AppManager.getInstance().getListArrayList().get(matchId.size()-1);
//
//        AppManager.getInstance().setParentModels(showDataList);


    }

    @Override
    protected void onResume() {
        super.onResume();

        mTracker.setScreenName(Constants.WWAAActivity + "-" + agency.getHomeMainDetails().get(0).getHomeMainTitle());
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

    }

    //    private ArrayList<ParentData>  getChildIdList(ArrayList<ParentData> list) {
//        ArrayList<ParentData> datas = new ArrayList<>();
//        for (int i = 0;i<list.size();i++)
//        {
//            if (childId == list.get(i).getChildId())
//            {
//                datas.add(list.get(i));
//            }
//        }
//        return datas;
//    }

    public void onClick(View v) {
        if (v.getId() == R.id.btn_back) {
            try {
                finish();
            }catch (Exception e){
                e.printStackTrace();
            }
//            Intent intent = new Intent(getParent(),CDPActivity.class);
//            TabGroupActivity parentActivity = (TabGroupActivity) getParent();
//            parentActivity.startChildActivity("CDPActivity", intent);
//            startActivity(new Intent(WwaaCdpActivity.this, HomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        } else if (v.getId() == R.id.btn_history) {
//            startActivity(new Intent(WwaaCdpActivity.this, WwaaCdpHistoryActivity.class));
//            finish();

            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory(Constants.kCatAgencyContact)
                    .setAction(Constants.WWAAHistory + "-" + agency.getHomeMainDetails().get(0).getHomeMainTitle())
                    .build());

            Intent intent = new Intent(getParent(),WwaaCdpHistoryActivity.class);
            TabGroupActivity parentActivity = (TabGroupActivity) getParent();
            parentActivity.startChildActivity("WwaaCdpHistoryActivity", intent);
        }
        else if(v.getId() == R.id.tv_website){

            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory(Constants.kCatAgencyContact)
                    .setAction(agency.getHomeMainDetails().get(0).getHomeMainTitle() + " -Contact Block Footer- "+agency.getWebsite().get(0).getTitle())
                    .build());

            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(agency.getWebsite().get(0).getUrl())));
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        else if(v.getId() == R.id.tv_phone){

            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory(Constants.kCatAgencyContact)
                    .setAction(agency.getHomeMainDetails().get(0).getHomeMainTitle() + " -Contact Block Footer- "+Constants.phone)
                    .build());

            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("tel:" + agency.getAgencyPhone().get(0).getNumber())));
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        else if(v.getId() == R.id.tv_connect){



            try {
                if (agency.getAgencyEmail().size() > 0) {
                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory(Constants.kCatAgencyContact)
                            .setAction(agency.getHomeMainDetails().get(0).getHomeMainTitle() + " -Contact Block Footer- "+agency.getAgencyEmail().get(0).getTitle())
                            .build());
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + agency.getAgencyEmail().get(0).getUrl())));
                } else if (agency.getCdpConnect().size() > 0) {
                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory(Constants.kCatAgencyContact)
                            .setAction(agency.getHomeMainDetails().get(0).getHomeMainTitle() + " -Contact Block Footer- "+agency.getCdpConnect().get(0).getTitle())
                            .build());
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(agency.getCdpConnect().get(0).getUrl())));
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        else if(v.getId()==R.id.tv_childname)
        {

//            mTracker.send(new HitBuilders.EventBuilder()
//                    .setCategory(Constants.kCatAgencyContact)
//                    .setAction(Constants.SelectChild)
//                    .build());

            try {
                WwaaCdpActivity.this.finish();
                Intent intent = new Intent(getParent(), MyTeamActivity.class);
                intent.putExtra("WWAA",true);
                TabGroupActivity parentActivity = (TabGroupActivity) getParent();
                parentActivity.startChildActivity("MyTeamActivity", intent);

            }catch (Exception e){
                e.printStackTrace();
            }

        }
    }

    private void setUpMenu() {

        Contact contct = null;

        // attach to current activity;
        resideMenu = new ResideMenu(this);
        resideMenu.setUse3D(false);
        resideMenu.setBackground(R.drawable.menu_bar_background);
        resideMenu.attachToActivity(this);
        resideMenu.setShadowVisible(false);
        resideMenu.setMenuListener(menuListener);
        //valid scale factor is between 0.0f and 1.0f. leftmenu'width is 150dip.
        resideMenu.setScaleValue(0.6f);

        for (int i=0;i<AppManager.getInstance().getContacts().size();i++)
        {
            if (agency.getNid().equals(AppManager.getInstance().getContacts().get(i).getAgency())){
                contct = AppManager.getInstance().getContacts().get(i);
                break;
            }
        }

        // create menu items;
        if (agency.getWwaaMainDetails().size()>0) {
            itemWhere = new ResideMenuItem(this, R.drawable.where_we_are_at, agency.getWwaaMainDetails().get(0).getWwaaMainTitle());
        }
        if (agency.getWywMainDetails().size() > 0) {
            itemWhile = new ResideMenuItem(this, R.drawable.while_you_wait, agency.getWywMainDetails().get(0).getWywMainTitle());
        }
//        itemSocial  = new ResideMenuItem(this, R.drawable.fb,R.drawable.twitter);
        if (contct != null) {
            try {

                for(int i=0;i<contct.getSocialLinks().size();i++) {
                    if (contct.getSocialLinks().get(i).getTitle().equalsIgnoreCase("Facebook")) {
                        fbLink = contct.getSocialLinks().get(i).getUrl();
                        itemFB = new ResideMenuItem(this, R.drawable.fb, "Facebook");
                    }
                }
                for(int i=0;i<contct.getSocialLinks().size();i++) {
                    if (contct.getSocialLinks().get(i).getTitle().equalsIgnoreCase("Twitter")){
                        twLink = contct.getSocialLinks().get(i).getUrl();
                        itemTW = new ResideMenuItem(this, R.drawable.twitter, "Twitter");
                    }

                }
                for(int i=0;i<contct.getSocialLinks().size();i++) {
                    if (contct.getSocialLinks().get(i).getTitle().equalsIgnoreCase("Pinterest")) {
                        pinLink = contct.getSocialLinks().get(i).getUrl();
                        itemPin = new ResideMenuItem(this, R.drawable.menu_bar_pin, "Pinterest");
                    }
                }
                for(int i=0;i<contct.getSocialLinks().size();i++) {
                    if (contct.getSocialLinks().get(i).getTitle().equalsIgnoreCase("YouTube")){
                        ytLink = contct.getSocialLinks().get(i).getUrl();
                        itemYT = new ResideMenuItem(this, R.drawable.menu_bar_yotube, "YouTube");
                    }

                }
//                itemSocial = new ResideMenuItem(this, R.drawable.fb, R.drawable.twitter,R.drawable.menu_bar_pin,R.drawable.menu_bar_yotube, twLink, fbLink,pinLink,ytLink);
            } catch (Exception e) {
                Log.e("CDP MENU", e.getLocalizedMessage());
            }
        } else {
//            itemSocial = new ResideMenuItem(this, R.drawable.fb, R.drawable.twitter);
        }

        itemWhere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (AppManager.getInstance().getChildModels().size() > 0) {
//                    MyDbHelper dbHelper = new MyDbHelper(WwaaCdpActivity.this);
//                    SharedPreferences pref = getSharedPreferences(Constants.WWAACHILD_PREF, Activity.MODE_PRIVATE);
//                    int pos = -1;
//                    if (pref.contains(Constants.CHILD_POS)) {
//                        pos = pref.getInt(Constants.CHILD_POS, -1);
//                    }
//
//                    if (dbHelper.getCdpStatus(AppManager.getInstance().getChildModels().get(pos).getId(),agency.getNid()).size() > 0) {
//                        Intent intent = new Intent(WwaaCdpActivity.this, WwaaCdpActivity.class);
//                        startActivity(intent);
//                    } else {
//                        AppManager.getInstance().setParentModels(new ArrayList<ParentData>());
//                        Intent intent = new Intent(WwaaCdpActivity.this, WwaaCdpStatusActivity.class);
//                        startActivity(intent);
//                    }
////                    }
//                }
//                else {
//                    Toast.makeText(getApplicationContext(), "Add Child First!", Toast.LENGTH_SHORT).show();
//                }

                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory(Constants.kCatSideMenu)
                        .setAction(Constants.MyServicesMenuWWAA+"-"+agency.getHomeMainDetails().get(0).getHomeMainTitle())
                        .build());

                resideMenu.closeMenu();
            }
        });
        itemWhile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory(Constants.kCatSideMenu)
                        .setAction(Constants.WYWMenuWWAA+"-"+agency.getHomeMainDetails().get(0).getHomeMainTitle())
                        .build());

                if (AppManager.getInstance().getChildModels().size() > 0) {
//                    Intent intent = new Intent(WwaaCdpActivity.this, WhileYouWaitActivity.class);
//                    startActivity(intent);
                    try {
                        resideMenu.closeMenu();
                        finish();
                        Intent intent = new Intent(getParent(), WhileYouWaitActivity.class);
                        TabGroupActivity parentActivity = (TabGroupActivity) getParent();
                        parentActivity.startChildActivity("WhileYouWaitActivity", intent);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Add Child First!", Toast.LENGTH_SHORT).show();
                }
                resideMenu.closeMenu();
            }
        });
        if (itemFB != null) {
            itemFB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!fbLink.equalsIgnoreCase("")) {

                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory(Constants.kCatSideMenu)
                                .setAction(agency.getHomeMainDetails().get(0).getHomeMainTitle() + " - Facebook")
                                .build());

                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(fbLink)));
                    }
                }
            });
        }
        if (itemTW != null) {
            itemTW.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!twLink.equalsIgnoreCase("")) {

                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory(Constants.kCatSideMenu)
                                .setAction(agency.getHomeMainDetails().get(0).getHomeMainTitle() + " - Twitter")
                                .build());
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(twLink)));
                    }
                }
            });
        }
        if (itemPin != null) {
            itemPin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!pinLink.equalsIgnoreCase("")) {
                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory(Constants.kCatSideMenu)
                                .setAction(agency.getHomeMainDetails().get(0).getHomeMainTitle() + " - Pinterest")
                                .build());
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(pinLink)));
                    }
                }
            });
        }
        if (itemYT != null) {
            itemYT.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!ytLink.equalsIgnoreCase("")) {
                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory(Constants.kCatSideMenu)
                                .setAction(agency.getHomeMainDetails().get(0).getHomeMainTitle() + " -  YouTube")
                                .build());
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(ytLink)));
                    }
                }
            });
        }
//        itemCalendar.setOnClickListener(this);
//        itemSettings.setOnClickListener(this);

//        resideMenu.addMenuItem(itemWhere, ResideMenu.DIRECTION_LEFT);
//        resideMenu.addMenuItem(itemWhile, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(itemWhere, ResideMenu.DIRECTION_RIGHT);
        resideMenu.addMenuItem(itemWhile, ResideMenu.DIRECTION_RIGHT);
//        resideMenu.addMenuItem(itemSocial, ResideMenu.DIRECTION_RIGHT);

        if (itemFB != null){
            resideMenu.addMenuItem(itemFB, ResideMenu.DIRECTION_RIGHT);
        }
        if (itemTW != null){
            resideMenu.addMenuItem(itemTW, ResideMenu.DIRECTION_RIGHT);
        }
        if (itemPin != null){
            resideMenu.addMenuItem(itemPin, ResideMenu.DIRECTION_RIGHT);
        }
        if (itemYT != null){
            resideMenu.addMenuItem(itemYT, ResideMenu.DIRECTION_RIGHT);
        }

        // You can disable a direction by setting ->
        resideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_LEFT);

//        findViewById(R.id.title_bar_left_menu).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                resideMenu.openMenu(ResideMenu.DIRECTION_LEFT);
//            }
//        });
        openDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resideMenu.openMenu(ResideMenu.DIRECTION_RIGHT);
                mTracker.setScreenName(Constants.SideMenuScreen+"-"+ agency.getHomeMainDetails().get(0).getHomeMainTitle());
                mTracker.send(new HitBuilders.ScreenViewBuilder().build());
            }
        });
    }

    private ResideMenu.OnMenuListener menuListener = new ResideMenu.OnMenuListener() {
        @Override
        public void openMenu() {
            rlHeader.setVisibility(View.GONE);
//            Toast.makeText(getApplicationContext(), "Menu is opened!", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void closeMenu() {
            rlHeader.setVisibility(View.VISIBLE);
//            Toast.makeText(getApplicationContext(), "Menu is closed!", Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    public void onBackPressed() {

//        startActivity(new Intent(WwaaCdpActivity.this, HomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        super.onBackPressed();
    }

    class CustomAdapter extends ArrayAdapter<ParentData> {
        ArrayList<ParentData> list;
        Context context;

        public CustomAdapter(Context context, int resource, ArrayList<ParentData> objects) {
            super(context, resource, objects);

            this.list = objects;
            this.context = context;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View view = convertView;
            final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (view == null) {
                view = inflater.inflate(R.layout.custom_cdp_list, null);
            }

            TextView program = (TextView) view.findViewById(R.id.tv_program);
            TextView chkbx = (TextView) view.findViewById(R.id.tv_chkbx);
            TextView status = (TextView) view.findViewById(R.id.tv_status);
            Button btnStatus = (Button) view.findViewById(R.id.btn_status);

            program.setTypeface(AppManager.getInstance().getBlackTypeface());
            chkbx.setTypeface(AppManager.getInstance().getBoldTypeface());
            status.setTypeface(AppManager.getInstance().getBoldTypeface());


            Wwaa parent1 = AppManager.getInstance().getAgencyModel().getWwaa().get(list.get(position).getParentId());
            program.setText(parent1.getWWAATitle().getMarkup());
            if (list.get(position).getChildData() != null) {
                chkbx.setText(parent1.getWWAAStatus().get(list.get(position).getChildData().getChildPos()).getMarkup());
                int statusId = list.get(position).getChildData().getStatusId();
                switch (statusId) {
                    case 0:
                        status.setText("Waiting");
                        status.setBackgroundColor(getResources().getColor(R.color.green));
                        break;
                    case 1:
                        status.setText("Accessing");
                        status.setBackgroundColor(getResources().getColor(R.color.yellow));
                        break;
                    case 2:
                        status.setText("Completed");
                        status.setBackgroundColor(getResources().getColor(R.color.red_text));
                        break;
                }
            }

            btnStatus.setOnClickListener(new View.OnClickListener() {
                int p = position;
                @Override
                public void onClick(View v) {
                    try {
                        finish();
                        Intent intent = new Intent(getParent(), WwaaCdpStatusActivity.class);
                        intent.putExtra("INDEX",list.get(p).getParentId());
                        TabGroupActivity parentActivity = (TabGroupActivity) getParent();
                        parentActivity.startChildActivity("WwaaCdpStatusActivity", intent);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });

            return view;
        }

    }

    public int GetPixelFromDips(float pixels) {
        // Get the screen's density scale
        final float scale = getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f);
    }

}
