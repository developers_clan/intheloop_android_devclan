package Model;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Contact {

    @SerializedName("contact_logo")
    @Expose
    private List<ContactLogo> contactLogo = new ArrayList<ContactLogo>();
    @SerializedName("phone")
    @Expose
    private ContactPhone phone;
    @SerializedName("color_code")
    @Expose
    private String colorCode;
    @SerializedName("agency")
    @Expose
    private String agency;
//    @SerializedName("agency_docs")
//    @Expose
//    private AgencyDocs agencyDocs;
    @SerializedName("social_links")
    @Expose
    private List<SocialLink> socialLinks = new ArrayList<SocialLink>();

    /**
     *
     * @return
     * The contactLogo
     */
    public List<ContactLogo> getContactLogo() {
        return contactLogo;
    }

    /**
     *
     * @param contactLogo
     * The contact_logo
     */
    public void setContactLogo(List<ContactLogo> contactLogo) {
        this.contactLogo = contactLogo;
    }

    /**
     *
     * @return
     * The phone
     */
    public ContactPhone getPhone() {
        return phone;
    }

    /**
     *
     * @param phone
     * The phone
     */
    public void setPhone(ContactPhone phone) {
        this.phone = phone;
    }

    /**
     *
     * @return
     * The colorCode
     */
    public String getColorCode() {
        return colorCode;
    }

    /**
     *
     * @param colorCode
     * The color_code
     */
    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    /**
     *
     * @return
     * The agency
     */
    public String getAgency() {
        return agency;
    }

    /**
     *
     * @param agency
     * The agency
     */
    public void setAgency(String agency) {
        this.agency = agency;
    }

    /**
//     *
//     * @return
//     * The agencyDocs
//     */
//    public AgencyDocs getAgencyDocs() {
//        return agencyDocs;
//    }
//
//    /**
//     *
//     * @param agencyDocs
//     * The agency_docs
//     */
//    public void setAgencyDocs(AgencyDocs agencyDocs) {
//        this.agencyDocs = agencyDocs;
//    }

    /**
     *
     * @return
     * The socialLinks
     */
    public List<SocialLink> getSocialLinks() {
        return socialLinks;
    }

    /**
     *
     * @param socialLinks
     * The social_links
     */
    public void setSocialLinks(List<SocialLink> socialLinks) {
        this.socialLinks = socialLinks;
    }

}