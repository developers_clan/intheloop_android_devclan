package Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class AgencyHome {

    @SerializedName("Home Title")
    @Expose
    private HomeTitle homeTitle;
    @SerializedName("Home Body")
    @Expose
    private HomeBody homeBody;

    /**
     *
     * @return
     * The HomeTitle
     */
    public HomeTitle getHomeTitle() {
        return homeTitle;
    }

    /**
     *
     * @param homeTitle
     * The Home Title
     */
    public void setHomeTitle(HomeTitle homeTitle) {
        this.homeTitle = homeTitle;
    }

    /**
     *
     * @return
     * The HomeBody
     */
    public HomeBody getHomeBody() {
        return homeBody;
    }

    /**
     *
     * @param homeBody
     * The Home Body
     */
    public void setHomeBody(HomeBody homeBody) {
        this.homeBody = homeBody;
    }

}