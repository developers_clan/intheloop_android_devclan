package Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Wyw {

    @SerializedName("WYW Title")
    @Expose
    private WYWTitle wywTitle;
    @SerializedName("WYW Body")
    @Expose
    private WYWBody wywBody;

    /**
     *
     * @return
     * The WYWTitle
     */
    public WYWTitle getWYWTitle() {
        return wywTitle;
    }

    /**
     *
     * @param wywTitle
     * The WYW Title
     */
    public void setWYWTitle(WYWTitle wywTitle) {
        this.wywTitle = wywTitle;
    }

    /**
     *
     * @return
     * The WYWBody
     */
    public WYWBody getWYWBody() {
        return wywBody;
    }

    /**
     *
     * @param wywBody
     * The WYW Body
     */
    public void setWYWBody(WYWBody wywBody) {
        this.wywBody = wywBody;
    }

}