package Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WwaaMainDetail {

    @SerializedName("wwaa_main_title")
    @Expose
    private String wwaaMainTitle;
    @SerializedName("wwaa_main_body")
    @Expose
    private String wwaaMainBody;

    /**
     *
     * @return
     * The wwaaMainTitle
     */
    public String getWwaaMainTitle() {
        return wwaaMainTitle;
    }

    /**
     *
     * @param wwaaMainTitle
     * The wwaa_main_title
     */
    public void setWwaaMainTitle(String wwaaMainTitle) {
        this.wwaaMainTitle = wwaaMainTitle;
    }

    /**
     *
     * @return
     * The wwaaMainBody
     */
    public String getWwaaMainBody() {
        return wwaaMainBody;
    }

    /**
     *
     * @param wwaaMainBody
     * The wwaa_main_body
     */
    public void setWwaaMainBody(String wwaaMainBody) {
        this.wwaaMainBody = wwaaMainBody;
    }

}