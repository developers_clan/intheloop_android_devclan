package Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Resources {

    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("summary")
    @Expose
    private String summary;
    @SerializedName("format")
    @Expose
    private String format;
    @SerializedName("safe_value")
    @Expose
    private String safeValue;
    @SerializedName("safe_summary")
    @Expose
    private String safeSummary;

    /**
     *
     * @return
     * The value
     */
    public String getValue() {
        return value;
    }

    /**
     *
     * @param value
     * The value
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     *
     * @return
     * The summary
     */
    public String getSummary() {
        return summary;
    }

    /**
     *
     * @param summary
     * The summary
     */
    public void setSummary(String summary) {
        this.summary = summary;
    }

    /**
     *
     * @return
     * The format
     */
    public String getFormat() {
        return format;
    }

    /**
     *
     * @param format
     * The format
     */
    public void setFormat(String format) {
        this.format = format;
    }

    /**
     *
     * @return
     * The safeValue
     */
    public String getSafeValue() {
        return safeValue;
    }

    /**
     *
     * @param safeValue
     * The safe_value
     */
    public void setSafeValue(String safeValue) {
        this.safeValue = safeValue;
    }

    /**
     *
     * @return
     * The safeSummary
     */
    public String getSafeSummary() {
        return safeSummary;
    }

    /**
     *
     * @param safeSummary
     * The safe_summary
     */
    public void setSafeSummary(String safeSummary) {
        this.safeSummary = safeSummary;
    }

}