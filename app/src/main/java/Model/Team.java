package Model;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Team {

    @SerializedName("agency_thumbnail")
    @Expose
    private List<TeamAgencyThumbnail> agencyThumbnail = new ArrayList<TeamAgencyThumbnail>();
    @SerializedName("discipline")
    @Expose
    private List<TeamDiscipline> discipline = new ArrayList<TeamDiscipline>();
    @SerializedName("email")
    @Expose
    private List<TeamEmail> email = new ArrayList<TeamEmail>();
    @SerializedName("offices")
    @Expose
    private List<TeamOffice> offices = new ArrayList<TeamOffice>();
    @SerializedName("headshot")
    @Expose
    private List<TeamHeadshot> headshot = new ArrayList<TeamHeadshot>();
    @SerializedName("gender")
    @Expose
    private List<TeamGender> gender = new ArrayList<TeamGender>();
    @SerializedName("name")
    @Expose
    private List<TeamName> name = new ArrayList<TeamName>();
    @SerializedName("team_phone")
    @Expose
    private List<TeamPhone> teamPhone = new ArrayList<TeamPhone>();
    @SerializedName("nid")
    @Expose
    private String nid;
    @SerializedName("title")
    @Expose
    private String title;


    /**
     *
     * @return
     * The agencyThumbnail
     */
    public List<TeamAgencyThumbnail> getAgencyThumbnail() {
        return agencyThumbnail;
    }

    /**
     *
     * @param agencyThumbnail
     * The agency_thumbnail
     */
    public void setAgencyThumbnail(List<TeamAgencyThumbnail> agencyThumbnail) {
        this.agencyThumbnail = agencyThumbnail;
    }

    /**
     *
     * @return
     * The discipline
     */
    public List<TeamDiscipline> getDiscipline() {
        return discipline;
    }

    /**
     *
     * @param discipline
     * The discipline
     */
    public void setDiscipline(List<TeamDiscipline> discipline) {
        this.discipline = discipline;
    }

    /**
     *
     * @return
     * The email
     */
    public List<TeamEmail> getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    public void setEmail(List<TeamEmail> email) {
        this.email = email;
    }

    /**
     *
     * @return
     * The offices
     */
    public List<TeamOffice> getOffices() {
        return offices;
    }

    /**
     *
     * @param offices
     * The offices
     */
    public void setOffices(List<TeamOffice> offices) {
        this.offices = offices;
    }

    /**
     *
     * @return
     * The headshot
     */
    public List<TeamHeadshot> getHeadshot() {
        return headshot;
    }

    /**
     *
     * @param headshot
     * The headshot
     */
    public void setHeadshot(List<TeamHeadshot> headshot) {
        this.headshot = headshot;
    }

    /**
     *
     * @return
     * The gender
     */
    public List<TeamGender> getGender() {
        return gender;
    }

    /**
     *
     * @param gender
     * The gender
     */
    public void setGender(List<TeamGender> gender) {
        this.gender = gender;
    }

    /**
     *
     * @return
     * The name
     */
    public List<TeamName> getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(List<TeamName> name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The teamPhone
     */
    public List<TeamPhone> getTeamPhone() {
        return teamPhone;
    }

    /**
     *
     * @param teamPhone
     * The team_phone
     */
    public void setTeamPhone(List<TeamPhone> teamPhone) {
        this.teamPhone = teamPhone;
    }

    /**
     *
     * @return
     * The nid
     */
    public String getNid() {
        return nid;
    }

    /**
     *
     * @param nid
     * The nid
     */
    public void setNid(String nid) {
        this.nid = nid;
    }

    /**
     *
     * @return
     * The title
     */
    public String getTitle() {
        return title;
    }

    /**
     *
     * @param title
     * The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

}