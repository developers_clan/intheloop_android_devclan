package Model;

/**
 * Created by macbook on 31/10/15.
 */
public class CtnStatusModel {

    private int childId;
    private String status;
    private String date;
    private String time;

    public int getChildId() {
        return childId;
    }

    public String getStatus() {
        return status;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public void setChildId(int childId) {
        this.childId = childId;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
