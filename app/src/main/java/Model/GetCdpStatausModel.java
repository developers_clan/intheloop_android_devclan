package Model;

/**
 * Created by macbook on 04/11/15.
 */
public class GetCdpStatausModel {

    int id;
    int childId;
    String date;
    String time;
    String list;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getChildId() {
        return childId;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public String getList() {
        return list;
    }

    public void setChildId(int childId) {
        this.childId = childId;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setList(String list) {
        this.list = list;
    }
}
