package Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StatusTitle {

    @SerializedName("#markup")
    @Expose
    private String Markup;

    /**
     *
     * @return
     * The Markup
     */
    public String getMarkup() {
        return Markup;
    }

    /**
     *
     * @param Markup
     * The #markup
     */
    public void setMarkup(String Markup) {
        this.Markup = Markup;
    }

}