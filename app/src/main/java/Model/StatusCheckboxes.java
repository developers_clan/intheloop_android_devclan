package Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StatusCheckboxes {

    @SerializedName("Status Title")
    @Expose
    private StatusTitle statusTitle;
    @SerializedName("Status Body")
    @Expose
    private StatusBody statusBody;
    @SerializedName("Status Checkbox")
    @Expose
    private StatusCheckbox statusCheckbox;

    /**
     *
     * @return
     * The StatusTitle
     */
    public StatusTitle getStatusTitle() {
        return statusTitle;
    }

    /**
     *
     * @param statusTitle
     * The Status Title
     */
    public void setStatusTitle(StatusTitle statusTitle) {
        this.statusTitle = statusTitle;
    }

    /**
     *
     * @return
     * The StatusBody
     */
    public StatusBody getStatusBody() {
        return statusBody;
    }

    /**
     *
     * @param statusBody
     * The Status Body
     */
    public void setStatusBody(StatusBody statusBody) {
        this.statusBody = statusBody;
    }

    /**
     *
     * @return
     * The StatusCheckbox
     */
    public StatusCheckbox getStatusCheckbox() {
        return statusCheckbox;
    }

    /**
     *
     * @param statusCheckbox
     * The Status Checkbox
     */
    public void setStatusCheckbox(StatusCheckbox statusCheckbox) {
        this.statusCheckbox = statusCheckbox;
    }

}