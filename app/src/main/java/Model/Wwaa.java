package Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Wwaa {

    @SerializedName("WWAA Title")
    @Expose
    private WWAATitle wwaaTitle;
    @SerializedName("WWAA Body")
    @Expose
    private WWAABody wwaaBody;
    @SerializedName("WWAA Status")
    @Expose
    private List<WWAAStatus> wwaaStatus = new ArrayList<WWAAStatus>();

    /**
     *
     * @return
     * The WWAATitle
     */
    public WWAATitle getWWAATitle() {
        return wwaaTitle;
    }

    /**
     *
     * @param wwaaTitle
     * The WWAA Title
     */
    public void setWWAATitle(WWAATitle wwaaTitle) {
        this.wwaaTitle = wwaaTitle;
    }

    /**
     *
     * @return
     * The WWAABody
     */
    public WWAABody getWWAABody() {
        return wwaaBody;
    }

    /**
     *
     * @param wwaaBody
     * The WWAA Body
     */
    public void setWWAABody(WWAABody wwaaBody) {
        this.wwaaBody = wwaaBody;
    }

    /**
     *
     * @return
     * The WWAAStatus
     */
    public List<WWAAStatus> getWWAAStatus() {
        return wwaaStatus;
    }

    /**
     *
     * @param wwaaStatus
     * The WWAA Status
     */
    public void setWWAAStatus(List<WWAAStatus> wwaaStatus) {
        this.wwaaStatus = wwaaStatus;
    }

}