package Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomeTextRes {

    @SerializedName("home")
    @Expose
    private String home;

    /**
     *
     * @return
     * The home
     */
    public String getHome() {
        return home;
    }

    /**
     *
     * @param home
     * The home
     */
    public void setHome(String home) {
        this.home = home;
    }

}