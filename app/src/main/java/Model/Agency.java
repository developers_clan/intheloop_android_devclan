package Model;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Agency {

    private String agencyImage;
    private byte[] agencyLogo;

    public String getAgencyImage() {
        return agencyImage;
    }

    public void setAgencyImage(String agencyImage) {
        this.agencyImage = agencyImage;
    }

    public byte[] getAgencyLogo() {
        return agencyLogo;
    }

    public void setAgencyLogo(byte[] agencyLogo) {
        this.agencyLogo = agencyLogo;
    }

    @SerializedName("image")
    @Expose
    private List<Image> image = new ArrayList<Image>();
    @SerializedName("contact_logo")
    @Expose
    private List<ContactLogo> contactLogo = new ArrayList<ContactLogo>();
    @SerializedName("color_code")
    @Expose
    private List<String> colorCode = new ArrayList<String>();
    @SerializedName("agency_phone")
    @Expose
    private List<AgencyPhone> agencyPhone = new ArrayList<AgencyPhone>();
    @SerializedName("website")
    @Expose
    private List<Website> website = new ArrayList<Website>();
    @SerializedName("cdp_connect")
    @Expose
    private List<CdpConnect> cdpConnect = new ArrayList<CdpConnect>();
    @SerializedName("agency_email")
    @Expose
    private List<CdpConnect> agencyEmail = new ArrayList<CdpConnect>();
    @SerializedName("resources")
    @Expose
    private List<Resources> resources = new ArrayList<Resources>();
    @SerializedName("home_main_details")
    @Expose
    private List<HomeMainDetails> homeMainDetails = new ArrayList<HomeMainDetails>();
    @SerializedName("agency_home")
    @Expose
    private List<AgencyHome> agencyHome = new ArrayList<AgencyHome>();
    @SerializedName("wwaa_main_details")
    @Expose
    private List<WwaaMainDetail> wwaaMainDetails = new ArrayList<WwaaMainDetail>();
    @SerializedName("wwaa")
    @Expose
    private List<Wwaa> wwaa = new ArrayList<Wwaa>();
    @SerializedName("wwaa_actual_status")
    @Expose
    private List<WwaaActualStatus> wwaaActualStatus = new ArrayList<WwaaActualStatus>();
    @SerializedName("wyw_main_details")
    @Expose
    private List<WywMainDetail> wywMainDetails = new ArrayList<WywMainDetail>();
    @SerializedName("wyw")
    @Expose
    private List<Wyw> wyw = new ArrayList<Wyw>();
    @SerializedName("nid")
    @Expose
    private String nid;

    /**
     *
     * @return
     * The image
     */
    public List<Image> getImage() {
        return image;
    }

    /**
     *
     * @param image
     * The image
     */
    public void setImage(List<Image> image) {
        this.image = image;
    }

    /**
     *
     * @return
     * The contactLogo
     */
    public List<ContactLogo> getContactLogo() {
        return contactLogo;
    }

    /**
     *
     * @param contactLogo
     * The contact_logo
     */
    public void setContactLogo(List<ContactLogo> contactLogo) {
        this.contactLogo = contactLogo;
    }

    /**
     *
     * @return
     * The colorCode
     */
    public List<String> getColorCode() {
        return colorCode;
    }

    /**
     *
     * @param colorCode
     * The color_code
     */
    public void setColorCode(List<String> colorCode) {
        this.colorCode = colorCode;
    }

    /**
     *
     * @return
     * The agencyPhone
     */
    public List<AgencyPhone> getAgencyPhone() {
        return agencyPhone;
    }

    /**
     *
     * @param agencyPhone
     * The agency_phone
     */
    public void setAgencyPhone(List<AgencyPhone> agencyPhone) {
        this.agencyPhone = agencyPhone;
    }

    /**
     *
     * @return
     * The website
     */
    public List<Website> getWebsite() {
        return website;
    }

    /**
     *
     * @param website
     * The website
     */
    public void setWebsite(List<Website> website) {
        this.website = website;
    }

    /**
     *
     * @return
     * The cdpConnect
     */
    public List<CdpConnect> getCdpConnect() {
        return cdpConnect;
    }

    /**
     *
     * @param cdpConnect
     * The cdp_connect
     */
    public void setCdpConnect(List<CdpConnect> cdpConnect) {
        this.cdpConnect = cdpConnect;
    }

    /**
     *
     * @return
     * The cdpConnect
     */
    public List<CdpConnect> getAgencyEmail() {
        return agencyEmail;
    }

    /**
     *
     * @param agencyEmail
     * The cdp_connect
     */
    public void setAgencyEmail(List<CdpConnect> agencyEmail) {
        this.agencyEmail = agencyEmail;
    }

    /**
     *
     * @return
     * The resources
     */
    public List<Resources> getResources() {
        return resources;
    }

    /**
     *
     * @param resources
     * The resources
     */
    public void setResources(List<Resources> resources) {
        this.resources = resources;
    }

    /**
     *
     * @return
     * The homeMainDetails
     */
    public List<HomeMainDetails> getHomeMainDetails() {
        return homeMainDetails;
    }

    /**
     *
     * @param homeMainDetails
     * The home_main_details
     */
    public void setHomeMainDetails(List<HomeMainDetails> homeMainDetails) {
        this.homeMainDetails = homeMainDetails;
    }

    /**
     *
     * @return
     * The agencyHome
     */
    public List<AgencyHome> getAgencyHome() {
        return agencyHome;
    }

    /**
     *
     * @param agencyHome
     * The agency_home
     */
    public void setAgencyHome(List<AgencyHome> agencyHome) {
        this.agencyHome = agencyHome;
    }

    /**
     *
     * @return
     * The wwaaMainDetails
     */
    public List<WwaaMainDetail> getWwaaMainDetails() {
        return wwaaMainDetails;
    }

    /**
     *
     * @param wwaaMainDetails
     * The wwaa_main_details
     */
    public void setWwaaMainDetails(List<WwaaMainDetail> wwaaMainDetails) {
        this.wwaaMainDetails = wwaaMainDetails;
    }

    /**
     *
     * @return
     * The wwaa
     */
    public List<Wwaa> getWwaa() {
        return wwaa;
    }

    /**
     *
     * @param wwaa
     * The wwaa
     */
    public void setWwaa(List<Wwaa> wwaa) {
        this.wwaa = wwaa;
    }

    /**
     *
     * @return
     * The wwaaActualStatus
     */
    public List<WwaaActualStatus> getWwaaActualStatus() {
        return wwaaActualStatus;
    }

    /**
     *
     * @param wwaaActualStatus
     * The wwaa_actual_status
     */
    public void setWwaaActualStatus(List<WwaaActualStatus> wwaaActualStatus) {
        this.wwaaActualStatus = wwaaActualStatus;
    }

    /**
     *
     * @return
     * The wywMainDetails
     */
    public List<WywMainDetail> getWywMainDetails() {
        return wywMainDetails;
    }

    /**
     *
     * @param wywMainDetails
     * The wyw_main_details
     */
    public void setWywMainDetails(List<WywMainDetail> wywMainDetails) {
        this.wywMainDetails = wywMainDetails;
    }

    /**
     *
     * @return
     * The wyw
     */
    public List<Wyw> getWyw() {
        return wyw;
    }

    /**
     *
     * @param wyw
     * The wyw
     */
    public void setWyw(List<Wyw> wyw) {
        this.wyw = wyw;
    }

    /**
     *
     * @return
     * The nid
     */
    public String getNid() {
        return nid;
    }

    /**
     *
     * @param nid
     * The nid
     */
    public void setNid(String nid) {
        this.nid = nid;
    }

}