package Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AgencyPhone {

    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("country_codes")
    @Expose
    private String countryCodes;
    @SerializedName("extension")
    @Expose
    private String extension;
    @SerializedName("number_alias")
    @Expose
    private String numberAlias;

    /**
     *
     * @return
     * The number
     */
    public String getNumber() {
        return number;
    }

    /**
     *
     * @param number
     * The number
     */
    public void setNumber(String number) {
        this.number = number;
    }

    /**
     *
     * @return
     * The countryCodes
     */
    public String getCountryCodes() {
        return countryCodes;
    }

    /**
     *
     * @param countryCodes
     * The country_codes
     */
    public void setCountryCodes(String countryCodes) {
        this.countryCodes = countryCodes;
    }

    /**
     *
     * @return
     * The extension
     */
    public String getExtension() {
        return extension;
    }

    /**
     *
     * @param extension
     * The extension
     */
    public void setExtension(String extension) {
        this.extension = extension;
    }

    /**
     *
     * @return
     * The numberAlias
     */
    public String getNumberAlias() {
        return numberAlias;
    }

    /**
     *
     * @param numberAlias
     * The extension
     */
    public void setNumberAlias(String numberAlias) {
        this.numberAlias = numberAlias;
    }
}