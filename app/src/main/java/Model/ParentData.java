package Model;

import java.util.ArrayList;

/**
 * Created by macbook on 03/11/15.
 */
public class ParentData {

    int parentId;
    int childId;
    String date;
    String time;
    ChildData childData;

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getChildId() {
        return childId;
    }

    public void setChildId(int childId) {
        this.childId = childId;
    }

    public int getParentId() {
        return parentId;
    }

    public ChildData getChildData() {
        return childData;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public void setChildData(ChildData childData) {
        this.childData = childData;
    }
}
