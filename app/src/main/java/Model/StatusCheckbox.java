package Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class StatusCheckbox {

    @SerializedName("Status Checkbox")
    @Expose
    private List<InnerStatusCheckbox> StatusCheckbox = new ArrayList<InnerStatusCheckbox>();

    /**
     *
     * @return
     * The StatusCheckbox
     */
    public List<InnerStatusCheckbox> getStatusCheckbox() {
        return StatusCheckbox;
    }

    /**
     *
     * @param StatusCheckbox
     * The Status Checkbox
     */
    public void setStatusCheckbox(List<InnerStatusCheckbox> StatusCheckbox) {
        this.StatusCheckbox = StatusCheckbox;
    }

}