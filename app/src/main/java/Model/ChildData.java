package Model;

/**
 * Created by macbook on 02/11/15.
 */
public class ChildData {

    String chkbx;
    int statusId;
    int childPos;
    String date;
    String time;

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getChildPos() {
        return childPos;
    }

    public void setChildPos(int childPos) {
        this.childPos = childPos;
    }

    public void setChkbx(String chkbx) {
        this.chkbx = chkbx;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public String getChkbx() {
        return chkbx;
    }

    public int getStatusId() {
        return statusId;
    }

}
