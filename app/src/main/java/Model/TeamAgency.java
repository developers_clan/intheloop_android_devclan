package Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TeamAgency {

    @SerializedName("#nid")
    @Expose
    private String Nid;

    /**
     *
     * @return
     * The Nid
     */
    public String getNid() {
        return Nid;
    }

    /**
     *
     * @param Nid
     * The #nid
     */
    public void setNid(String Nid) {
        this.Nid = Nid;
    }

}