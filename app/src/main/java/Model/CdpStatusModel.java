package Model;

/**
 * Created by macbook on 02/11/15.
 */
public class CdpStatusModel {

    int programId;
    ChildData childData;

    public ChildData getChildData() {
        return childData;
    }

    public void setChildData(ChildData childData) {
        this.childData = childData;
    }

    public void setProgramId(int programId) {
        this.programId = programId;
    }

    public int getProgramId() {
        return programId;
    }


}
