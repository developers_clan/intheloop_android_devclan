package Model;

/**
 * Created by macbook on 29/10/15.
 */
public class ChildModel {

    int id;
    String name;
    int gender;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public int getGender() {
        return gender;
    }
}
