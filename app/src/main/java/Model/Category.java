package Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Category {

    @SerializedName("tid")
    @Expose
    private String tid;

    /**
     *
     * @return
     * The tid
     */
    public String getTid() {
        return tid;
    }

    /**
     *
     * @param tid
     * The tid
     */
    public void setTid(String tid) {
        this.tid = tid;
    }

}