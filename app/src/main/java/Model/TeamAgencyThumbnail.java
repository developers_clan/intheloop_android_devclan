package Model;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TeamAgencyThumbnail {

    @SerializedName("fid")
    @Expose
    private String fid;
    @SerializedName("uid")
    @Expose
    private String uid;
    @SerializedName("filename")
    @Expose
    private String filename;
    @SerializedName("uri")
    @Expose
    private String uri;
    @SerializedName("filemime")
    @Expose
    private String filemime;
    @SerializedName("filesize")
    @Expose
    private String filesize;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;
    @SerializedName("rdf_mapping")
    @Expose
    private List<Object> rdfMapping = new ArrayList<Object>();
    @SerializedName("alt")
    @Expose
    private String alt;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("width")
    @Expose
    private String width;
    @SerializedName("height")
    @Expose
    private String height;
    @SerializedName("agency")
    @Expose
    private TeamAgency teamAgency;

    /**
     *
     * @return
     * The fid
     */
    public String getFid() {
        return fid;
    }

    /**
     *
     * @param fid
     * The fid
     */
    public void setFid(String fid) {
        this.fid = fid;
    }

    /**
     *
     * @return
     * The uid
     */
    public String getUid() {
        return uid;
    }

    /**
     *
     * @param uid
     * The uid
     */
    public void setUid(String uid) {
        this.uid = uid;
    }

    /**
     *
     * @return
     * The filename
     */
    public String getFilename() {
        return filename;
    }

    /**
     *
     * @param filename
     * The filename
     */
    public void setFilename(String filename) {
        this.filename = filename;
    }

    /**
     *
     * @return
     * The uri
     */
    public String getUri() {
        return uri;
    }

    /**
     *
     * @param uri
     * The uri
     */
    public void setUri(String uri) {
        this.uri = uri;
    }

    /**
     *
     * @return
     * The filemime
     */
    public String getFilemime() {
        return filemime;
    }

    /**
     *
     * @param filemime
     * The filemime
     */
    public void setFilemime(String filemime) {
        this.filemime = filemime;
    }

    /**
     *
     * @return
     * The filesize
     */
    public String getFilesize() {
        return filesize;
    }

    /**
     *
     * @param filesize
     * The filesize
     */
    public void setFilesize(String filesize) {
        this.filesize = filesize;
    }

    /**
     *
     * @return
     * The status
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The timestamp
     */
    public String getTimestamp() {
        return timestamp;
    }

    /**
     *
     * @param timestamp
     * The timestamp
     */
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    /**
     *
     * @return
     * The rdfMapping
     */
    public List<Object> getRdfMapping() {
        return rdfMapping;
    }

    /**
     *
     * @param rdfMapping
     * The rdf_mapping
     */
    public void setRdfMapping(List<Object> rdfMapping) {
        this.rdfMapping = rdfMapping;
    }

    /**
     *
     * @return
     * The alt
     */
    public String getAlt() {
        return alt;
    }

    /**
     *
     * @param alt
     * The alt
     */
    public void setAlt(String alt) {
        this.alt = alt;
    }

    /**
     *
     * @return
     * The title
     */
    public String getTitle() {
        return title;
    }

    /**
     *
     * @param title
     * The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     *
     * @return
     * The width
     */
    public String getWidth() {
        return width;
    }

    /**
     *
     * @param width
     * The width
     */
    public void setWidth(String width) {
        this.width = width;
    }

    /**
     *
     * @return
     * The height
     */
    public String getHeight() {
        return height;
    }

    /**
     *
     * @param height
     * The height
     */
    public void setHeight(String height) {
        this.height = height;
    }

    /**
     *
     * @return
     * The agency
     */
    public TeamAgency getAgency() {
        return teamAgency;
    }

    /**
     *
     * @param teamAgency
     * The agency
     */
    public void setAgency(TeamAgency teamAgency) {
        this.teamAgency = teamAgency;
    }

}