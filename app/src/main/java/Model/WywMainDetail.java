package Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WywMainDetail {

    @SerializedName("wyw_main_title")
    @Expose
    private String wywMainTitle;
    @SerializedName("wyw_main_body")
    @Expose
    private String wywMainBody;

    /**
     *
     * @return
     * The wywMainTitle
     */
    public String getWywMainTitle() {
        return wywMainTitle;
    }

    /**
     *
     * @param wywMainTitle
     * The wyw_main_title
     */
    public void setWywMainTitle(String wywMainTitle) {
        this.wywMainTitle = wywMainTitle;
    }

    /**
     *
     * @return
     * The wywMainBody
     */
    public String getWywMainBody() {
        return wywMainBody;
    }

    /**
     *
     * @param wywMainBody
     * The wyw_main_body
     */
    public void setWywMainBody(String wywMainBody) {
        this.wywMainBody = wywMainBody;
    }

}