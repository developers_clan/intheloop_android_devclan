package Model;

/**
 * Created by developerclan on 18/02/2016.
 */
public class HistoryModel {

    int parentId;
    String date;
    String time;
    String chkbx;
    int statusId;


    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getChkbx() {
        return chkbx;
    }

    public void setChkbx(String chkbx) {
        this.chkbx = chkbx;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }
}
