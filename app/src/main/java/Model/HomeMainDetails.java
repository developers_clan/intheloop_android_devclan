package Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomeMainDetails {

    @SerializedName("home_main_title")
    @Expose
    private String homeMainTitle;
    @SerializedName("home_main_body")
    @Expose
    private String homeMainBody;

    /**
     *
     * @return
     * The homeMainTitle
     */
    public String getHomeMainTitle() {
        return homeMainTitle;
    }

    /**
     *
     * @param homeMainTitle
     * The home_main_title
     */
    public void setHomeMainTitle(String homeMainTitle) {
        this.homeMainTitle = homeMainTitle;
    }

    /**
     *
     * @return
     * The homeMainBody
     */
    public String getHomeMainBody() {
        return homeMainBody;
    }

    /**
     *
     * @param homeMainBody
     * The home_main_body
     */
    public void setHomeMainBody(String homeMainBody) {
        this.homeMainBody = homeMainBody;
    }

}