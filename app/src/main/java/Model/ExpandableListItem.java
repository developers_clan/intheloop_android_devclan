package Model;

/**
 * Created by macbook on 15/10/15.
 */
public class ExpandableListItem {

    private  String dataHeader;

    public String getDataHeader() {
        return dataHeader;
    }

    public void setDataHeader(String dataHeader) {
        this.dataHeader = dataHeader;
    }
}
