package Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TeamName {

    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("format")
    @Expose
    private Object format;
    @SerializedName("safe_value")
    @Expose
    private String safeValue;

    /**
     *
     * @return
     * The value
     */
    public String getValue() {
        return value;
    }

    /**
     *
     * @param value
     * The value
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     *
     * @return
     * The format
     */
    public Object getFormat() {
        return format;
    }

    /**
     *
     * @param format
     * The format
     */
    public void setFormat(Object format) {
        this.format = format;
    }

    /**
     *
     * @return
     * The safeValue
     */
    public String getSafeValue() {
        return safeValue;
    }

    /**
     *
     * @param safeValue
     * The safe_value
     */
    public void setSafeValue(String safeValue) {
        this.safeValue = safeValue;
    }

}