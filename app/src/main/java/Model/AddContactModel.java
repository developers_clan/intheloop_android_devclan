package Model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by macbook on 12/11/15.
 */
public class AddContactModel {

    private int id;
    private String nodeId;
    private String name;
    private String discipline;
    private String agency;
    private String phone;
    private String extension;
    private String email;
    private String gender;
    private String image;
    List<TeamPhone> phoneList = new ArrayList<>();
    List<TeamOffice> officeList = new ArrayList<>();
    List<String> agencyThumbnailList = new ArrayList<>();

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDiscipline() {
        return discipline;
    }

    public void setDiscipline(String discipline) {
        this.discipline = discipline;
    }

    public String getAgency() {
        return agency;
    }

    public void setAgency(String agency) {
        this.agency = agency;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<TeamOffice> getOfficeList() {
        return officeList;
    }

    public void setOfficeList(List<TeamOffice> officeList) {
        this.officeList = officeList;
    }

    public List<String> getAgencyThumbnailList() {
        return agencyThumbnailList;
    }

    public void setAgencyThumbnailList(List<String> agencyList) {
        this.agencyThumbnailList = agencyList;
    }

    public List<TeamPhone> getPhoneList() {
        return phoneList;
    }

    public void setPhoneList(List<TeamPhone> phoneList) {
        this.phoneList = phoneList;
    }
}
