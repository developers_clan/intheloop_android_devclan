package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import Model.WWAAStatus;
import Model.Wwaa;
import util.AppManager;
import Model.CdpStatusModel;
import com.childrencareapp.R;

/**
 * Created by macbook on 02/11/15.
 */
public class CustomAdapter extends BaseExpandableListAdapter
{

    List<Wwaa> list;
    Context context;
    ArrayList<CdpStatusModel> statusModels;
    int cPos=-1;
    int gPos=-1;

    public CustomAdapter(List<Wwaa> list,Context context,ArrayList<CdpStatusModel> models)
    {

        this.list = list;
        this.context = context;
        this.statusModels = models;

    }


    @Override
    public int getGroupCount() {
        return list.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;// list.get(groupPosition).getChildItemList().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return list.get(groupPosition).getWWAATitle().getMarkup();
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return list.get(groupPosition).getWWAAStatus().get(childPosition).getMarkup();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        View view = LayoutInflater.from(context).inflate(R.layout.cdp_list_parent_vertical, parent, false);

        TextView title = (TextView) view.findViewById(R.id.tv_title);
        title.setTypeface(AppManager.getInstance().getBlackTypeface());
        title.setText(list.get(groupPosition).getWWAATitle().getMarkup());

//        int h = ((RelativeLayout) view.findViewById(R.id.relativeLayout5)).getMinimumHeight()+5;
//
//        AppManager.getInstance().setListHieght(AppManager.getInstance().getListHieght() + h);

        return view;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
//        View view = LayoutInflater.from(context).inflate(R.layout.childlist, parent, false);

        View view = convertView;
//        View childView = null;

        //if the type is past, use the past travel layout
        if(view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.childlist, parent, false);
//            childView = LayoutInflater.from(context).inflate(R.layout.cdp_list_child_vertical, null, false);

        }

        List<WWAAStatus> child = list.get(groupPosition).getWWAAStatus();

        ListView listView = (ListView) view.findViewById(R.id.listView);

        ViewGroup.LayoutParams prams2 = listView.getLayoutParams();
        prams2.height = GetPixelFromDips(130) * child.size(); //AppManager.getInstance().getListHieght();
        listView.findViewById(R.id.listView).setLayoutParams(prams2);



        CustomAdapterChild adapter = new CustomAdapterChild(context,R.layout.cdp_list_child_vertical,child,groupPosition);
        listView.setAdapter(adapter);

//        TextView chkbx = (TextView) view.findViewById(R.id.tv_chkbx);
//        CheckBox chkBox = (CheckBox) view.findViewById(R.id.checkBox);
//        final Spinner spn = (Spinner) view.findViewById(R.id.spn);
//
//        chkbx.setText(list.get(groupPosition).getChildItemList().get(childPosition).getChildText());
//
////        if(cPos == childPosition)
////        {
////            chkBox.setChecked(true);
////        }
////        else
////        {
////            chkBox.setChecked(false);
////        }
//
//
//
//        LinearLayout linear = (LinearLayout) view.findViewById(R.id.linear);
//
//        spn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//
////                childData.setStatusId(position);
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//
//        chkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            int cp = childPosition;
//            int gp = groupPosition;
//
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isChecked) {
//                    spn.setVisibility(View.VISIBLE);
//
////                    for (int i = 0; i < AppManager.getInstance().getStatusModels().size(); i++) {
////                        if (groupPosition == AppManager.getInstance().getStatusModels().get(i).getProgramId()) {
////                            AppManager.getInstance().getStatusModels().remove(i);
////                        }
////                    }
//
////                    childData.setStatusId(spn.getSelectedItemPosition());
////                    childData.setChkbx(list.get(groupPosition).getChildItemList().get(p).getChildText());
//
//
////
////                    cPos = cp;
////                    gPos = gp;
//
////                    notifyDataSetChanged();
//
//                } else {
//                    spn.setVisibility(View.GONE);
//
//                }
//            }
//        });




        return view;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public int GetPixelFromDips(float pixels) {
        // Get the screen's density scale
        final float scale = context.getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f);
    }
}
