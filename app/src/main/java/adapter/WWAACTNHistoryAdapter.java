package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import Model.CtnStatusModel;
import util.AppManager;
import com.childrencareapp.R;

/**
 * Created by macbook on 31/10/15.
 */
public class WWAACTNHistoryAdapter extends ArrayAdapter<CtnStatusModel> {

    Context context;
    List<CtnStatusModel> list;
    public int p = -1;

    public WWAACTNHistoryAdapter(Context context, int resource, List<CtnStatusModel> objects) {
        super(context, resource, objects);

        this.context = context;
        this.list = objects;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null)
        {
            view = inflater.inflate(R.layout.custom_ctn_history,null);
        }

        TextView tvStatus = (TextView) view.findViewById(R.id.tv_status);
        TextView tvDate = (TextView) view.findViewById(R.id.tv_date);
        TextView tvTime = (TextView) view.findViewById(R.id.tv_time);

        tvStatus.setTypeface(AppManager.getInstance().getBlackTypeface());
        tvDate.setTypeface(AppManager.getInstance().getRegularTypeface());
        tvTime.setTypeface(AppManager.getInstance().getRegularTypeface());

        tvStatus.setText(list.get(position).getStatus());
        tvDate.setText("Date: "+list.get(position).getDate());
        tvTime.setText("Time: "+list.get(position).getTime());

        return view;
    }
}
