package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import Model.Wwaa;
import util.AppManager;
import com.childrencareapp.R;

/**
 * Created by macbook on 31/10/15.
 */
public class WWAACTNAdapter extends ArrayAdapter<Wwaa> {

    Context context;
    List<Wwaa> list;
    public int p = -1;

    public WWAACTNAdapter(Context context, int resource, List<Wwaa> objects) {
        super(context, resource, objects);

        this.context = context;
        this.list = objects;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null)
        {
            view = inflater.inflate(R.layout.custom_wwaa_ctn_status_list,null);
        }

        final CheckBox chkbox = (CheckBox) view.findViewById(R.id.checkBox);
        TextView tvStatus = (TextView) view.findViewById(R.id.tv_status);
        tvStatus.setTypeface(AppManager.getInstance().getBoldTypeface());
        RelativeLayout rel = (RelativeLayout) view.findViewById(R.id.relative);

//        tvStatus.setText(list.get(position).getWWAAStatus().getMarkup());

        if (p==position)
        {
            chkbox.setChecked(true);
        }
        else
        {
            chkbox.setChecked(false);
        }

        rel.setOnClickListener(new View.OnClickListener() {
            int pos = position;
            @Override
            public void onClick(View v) {

                chkbox.setChecked(true);
                p = pos;

                notifyDataSetChanged();

            }
        });

        return view;
    }
}
