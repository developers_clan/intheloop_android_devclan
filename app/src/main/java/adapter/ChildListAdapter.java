package adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;

import Database.MyDbHelper;
import Model.AddChildTeamModel;
import Model.ChildModel;
import util.AppManager;
import util.Constants;
import com.childrencareapp.AccountSettingActivity;
import com.childrencareapp.ChildViewActivity;
import com.childrencareapp.R;
import com.childrencareapp.ViewMyTeamActivity;

/**
 * Created by macbook on 30/10/15.
 */
public class ChildListAdapter extends ArrayAdapter<ChildModel> {

    Context context;
    ArrayList<ChildModel> list;
    MyDbHelper dbHelper;
    AlertDialog.Builder builder;

    SharedPreferences child_pr;

    boolean flag;
    String contact;
    Tracker mTracker;


    public ChildListAdapter(Context context, int resource, ArrayList<ChildModel> objects,boolean flag,String contact, Tracker mTracker) {
        super(context, resource, objects);

        this.context = context;
        this.list = objects;
        dbHelper = new MyDbHelper(context);
        builder = new AlertDialog.Builder(getContext());
        this.flag = flag;
        this.contact = contact;
        this.mTracker = mTracker;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null)
        {
            view = inflater.inflate(R.layout.custom_childview,null);
        }

        child_pr = context.getSharedPreferences(Constants.WWAACHILD_PREF, Activity.MODE_PRIVATE);

        TextView tvName = (TextView) view.findViewById(R.id.img);
        tvName.setTypeface(AppManager.getInstance().getRegularTypeface());
        Button btnDel = (Button) view.findViewById(R.id.btn_del);
        Button btnEdit = (Button) view.findViewById(R.id.btn_update);
        ImageView img = (ImageView) view.findViewById(R.id.img_child);
        ImageView imgTeam = (ImageView) view.findViewById(R.id.img_team);
        final RelativeLayout rel = (RelativeLayout) view.findViewById(R.id.relative);
        imgTeam.setVisibility(View.GONE);

        if (flag)
        {
            btnDel.setVisibility(View.GONE);
            btnEdit.setVisibility(View.GONE);

        }
        else
        {
            btnDel.setVisibility(View.VISIBLE);
            btnEdit.setVisibility(View.VISIBLE);

        }

        if (!flag) {
            int p = -1;
            if (child_pr.contains(Constants.CHILD_POS)) {
                p = child_pr.getInt(Constants.CHILD_POS, -1);

            }

            if (p == position) {
                rel.setBackgroundResource(R.drawable.selected_et_border);
            } else {
                rel.setBackgroundResource(R.drawable.et_border);
            }
        }

        tvName.setText(list.get(position).getName());
        if (list.get(position).getGender()==0)
        {
            img.setBackgroundResource(R.drawable.boy);
        }
        else if (list.get(position).getGender()==1) {
            img.setBackgroundResource(R.drawable.girl);
        }

        rel.setOnClickListener(new View.OnClickListener() {
            int pos = position;
            @Override
            public void onClick(View v) {

//                mTracker.send(new HitBuilders.EventBuilder()
//                        .setCategory(Constants.kCatAccountSettings)
//                        .setAction(Constants.SelectChild)
//                        .build());

                if (flag)
                {
                    int childId = list.get(pos).getId();

                    AddChildTeamModel model = new AddChildTeamModel();
                    model.setChildId(childId);
                    model.setContact(contact);

                    if (dbHelper.addChildTeam(model)) {
//                        Toast.makeText(context, "added", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(context, ViewMyTeamActivity.class);
                        intent.putExtra("CHILD_ID", pos);
                        context.startActivity(intent);
                    }
                }
                else {
                    rel.setBackgroundColor(Color.parseColor("#c8c8c8"));

                    SharedPreferences.Editor editor = child_pr.edit();
                    editor.clear();
                    editor.putInt(Constants.CHILD_POS, pos);
                    editor.commit();

                    notifyDataSetChanged();
                }

            }
        });

        btnDel.setOnClickListener(new View.OnClickListener() {
            int pos = position;

            @Override
            public void onClick(View v) {

                builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory(Constants.kCatAccountSettings)
                                .setAction(Constants.DeleteChild)
                                .build());

                        if (dbHelper.deleteChild(list.get(pos).getId())) {
                            list.remove(pos);
                            Toast.makeText(context, "Successfully Deleted", Toast.LENGTH_SHORT).show();
                            notifyDataSetChanged();
                            if (list.size() == 0 && ChildViewActivity.ref != null) {
                                ChildViewActivity.goBack();
                            }
                        }
                    }
                });
                builder.setNegativeButton("Cancel", null);
                builder.setMessage("Do you want to Delete ?");
                builder.create().show();

            }
        });

        btnEdit.setOnClickListener(new View.OnClickListener() {

            int pos = position;

            @Override
            public void onClick(View v) {

                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory(Constants.kCatAccountSettings)
                        .setAction(Constants.EditChild)
                        .build());

                Intent intent = new Intent(context, AccountSettingActivity.class);
                intent.putExtra("Pos", pos);
                context.startActivity(intent);

            }
        });



        return view;
    }
}
