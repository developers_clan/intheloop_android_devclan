package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import util.AppManager;
import com.childrencareapp.R;

/**
 * Created by mac on 1/22/16.
 */
public class GenderAdapter extends ArrayAdapter<String> {
    Context ctx;
    List<String> stringList;
    Integer[] arrImg = new Integer[]{R.drawable.boy,R.drawable.girl,0};

    public GenderAdapter(Context context, int resource, List<String> objects) {
        super(context, resource, objects);

        this.ctx = context;
        this.stringList = objects;

    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null)
        {
            view = inflater.inflate(R.layout.custom_spnr,parent,false);
        }

        TextView tvText = (TextView) view.findViewById(R.id.text);
        ImageView img = (ImageView) view.findViewById(R.id.imgGender);

        tvText.setTypeface(AppManager.getInstance().getRegularTypeface());

        tvText.setText(stringList.get(position));
        img.setBackgroundResource(arrImg[position]);

        view.setBackgroundResource(R.drawable.spn_border);

        return  view;
    }

    @Override
    public int getCount() {
//        return stringList.size();
        int count = super.getCount();
        return count > 0 ? count - 1 : count;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null)
        {
            view = inflater.inflate(R.layout.custom_spnr,parent,false);
        }

        TextView tvText = (TextView) view.findViewById(R.id.text);
        ImageView img = (ImageView) view.findViewById(R.id.imgGender);

        img.setBackgroundResource(arrImg[position]);
        tvText.setTypeface(AppManager.getInstance().getRegularTypeface());

        tvText.setText(stringList.get(position));
        if(position==2) {
            img.setVisibility(View.GONE);
        }
        else {
            img.setVisibility(View.VISIBLE);
        }
        return  view;


    }
}
