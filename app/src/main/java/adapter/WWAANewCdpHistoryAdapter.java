package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import Model.HistoryModel;
import util.AppManager;
import com.childrencareapp.R;

/**
 * Created by macbook on 04/11/15.
 */
public class WWAANewCdpHistoryAdapter extends ArrayAdapter<HistoryModel> {

    Context context;
    ArrayList<HistoryModel> list;

    public WWAANewCdpHistoryAdapter(Context context, int resource, ArrayList<HistoryModel> objects) {
        super(context, resource, objects);

        this.list = objects;
        this.context = context;

    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null)
        {
            view = inflater.inflate(R.layout.custom_ctn_history,null);
        }

        TextView tvStatus = (TextView) view.findViewById(R.id.tv_status);
        TextView tvDate = (TextView) view.findViewById(R.id.tv_date);
        TextView tvTime = (TextView) view.findViewById(R.id.tv_time);

        tvStatus.setTypeface(AppManager.getInstance().getBoldTypeface());
        tvDate.setTypeface(AppManager.getInstance().getRegularTypeface());
        tvTime.setTypeface(AppManager.getInstance().getRegularTypeface());

//        Wwaa parent1 = AppManager.getInstance().getAgencyModel().getWwaa().get(list.get(position).getParentId());
//        if (list.get(position).getChildData() != null)
        {
            tvStatus.setText(list.get(position).getChkbx());
            tvDate.setText("Date: " + list.get(position).getDate());
//            tvTime.setText("Time: " + list.get(position).getChildData().getTime());

            int statusId = list.get(position).getStatusId();
            switch (statusId) {
                case 0:
                    tvTime.setText("Waiting");
                    tvTime.setTextColor(context.getResources().getColor(R.color.green));
                    break;
                case 1:
                    tvTime.setText("Accessing");
                    tvTime.setTextColor(context.getResources().getColor(R.color.yellow));
                    break;
                case 2:
                    tvTime.setText("Completed");
                    tvTime.setTextColor(context.getResources().getColor(R.color.red_text));
                    break;
            }

        }
        return view;
    }

}
