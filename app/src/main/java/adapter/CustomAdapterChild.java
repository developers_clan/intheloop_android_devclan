package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import Model.ChildData;
import Model.WWAAStatus;
import com.childrencareapp.R;
import util.AppManager;

class CustomAdapterChild extends ArrayAdapter<WWAAStatus> {

    List<WWAAStatus> list;
    Context context;
    int pos = -1;
    int groupPosition;
    int gPos = -1;
    ArrAdapter adapter;
    List<String> strList;
    Integer[] arr;

    public CustomAdapterChild(Context context, int textViewResourceId, List<WWAAStatus> list, int groupPosition) {
        super(context, textViewResourceId, list);

        this.context = context;
        this.list = list;
        this.groupPosition = groupPosition;

        strList = new ArrayList<String>();

        strList.add("Waiting");
        strList.add("Accessing");
        strList.add("Completed");
        strList.add("Please Set Status");

        adapter = new ArrAdapter(context,R.layout.custom_spinner,strList);
        arr = new Integer[]{R.drawable.waiting_ball,R.drawable.accessing_ball,R.drawable.completed_ball,0};
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;

        //if the type is past, use the past travel layout
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.cdp_list_child_vertical, parent, false);

        }

        Date date = new Date();
        String strTimeFormat = "hh:mm a";
        String strDateFormat = "MMM-dd-yyyy";
        DateFormat timeFormat = new SimpleDateFormat(strTimeFormat, Locale.US);
        DateFormat dateFormat = new SimpleDateFormat(strDateFormat, Locale.US);
        final String formattedTime= timeFormat.format(date);
        final String formattedDate= dateFormat.format(date);

        final ChildData childData = new ChildData();

        TextView chkbx = (TextView) view.findViewById(R.id.tv_chkbx);
        chkbx.setTypeface(AppManager.getInstance().getBoldTypeface());
        final CheckBox chkBox = (CheckBox) view.findViewById(R.id.checkBox);
        final ImageView imgArrow = (ImageView) view.findViewById(R.id.img_arrow);
        final Spinner spn = (Spinner) view.findViewById(R.id.spn);
        spn.setDropDownVerticalOffset(1);
        spn.setAdapter(adapter);
        spn.setSelection(adapter.getCount());

        chkbx.setText(list.get(position).getMarkup());

        if (getIndex()!=-1) {
            if (AppManager.getInstance().getParentModels().get(getIndex()).getChildData() != null) {
                if (AppManager.getInstance().getParentModels().get(getIndex()).getChildData().getChildPos() == position) {
                    chkBox.setChecked(true);
                    spn.setVisibility(View.VISIBLE);
                    imgArrow.setVisibility(View.VISIBLE);
                } else {
                    chkBox.setChecked(false);
//            AppManager.getInstance().getParentModels().get(getIndex()).setChildData(new ChildData());
                }
            }
        }
        LinearLayout linear = (LinearLayout) view.findViewById(R.id.linear);

        spn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            int p = position;
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                childData.setChkbx(list.get(p).getMarkup());
                childData.setStatusId(spn.getSelectedItemPosition());
                childData.setDate(formattedDate);
                childData.setTime(formattedTime);
                childData.setChildPos(p);
                if (getIndex() != -1) {
                    AppManager.getInstance().getParentModels().get(getIndex()).setChildData(childData);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        chkbx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chkBox.setChecked(!chkBox.isChecked());
            }
        });

        chkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            int p = position;
            int gp = groupPosition;

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    spn.setVisibility(View.VISIBLE);
                    imgArrow.setVisibility(View.VISIBLE);

                    if (getIndex()>-1)
                    {
                        childData.setChkbx(list.get(p).getMarkup());
                        childData.setStatusId(spn.getSelectedItemPosition());
                        childData.setDate(formattedDate);
                        childData.setTime(formattedTime);
                        childData.setChildPos(p);
                        AppManager.getInstance().getParentModels().get(getIndex()).setChildData(childData);
                    }


                    pos = p;
                    gPos = gp;
                    notifyDataSetChanged();
                } else {
                    spn.setVisibility(View.GONE);
                    imgArrow.setVisibility(View.GONE);
                    if (getIndex()!=-1) {
                        if (p == AppManager.getInstance().getParentModels().get(getIndex()).getChildData().getChildPos()) {
                            AppManager.getInstance().getParentModels().get(getIndex()).setChildData(null);
                        }
                    }
                }
//                ChildData childData = AppManager.getInstance().getParentModels().get(getIndex()).getChildData();
            }
        });

        return view;
    }

    private int getIndex() {
        for (int i = 0; i < AppManager.getInstance().getParentModels().size(); i++) {
            if (groupPosition == AppManager.getInstance().getParentModels().get(i).getParentId()) {
                return i;
            }
        }
        return -1;
    }

    class ArrAdapter extends ArrayAdapter<String> {
        Context ctx;
        List<String> stringList;

        public ArrAdapter(Context context, int resource, List<String> objects) {
            super(context, resource, objects);

            this.ctx = context;
            this.stringList = objects;

        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (view == null)
            {
                view = inflater.inflate(R.layout.custom_spinner,parent,false);
            }

            TextView tvText = (TextView) view.findViewById(R.id.text);
            ImageView img = (ImageView) view.findViewById(R.id.img);

            tvText.setText(stringList.get(position));
            tvText.setTypeface(AppManager.getInstance().getRegularTypeface());
            img.setImageResource(arr[position]);

            view.setBackgroundResource(R.drawable.spn_border);

            return  view;
        }

        @Override
        public int getCount() {
//            return stringList.size();
            int count = super.getCount();
            return count > 0 ? count - 1 : count;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (view == null)
            {
                view = inflater.inflate(R.layout.custom_spinner,parent,false);
            }

            TextView tvText = (TextView) view.findViewById(R.id.text);
            ImageView img = (ImageView) view.findViewById(R.id.img);
            tvText.setTypeface(AppManager.getInstance().getRegularTypeface());

//            if (position == stringList.size()-1){
//                img.setVisibility(View.GONE);
//            }
//            else {
//                img.setVisibility(View.VISIBLE);
//            }

            tvText.setText(stringList.get(position));
            img.setImageResource(arr[position]);

            if (position==3){
                img.setVisibility(View.GONE);
            }
            else {
                img.setVisibility(View.VISIBLE);
            }

            return  view;


    }
    }

}
