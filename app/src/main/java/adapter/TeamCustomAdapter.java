package adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.childrencareapp.AddContactActivity;
import com.childrencareapp.ChooseImage;
import com.childrencareapp.R;
import com.childrencareapp.TeamActivity;
import com.childrencareapp.ViewMyTeamActivity;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import Database.MyDbHelper;
import Model.AddChildTeamModel;
import Model.AddContactModel;
import Model.TeamOffice;
import Model.TeamPhone;
import thirdparty.HorizontialListView;
import util.AppManager;
import util.Constants;

/**
 * Created by macbook on 02/11/15.
 */
public class TeamCustomAdapter extends BaseExpandableListAdapter {

    List<AddContactModel> list;
    Activity context;
    MyDbHelper dbHelper;
    ArrayList<AddContactModel> contactList;
    AlertDialog.Builder builder;
    Tracker mTracker;

    public TeamCustomAdapter(List<AddContactModel> list, Activity context,Tracker mTracker) {

        this.list = list;
        this.context = context;
        this.mTracker = mTracker;
        dbHelper = new MyDbHelper(context);
        builder = new AlertDialog.Builder(context);

        if (!AppManager.getInstance().isView()) {
            SharedPreferences pref = context.getSharedPreferences(Constants.WWAACHILD_PREF, Activity.MODE_PRIVATE);
            ArrayList<AddContactModel> conList = null;
            if (pref.contains(Constants.CHILD_POS)) {
                int pos = pref.getInt(Constants.CHILD_POS, -1);
                int childId = AppManager.getInstance().getChildModels().get(pos).getId();
                contactList = getChildTeam(dbHelper.getChildTeam(childId));
            }
        }

    }

    private ArrayList<AddContactModel> getChildTeam(ArrayList<AddChildTeamModel> childTeam) {
        ArrayList<AddContactModel> conList;
        AppManager.getInstance().childTeamIds.clear();
        conList = new ArrayList<>();
        for (int i = 0; i < childTeam.size(); i++) {
            GsonBuilder gsonb = new GsonBuilder();
            Gson gson = gsonb.create();
            Type type = new TypeToken<AddContactModel>() {
            }.getType();
            AddContactModel model = gson.fromJson(childTeam.get(i).getContact(), type);
            AppManager.getInstance().childTeamIds.add(childTeam.get(i).getId());
            conList.add(model);
        }
        return conList;
    }


    @Override
    public int getGroupCount() {
        return list.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;// list.get(groupPosition).getChildItemList().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return list.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return list.get(groupPosition).getOfficeList();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    private class ViewHolder {
        TextView name;
        TextView discipline;
//        TextView phoneExt;
        TextView email;
        TextView more;
        TextView agency;
        Button btnAddRem;
        ImageView img;
        LinearLayout linearLayout;
        LinearLayout linearView;
        RelativeLayout rel;
        RelativeLayout relPhone;
        RelativeLayout relEmail;
        RelativeLayout relBar;
        HorizontialListView listViewAgency;
        ImageView imgPhone;
        ImageView imgLoc;
        ImageView imgEmail;
        ImageView imgEdit;
        RelativeLayout relAgency;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View view, ViewGroup parent) {

        ViewHolder holder = null;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.team_list_parent_vertical, parent, false);

            holder = new ViewHolder();
            holder.name = (TextView) view.findViewById(R.id.tv_name);
            holder.discipline = (TextView) view.findViewById(R.id.tv_discipline);
//            holder.phoneExt = (TextView) view.findViewById(R.id.tv_phoneExt);
//            holder.email = (TextView) view.findViewById(R.id.imgPhone);
            holder.more = (TextView) view.findViewById(R.id.tv_more);
            holder.agency = (TextView) view.findViewById(R.id.tv_agency);
            holder.img = (ImageView) view.findViewById(R.id.img);
            holder.btnAddRem = (Button) view.findViewById(R.id.btn_addremove);
            holder.linearLayout = (LinearLayout) view.findViewById(R.id.linear);
            holder.linearView = (LinearLayout) view.findViewById(R.id.linearView);
            holder.rel = (RelativeLayout) view.findViewById(R.id.rel);
            holder.relPhone = (RelativeLayout) view.findViewById(R.id.relPhone);
            holder.relEmail = (RelativeLayout) view.findViewById(R.id.relEmail);
            holder.relAgency = (RelativeLayout) view.findViewById(R.id.relAgency);
            holder.relBar = (RelativeLayout) view.findViewById(R.id.relbar);
            holder.listViewAgency = (HorizontialListView) view.findViewById(R.id.listview);
            holder.imgEmail = (ImageView) view.findViewById(R.id.imgEmail);
            holder.imgLoc = (ImageView) view.findViewById(R.id.imgLoc);
            holder.imgPhone = (ImageView) view.findViewById(R.id.imgPhone);
            holder.imgEdit = (ImageView) view.findViewById(R.id.imgEdit);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

//        DisplayMetrics displaymetrics = new DisplayMetrics();
//        context.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
//        int width = displaymetrics.widthPixels;
//        if (width <=480){
//            holder.img.getLayoutParams().width = (int) dipToPixels(context, 75);
//            holder.img.getLayoutParams().height = (int) dipToPixels(context, 75);
//        } else {
//            holder.img.getLayoutParams().width = (int) dipToPixels(context, 100);
//            holder.img.getLayoutParams().height = (int) dipToPixels(context, 100);
//        }
//        holder.img.requestLayout();

        holder.name.setText(list.get(groupPosition).getName());
        holder.discipline.setText(list.get(groupPosition).getDiscipline());
        if (list.get(groupPosition).getPhone() != null && !list.get(groupPosition).getPhone().equalsIgnoreCase("")) {
//            holder.imgPhone.setText(list.get(groupPosition).getPhone() + "\n" + list.get(groupPosition).getExtension());
            holder.imgPhone.setVisibility(View.VISIBLE);
        } else {
            holder.imgPhone.setVisibility(View.GONE);
        }
        if (list.get(groupPosition).getEmail() != null && !list.get(groupPosition).getEmail().equalsIgnoreCase("")) {
//            holder.email.setText(list.get(groupPosition).getEmail());
            holder.imgEmail.setVisibility(View.VISIBLE);
        } else {
            holder.imgEmail.setVisibility(View.GONE);
        }

        if (list.get(groupPosition).getOfficeList() != null && list.get(groupPosition).getOfficeList().size()>0) {
//            holder.email.setText(list.get(groupPosition).getEmail());
            holder.imgLoc.setVisibility(View.VISIBLE);
        } else {
            holder.imgLoc.setVisibility(View.GONE);
        }

        holder.agency.setText(list.get(groupPosition).getAgency());
        if (!AppManager.getInstance().isView()) {
            if (ifAlreadyExist(list.get(groupPosition).getId(), list.get(groupPosition).getNodeId())) {
                holder.btnAddRem.setBackgroundResource(R.drawable.my_team_icon_minus);

//                final Animation animAccDecc = AnimationUtils.loadAnimation(context, R.anim.accelrate);
//                view.startAnimation(animAccDecc);
//                view.bringToFront();


            } else {
                holder.btnAddRem.setBackgroundResource(R.drawable.my_team_icon_plus);
            }
        } else {
            if (AppManager.getInstance().isView()) {
                holder.btnAddRem.setBackgroundResource(R.drawable.my_team_icon_minus);
            } else {
                holder.btnAddRem.setBackgroundResource(R.drawable.my_team_icon_plus);
            }
        }

        if (list.get(groupPosition).getImage() != null && !list.get(groupPosition).getImage().equalsIgnoreCase("")) {

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            options.inSampleSize = calculateInSampleSize(options, 200, 200);
            options.inJustDecodeBounds = false;

            File imgFile = new File(list.get(groupPosition).getImage());
            if (imgFile.exists()) {
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);
                holder.img.setImageBitmap(myBitmap);
            }

        } else {
            if (list.get(groupPosition).getGender() != null && list.get(groupPosition).getGender().equalsIgnoreCase("Male")) {
                holder.img.setImageResource(R.drawable.my_team_user_boy_placeholder);
//                holder.img.setImageBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.my_team_user_boy_placeholder));
            } else if (list.get(groupPosition).getGender() != null && list.get(groupPosition).getGender().equalsIgnoreCase("Female")) {
                holder.img.setImageResource(R.drawable.my_team_user_girl_placeholder);
            } else {
                //img.setImageBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.my_team_user_boy_placeholder));
            }
        }

        List<String> thumbs = list.get(groupPosition).getAgencyThumbnailList();
        if (thumbs != null) {
//            holder.listViewAgency.setAdapter(new ArrAdapter(context, R.layout.custom, thumbs));

            holder.linearView.removeAllViews();
            for (int i=0;i<thumbs.size();i++){

                ImageView img = new ImageView(context);
                img.setLayoutParams(new RelativeLayout.LayoutParams((int)dipToPixels(context,30), (int)dipToPixels(context,30)));

//                img.getLayoutParams().width = (int)dipToPixels(context,40);
//                img.getLayoutParams().height = (int)dipToPixels(context,40);
                img.setScaleType(ImageView.ScaleType.FIT_CENTER);

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                options.inSampleSize = calculateInSampleSize(options, 200, 200);
                options.inJustDecodeBounds = false;

                File imgFile = new File(thumbs.get(i));
                if (imgFile.exists()) {
                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);
                    img.setImageBitmap(myBitmap);
                }

                holder.linearView.addView(img);

            }

        }




        holder.imgPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.get(groupPosition).getPhone() != null && !list.get(groupPosition).getPhone().equalsIgnoreCase("")) {

                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory(Constants.kCatTeam)
                            .setAction(Constants.kActTeamContactPhonePressed)
                            .build());

                    String ph = "tel:" + list.get(groupPosition).getPhone() + ";" + list.get(groupPosition).getExtension();
                    Uri uri = Uri.parse(ph);
                    context.startActivity(new Intent(Intent.ACTION_CALL, uri));
                }
            }
        });

        holder.imgEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.get(groupPosition).getEmail() != null && !list.get(groupPosition).getEmail().equalsIgnoreCase("")) {

                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory(Constants.kCatTeam)
                            .setAction(Constants.kActTeamContactEmailPressed)
                            .build());
                    try {
                        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + list.get(groupPosition).getEmail())));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        holder.imgLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.get(groupPosition).getOfficeList() != null) {

                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory(Constants.kCatTeam)
                            .setAction(Constants.kActTeamContactMapPressed)
                            .build());

                    try {
                        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(String.format("geo:0,0?q=%s", URLEncoder.encode(list.get(groupPosition).getOfficeList().get(0).getOffice()))))); //Uri.parse("mailto:" + list.get(groupPosition).getOfficeList().get(0))));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        final ViewHolder finalHolder = holder;
        holder.btnAddRem.setOnTouchListener(new View.OnTouchListener() {
            int gp = groupPosition;

            @Override
            public boolean onTouch(View v, MotionEvent event) {



                if (event.getAction() == MotionEvent.ACTION_DOWN) {

                    if (AppManager.getInstance().isView() || ifAlreadyExist(list.get(gp).getId(), list.get(gp).getNodeId())) {

                        finalHolder.btnAddRem.setBackgroundResource(R.drawable.my_team_icon_minus_selected);
                        finalHolder.rel.setBackgroundColor(context.getResources().getColor(R.color.select_child_color));
                        finalHolder.listViewAgency.setBackgroundColor(context.getResources().getColor(R.color.select_child_color));

                    } else {
                        finalHolder.btnAddRem.setBackgroundResource(R.drawable.my_team_icon_plus_green);
                        finalHolder.rel.setBackgroundColor(context.getResources().getColor(R.color.select_child_color));
                        finalHolder.listViewAgency.setBackgroundColor(context.getResources().getColor(R.color.select_child_color));
                    }

                }
                if (event.getAction() == MotionEvent.ACTION_UP) {

                    finalHolder.rel.setBackgroundColor(context.getResources().getColor(R.color.white));
                    finalHolder.listViewAgency.setBackgroundColor(context.getResources().getColor(R.color.white));


                    int childId = -1;
                    SharedPreferences pref = context.getSharedPreferences(Constants.WWAACHILD_PREF, Activity.MODE_PRIVATE);
                    if (pref.contains(Constants.CHILD_POS)) {
                        int pos = pref.getInt(Constants.CHILD_POS, -1);
                        childId = AppManager.getInstance().getChildModels().get(pos).getId();
                    }

                    final AddContactModel contact = list.get(gp);
                    Gson gson = new Gson();
                    String value = gson.toJson(contact);

                    if (AppManager.getInstance().isView() || ifAlreadyExist(list.get(gp).getId(), list.get(gp).getNodeId())) {

                        finalHolder.btnAddRem.setBackgroundResource(R.drawable.my_team_icon_minus);

                        final AddChildTeamModel model = new AddChildTeamModel();
                        model.setChildId(childId);
                        model.setContactId(contact.getId());
//                    model.setId(AppManager.getInstance().childTeamIds.get(gp));
                        model.setContact(value);

                        final int finalChildId = childId;

                        builder = new AlertDialog.Builder(context);
                        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                if (dbHelper.removeChildTeam(model)) {

                                    mTracker.send(new HitBuilders.EventBuilder()
                                            .setCategory(Constants.kCatTeam)
                                            .setAction(Constants.TeamCardRemoved)
                                            .build());

                                    ArrayList<AddChildTeamModel> childTeam = dbHelper.getChildTeam(finalChildId);

                                    if (AppManager.getInstance().isView()) {
                                        list.remove(gp);
                                        ViewMyTeamActivity.updateUI(childTeam.size());
                                    }

                                    if (childTeam.size() > 0) {
                                        if (TeamActivity.btnViewTeam != null) {
                                            TeamActivity.btnViewTeam.setBackgroundResource(R.drawable.big_btn_bg);
                                            TeamActivity.btnViewTeam.setEnabled(true);
                                        }
                                        if (ViewMyTeamActivity.tvCount != null) {
                                            ViewMyTeamActivity.tvCount.setText("" + childTeam.size());
                                        }
                                        if (TeamActivity.tvCount != null) {
                                            TeamActivity.tvCount.setText("" + childTeam.size());
                                        }
                                    } else {
                                        if (TeamActivity.btnViewTeam != null) {
                                            TeamActivity.btnViewTeam.setBackgroundResource(R.drawable.big_disable_btn_bg);
                                            TeamActivity.btnViewTeam.setEnabled(false);
                                        }
                                        if (ViewMyTeamActivity.tvCount != null) {
                                            ViewMyTeamActivity.tvCount.setText("0");
                                        }
                                        if (TeamActivity.tvCount != null) {
                                            TeamActivity.tvCount.setText("0");
                                        }
                                    }
                                    contactList = getChildTeam(childTeam);
                                    notifyDataSetChanged();
                                }

                            }
                        });
                        builder.setNegativeButton("Cancel", null);
                        builder.setMessage("Do you want to Delete ?");
                        builder.create().show();


                    } else {
                        AddChildTeamModel model = new AddChildTeamModel();

                        finalHolder.btnAddRem.setBackgroundResource(R.drawable.my_team_icon_plus);
                        model.setChildId(childId);
                        model.setContactId(contact.getId());
                        model.setContact(value);

                        if (dbHelper.addChildTeam(model)) {

                            mTracker.send(new HitBuilders.EventBuilder()
                                    .setCategory(Constants.kCatTeam)
                                    .setAction(Constants.TeamCardAdded)
                                    .build());

                            ////// ANIMATION ///////

                            int[] loc = new int[2];
                            v.getLocationOnScreen(loc);

                            DisplayMetrics displaymetrics = new DisplayMetrics();
                            context.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
                            int height = displaymetrics.heightPixels;
                            int width = displaymetrics.widthPixels;

                            Animation animAnticipate = null;
//
                            if (loc[1] < (height / 4)) {
                                animAnticipate = AnimationUtils.loadAnimation(context, R.anim.accelrate_top);
                            } else if (loc[1] < (height / 3)) {
                                animAnticipate = AnimationUtils.loadAnimation(context, R.anim.accelrate);
                            } else if (loc[1] < (height / 2)) {
                                animAnticipate = AnimationUtils.loadAnimation(context, R.anim.accelrate_btm);
                            } else if (loc[1] > (height / 2) && loc[1] < ((height / 2) + (height / 4) / 2)) {
                                animAnticipate = AnimationUtils.loadAnimation(context, R.anim.accelrate_scnd_most_btm);
                            } else {
                                animAnticipate = AnimationUtils.loadAnimation(context, R.anim.accelrate_most_btm);
                            }

                            int h = (int) dipToPixels(context, 140);
                            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, h);
                            params.rightMargin = 10;
                            params.leftMargin = 10;
                            params.topMargin = loc[1] - 20;

                            if (TeamActivity.relAnim != null) {

                                TeamActivity.relAnim.setVisibility(View.VISIBLE);
                                TeamActivity.relAnim.setLayoutParams(params);

                                TeamActivity.relAnim.startAnimation(animAnticipate);
                                TeamActivity.relAnim.bringToFront();

                                TeamActivity.relAnimName.setText(list.get(gp).getName());
                                TeamActivity.relAnimdiscipline.setText(list.get(gp).getDiscipline());
//                                TeamActivity.relAnimPhoneExt.setText(list.get(gp).getPhone() + " " + list.get(gp).getExtension());
//                                TeamActivity.relAnimPhoneExt.setText(list.get(gp).getEmail());
                                TeamActivity.relAnimAgency.setText(list.get(gp).getAgency());

                                if (list.get(groupPosition).getPhone() != null && !list.get(groupPosition).getPhone().equalsIgnoreCase("")) {
                                    TeamActivity.imgPhone.setVisibility(View.VISIBLE);
                                } else {
                                    TeamActivity.imgPhone.setVisibility(View.GONE);
                                }
                                if (list.get(groupPosition).getEmail() != null && !list.get(groupPosition).getEmail().equalsIgnoreCase("")) {

                                    TeamActivity.imgEmail.setVisibility(View.VISIBLE);
                                } else {
                                    TeamActivity.imgEmail.setVisibility(View.GONE);
                                }

                                if (list.get(groupPosition).getOfficeList() != null && list.get(groupPosition).getOfficeList().size()>0) {

                                    TeamActivity.imgLoc.setVisibility(View.VISIBLE);
                                } else {
                                    TeamActivity.imgLoc.setVisibility(View.GONE);
                                }

                                List<String> thumbs = list.get(groupPosition).getAgencyThumbnailList();
                                if (thumbs != null) {
                                    TeamActivity.listViewAgency.setAdapter(new ArrAdapter(context, R.layout.custom, thumbs));
                                }

                                if (list.get(groupPosition).getImage() != null && !list.get(groupPosition).getImage().equalsIgnoreCase("")) {

                                    BitmapFactory.Options options = new BitmapFactory.Options();
                                    options.inJustDecodeBounds = true;
                                    options.inSampleSize = calculateInSampleSize(options, 200, 200);
                                    options.inJustDecodeBounds = false;

                                    File imgFile = new File(list.get(groupPosition).getImage());
                                    if (imgFile.exists()) {
                                        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);
                                        TeamActivity.relAnimImg.setImageBitmap(myBitmap);
                                    }

                                } else {
                                    if (list.get(groupPosition).getGender() != null && list.get(groupPosition).getGender().equalsIgnoreCase("Male")) {
                                        TeamActivity.relAnimImg.setImageBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.my_team_user_boy_placeholder));
                                    } else if (list.get(groupPosition).getGender() != null && list.get(groupPosition).getGender().equalsIgnoreCase("Female")) {
                                        TeamActivity.relAnimImg.setImageBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.my_team_user_girl_placeholder));
                                    }
                                }

                            }

                            /////// END /////////

                            final ArrayList<AddChildTeamModel> childTeam = dbHelper.getChildTeam(childId);
                            if (childTeam.size() > 0) {
                                if (TeamActivity.btnViewTeam != null) {
                                    TeamActivity.btnViewTeam.setBackgroundResource(R.drawable.big_btn_bg);
                                    TeamActivity.btnViewTeam.setEnabled(true);
                                }
                                if (ViewMyTeamActivity.tvCount != null) {
                                    ViewMyTeamActivity.tvCount.setText("" + childTeam.size());
                                }
                                if (TeamActivity.tvCount != null) {
                                    TeamActivity.tvCount.setText("" + childTeam.size());
                                }
                            } else {
                                if (TeamActivity.btnViewTeam != null) {
                                    TeamActivity.btnViewTeam.setBackgroundResource(R.drawable.big_disable_btn_bg);
                                    TeamActivity.btnViewTeam.setEnabled(false);
                                }
                                if (ViewMyTeamActivity.tvCount != null) {
                                    ViewMyTeamActivity.tvCount.setText("0");
                                }
                                if (TeamActivity.tvCount != null) {
                                    TeamActivity.tvCount.setText("0");
                                }
                            }

                            Animation.AnimationListener animLis = new Animation.AnimationListener() {
                                @Override
                                public void onAnimationStart(Animation animation) {

                                }

                                @Override
                                public void onAnimationEnd(Animation animation) {
                                    TeamActivity.relAnim.setVisibility(View.INVISIBLE);

                                    contactList = getChildTeam(childTeam);
                                    notifyDataSetChanged();
                                }

                                @Override
                                public void onAnimationRepeat(Animation animation) {

                                }
                            };

                            animAnticipate.setAnimationListener(animLis);
//                            contactList = getChildTeam(childTeam);
//                            notifyDataSetChanged();
                        }
                    }


                }
                return false;
            }
        });

        holder.img.setOnClickListener(new View.OnClickListener() {
            int gp = groupPosition;

            @Override
            public void onClick(View v) {
                if (!AppManager.getInstance().isView()) {
                    Intent intent = new Intent(context, ChooseImage.class);
                    intent.putExtra("ID", list.get(gp).getId());
                    context.startActivity(intent);
                }
            }
        });

        holder.imgEdit.setOnClickListener(new View.OnClickListener() {
            int gp = groupPosition;
            @Override
            public void onClick(View v) {


                final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                if (AppManager.getInstance().isSearch()) {
                    if (list.get(gp).getOfficeList() != null && list.get(gp).getOfficeList().size() > 0) {

                    } else {
                        if (list.get(gp).getNodeId() == null || list.get(gp).getNodeId().equalsIgnoreCase("")) {

                            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    mTracker.send(new HitBuilders.EventBuilder()
                                            .setCategory(Constants.kCatTeam)
                                            .setAction(Constants.kActTeamContactEditPressed)
                                            .build());

                                    AddContactModel contact = list.get(gp);
                                    Intent intent = new Intent(context,AddContactActivity.class);
                                    Gson gson = new Gson();
                                    String value = gson.toJson(contact);
                                    intent.putExtra("DATA",value);
                                    context.startActivity(intent);
                                }
                            });
                            builder.setNegativeButton("NO", null);
                            builder.setMessage("Do you want to Edit ?");
                            builder.create().show();

                        }
                    }
                } else {
                    if (list.get(gp).getOfficeList() != null && list.get(gp).getOfficeList().size() > 0) {

                    } else {
                        if (list.get(gp).getNodeId() == null || list.get(gp).getNodeId().equalsIgnoreCase("")) {

                            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    AddContactModel contact = list.get(gp);
                                    Intent intent = new Intent(context, AddContactActivity.class);
                                    Gson gson = new Gson();
                                    String value = gson.toJson(contact);
                                    intent.putExtra("DATA", value);
                                    context.startActivity(intent);
                                }
                            });
                            builder.setNegativeButton("NO", null);
                            builder.setMessage("Do you want to Edit ?");
                            builder.create().show();

                        }
                    }
                }

            }
        });

        if (list.get(groupPosition).getOfficeList() != null && list.get(groupPosition).getOfficeList().size() > 0) {
            holder.more.setVisibility(View.VISIBLE);
            holder.imgEdit.setVisibility(View.GONE);
            holder.agency.setVisibility(View.GONE);
            holder.relAgency.setVisibility(View.VISIBLE);
            holder.relBar.setBackgroundColor(context.getResources().getColor(R.color.background));
        } else {
            holder.more.setVisibility(View.GONE);
            if (AppManager.getInstance().isView()){
                holder.imgEdit.setVisibility(View.GONE);
            } else {
                holder.imgEdit.setVisibility(View.VISIBLE);
            }
            holder.agency.setVisibility(View.VISIBLE);
            holder.relAgency.setVisibility(View.GONE);
            holder.relBar.setBackgroundColor(context.getResources().getColor(R.color.background));
        }

        if (isExpanded) {
//            more.setText("-Collapse");
//            holder.more.setBackgroundResource(R.drawable.up);
            holder.more.setVisibility(View.GONE);
            holder.relBar.setBackgroundColor(context.getResources().getColor(R.color.white));
            holder.linearLayout.setBackgroundResource(0);
        } else {
//            more.setText("+More");
//            holder.more.setBackgroundResource(R.drawable.down);
//            holder.more.setVisibility(View.VISIBLE);
            holder.relBar.setBackgroundColor(context.getResources().getColor(R.color.background));
            holder.linearLayout.setBackgroundResource(R.drawable.et_border);
        }

//        title.setText(list.get(groupPosition).getParentText());
        return view;
    }

    public float dipToPixels(Context context, float dipValue) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        float value = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue, metrics);
        return value;
    }

    private class ChildViewHolder {
        View view2;
        View phoneView;
        TextView textPhone;
        ListView listView;
        ListView lvPhone;
        TextView tvPhone;
        TextView tvEmail;
        RelativeLayout relPhone;
        RelativeLayout relEmail;
        RelativeLayout relPhoneSep;
        RelativeLayout relParent;
        RelativeLayout relEmailSep;
        HorizontialListView listViewAgency;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View view, ViewGroup parent) {
//        View view = LayoutInflater.from(context).inflate(R.layout.childlist, parent, false);

        ChildViewHolder childViewHolder = null;
        //if the type is past, use the past travel layout
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.team_list_child_vertical, parent, false);

            childViewHolder = new ChildViewHolder();
            childViewHolder.view2 = LayoutInflater.from(context).inflate(R.layout.list_item_child_vertical, null, false);
            childViewHolder.phoneView = LayoutInflater.from(context).inflate(R.layout.custom_phone, null, false);
            childViewHolder.textPhone = (TextView) childViewHolder.phoneView.findViewById(R.id.text);
            childViewHolder.relEmail = (RelativeLayout) view.findViewById(R.id.relEmail);
            childViewHolder.relPhone = (RelativeLayout) view.findViewById(R.id.rel);
            childViewHolder.relParent = (RelativeLayout) view.findViewById(R.id.linear);
            childViewHolder.relPhoneSep = (RelativeLayout) view.findViewById(R.id.relPhoneSep);
            childViewHolder.relEmailSep = (RelativeLayout) view.findViewById(R.id.relEmailSep);
            childViewHolder.listView = (ListView) view.findViewById(R.id.lv_office);
            childViewHolder.lvPhone = (ListView) view.findViewById(R.id.phoneList);
            childViewHolder.tvPhone = (TextView) view.findViewById(R.id.tvPhone);
            childViewHolder.tvEmail = (TextView) view.findViewById(R.id.tvEmail);
            childViewHolder.listViewAgency = (HorizontialListView) view.findViewById(R.id.listview);

            view.setTag(childViewHolder);

        } else {
            childViewHolder = (ChildViewHolder) view.getTag();
        }

//        DisplayMetrics displaymetrics = new DisplayMetrics();
//        context.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
//        int width = displaymetrics.widthPixels;
//        LinearLayout.LayoutParams relativeParams =
//                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//        if (width <=480){
//
//            relativeParams.setMargins((int) dipToPixels(context, 95), 0, 0, 0);
//
//        } else {
//            relativeParams.setMargins((int) dipToPixels(context, 120), 0, 0, 0);
//        }
//        childViewHolder.relParent.setLayoutParams(relativeParams);
//        childViewHolder.relParent.requestLayout();


        if (list.get(groupPosition).getEmail() != null && !list.get(groupPosition).getEmail().equalsIgnoreCase("")) {
            childViewHolder.tvEmail.setText(list.get(groupPosition).getEmail());
            childViewHolder.relEmail.setVisibility(View.VISIBLE);
            childViewHolder.relEmailSep.setVisibility(View.VISIBLE);
        } else {
            childViewHolder.relEmail.setVisibility(View.GONE);
            childViewHolder.relEmailSep.setVisibility(View.GONE);
        }

        List<TeamOffice> child = list.get(groupPosition).getOfficeList();
        String phones = "";
        if (list.get(groupPosition).getPhoneList() != null && list.get(groupPosition).getPhoneList().size() > 0) {
//            for (int i = 0; i < list.get(childPosition).getPhoneList().size(); i++) {
//                phones += list.get(childPosition).getPhoneList().get(i).getNumber() + "\n";
//            }



            List<TeamPhone> phoneList = list.get(groupPosition).getPhoneList();
//
//            for (int x = 0; x < list.get(groupPosition).getPhoneList().size(); x++) {
//                if (!list.get(groupPosition).getPhoneList().get(x).getPrimary()) {
//                    phoneList.add(list.get(groupPosition).getPhoneList().get(x));
//                }
//            }

            ViewGroup.LayoutParams prams2 = childViewHolder.lvPhone.getLayoutParams();
            int h = childViewHolder.textPhone.getMinimumHeight();
            prams2.height = phoneList.size() * h;
            childViewHolder.lvPhone.setLayoutParams(prams2);

            if (phoneList.size() > 0) {

                childViewHolder.relPhone.setVisibility(View.VISIBLE);
                childViewHolder.relPhoneSep.setVisibility(View.VISIBLE);

                PhoneArrAdapter adapter = new PhoneArrAdapter(context, R.layout.custom_phone, phoneList);
                childViewHolder.lvPhone.setAdapter(adapter);
            } else {
                childViewHolder.relPhone.setVisibility(View.INVISIBLE);
                childViewHolder.relPhoneSep.setVisibility(View.INVISIBLE);
            }

        } else {
            childViewHolder.relPhone.setVisibility(View.INVISIBLE);
            childViewHolder.relPhoneSep.setVisibility(View.INVISIBLE);
        }
//        tvPhone.setText(phones);

        if (child != null && child.size() > 0) {
            ViewGroup.LayoutParams prams2 = childViewHolder.listView.getLayoutParams();
            prams2.height = child.size() * childViewHolder.view2.getMinimumHeight();
            childViewHolder.listView.setLayoutParams(prams2);


            TeamCustomAdapterChild adapter = new TeamCustomAdapterChild(context, R.layout.list_item_child_vertical, child);
            childViewHolder.listView.setAdapter(adapter);
        }

        List<String> thumbs = list.get(groupPosition).getAgencyThumbnailList();
        if (thumbs != null) {
            childViewHolder.listViewAgency.setAdapter(new ArrAdapter(context, R.layout.custom, thumbs));
        }



        childViewHolder.relEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.get(groupPosition).getEmail() != null && !list.get(groupPosition).getEmail().equalsIgnoreCase("")) {
                    try {
                        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + list.get(groupPosition).getEmail())));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        return view;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    private boolean ifAlreadyExist(int id, String nodeId) {
        boolean flag = false;
        for (int i = 0; i < contactList.size(); i++) {
            if (nodeId.equalsIgnoreCase("")) {
                if (contactList.get(i).getId() == id) { //|| contactList.get(i).getNodeId().equalsIgnoreCase(nodeId)
                    flag = true;
                    break;
                } else {
                    flag = false;
                }
            }else {
                if (contactList.get(i).getNodeId().equalsIgnoreCase(nodeId)) { //|| contactList.get(i).getNodeId().equalsIgnoreCase(nodeId)
                    flag = true;
                    break;
                } else {
                    flag = false;
                }
            }
        }
        return flag;
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 2;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    private int getRelativeLeft(View myView) {
        if (myView.getParent() == myView.getRootView())
            return myView.getLeft();
        else
            return myView.getLeft() + getRelativeLeft((View) myView.getParent());
    }

}


class ArrAdapter extends ArrayAdapter<String> {

    Context context;
    List<String> list;

    public ArrAdapter(Context context, int resource, List<String> objects) {
        super(context, resource, objects);
        this.context = context;
        this.list = objects;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.custom, parent, false);
        }

        ImageView img = (ImageView) view.findViewById(R.id.imgThumb);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inSampleSize = calculateInSampleSize(options, 200, 200);
        options.inJustDecodeBounds = false;

        File imgFile = new File(list.get(position));
        if (imgFile.exists()) {
            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);
            img.setImageBitmap(myBitmap);
        }
//        Uri uri = Uri.fromFile(imgFile);
//        img.setImageURI(uri);
//        img.setImageBitmap(BitmapFactory.decodeByteArray(list.get(position), 0,
//                list.get(position).length));

        return view;
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
}

class PhoneArrAdapter extends ArrayAdapter<TeamPhone> {

    Context context;
    List<TeamPhone> list;

    public PhoneArrAdapter(Context context, int resource, List<TeamPhone> objects) {
        super(context, resource, objects);
        this.context = context;
        this.list = objects;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.custom_phone, parent, false);
        }

        TextView text = (TextView) view.findViewById(R.id.text);
        text.setText(list.get(position).getNumber()+" Ext. "+list.get(position).getExtension());

        text.setOnClickListener(new View.OnClickListener() {
            int pos = position;

            @Override
            public void onClick(View v) {
                String ph = "tel:" + list.get(position).getNumber() + ";" + list.get(position).getExtension();
                context.startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(ph)));
            }
        });


        return view;
    }

}
