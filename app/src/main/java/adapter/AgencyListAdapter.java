package adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import Model.Agency;
import util.AppManager;
import com.childrencareapp.R;

/**
 * Created by macbook on 15/10/15.
 */
public class AgencyListAdapter extends ArrayAdapter<Agency> {

    Context mContext;
    List<Agency> agency;
    ImageView img;
    DisplayImageOptions options;
    ImageLoadingListener animateFirstListener;


    public AgencyListAdapter(Context context, int resource, List<Agency> agency) {
        super(context, resource, agency);

        this.mContext = context;
        this.agency = agency;


        animateFirstListener = new AnimateFirstDisplayListener();


        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
//                .bitmapConfig(Bitmap.Config.RGB_565)
//                .displayer(new RoundedBitmapDisplayer(20))
                .build();

    }

    @Override
    public int getCount() {
        return agency.size();
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null)
        {
            view = inflater.inflate(R.layout.custom_list_home,null);
        }

        TextView sideBar = (TextView)view.findViewById(R.id.sideBar);
        img = (ImageView) view.findViewById(R.id.img);
        LinearLayout linear = (LinearLayout)view.findViewById(R.id.linear);
        RelativeLayout relAnim = (RelativeLayout) view.findViewById(R.id.relAnim);

        if (agency.get(position).getColorCode().size()>0) {
            sideBar.setBackgroundColor(Color.parseColor(agency.get(position).getColorCode().get(0)));





//            Canvas canvas = new Canvas();
////            Paint paint = new Paint();
////            paint.setColor(Color.parseColor(agency.get(position).getColorCode().get(0)));
////            paint.setStrokeWidth(5);
//            int[] location = new int[2];
//            linear.getLocationOnScreen(location);
//////            canvas.drawLine(location[0],location[1],location[0]+linear.getWidth(),location[1],paint);
////            canvas.drawLine(25,25,100,100,paint);
//
//            Paint paint = new Paint() {
//                {
//                    setStyle(Paint.Style.STROKE);
//                    setStrokeCap(Paint.Cap.ROUND);
//                    setColor(Color.BLACK);
//                    setStrokeWidth(3.0f);
//                    setAntiAlias(true);
//                }
//            };
//            final Path path = new Path();
//            path.moveTo(80, 80);
//
//            final float x2 = (100 + 0) / 2;
//            final float y2 = (50 + 0) / 2;
//            path.quadTo(x2, y2, 100, 50);
//            canvas.drawPath(path, paint);

//            new DrawView(mContext,null);


//            GradientDrawable gdDefault = new GradientDrawable();
//            gdDefault.setColor(mContext.getResources().getColor(R.color.bg_white));
//            gdDefault.setCornerRadii(new float[]{0, 0, 0, 0, 0, 0, 0, 0});
//            gdDefault.setStroke(10, Color.parseColor(agency.get(position).getColorCode().get(0)));
//
//            GradientDrawable gdDefault2 = new GradientDrawable();
//            gdDefault2.setColor(mContext.getResources().getColor(R.color.bg_white));
//
//            StateListDrawable states = new StateListDrawable();
//            states.addState(new int[] {android.R.attr.state_pressed}, gdDefault);
//            states.addState(new int[]{android.R.attr.state_focused}, gdDefault);
//            states.addState(new int[]{}, gdDefault2);
//
//            linear.setBackground(states);//FOR BUTTON
        }

        if (AppManager.getInstance().isFromServer()) {
            if (agency.get(position).getContactLogo().size() > 0) {
                Picasso.with(mContext).load(agency.get(position).getContactLogo().get(0).getUri()).into(img);
//                ImageLoader.getInstance().displayImage(agency.get(position).getContactLogo().get(0).getUri(), img, options,animateFirstListener);
            }
        }
        else{
            if (agency.get(position).getAgencyLogo() != null) {
                img.setImageBitmap(BitmapFactory.decodeByteArray(agency.get(position).getAgencyLogo(), 0,
                        agency.get(position).getAgencyLogo().length));
            }
        }
//        Picasso.with(mContext)
//                .load(Constants.IMG_URL + agency.get(position).getContactLogo().get(0).getFilename())
//                .networkPolicy(NetworkPolicy.OFFLINE)
//                .into(img, new com.squareup.picasso.Callback() {
//                    @Override
//                    public void onSuccess() {
//
//                    }
//
//                    @Override
//                    public void onError() {
//
//                        Picasso.with(mContext)
//                                .load(Constants.IMG_URL + agency.get(position).getContactLogo().get(0).getFilename())
//                                .error(null)
//                                .into(img, new com.squareup.picasso.Callback() {
//                                    @Override
//                                    public void onSuccess() {
//
//                                    }
//
//                                    @Override
//                                    public void onError() {
//
//                                    }
//                                });
//
//                    }
//                });


//        ImageLoader imageLoader = ImageLoader.getInstance();
//        imageLoader.displayImage(Constants.IMG_URL + agency.get(position).getContactLogo().get(0).getFilename(), img);

        return view;
    }


    private static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

        static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 100);
                    displayedImages.add(imageUri);
                }
            }
        }
    }
}
