package adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.net.URLEncoder;
import java.util.List;

import Model.ChildItemOffice;
import Model.TeamOffice;
import com.childrencareapp.R;

class TeamCustomAdapterChild extends ArrayAdapter<TeamOffice> {

    List<TeamOffice> list;
    Context context;

    public TeamCustomAdapterChild(Context context, int textViewResourceId, List<TeamOffice> objects) {
        super(context, textViewResourceId, objects);

        this.context = context;
        this.list = objects;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;

        //if the type is past, use the past travel layout
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.list_item_child_vertical, parent, false);
        }

        TextView tv = (TextView) view.findViewById(R.id.list_item_vertical_child_textView);
        tv.setText(list.get(position).getOffice());

        tv.setOnClickListener(new View.OnClickListener() {
            int pos = position;
            @Override
            public void onClick(View v) {
                try {
                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(String.format("geo:0,0?q=%s", URLEncoder.encode(list.get(pos).getOffice()))))); //Uri.parse("mailto:" + list.get(groupPosition).getOfficeList().get(0))));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        return view;
    }


}
