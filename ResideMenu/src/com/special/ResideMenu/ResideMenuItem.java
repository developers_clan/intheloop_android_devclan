package com.special.ResideMenu;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * User: special
 * Date: 13-12-10
 * Time: 下午11:05
 * Mail: specialcyci@gmail.com
 */
public class ResideMenuItem extends LinearLayout{

    /** menu item  icon  */
    private ImageView iv_icon,fb,twitter,pintrst,youtube;
    private LinearLayout linearFb,linearTwtr,linearPin,linearYt;
    /** menu item  title */
    private TextView tv_title;
    private String fbLink = "", twtrLink = "",pinLink = "", ytLink = "";

    public ResideMenuItem(Context context) {
        super(context);
        initViews(context);
        initSocialViews(context);
    }

//    public ResideMenuItem(Context context, int icon, int title) {
//        super(context);
//        initViews(context);
//        iv_icon.setImageResource(icon);
//        tv_title.setText(title);
//    }

    public ResideMenuItem(Context context, int icon, String title) {
        super(context);
        initViews(context);
        iv_icon.setImageResource(icon);
        tv_title.setText(title);
    }

    public ResideMenuItem(Context context, int fb_icon, int tw_icon, int pin_icon, int yt_icon,String twtrLink, String fbLink,String pinLink, String ytLink) { //,String twtrLink, String fbLink
        super(context);
        initSocialViews(context);
        fb.setImageResource(fb_icon);
        twitter.setImageResource(tw_icon);
        pintrst.setImageResource(pin_icon);
        youtube.setImageResource(yt_icon);
        this.fbLink = fbLink;
        this.twtrLink = twtrLink;
        this.ytLink = ytLink;
        this.pinLink = pinLink;

        showHideView();

    }

    private void showHideView(){
        if (fbLink.equalsIgnoreCase("")) {
            linearFb.setVisibility(View.GONE);
        }
        else {
            linearFb.setVisibility(View.VISIBLE);
        }
        if (twtrLink.equalsIgnoreCase("")) {
            linearTwtr.setVisibility(View.GONE);
        }
        else {
            linearTwtr.setVisibility(View.VISIBLE);
        }
        if (ytLink.equalsIgnoreCase("")) {
            linearYt.setVisibility(View.GONE);
        }
        else {
            linearYt.setVisibility(View.VISIBLE);
        }
        if (pinLink.equalsIgnoreCase("")) {
            linearPin.setVisibility(View.GONE);
        }
        else {
            linearPin.setVisibility(View.VISIBLE);
        }
    }

    public ResideMenuItem(Context context, int fb_icon, int tw_icon) { //,String twtrLink, String fbLink
        super(context);
        initSocialViews(context);
        fb.setImageResource(fb_icon);
        twitter.setImageResource(tw_icon);

        showHideView();

    }

    private void initViews(Context context){
        LayoutInflater inflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.residemenu_item, this);
        iv_icon = (ImageView) findViewById(R.id.iv_icon);
        tv_title = (TextView) findViewById(R.id.tv_title);
    }
    private void initSocialViews(final Context context){
        LayoutInflater inflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.residemenu_social_item, this);
        fb = (ImageView) findViewById(R.id.fb);
        twitter = (ImageView) findViewById(R.id.twitter);
        pintrst = (ImageView) findViewById(R.id.pintrt);
        youtube = (ImageView) findViewById(R.id.yt);
        linearFb = (LinearLayout) findViewById(R.id.linearFb);
        linearTwtr = (LinearLayout) findViewById(R.id.linearTwtr);
        linearPin = (LinearLayout) findViewById(R.id.linearPintrst);
        linearYt = (LinearLayout) findViewById(R.id.linearYt);

        linearFb.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!fbLink.equalsIgnoreCase("")) {

                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(fbLink)));
                }
            }
        });

        linearTwtr.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!twtrLink.equalsIgnoreCase("")){
                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(twtrLink)));
                }
            }
        });

        linearYt.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!fbLink.equalsIgnoreCase("")) {
                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(ytLink)));
                }
            }
        });

        linearPin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!twtrLink.equalsIgnoreCase("")) {
                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(pinLink)));
                }
            }
        });

    }

    /**
     * set the icon color;
     *
     * @param icon
     */
    public void setIcon(int icon){
        iv_icon.setImageResource(icon);
    }

    /**
     * set the title with resource
     * ;
     * @param title
     */
    public void setTitle(int title){
        tv_title.setText(title);
    }

    /**
     * set the title with string;
     *
     * @param title
     */
    public void setTitle(String title){
        tv_title.setText(title);
    }
}
